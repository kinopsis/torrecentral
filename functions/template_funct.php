<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /functions/template_funct.php
	# ----------------------------------------------------------------------------------------------------

	function template_CreateDynamicField($fieldvalues, $themeTemplate = false, &$hideExtraFieldsTable) {
		$fieldType = preg_replace('/[0-9]/i', '', $fieldvalues["field"]);
        if ($themeTemplate || (string_strpos($fieldvalues["label"], "LANG_LABEL") !== false)){
            $fieldvalues["label"] = @constant($fieldvalues["label"]);
        }
        
        if (string_strpos($_SERVER["PHP_SELF"], "/".SITEMGR_ALIAS) !== false) {
            switch ($fieldType) {
                case "custom_text":
                    $hideExtraFieldsTable = false; ?>
                    <div class="form-group">
                        <label for="<?=$fieldvalues["field"]?>"><?=$fieldvalues["label"]?> <?=($fieldvalues["required"] == "y") ? "" : "<small class=\"small text-muted\">(optional)</small>"?></label>
                        <input class="form-control" type="text" name="<?=$fieldvalues["field"]?>" id="<?=$fieldvalues["field"]?>" value="<?=$fieldvalues["form_value"]?>" maxlength="250" <?=($fieldvalues["instructions"]) ? "placeholder=\"".$fieldvalues["instructions"]."\"" : ""?> />
                    </div>
                    <?
                break;
                case "custom_short_desc":
                    $hideExtraFieldsTable = false;
                    $_SESSION["custom_type_field"] = $fieldvalues["field"]; //used for auxiliary script on footer
                    ?>
                    <div class="form-group">
                        <label for="<?=$fieldvalues["field"]?>"><?=$fieldvalues["label"]?> <?=($fieldvalues["required"] == "y") ? "" : "<small class=\"small text-muted\">(optional)</small>"?></label>
                        <textarea id="<?=$fieldvalues["field"]?>" name="<?=$fieldvalues["field"]?>" class="form-control textarea-counter" rows="5" cols="1" data-chars="250" data-msg="<?=system_showText(LANG_MSG_CHARS_LEFT)?>"><?=$fieldvalues["form_value"]?></textarea>
                    </div>
                    <?
                break;
                case "custom_long_desc":
                    $hideExtraFieldsTable = false; ?>
                    <div class="form-group">
                        <label for="<?=$fieldvalues["field"]?>"><?=$fieldvalues["label"]?> <?=($fieldvalues["required"] == "y") ? "" : "<small class=\"small text-muted\">(optional)</small>"?></label>
                        <textarea id="<?=$fieldvalues["field"]?>" name="<?=$fieldvalues["field"]?>" class="form-control" rows="5" <?=($fieldvalues["instructions"]) ? "placeholder=\"".$fieldvalues["instructions"]."\"" : ""?>><?=$fieldvalues["form_value"]?></textarea>
                    </div>
                    <?
                break;
                case "custom_checkbox":
                    $hideExtraFieldsTable = false; ?>
                    <div class="form-group">
                        <div class="checkbox">
                            <label for="<?=$fieldvalues["field"]?>">
                                <input type="checkbox" name="<?=$fieldvalues["field"]?>" id="<?=$fieldvalues["field"]?>" value="y" <?=($fieldvalues["form_value"] == "y") ? "checked" : ""?> />
                                <?=$fieldvalues["label"]?> <small class="small text-muted"><?=($fieldvalues["instructions"]) ? "(".$fieldvalues["instructions"].")" : ""?></small>
                            </label>
                        </div>
                    </div>
                    <?
                break;
                case "custom_dropdown":
                    $hideExtraFieldsTable = false; ?>
                    <div class="form-group selectize">
                        <label><?=$fieldvalues["label"]?> <small class="small text-muted"><?=($fieldvalues["instructions"]) ? "(".$fieldvalues["instructions"].")" : ""?></small> <?=($fieldvalues["required"] == "y") ? "" : "<small class=\"small text-muted\">(optional)</small>"?></label>
                        <select name="<?=$fieldvalues["field"]?>">
                            <option value=""><?=$fieldvalues["label"];?></option>
                            <?
                            $auxfieldvalues = explode(",", $fieldvalues["fieldvalues"]);
                            foreach ($auxfieldvalues as $fieldvalue) {
                                ?><option value="<?=$fieldvalue;?>" <? if ($fieldvalue == $fieldvalues["form_value"]) { echo "selected"; } ?>><?=$fieldvalue;?></option><?
                            }
                            ?>
                        </select>
                    </div>
                    <?
                break;
            }
        } else {
            switch ($fieldType) {
                case "custom_text":
                    $hideExtraFieldsTable = false; ?>
                    <tr>
                        <th class="wrap"><?=($fieldvalues["required"]=="y") ? "* " : ""?><?=$fieldvalues["label"]?>:</th>
                        <td><input type="text" name="<?=$fieldvalues["field"]?>" value="<?=$fieldvalues["form_value"]?>" maxlength="250" /><?=($fieldvalues["instructions"]) ? "<span>".$fieldvalues["instructions"]."</span>" : ""?></td>
                    </tr>
                    <?
                break;
                case "custom_short_desc":
                    $hideExtraFieldsTable = false; ?>
                    <script type="text/javascript">

                        $(document).ready(function(){

                            var field_name = '<?=$fieldvalues["field"]?>';
                            var count_field_name = '<?=$fieldvalues["field"]?>_remLen';

                            var options = {
                                        'maxCharacterSize': 250,
                                        'originalStyle': 'originalTextareaInfo',
                                        'warningStyle' : 'warningTextareaInfo',
                                        'warningNumber': 40,
                                        'displayFormat' : '<span><input readonly="readonly" type="text" id="'+count_field_name+'" name="'+count_field_name+'" size="3" maxlength="3" value="#left" class="textcounter" disabled="disabled" /> <?=system_showText(LANG_MSG_CHARS_LEFT)?> <?=system_showText(LANG_MSG_INCLUDING_SPACES_LINE_BREAKS)?></span>' 
                                };
                            $('#'+field_name).textareaCount(options);

                        });
                    </script>
                    <tr>
                        <th class="wrap"><?=($fieldvalues["required"]=="y") ? "* " : ""?><?=$fieldvalues["label"]?>:</th>
                        <td>
                            <textarea id="<?=$fieldvalues["field"]?>" name="<?=$fieldvalues["field"]?>" rows="5" cols="1" ><?=$fieldvalues["form_value"]?></textarea>
                        </td>
                    </tr>
                    <?
                break;
                case "custom_long_desc":
                    $hideExtraFieldsTable = false; ?>
                    <tr>
                        <th class="wrap"><?=($fieldvalues["required"]=="y") ? "* " : ""?><?=$fieldvalues["label"]?>:</th>
                        <td><textarea name="<?=$fieldvalues["field"]?>" rows="5"><?=$fieldvalues["form_value"]?></textarea><?=($fieldvalues["instructions"]) ? "<span>".$fieldvalues["instructions"]."</span>" : ""?></td>
                    </tr>
                    <?
                break;
                case "custom_checkbox":
                    $hideExtraFieldsTable = false; ?>
                    <tr>
                        <th class="wrap"><input type="checkbox" name="<?=$fieldvalues["field"]?>" value="y" <?=($fieldvalues["form_value"] == "y") ? "checked" : ""?> class="inputCheck" /></th>
                        <td><?=($fieldvalues["required"]=="y") ? "* " : ""?><?=$fieldvalues["label"]?><?=($fieldvalues["instructions"]) ? "<span>".$fieldvalues["instructions"]."</span>" : ""?></td>
                    </tr>
                    <?
                break;
                case "custom_dropdown":
                    $hideExtraFieldsTable = false; ?>
                    <tr>
                        <th class="wrap"><?=($fieldvalues["required"]=="y") ? "* " : ""?><?=$fieldvalues["label"]?>:</th>
                        <td>
                            <select name="<?=$fieldvalues["field"]?>">
                                <option value=""><?=$fieldvalues["label"];?></option>
                                <?
                                $auxfieldvalues = explode(",", $fieldvalues["fieldvalues"]);
                                foreach ($auxfieldvalues as $fieldvalue) {
                                    ?><option value="<?=$fieldvalue;?>" <? if ($fieldvalue == $fieldvalues["form_value"]) { echo "selected"; } ?>><?=$fieldvalue;?></option><?
                                }
                                ?>
                            </select>
                            <?=($fieldvalues["instructions"]) ? "<span>".$fieldvalues["instructions"]."</span>" : "";?>
                        </td>
                    </tr>
                    <?
                break;
            }
        }
	}
?>