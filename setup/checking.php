<?php

if(!defined('MrQaidi')) {die('Direct access not permitted');}

$error          = 0;
$system_checked = false;
if (!isset($_GET["steps"]) && !isset($_GET["error"])) {

    if (!is_writable(ABSPATH . '/custom/domain_1/tmp')) {
        echo '<div class="msg-warning">tmp folder (' . ABSPATH . '/custom/domain_1/tmp) is not writeable</div>';
        $error++;
    }
    if (!is_writable(ABSPATH . '/custom/domain_1/cache_filter')) {
        echo '<div class="msg-warning">(' . ABSPATH . '/custom/domain_1/cache_filter) is not writeable</div>';
        $error++;
    }
    if (!is_writable(ABSPATH . '/custom/domain_1/cache_full')) {
        echo '<div class="msg-warning">(' . ABSPATH . '/custom/domain_1/cache_full) is not writeable</div>';
        $error++;
    }
    if (!is_writable(ABSPATH . '/custom/domain_1/cache_partial')) {
        echo '<div class="msg-warning">(' . ABSPATH . '/custom/domain_1/cache_partial) is not writeable</div>';
        $error++;
    }
    if (!is_writable(ABSPATH . '/custom/domain_1/cacheExpirationQueries')) {
        echo '<div class="msg-warning">(' . ABSPATH . '/custom/domain_1/cacheExpirationQueries) is not writeable</div>';
        $error++;
    }
    if (!is_writable(ABSPATH . '/custom/domain_1/cacheUpdateToken')) {
        echo '<div class="msg-warning">(' . ABSPATH . '/custom/domain_1/cacheUpdateToken) is not writeable</div>';
        $error++;
    }
    if (!is_writable(ABSPATH . '/custom/domain_1/cacheVerbose')) {
        echo '<div class="msg-warning">(' . ABSPATH . '/custom/domain_1/cacheVerbose) is not writeable</div>';
        $error++;
    }
    if (!is_writable(ABSPATH . '/custom/domain_1/content_files')) {
        echo '<div class="msg-warning">(' . ABSPATH . '/custom/domain_1/content_files) is not writeable</div>';
        $error++;
    }
    if (!is_writable(ABSPATH . '/custom/domain_1/editor_files')) {
        echo '<div class="msg-warning">(' . ABSPATH . '/custom/domain_1/editor_files) is not writeable</div>';
        $error++;
    }
    if (!is_writable(ABSPATH . '/custom/domain_1/export_files')) {
        echo '<div class="msg-warning">(' . ABSPATH . '/custom/domain_1/export_files) is not writeable</div>';
        $error++;
    }
    if (!is_writable(ABSPATH . '/custom/domain_1/image_files')) {
        echo '<div class="msg-warning">(' . ABSPATH . '/custom/domain_1/image_files) is not writeable</div>';
        $error++;
    }
    if (!is_writable(ABSPATH . '/custom/domain_1/extra_files')) {
        echo '<div class="msg-warning">(' . ABSPATH . '/custom/domain_1/extra_files) is not writeable</div>';
        $error++;
    }
    if (!is_writable(ABSPATH . '/custom/domain_1/import_files')) {
        echo '<div class="msg-warning">(' . ABSPATH . '/custom/domain_1/import_files) is not writeable</div>';
        $error++;
    }
    
    
    
    if (!function_exists('system')) {
        echo '<div class="msg-warning">PHP  system() Function disabled  without this function this script will not work</div>';
        $error++;
    }
    if (!function_exists('exec')) {
        echo '<div class="msg-warning">PHP  exec() Function disabled  without this function this script will not work</div>';
        $error++;
    }
    if (!extension_loaded('mcrypt')) {
        echo '<div class="msg-warning">Seems your host misses the mcrypt extension </div>';
        $error++;
    }
    if (!extension_loaded('PHPshadow')) {
        echo '<div class="msg-warning">Qaidi Extension failed to load
		<a id="phpini" data-toggle="modal" href="#" class="phpini-link btn btn-large btn-win pull-right">Install Qaidi Loader</a></div>';
        $error++;
    }
    if (check_php_ini() !== false) {
        echo '<div class="msg-warning">PHP.ini override not allowed</div>';
        $error++;
    }
    if (!extension_loaded('mysql')) {
        echo '<div class="msg-warning">Seems your host misses the MySQLi extension </div>';
        $error++;
    }
    if (ini_get('safe_mode')) {
        echo '<div class="msg-hint">Safemode enabled on this server please  turn it off </div>';
    }
    if (ini_get('open_basedir') !== '') {
        echo '<div class="msg-hint">open_basedir disabled on this server</div>';
    }
    if (!extension_loaded('cURL')) {
        echo '<div class="msg-warning">cURL not enabled </div>';
        $error++;
    }
    if (!extension_loaded('JSON')) {
        echo '<div class="msg-warning">JSON not enabled </div>';
        $error++;
    }
    if (!extension_loaded('gd') && function_exists('gd_info')) {
        echo '<div class="msg-warning">GD 2+ Image libraries need it for this script </div>';
        $error++;
    }
    if (!extension_loaded('mcrypt')) {
        echo '<div class="msg-warning">mcrypt not enabled </div>';
        $error++;
    }
    if (function_exists('apache_get_modules')) {
        if (apache_module_exists("mod_security") == false) {
            echo '<div class="msg-hint">mod_security not enabled </div>';
        }
    }
    if (phpversion() < 5.3) {
        echo '<div class="msg-warning">Requested  PHP version is <b>5.3</b> (and above).   your version is <b>' . phpversion() . '</b>  please contact your host provider to upgrade php version.</div>';
        $error++;
    }
    
} elseif (isset($_GET["error"])) {
    echo base64_decode($_GET["error_msg"]);
    die();
}
?>