<?php
ob_start();
error_reporting(1);
ini_set('display_errors', 1);
?>
	<!-- twitter content -->
	<div id="phpini-div" class="modal hide fade in" style="display: none; ">
	        <div class="modal-header">
	              <a class="close" data-dismiss="modal">×</a>
	              <h3>Qaidi Loader and PHP.ini Generator  </h3>
	        </div>
		<div>
		         <div class="modal-body"></div>

		</div>
	     <div class="modal-footer">
	         <button class="btn btn-success" onClick="window.location.reload()">refresh</button>
	         <a href="#" class="btn" data-dismiss="modal">Close</a>
  		</div>
	</div>
<?php

define('MrQaidi', TRUE);

//Install Config include
require_once("./config.php");
// put header
print setup_header();
// checking server
require_once(INSTALL_PATH . '/checking.php');

if ($error > 0) {
    echo '<hr></br><div class="msg-warning"><b>Please correct the ' . $error . ' errors listed above to continue.</b></div>';
    die();
} else {
    if (!$_POST && !$_GET && !isset($_GET["error"])) {
?><h3> Main Database Connection </h3>

	<form id="validate" class="form-horizontal styled" action="<?php
        echo $base_href;
?>" enctype="multipart/form-data" method="post">
	<input type="hidden" name="system_checked"  value="true" /> 						
	<fieldset>
	<div class="control-group">
	<label class="control-label">Database host</label>
	<div class="controls">
	<input type="text" name="db_host" class="validate[required] span6" value="" /> 						
	<span class="help-block" id="limit-text">please enter your database host (localhost)</span>
	</div>	
	</div>
	<div class="control-group">
	<label class="control-label">Database username</label>
	<div class="controls">
	<input type="text" name="db_username" class="validate[required] span6" value="" /> 						
	<span class="help-block" id="limit-text">please enter your database username</span>
	</div>	
	</div>	
	<div class="control-group">
	<label class="control-label">Database password</label>
	<div class="controls">
	<input type="text" name="db_password" class=" span6" value="" /> 						
	<span class="help-block" id="limit-text">please enter your database password</span>
	</div>	
	</div>
	<div class="control-group">
	<label class="control-label">Database name</label>
	<div class="controls">
	<input type="text" name="db_name" class="validate[required] span6" value="" /> 						
	<span class="help-block" id="limit-text">please enter your database name. example:<b>edir_main</b></span>
	</div>	
	</div>	
	<div class="control-group">
	<label class="control-label">Database name prefix</label>
	<div class="controls">
	<input type="text" name="db_name_prefix" class="validate[required] span6" value="" /> 						
	<span class="help-block" id="limit-text">please enter your database name prefix need it for add sites. example:<b>edir</b></span>
	</div>	
	</div>			
	<div class="control-group">
	<label class="control-label">License Key</label>
	<div class="controls">
	<input type="text" name="lic_key" class="validate[required] span6" value="" /> 						
	<span class="help-block" id="limit-text">Please enter your License Key</span>
	</div>	
	</div>
	<div class="control-group">
	<button class="btn btn-large btn-primary pull-right" type="submit" name="step1" onclick="">Next ></button>	
	</div>	
	</fieldset>					
	</form>
	<?php
    }
    
    
    
}
// Start the session
session_start();

// steps
if (isset($_POST["step1"])) {
    $post_details              = $_POST;
    $post_details['subfolder'] = $EDIRECTORY_FOLDER;
    // check  MySQL
    $link                      = mysql_connect($post_details['db_host'], $post_details['db_username'], $post_details['db_password']);
    if (!$link) {
        my_error('<div class="msg-warning">MySQL Connection error</div><button class="btn btn-large btn-primary pull-right" type="submit" onclick="history.go(-1);">< Back</button>');
    } elseif (!mysql_select_db($post_details['db_name'], $link)) {
        my_error('<div class="msg-warning">MySQL Database Connection error</div><button class="btn btn-large btn-primary pull-right" type="submit" onclick="history.go(-1);">< Back</button>');
    } else {
		if (strpos($post_details['lic_key'], "mob") !== false) {
			$mobile_on=file_get_contents("../custom/domain_1/conf/constants.inc.php");
			$mobile_on=str_replace('"MOBILE_FEATURE", "off"','"MOBILE_FEATURE", "on"',$mobile_on);
			file_put_contents("../custom/domain_1/conf/constants.inc.php", $mobile_on);
 
			}
	    
        $create_config = create_config($post_details);
        $page_content  = '<p>' . $create_config['display'] . '</p> ';
        if ($create_config['result'] == true) {
            mysql_close($link);
            $_SESSION["step1"] = true;
?>
	<h3> Creating database configuration  file </h3>
	<form class="form-horizontal styled" action="<?php
            echo $base_href;
?>" method="GET">
	<input type="hidden" name="step2" value="true">

	<fieldset>
	<?php
            echo $page_content;
?>
	<div class="control-group">
	<button class="btn btn-large btn-primary pull-right" type="submit" >Next ></button>
	<button class="btn btn-large btn-primary pull-left" type="button" onclick="history.go(-1);">< Back</button>	
	</div>	
	</fieldset>					
	</form>
	
	<?php
            
        }
        
        
    }
    
    
    
    
    
    
}
if (isset($_GET["step2"]) && $_SESSION["step1"] == true) {
    
    $path    = "db/";
    // SQL File
    $SQLFile = $path . "main.sql"; // Filename of dump, default: dump.sql
    // Connect MySQL
    $link    = mysql_connect(_DIRECTORYDB_HOST, _DIRECTORYDB_USER, _DIRECTORYDB_PASS);
    if (!$link) {
        my_error('<div class="msg-warning">MySQL Connection error</div><button class="btn btn-large btn-primary pull-right" type="submit" onclick="history.go(-1);">< Back</button></div>');
    }
    if (!mysql_select_db(_DIRECTORYDB_NAME, $link)) {
        my_error('<div class="msg-warning">MySQL Database Connection error</div><button class="btn btn-large btn-primary pull-right" type="submit" onclick="history.go(-1);">< Back</button></div>');
    }
    if (mysql_query("DESCRIBE `Domain`")) {
        my_error('<div class="msg-warning">The tables already exist!</div><button class="btn btn-large btn-primary pull-right" type="submit" onclick="history.go(-2);">< Back</button>');
    }
    
    /* * * import main.sql ** */
    MultiQuery($SQLFile);
    if (MultiQuery($SQLFile) == "1") {
        mysql_close($link);
        $_SESSION["step2"] = true;
        echo '<form id="validate" class="form-horizontal styled" action="' . $base_href . '"  method="GET"><input type="hidden" name="step3" value="true"><div class="msg-win">Database successfully installed!</div><button class="btn btn-large btn-primary pull-right" type="submit" autofocus="autofocus">Next ></button></form>';
        
        print '<script> $(function() {$("body").scrollTo("#validate");});</script>';
    } else {
        my_error('<div class="msg-warning">unsespected error</div>');
    }
    
    
} elseif (isset($_GET["step3"]) && $_SESSION["step2"] == true) {
?>
	<h3> Domain Database Connection </h3>

	<form id="validate" class="form-horizontal styled" action="<?php
    echo $base_href;
?>"  method="post">
	<fieldset>
	<div class="control-group">
	<label class="control-label">domain name</label>
	<div class="controls">
	<input type="text" name="domain_name" class="validate[required] span6" value="" /> 						
	<span class="help-block" id="limit-text">please enter domain name (without www.)</span>
	</div>	
	</div>
	<div class="control-group">
	<label class="control-label">Database host</label>
	<div class="controls">
	<input type="text" name="db_host" class="validate[required] span6" value="<?php
    echo _DIRECTORYDB_HOST;
?>" /> 						
	<span class="help-block" id="limit-text">please enter your database host (localhost)</span>
	</div>	
	</div>
	<div class="control-group">
	<label class="control-label">Database username</label>
	<div class="controls">
	<input type="text" name="db_username" class="validate[required] span6" value="<?php
    echo _DIRECTORYDB_USER;
?>" /> 						
	<span class="help-block" id="limit-text">please enter your database username</span>
	</div>	
	</div>	
	<div class="control-group">
	<label class="control-label">Database password</label>
	<div class="controls">
	<input type="text" name="db_password" class=" span6" value="<?php
    echo _DIRECTORYDB_PASS;
?>" /> 						
	<span class="help-block" id="limit-text">please enter your database password</span>
	</div>	
	</div>
	<div class="control-group">
	<label class="control-label">Database name</label>
	<div class="controls">
	<input type="text" name="db_name" class="validate[required] span6" value="" /> 						
	<span class="help-block" id="limit-text">please enter your  domain database name</span>
	</div>	
	</div>			

	<div class="control-group">
	<button class="btn btn-large btn-primary pull-right" type="submit" name="step4">Next ></button>	
	</div>	
	</fieldset>					
	</form>
	<?php
    
    
} elseif (isset($_POST["step4"])) {
    $db_details = $_POST;
    $path       = "db/";
    // SQL File
    $SQLFile    = $path . "domain.sql";
    // Connect MySQL
    $link       = mysql_connect($db_details['db_host'], $db_details['db_username'], $db_details['db_password']);
    if (!$link) {
        my_error('<div class="msg-warning">MySQL Connection error</div><button class="btn btn-large btn-primary pull-right" type="submit" onclick="window.history.go(-1);">< Back</button>');
    }
    if (!mysql_select_db($db_details['db_name'], $link)) {
        my_error('<div class="msg-warning">MySQL Domain Database Connection error</div><button class="btn btn-large btn-primary pull-right" type="submit" onclick="history.go(-1);">< Back</button>');
    }
    if (mysql_query("DESCRIBE `AccountProfileContact`")) {
        my_error('<div class="msg-warning">Domain database tables already exist!</div><button class="btn btn-large btn-primary pull-right" type="submit" onclick="history.go(-1);">< Back</button>');
    }
    
    /* * * import domain.sql ** */
    MultiQuery($SQLFile);
    if (MultiQuery($SQLFile) == "1") {
        
        $date_time = db_formatString(date("Y-m-d H:i:s"));
        mysql_close($link);
        mysql_connect(_DIRECTORYDB_HOST, _DIRECTORYDB_USER, _DIRECTORYDB_PASS);
        mysql_select_db(_DIRECTORYDB_NAME);
        $sql = "INSERT INTO Domain (smaccount_id,name,database_host,database_port,database_username,database_password,database_name,url,status,activation_status,created,deleted_date,article_feature,banner_feature,classified_feature,event_feature,subfolder)" . " VALUES ('1','" . $db_details['domain_name'] . "','" . $db_details['db_host'] . "','3306','" . $db_details['db_username'] . "','" . $db_details['db_password'] . "','" . $db_details['db_name'] . "','" . $site_link . "','A','A'," . $date_time . ",'0000-00-00','on','on','on','on','/')";
        mysql_query($sql);
        mysql_close();
        
        $_SESSION["step4"] = true;
        
        $create_domain_file = create_domain_file($site_link);
        $create_domain_file = '<p>' . $create_domain_file['display'] . '</p> ';
        echo $create_domain_file;
        echo '<form id="validate" class="form-horizontal styled" action="' . $base_href . '"  method="GET"><input type="hidden" name="step5" value="true"><div class="msg-win">Database successfully installed!</div><button class="btn btn-large btn-primary pull-right" type="submit" onclick="" autofocus="autofocus">Next ></button></form>';
        print '<script> $(function() {$("body").scrollTo("#validate");});</script>';
        
    } else {
        my_error('<div class="msg-warning">unsespected error</div>');
    }
    
} elseif (isset($_GET["step5"]) && $_SESSION["step4"] == true) {
    
?>
	<h3>Admin account</h3>

	<form id="validate" class="form-horizontal styled" action="<?php
    echo $base_href;
?>"  method="post">
	<fieldset>
	<div class="control-group">
	<label class="control-label">Admin Email</label>
	<div class="controls">
	<input type="text" name="admin_email" class="validate[required] span6" value="" /> 						
	<span class="help-block" id="limit-text">Please enter your Admin Email</span>
	</div>	
	</div>


	<div class="control-group">
	<label class="control-label">Admin password</label>
	<div class="controls">
	<input type="text" name="admin_pass" class="validate[required] span6" value="" /> 						
	<span class="help-block" id="limit-text">Please enter your Admin password</span>
	</div>	
	</div>
		

		

	<div class="control-group">
	<button class="btn btn-large btn-primary pull-right" type="submit" name="step6">Next ></button>
	<button class="btn btn-large btn-primary pull-left" type="button" onclick="history.go(-1);">< Back</button>	
	</div>	
	</fieldset>					
	</form>
	<?php
    die();
    
}
if (isset($_POST["step6"])) {
    $posts     = $_POST;
    $date_time = db_formatString(date("Y-m-d H:i:s"));
    mysql_connect(_DIRECTORYDB_HOST, _DIRECTORYDB_USER, _DIRECTORYDB_PASS);
    mysql_select_db(_DIRECTORYDB_NAME);
    $sql = "INSERT INTO Setting (name,value)" . " VALUES ('sitemgr_password','" . md5($posts['admin_pass']) . "')";
    mysql_query($sql);
    
    $sql2 = "INSERT INTO Setting (name,value)" . " VALUES ('sitemgr_username','" . $posts['admin_email'] . "')";
    mysql_query($sql2);
    mysql_close();
    
    $new_url = str_replace("/setup/", "", $site_url);
    
    
    echo '<div class="msg-win">All done. Remember to remove Setup folder in root.</div>';
    
    echo '<div class="msg-info"><b>Login information to your Site Manager Account </b> </br> 
	E-mail Address; ' . $posts['admin_email'] . '</br>
	Password; ' . $posts['admin_pass'] . '</br>

	 </div>';
    
    $host_path = realpath('..');
    echo '<div class="msg-warning">Do Not Forget Cron job</div>';
    
    echo "

<blockquote class='spi'>
*/5	*	*	*	*	php -c " . ABSPATH . "/php.ini " . ABSPATH . "/cron/cron_manager.php 1>&2 >> " . ABSPATH . "/cron/cron.log
</blockquote>

<blockquote class='shin'>
*/5	*	*	*	*	php -c " . ABSPATH . "/php.ini " . ABSPATH . "/cron/import.php 1>&2 >> " . ABSPATH . "/cron/cron.log<br>
</blockquote>

<blockquote class='spi'>
*/5	*	*	*	*	php -c " . ABSPATH . "/php.ini " . ABSPATH . "/cron/import_events.php 1>&2 >> " . ABSPATH . "/cron/cron.log<br>
</blockquote>

<blockquote class='shin'>
*/10	*	*	*	*	php -c " . ABSPATH . "/php.ini " . ABSPATH . "/cron/prepare_import.php 1>&2 >> " . ABSPATH . "/cron/cron.log<br>
</blockquote>

<blockquote class='spi'>
*/10	*	*	*	*	php -c " . ABSPATH . "/php.ini " . ABSPATH . "/cron/prepare_import_events.php 1>&2 >> " . ABSPATH . "/cron/cron.log<br>
</blockquote>

";
    
    echo '<div class="msg-info"><b>*/5	*	*	*	*   </b>means every 5 minutes</div>';
    echo '<a class="btn btn-large btn-primary pull-right" href="' . $new_url . '/sitemgr/">Setup is complete. Login</a>';
    
}


echo '</div></div></div></body></html>';

ob_end_flush();
?>