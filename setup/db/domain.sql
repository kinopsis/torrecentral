-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 25, 2015 at 10:02 PM
-- Server version: 5.5.40-cll
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `qaidi_edir_domain`
--

-- --------------------------------------------------------

--
-- Table structure for table `AccountProfileContact`
--

CREATE TABLE IF NOT EXISTS `AccountProfileContact` (
  `account_id` int(11) NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nickname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_id` int(11) NOT NULL,
  `facebook_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_image_height` int(11) NOT NULL,
  `facebook_image_width` int(11) NOT NULL,
  `has_profile` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`account_id`),
  KEY `image_id` (`image_id`,`has_profile`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `AccountProfileContact`
--

-- --------------------------------------------------------

--
-- Table structure for table `AppAdvert`
--

CREATE TABLE IF NOT EXISTS `AppAdvert` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `image_id` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `device` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `expiration_date` date NOT NULL,
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `entered` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `AppCustomPages`
--

CREATE TABLE IF NOT EXISTS `AppCustomPages` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `icon` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `json` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `AppNotification`
--

CREATE TABLE IF NOT EXISTS `AppNotification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `expiration_date` date NOT NULL,
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `entered` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Article`
--

CREATE TABLE IF NOT EXISTS `Article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL DEFAULT '0',
  `image_id` int(11) NOT NULL DEFAULT '0',
  `thumb_id` int(11) NOT NULL DEFAULT '0',
  `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `entered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `renewal_date` date NOT NULL DEFAULT '0000-00-00',
  `discount_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `author_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publication_date` date NOT NULL DEFAULT '0000-00-00',
  `abstract` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_abstract` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fulltextsearch_keyword` text COLLATE utf8_unicode_ci NOT NULL,
  `fulltextsearch_where` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `suspended_sitemgr` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `level` tinyint(3) NOT NULL DEFAULT '0',
  `number_views` int(11) NOT NULL DEFAULT '0',
  `avg_review` int(11) NOT NULL DEFAULT '0',
  `random_number` bigint(15) NOT NULL DEFAULT '0',
  `cat_1_id` int(11) NOT NULL DEFAULT '0',
  `parcat_1_level1_id` int(11) NOT NULL DEFAULT '0',
  `parcat_1_level2_id` int(11) NOT NULL DEFAULT '0',
  `parcat_1_level3_id` int(11) NOT NULL DEFAULT '0',
  `parcat_1_level4_id` int(11) NOT NULL DEFAULT '0',
  `cat_2_id` int(11) NOT NULL DEFAULT '0',
  `parcat_2_level1_id` int(11) NOT NULL DEFAULT '0',
  `parcat_2_level2_id` int(11) NOT NULL DEFAULT '0',
  `parcat_2_level3_id` int(11) NOT NULL DEFAULT '0',
  `parcat_2_level4_id` int(11) NOT NULL DEFAULT '0',
  `cat_3_id` int(11) NOT NULL DEFAULT '0',
  `parcat_3_level1_id` int(11) NOT NULL DEFAULT '0',
  `parcat_3_level2_id` int(11) NOT NULL DEFAULT '0',
  `parcat_3_level3_id` int(11) NOT NULL DEFAULT '0',
  `parcat_3_level4_id` int(11) NOT NULL DEFAULT '0',
  `cat_4_id` int(11) NOT NULL DEFAULT '0',
  `parcat_4_level1_id` int(11) NOT NULL DEFAULT '0',
  `parcat_4_level2_id` int(11) NOT NULL DEFAULT '0',
  `parcat_4_level3_id` int(11) NOT NULL DEFAULT '0',
  `parcat_4_level4_id` int(11) NOT NULL DEFAULT '0',
  `cat_5_id` int(11) NOT NULL DEFAULT '0',
  `parcat_5_level1_id` int(11) NOT NULL DEFAULT '0',
  `parcat_5_level2_id` int(11) NOT NULL DEFAULT '0',
  `parcat_5_level3_id` int(11) NOT NULL DEFAULT '0',
  `parcat_5_level4_id` int(11) NOT NULL DEFAULT '0',
  `package_id` int(11) NOT NULL,
  `package_price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `publication_date` (`publication_date`),
  KEY `status` (`status`),
  KEY `level` (`level`),
  KEY `account_id` (`account_id`),
  KEY `friendly_url` (`friendly_url`),
  KEY `random_number` (`random_number`),
  KEY `entered` (`entered`),
  KEY `updated` (`updated`),
  KEY `cat_1_id` (`cat_1_id`),
  KEY `parcat_1_level1_id` (`parcat_1_level1_id`),
  KEY `cat_2_id` (`cat_2_id`),
  KEY `parcat_2_level1_id` (`parcat_2_level1_id`),
  KEY `cat_3_id` (`cat_3_id`),
  KEY `parcat_3_level1_id` (`parcat_3_level1_id`),
  KEY `cat_4_id` (`cat_4_id`),
  KEY `parcat_4_level1_id` (`parcat_4_level1_id`),
  KEY `cat_5_id` (`cat_5_id`),
  KEY `parcat_5_level1_id` (`parcat_5_level1_id`),
  FULLTEXT KEY `fulltextsearch_keyword` (`fulltextsearch_keyword`),
  FULLTEXT KEY `fulltextsearch_where` (`fulltextsearch_where`),
  FULLTEXT KEY `fulltextsearch_title` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ArticleCategory`
--

CREATE TABLE IF NOT EXISTS `ArticleCategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `thumb_id` int(11) NOT NULL DEFAULT '0',
  `image_id` int(11) NOT NULL DEFAULT '0',
  `featured` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `summary_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `active_article` int(11) NOT NULL DEFAULT '0',
  `enabled` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `count_sub` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `active_article` (`active_article`),
  KEY `title1` (`title`),
  KEY `friendly_url1` (`friendly_url`),
  FULLTEXT KEY `keywords` (`keywords`,`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ArticleLevel`
--

CREATE TABLE IF NOT EXISTS `ArticleLevel` (
  `value` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `defaultlevel` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `detail` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `images` int(3) NOT NULL DEFAULT '0',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `active` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `featured` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `theme` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  PRIMARY KEY (`value`,`theme`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ArticleLevel`
--

INSERT INTO `ArticleLevel` (`value`, `name`, `defaultlevel`, `detail`, `images`, `price`, `active`, `content`, `featured`, `theme`) VALUES
(50, 'article', 'y', 'y', 5, '30.00', 'y', '', 'y', 'default'),
(50, 'article', 'y', 'y', 5, '30.00', 'y', '', 'y', 'realestate'),
(50, 'article', 'y', 'y', 5, '30.00', 'y', '', 'y', 'diningguide'),
(50, 'article', 'y', 'y', 5, '30.00', 'y', '', 'y', 'contractors');

-- --------------------------------------------------------

--
-- Table structure for table `Banner`
--

CREATE TABLE IF NOT EXISTS `Banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL DEFAULT '0',
  `image_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `renewal_date` date NOT NULL DEFAULT '0000-00-00',
  `discount_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `caption` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'P',
  `suspended_sitemgr` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `random_number` bigint(15) NOT NULL DEFAULT '0',
  `target_window` tinyint(1) NOT NULL DEFAULT '1',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `section` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'general',
  `content_line1` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `content_line2` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `destination_protocol` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `display_url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `destination_url` text COLLATE utf8_unicode_ci NOT NULL,
  `unpaid_impressions` int(11) NOT NULL DEFAULT '0',
  `impressions` int(11) NOT NULL DEFAULT '0',
  `unlimited_impressions` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `expiration_setting` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `entered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `show_type` tinyint(4) NOT NULL DEFAULT '0',
  `script` text COLLATE utf8_unicode_ci,
  `package_id` int(11) NOT NULL,
  `package_price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `type` (`type`),
  KEY `section` (`section`),
  KEY `expiration_setting` (`expiration_setting`),
  KEY `impressions` (`impressions`),
  KEY `account_id` (`account_id`),
  KEY `category_id` (`category_id`),
  KEY `random_number` (`random_number`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `BannerLevel`
--

CREATE TABLE IF NOT EXISTS `BannerLevel` (
  `value` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `defaultlevel` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `width` int(11) NOT NULL DEFAULT '0',
  `height` int(11) NOT NULL DEFAULT '0',
  `impression_block` mediumint(9) NOT NULL DEFAULT '0',
  `impression_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `active` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `popular` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `displayName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `theme` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  PRIMARY KEY (`value`,`theme`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `BannerLevel`
--

INSERT INTO `BannerLevel` (`value`, `name`, `defaultlevel`, `price`, `width`, `height`, `impression_block`, `impression_price`, `active`, `popular`, `content`, `displayName`, `theme`) VALUES
(1, 'top', 'y', '50.00', 728, 90, 1000, '50.00', 'y', '', '', 'top', 'default'),
(2, 'bottom', 'n', '20.00', 468, 60, 1000, '20.00', 'y', '', '', 'bottom', 'default'),
(3, 'top right', 'n', '40.00', 120, 90, 1000, '40.00', 'y', '', '', 'Top Right', 'default'),
(50, 'sponsored links', 'n', '10.00', 180, 66, 1000, '10.00', 'y', '', '', 'sponsored links', 'default'),
(2, 'bottom', 'y', '20.00', 468, 60, 1000, '20.00', 'y', '', '', 'bottom', 'realestate'),
(3, 'featured', 'n', '40.00', 180, 150, 1000, '40.00', 'y', '', '', 'featured', 'realestate'),
(50, 'sponsored links', 'n', '10.00', 180, 100, 1000, '10.00', 'y', '', '', 'sponsored links', 'realestate'),
(1, 'top', 'y', '50.00', 728, 90, 1000, '50.00', 'y', '', '', 'top', 'diningguide'),
(2, 'bottom', 'n', '20.00', 468, 60, 1000, '20.00', 'y', '', '', 'bottom', 'diningguide'),
(3, 'top right', 'n', '40.00', 120, 90, 1000, '40.00', 'y', '', '', 'top right', 'diningguide'),
(50, 'sponsored links', 'n', '10.00', 180, 66, 1000, '10.00', 'y', '', '', 'sponsored links', 'diningguide'),
(1, 'top', 'y', '50.00', 728, 90, 1000, '50.00', 'y', '', '', 'top', 'contractors'),
(2, 'bottom', 'n', '20.00', 468, 60, 1000, '20.00', 'y', '', '', 'bottom', 'contractors'),
(3, 'top right', 'n', '40.00', 120, 90, 1000, '40.00', 'y', '', '', 'top right', 'contractors'),
(50, 'sponsored links', 'n', '10.00', 180, 66, 1000, '10.00', 'y', '', '', 'sponsored links', 'contractors');

-- --------------------------------------------------------

--
-- Table structure for table `BlogCategory`
--

CREATE TABLE IF NOT EXISTS `BlogCategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `root_id` int(11) NOT NULL DEFAULT '0',
  `left` int(11) NOT NULL DEFAULT '0',
  `right` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `thumb_id` int(11) NOT NULL DEFAULT '0',
  `image_id` int(11) NOT NULL DEFAULT '0',
  `featured` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `summary_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `active_post` int(11) NOT NULL DEFAULT '0',
  `full_friendly_url` text COLLATE utf8_unicode_ci,
  `level` tinyint(3) NOT NULL DEFAULT '0',
  `legacy_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `legacy_id` (`legacy_id`),
  KEY `category_id` (`category_id`),
  KEY `active_post` (`active_post`),
  KEY `title1` (`title`),
  KEY `friendly_url1` (`friendly_url`),
  KEY `level` (`level`),
  FULLTEXT KEY `keywords` (`keywords`,`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Blog_Category`
--

CREATE TABLE IF NOT EXISTS `Blog_Category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `category_root_id` int(11) NOT NULL,
  `category_node_left` int(11) NOT NULL,
  `category_node_right` int(11) NOT NULL,
  `status` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  KEY `category_id` (`category_id`),
  KEY `status` (`status`),
  KEY `category_status` (`category_id`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `CheckIn`
--

CREATE TABLE IF NOT EXISTS `CheckIn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL DEFAULT '0',
  `item_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'listing' COMMENT 'listing/event',
  `member_id` int(11) NOT NULL,
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quick_tip` text COLLATE utf8_unicode_ci,
  `checkin_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `listing_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Claim`
--

CREATE TABLE IF NOT EXISTS `Claim` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `listing_id` int(11) NOT NULL,
  `listing_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_time` datetime NOT NULL,
  `step` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `old_location_1` int(11) NOT NULL,
  `new_location_1` int(11) NOT NULL,
  `old_location_2` int(11) NOT NULL,
  `new_location_2` int(11) NOT NULL,
  `old_location_3` int(11) NOT NULL,
  `new_location_3` int(11) NOT NULL,
  `old_location_4` int(11) NOT NULL,
  `new_location_4` int(11) NOT NULL,
  `old_location_5` int(11) NOT NULL,
  `new_location_5` int(11) NOT NULL,
  `old_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `new_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `old_friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `new_friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `old_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `new_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `old_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `new_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `old_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `new_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `old_fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `new_fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `old_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `new_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `old_address2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `new_address2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `old_zip_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `new_zip_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `old_level` tinyint(3) NOT NULL,
  `new_level` tinyint(3) NOT NULL,
  `old_listingtemplate_id` int(11) NOT NULL DEFAULT '0',
  `new_listingtemplate_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Classified`
--

CREATE TABLE IF NOT EXISTS `Classified` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL DEFAULT '0',
  `image_id` int(11) NOT NULL DEFAULT '0',
  `thumb_id` int(11) NOT NULL DEFAULT '0',
  `entered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `renewal_date` date NOT NULL DEFAULT '0000-00-00',
  `discount_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `summarydesc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_summarydesc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `detaildesc` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fulltextsearch_keyword` text COLLATE utf8_unicode_ci NOT NULL,
  `fulltextsearch_where` text COLLATE utf8_unicode_ci NOT NULL,
  `zip_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `zip5` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `latitude` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `maptuning` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `maptuning_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `location_1` int(11) NOT NULL DEFAULT '0',
  `location_2` int(11) NOT NULL DEFAULT '0',
  `location_3` int(11) NOT NULL DEFAULT '0',
  `location_4` int(11) NOT NULL DEFAULT '0',
  `location_5` int(11) NOT NULL DEFAULT '0',
  `level` int(3) NOT NULL DEFAULT '0',
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `suspended_sitemgr` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `random_number` bigint(15) NOT NULL DEFAULT '0',
  `cat_1_id` int(11) NOT NULL DEFAULT '0',
  `parcat_1_level1_id` int(11) NOT NULL DEFAULT '0',
  `parcat_1_level2_id` int(11) NOT NULL DEFAULT '0',
  `parcat_1_level3_id` int(11) NOT NULL DEFAULT '0',
  `parcat_1_level4_id` int(11) NOT NULL DEFAULT '0',
  `cat_2_id` int(11) NOT NULL DEFAULT '0',
  `parcat_2_level1_id` int(11) NOT NULL DEFAULT '0',
  `parcat_2_level2_id` int(11) NOT NULL DEFAULT '0',
  `parcat_2_level3_id` int(11) NOT NULL DEFAULT '0',
  `parcat_2_level4_id` int(11) NOT NULL DEFAULT '0',
  `cat_3_id` int(11) NOT NULL DEFAULT '0',
  `parcat_3_level1_id` int(11) NOT NULL DEFAULT '0',
  `parcat_3_level2_id` int(11) NOT NULL DEFAULT '0',
  `parcat_3_level3_id` int(11) NOT NULL DEFAULT '0',
  `parcat_3_level4_id` int(11) NOT NULL DEFAULT '0',
  `cat_4_id` int(11) NOT NULL DEFAULT '0',
  `parcat_4_level1_id` int(11) NOT NULL DEFAULT '0',
  `parcat_4_level2_id` int(11) NOT NULL DEFAULT '0',
  `parcat_4_level3_id` int(11) NOT NULL DEFAULT '0',
  `parcat_4_level4_id` int(11) NOT NULL DEFAULT '0',
  `cat_5_id` int(11) NOT NULL DEFAULT '0',
  `parcat_5_level1_id` int(11) NOT NULL DEFAULT '0',
  `parcat_5_level2_id` int(11) NOT NULL DEFAULT '0',
  `parcat_5_level3_id` int(11) NOT NULL DEFAULT '0',
  `parcat_5_level4_id` int(11) NOT NULL DEFAULT '0',
  `classified_price` double(9,2) DEFAULT NULL,
  `number_views` int(11) NOT NULL DEFAULT '0',
  `map_zoom` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `package_price` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`location_1`),
  KEY `state_id` (`location_2`),
  KEY `region_id` (`location_3`),
  KEY `latitude` (`latitude`),
  KEY `longitude` (`longitude`),
  KEY `level` (`level`),
  KEY `status` (`status`),
  KEY `account_id` (`account_id`),
  KEY `city_id` (`location_4`),
  KEY `area_id` (`location_5`),
  KEY `title` (`title`),
  KEY `friendly_url` (`friendly_url`),
  KEY `random_number` (`random_number`),
  KEY `cat_1_id` (`cat_1_id`),
  KEY `parcat_1_level1_id` (`parcat_1_level1_id`),
  FULLTEXT KEY `fulltextsearch_keyword` (`fulltextsearch_keyword`),
  FULLTEXT KEY `fulltextsearch_where` (`fulltextsearch_where`),
  FULLTEXT KEY `fulltextsearch_title` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ClassifiedCategory`
--

CREATE TABLE IF NOT EXISTS `ClassifiedCategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `thumb_id` int(11) NOT NULL DEFAULT '0',
  `image_id` int(11) NOT NULL DEFAULT '0',
  `featured` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `summary_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `active_classified` int(11) NOT NULL DEFAULT '0',
  `enabled` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `count_sub` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `active_classified` (`active_classified`),
  KEY `title1` (`title`),
  KEY `friendly_url1` (`friendly_url`),
  FULLTEXT KEY `keywords` (`keywords`,`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ClassifiedLevel`
--

CREATE TABLE IF NOT EXISTS `ClassifiedLevel` (
  `value` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `defaultlevel` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `detail` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `images` int(3) NOT NULL DEFAULT '0',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `active` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `popular` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `featured` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `theme` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  PRIMARY KEY (`value`,`theme`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ClassifiedLevel`
--

INSERT INTO `ClassifiedLevel` (`value`, `name`, `defaultlevel`, `detail`, `images`, `price`, `active`, `popular`, `featured`, `content`, `theme`) VALUES
(50, 'silver', 'n', 'n', 0, '0.00', 'y', '', '', '', 'default'),
(30, 'gold', 'n', 'y', 3, '25.00', 'y', '', 'y', '', 'default'),
(10, 'diamond', 'y', 'y', 7, '50.00', 'y', 'y', 'y', '', 'default'),
(50, 'silver', 'n', 'y', 0, '0.00', 'y', '', 'y', '', 'realestate'),
(30, 'gold', 'n', 'y', 3, '25.00', 'y', '', 'y', '', 'realestate'),
(10, 'diamond', 'y', 'y', 7, '50.00', 'y', 'y', 'y', '', 'realestate'),
(50, 'silver', 'n', 'n', 0, '0.00', 'y', '', '', '', 'diningguide'),
(30, 'gold', 'n', 'y', 3, '25.00', 'y', '', 'y', '', 'diningguide'),
(10, 'diamond', 'y', 'y', 7, '50.00', 'y', 'y', 'y', '', 'diningguide'),
(50, 'silver', 'n', 'n', 0, '0.00', 'y', '', '', '', 'contractors'),
(30, 'gold', 'n', 'y', 3, '25.00', 'y', '', 'y', '', 'contractors'),
(10, 'diamond', 'y', 'y', 7, '50.00', 'y', 'y', 'y', '', 'contractors');

-- --------------------------------------------------------

--
-- Table structure for table `ClassifiedLevel_Field`
--

CREATE TABLE IF NOT EXISTS `ClassifiedLevel_Field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `theme` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `level` int(3) NOT NULL,
  `field` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `theme` (`theme`,`level`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=106 ;

--
-- Dumping data for table `ClassifiedLevel_Field`
--

INSERT INTO `ClassifiedLevel_Field` (`id`, `theme`, `level`, `field`) VALUES
(1, 'default', 10, 'contact_name'),
(2, 'default', 30, 'contact_name'),
(3, 'realestate', 10, 'contact_name'),
(4, 'realestate', 30, 'contact_name'),
(5, 'default', 10, 'fax'),
(6, 'realestate', 10, 'fax'),
(7, 'default', 10, 'url'),
(8, 'realestate', 10, 'url'),
(9, 'default', 10, 'long_description'),
(10, 'default', 30, 'long_description'),
(11, 'realestate', 10, 'long_description'),
(12, 'realestate', 30, 'long_description'),
(13, 'default', 10, 'main_image'),
(14, 'default', 30, 'main_image'),
(15, 'default', 50, 'main_image'),
(16, 'realestate', 10, 'main_image'),
(17, 'realestate', 30, 'main_image'),
(18, 'realestate', 50, 'main_image'),
(19, 'realestate', 10, 'summary_description'),
(20, 'realestate', 30, 'summary_description'),
(21, 'realestate', 50, 'summary_description'),
(22, 'default', 10, 'summary_description'),
(23, 'default', 30, 'summary_description'),
(24, 'default', 50, 'summary_description'),
(25, 'realestate', 10, 'contact_phone'),
(26, 'realestate', 30, 'contact_phone'),
(27, 'realestate', 50, 'contact_phone'),
(28, 'default', 10, 'contact_phone'),
(29, 'default', 30, 'contact_phone'),
(30, 'default', 50, 'contact_phone'),
(31, 'realestate', 10, 'contact_email'),
(32, 'realestate', 30, 'contact_email'),
(33, 'realestate', 50, 'contact_email'),
(34, 'default', 10, 'contact_email'),
(35, 'default', 30, 'contact_email'),
(36, 'default', 50, 'contact_email'),
(37, 'realestate', 10, 'price'),
(38, 'realestate', 30, 'price'),
(39, 'realestate', 50, 'price'),
(40, 'default', 10, 'price'),
(41, 'default', 30, 'price'),
(42, 'default', 50, 'price'),
(43, 'diningguide', 10, 'contact_name'),
(44, 'diningguide', 30, 'contact_name'),
(45, 'diningguide', 10, 'fax'),
(46, 'diningguide', 10, 'url'),
(49, 'diningguide', 10, 'main_image'),
(52, 'diningguide', 10, 'summary_description'),
(54, 'diningguide', 50, 'summary_description'),
(57, 'diningguide', 50, 'contact_phone'),
(61, 'diningguide', 10, 'price'),
(62, 'diningguide', 30, 'price'),
(63, 'diningguide', 50, 'price'),
(68, 'diningguide', 10, 'long_description'),
(69, 'diningguide', 30, 'long_description'),
(71, 'diningguide', 30, 'main_image'),
(72, 'diningguide', 50, 'main_image'),
(74, 'diningguide', 30, 'summary_description'),
(76, 'diningguide', 10, 'contact_phone'),
(77, 'diningguide', 30, 'contact_phone'),
(79, 'diningguide', 10, 'contact_email'),
(80, 'diningguide', 30, 'contact_email'),
(81, 'diningguide', 50, 'contact_email'),
(85, 'contractors', 10, 'contact_name'),
(86, 'contractors', 30, 'contact_name'),
(87, 'contractors', 10, 'fax'),
(88, 'contractors', 10, 'url'),
(89, 'contractors', 10, 'long_description'),
(90, 'contractors', 30, 'long_description'),
(91, 'contractors', 10, 'main_image'),
(92, 'contractors', 30, 'main_image'),
(93, 'contractors', 50, 'main_image'),
(94, 'contractors', 10, 'summary_description'),
(95, 'contractors', 30, 'summary_description'),
(96, 'contractors', 50, 'summary_description'),
(97, 'contractors', 10, 'contact_phone'),
(98, 'contractors', 30, 'contact_phone'),
(99, 'contractors', 50, 'contact_phone'),
(100, 'contractors', 10, 'contact_email'),
(101, 'contractors', 30, 'contact_email'),
(102, 'contractors', 50, 'contact_email'),
(103, 'contractors', 10, 'price'),
(104, 'contractors', 30, 'price'),
(105, 'contractors', 50, 'price');

-- --------------------------------------------------------

--
-- Table structure for table `Classified_LocationCounter`
--

CREATE TABLE IF NOT EXISTS `Classified_LocationCounter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_level` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `full_friendly_url` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Comments`
--

CREATE TABLE IF NOT EXISTS `Comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL DEFAULT '0',
  `reply_id` int(11) NOT NULL DEFAULT '0',
  `member_id` int(11) NOT NULL,
  `member_name` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text COLLATE utf8_unicode_ci,
  `approved` int(1) NOT NULL,
  `legacy_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `legacy_id` (`legacy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Content`
--

CREATE TABLE IF NOT EXISTS `Content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `section` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'general',
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `sitemap` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `section` (`section`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=91 ;

--
-- Dumping data for table `Content`
--

INSERT INTO `Content` (`id`, `updated`, `type`, `title`, `description`, `keywords`, `url`, `section`, `content`, `sitemap`) VALUES
(1, '2014-04-30 09:40:40', 'Home Page', 'Home Page', '', '', '', 'general', '', 0),
(2, '0000-00-00 00:00:00', 'Home Page Bottom', 'Home Page Bottom', '', '', '', 'general', '', 0),
(3, '0000-00-00 00:00:00', 'Directory Results', 'Directory Results', '', '', '', 'general', '', 0),
(4, '0000-00-00 00:00:00', 'Directory Results Bottom', 'Directory Results Bottom', '', '', '', 'general', '', 0),
(5, '2014-09-26 11:42:06', 'Advertise with Us', 'Advertise with Us', '', '', '', 'advertise_general', '<h1><span class="short_text" lang="pt"><span class="hps">Sign up today - It''s quick and simple!</span></span></h1>\r\n<p><span class="hps" lang="pt"><span class="hps"><br /></span></span></p>\r\n<p><span class="hps" lang="pt"><span class="hps">Demo Directory is proud to announce its new directory service which is now available online to visitors and new suppliers. It boasts endless amounts of new features for customers and suppliers.<br /><br />Your directory items are also controlled entirely by you. We have a members interface where you can log in and change any details, add special promotions for Demo Directory customers and much more!</span></span></p>', 0),
(6, '0000-00-00 00:00:00', 'Advertise with Us Bottom', 'Advertise with Us Bottom', '', '', '', 'advertise_general', '', 0),
(7, '0000-00-00 00:00:00', 'Contact Us', 'Contact Us', '', '', '', 'general', '<h4>Contact Us</h4>\r\n<hr />', 0),
(8, '0000-00-00 00:00:00', 'Terms of Use', '', '', '', '', 'general', '<h2>Terms of Use</h2>\r\n<p>&nbsp;</p>\r\n<p>Pellentesque nulla sem, suscipit quis mattis et, imperdiet nec massa. Mauris faucibus fermentum aliquam. Aliquam commodo egestas iaculis. Pellentesque ut mauris nisi, commodo gravida elit. Phasellus eget diam eros. Donec ante velit, dignissim in congue eget, congue in quam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed ut ipsum nisi, ut pulvinar augue. Quisque blandit facilisis velit non ornare. Cras et urna nunc. Praesent non ante urna. Sed vehicula nulla sit amet lacus mattis scelerisque. Sed non felis arcu. Morbi congue aliquet ante, quis vestibulum ipsum gravida non. Sed ante felis, lobortis ac pharetra fermentum, aliquet id elit.<br /><br />Maecenas lobortis eleifend turpis, eu luctus sapien ultricies vitae. Aliquam erat volutpat. Nullam dolor odio, dapibus sed pellentesque nec, adipiscing eget quam. Integer mi sem, pharetra ac convallis eget, sodales in mauris. Suspendisse quis urna non nisl ullamcorper imperdiet a quis eros. Vivamus egestas posuere consequat. Nulla a commodo nunc. Morbi ut enim lectus, vitae pretium dui. Phasellus lacinia, lorem id malesuada luctus, enim augue fermentum augue, eu luctus dolor magna et turpis. Praesent vitae ornare mauris.<br /><br />Nam eget libero at tortor auctor placerat at vitae orci. Fusce tempus luctus dolor. Vivamus pharetra erat vitae ipsum pharetra ut tincidunt lacus pharetra. Sed viverra interdum fringilla. Aenean cursus nulla at neque luctus et pellentesque ante dignissim. Ut luctus odio et velit suscipit imperdiet. Cras semper, leo quis venenatis elementum, nibh diam blandit nulla, ut dictum ipsum lectus non diam. Vivamus ac convallis velit. Nulla est magna, bibendum eu luctus sed, dignissim in libero. Etiam sit amet purus ut odio mollis varius. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec non velit vel augue mollis blandit eu eu metus. Morbi dapibus porttitor scelerisque. Praesent ultricies erat ac nisi laoreet sit amet consequat leo fringilla. Fusce egestas neque ut nisl mattis eget dignissim elit accumsan. Phasellus mollis dapibus tristique. Sed ut velit non nibh sagittis ornare sed feugiat nibh.</p>', 0),
(9, '0000-00-00 00:00:00', 'Listing Advertisement', 'Listing Advertisement', '', '', '', 'advertise_listing', '', 0),
(10, '0000-00-00 00:00:00', 'Listing Home', 'Listing Home', '', '', '', 'listing', '', 0),
(11, '0000-00-00 00:00:00', 'Listing Home Bottom', 'Listing Home Bottom', '', '', '', 'listing', '', 0),
(12, '0000-00-00 00:00:00', 'Listing Results', 'Listing Results', '', '', '', 'listing', '', 0),
(13, '0000-00-00 00:00:00', 'Listing Results Bottom', 'Listing Results Bottom', '', '', '', 'listing', '', 0),
(14, '0000-00-00 00:00:00', 'Listing View All Categories', 'Listing View All Categories', '', '', '', 'listing', '', 0),
(15, '0000-00-00 00:00:00', 'Listing View All Categories Bottom', 'Listing View All Categories Bottom', '', '', '', 'listing', '', 0),
(16, '0000-00-00 00:00:00', 'Listing View All Locations', 'Listing View All Locations', '', '', '', 'listing', '', 0),
(17, '0000-00-00 00:00:00', 'Listing View All Locations Bottom', 'Listing View All Locations Bottom', '', '', '', 'listing', '', 0),
(18, '0000-00-00 00:00:00', 'Deal Home', 'Deal Home', '', '', '', 'deal', '', 0),
(19, '0000-00-00 00:00:00', 'Deal Home Bottom', 'Deal Home Bottom', '', '', '', 'deal', '', 0),
(20, '0000-00-00 00:00:00', 'Deal Results', 'Deal Results', '', '', '', 'deal', '', 0),
(21, '0000-00-00 00:00:00', 'Deal Results Bottom', 'Deal Results Bottom', '', '', '', 'deal', '', 0),
(22, '0000-00-00 00:00:00', 'Deal View All Categories', 'Deal View All Categories', '', '', '', 'deal', '', 0),
(23, '0000-00-00 00:00:00', 'Deal View All Categories Bottom', 'Deal View All Categories Bottom', '', '', '', 'deal', '', 0),
(24, '0000-00-00 00:00:00', 'Deal View All Locations', 'Deal View All Locations', '', '', '', 'deal', '', 0),
(25, '0000-00-00 00:00:00', 'Deal View All Locations Bottom', 'Deal View All Locations Bottom', '', '', '', 'deal', '', 0),
(26, '0000-00-00 00:00:00', 'Event Advertisement', 'Event Advertisement', '', '', '', 'advertise_event', '', 0),
(27, '0000-00-00 00:00:00', 'Event Home', 'Event Home', '', '', '', 'event', '', 0),
(28, '0000-00-00 00:00:00', 'Event Home Bottom', 'Event Home Bottom', '', '', '', 'event', '', 0),
(29, '0000-00-00 00:00:00', 'Event Results', 'Event Results', '', '', '', 'event', '', 0),
(30, '0000-00-00 00:00:00', 'Event Results Bottom', 'Event Results Bottom', '', '', '', 'event', '', 0),
(31, '0000-00-00 00:00:00', 'Event View All Categories', 'Event View All Categories', '', '', '', 'event', '', 0),
(32, '0000-00-00 00:00:00', 'Event View All Categories Bottom', 'Event View All Categories Bottom', '', '', '', 'event', '', 0),
(33, '0000-00-00 00:00:00', 'Event View All Locations', 'Event View All Locations', '', '', '', 'event', '', 0),
(34, '0000-00-00 00:00:00', 'Event View All Locations Bottom', 'Event View All Locations Bottom', '', '', '', 'event', '', 0),
(35, '2014-09-26 11:36:47', 'Banner Advertisement', '', '', '', '', 'advertise_banner', '<h2>Banners</h2>\r\n<p>&nbsp;</p>\r\n<table style="width: 527px; height: 61px;">\r\n<tbody>\r\n<tr>\r\n<td class="advertiseScreen">\r\n<ul>\r\n<li>Display targeted ads that match the directory content</li>\r\n<li><span lang="pt"><span class="hps">Reach people seeking information on products and services in the directory</span></span></li>\r\n<li>Build brand recognition</li>\r\n<li><span lang="pt"><span class="hps">Return on Investment - Start getting customers in minutes!</span></span></li>\r\n</ul>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p><span lang="pt"><span class="hps"><br class="hps" /></span></span></p>', 0),
(36, '0000-00-00 00:00:00', 'Classified Advertisement', 'Classified Advertisement', '', '', '', 'advertise_classified', '', 0),
(37, '0000-00-00 00:00:00', 'Classified Home', 'Classified Home', '', '', '', 'classified', '', 0),
(38, '0000-00-00 00:00:00', 'Classified Home Bottom', 'Classified Home Bottom', '', '', '', 'classified', '', 0),
(39, '0000-00-00 00:00:00', 'Classified Results', 'Classified Results', '', '', '', 'classified', '', 0),
(40, '0000-00-00 00:00:00', 'Classified Results Bottom', 'Classified Results Bottom', '', '', '', 'classified', '', 0),
(41, '0000-00-00 00:00:00', 'Classified View All Categories', 'Classified View All Categories', '', '', '', 'classified', '', 0),
(42, '0000-00-00 00:00:00', 'Classified View All Categories Bottom', 'Classified View All Categories Bottom', '', '', '', 'classified', '', 0),
(43, '0000-00-00 00:00:00', 'Classified View All Locations', 'Classified View All Locations', '', '', '', 'classified', '', 0),
(44, '0000-00-00 00:00:00', 'Classified View All Locations Bottom', 'Classified View All Locations Bottom', '', '', '', 'classified', '', 0),
(45, '2014-09-26 11:36:50', 'Article Advertisement', '', '', '', '', 'advertise_article', '<table>\r\n<tbody>\r\n<tr>\r\n<td class="advertiseScreen"><span lang="pt"><span class="hps">Post your article and become known to prospective clients and customers.</span></span></td>\r\n</tr>\r\n</tbody>\r\n</table>', 0),
(46, '0000-00-00 00:00:00', 'Article Home', 'Article Home', '', '', '', 'article', '', 0),
(47, '0000-00-00 00:00:00', 'Article Home Bottom', 'Article Home Bottom', '', '', '', 'article', '', 0),
(48, '0000-00-00 00:00:00', 'Article Results', 'Article Results', '', '', '', 'article', '', 0),
(49, '0000-00-00 00:00:00', 'Article Results Bottom', 'Article Results Bottom', '', '', '', 'article', '', 0),
(50, '0000-00-00 00:00:00', 'Article View All Categories', 'Article View All Categories', '', '', '', 'article', '', 0),
(51, '0000-00-00 00:00:00', 'Article View All Categories Bottom', 'Article View All Categories Bottom', '', '', '', 'article', '', 0),
(52, '0000-00-00 00:00:00', 'Sponsor Home', 'Sponsor Home', '', '', '', 'member', '', 0),
(53, '0000-00-00 00:00:00', 'Sponsor Home Bottom', 'Sponsor Home Bottom', '', '', '', 'member', '', 0),
(54, '0000-00-00 00:00:00', 'Manage Account', 'Manage Account', '', '', '', 'member', '', 0),
(55, '0000-00-00 00:00:00', 'Manage Account Bottom', 'Manage Account Bottom', '', '', '', 'member', '', 0),
(56, '0000-00-00 00:00:00', 'Sponsor Help', 'Sponsor Help', '', '', '', 'member', '', 0),
(57, '0000-00-00 00:00:00', 'Sponsor Help Bottom', 'Sponsor Help Bottom', '', '', '', 'member', '', 0),
(89, '0000-00-00 00:00:00', 'Event Change Level', 'Event Change Level', '', '', '', 'member', '', 0),
(88, '0000-00-00 00:00:00', 'Listing Change Level', 'Listing Change Level', '', '', '', 'member', '', 0),
(70, '0000-00-00 00:00:00', 'Transaction', 'Transaction', '', '', '', 'member', '', 0),
(71, '0000-00-00 00:00:00', 'Transaction Bottom', 'Transaction Bottom', '', '', '', 'member', '', 0),
(72, '0000-00-00 00:00:00', 'Sitemap', 'Sitemap', '', '', '', 'general', '', 0),
(73, '0000-00-00 00:00:00', 'Error Page', 'Error Page', 'Page not found', '', '', 'general', '<table style="width: 100%;" border="0" cellspacing="0" cellpadding="0" align="center">\r\n<tbody>\r\n<tr>\r\n<td style="text-align: center; background-color: #eaf4fb; height: 175px;" align="center" valign="middle"><span style="color: #3498db;"><strong><span style="font-size: x-large;">It appears something in the machine has broken</span></strong></span></td>\r\n</tr>\r\n<tr>\r\n<td>&nbsp;\r\n<table style="width: 100%;" border="0" cellpadding="5">\r\n<tbody>\r\n<tr>\r\n<td style="width: 50%;" align="center" valign="middle">&nbsp;</td>\r\n<td align="center" valign="middle">\r\n<p style="text-align: left;"><span style="color: #3498db;"><strong><span style="font-size: large;">Did you destroy something?</span></strong></span></p>\r\n<p style="text-align: left;"><span style="color: #3498db;"><strong><span style="font-size: large;"><br /></span></strong></span></p>\r\n<p style="text-align: left;"><span style="font-size: small;">Of course not, just a simple error on our part, perhaps you visited a bad URL or typed something incorrectly. No problems, these things happen, the important thing is we&rsquo;re still here and so are you.</span></p>\r\n<p style="text-align: left;"><span style="font-size: medium;"><br /></span></p>\r\n<p style="text-align: left;"><span style="font-size: medium;"><br /></span></p>\r\n<p style="text-align: left;"><strong><span style="font-size: large; color: #3498db;">What to do about this pickle...</span></strong></p>\r\n<p style="text-align: left;"><strong><span style="font-size: large; color: #3498db;"><br /></span></strong></p>\r\n<ol>\r\n<li style="text-align: left;"><span style="font-size: small;">Try using the search at at the top there.</span></li>\r\n<li style="text-align: left;"><span style="font-size: small;">Try using the navigation at the top as well.</span></li>\r\n<li style="text-align: left;"><span style="font-size: small;">Sure, there&rsquo;s a problem - Contact Us (We&rsquo;ll fix it)</span></li>\r\n</ol>\r\n<p style="text-align: left;"><span style="font-size: x-large;"><br /></span></p>\r\n<p style="text-align: left;"><span style="font-size: medium;"><br /></span></p>\r\n<p style="text-align: left;"><span style="font-size: medium;"><br /></span></p>\r\n<p>&nbsp;</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', 0),
(74, '2014-09-26 11:33:26', 'Maintenance Page', 'Maintenance Page', '', '', '', 'general', '<table id="maintenancemode" style="width: 100%;">\r\n<tbody>\r\n<tr>\r\n<td id="bg-maintenancemode" style="text-align: center; height: 245px;">&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td style="text-align: center; background-color: #eaf4fb; height: 175px;"><span style="color: #3498db;"><strong><span style="font-size: x-large;">Whoa there Cowboy... This Website is currently down for maintenance, back soon. </span></strong></span></td>\r\n</tr>\r\n<tr>\r\n<td>&nbsp;\r\n<p style="text-align: center;"><span style="font-size: small;">We&rsquo;ll be down for a brief period while we make some changes.</span></p>\r\n<p style="text-align: center;"><span style="font-size: small;">Please, come back later for more information.</span></p>\r\n<div style="text-align: center;"><span><br /></span></div>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', 0),
(75, '0000-00-00 00:00:00', 'Profile Page', 'Profile Page', '', '', '', 'general', '', 0),
(76, '0000-00-00 00:00:00', 'Profile Page Bottom', 'Profile Page Bottom', '', '', '', 'general', '', 0),
(77, '0000-00-00 00:00:00', 'Add Profile Page', 'Add Profile Page', '', '', '', 'general', '', 0),
(78, '0000-00-00 00:00:00', 'Packages Offer', 'Packages Offer', '', '', '', 'advertise_general', '', 0),
(79, '0000-00-00 00:00:00', 'Blog Home', 'Blog Home', '', '', '', 'blog', '', 0),
(80, '0000-00-00 00:00:00', 'Blog Home Bottom', 'Blog Home Bottom', '', '', '', 'blog', '', 0),
(81, '0000-00-00 00:00:00', 'Blog Results', 'Blog Results', '', '', '', 'blog', '', 0),
(82, '0000-00-00 00:00:00', 'Blog Results Bottom', 'Blog Results Bottom', '', '', '', 'blog', '', 0),
(83, '0000-00-00 00:00:00', 'Blog View All Categories', 'Blog View All Categories', '', '', '', 'blog', '', 0),
(84, '0000-00-00 00:00:00', 'Blog View All Categories Bottom', 'Blog View All Categories Bottom', '', '', '', 'blog', '', 0),
(90, '0000-00-00 00:00:00', 'Classified Change Level', 'Classified Change Level', '', '', '', 'member', '', 0),
(86, '0000-00-00 00:00:00', 'Best Of', 'Best Of', '', '', '', 'general', '', 0),
(87, '0000-00-00 00:00:00', 'Leads Form', 'Enquire', '', '', '', 'general', '<h1>Did you not find what you''re looking for?</h1> <p>Please, let us know more about what you''re looking for.&nbsp;<span>We''d like to know a few details so we can present you with the best options available.</span></p>', 0);

-- --------------------------------------------------------

--
-- Table structure for table `CustomInvoice`
--

CREATE TABLE IF NOT EXISTS `CustomInvoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sent_date` text COLLATE utf8_unicode_ci NOT NULL,
  `subtotal` decimal(10,2) NOT NULL DEFAULT '0.00',
  `tax` decimal(10,2) NOT NULL DEFAULT '0.00',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `paid` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `sent` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `completed` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `paid` (`paid`),
  KEY `sent` (`sent`),
  KEY `completed` (`completed`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `CustomInvoice_Items`
--

CREATE TABLE IF NOT EXISTS `CustomInvoice_Items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `custominvoice_id` int(11) NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `custominvoice_id` (`custominvoice_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `CustomText`
--

CREATE TABLE IF NOT EXISTS `CustomText` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `CustomText`
--

INSERT INTO `CustomText` (`name`, `value`) VALUES
('header_title', 'MrQaidi Edirectory'),
('header_author', 'Ayman Qaidi'),
('header_description', 'MrQaidi Edirectory 10.4 Nulled'),
('header_keywords', 'Edirectory 10.4 Nulled'),
('footer_copyright', 'Copyright © 2015'),
('claim_textlink', 'Is this your listing?'),
('promotion_default_conditions', 'There is a limit of 1 deal per person. The promotional value of this deal expires in 3 months. Deal must be presented in order to receive discount. This deal is not valid for cash back, can only be used once, does not cover tax or gratuities. This deal can not be combined with other offers.'),
('payment_tax_label', 'Sales Tax');

-- --------------------------------------------------------

--
-- Table structure for table `Discount_Code`
--

CREATE TABLE IF NOT EXISTS `Discount_Code` (
  `id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `expire_date` date NOT NULL,
  `recurring` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `listing` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `event` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `banner` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `classified` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `article` char(3) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Editor_Choice`
--

CREATE TABLE IF NOT EXISTS `Editor_Choice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `available` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `image_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `available` (`available`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `Editor_Choice`
--

INSERT INTO `Editor_Choice` (`id`, `available`, `image_id`, `name`) VALUES
(1, '0', 1, '[Edit Title]'),
(2, '0', 2, '[Edit Title]'),
(3, '0', 3, '[Edit Title]'),
(4, '0', 4, '[Edit Title]'),
(5, '0', 5, '[Edit Title]'),
(6, '0', 6, '[Edit Title]'),
(7, '0', 7, '[Edit Title]'),
(8, '0', 8, '[Edit Title]'),
(9, '0', 9, '[Edit Title]');

-- --------------------------------------------------------

--
-- Table structure for table `Email_Notification`
--

CREATE TABLE IF NOT EXISTS `Email_Notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `days` tinyint(3) NOT NULL DEFAULT '0',
  `deactivate` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `bcc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_type` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `use_variables` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `deactivate` (`deactivate`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=41 ;

--
-- Dumping data for table `Email_Notification`
--

INSERT INTO `Email_Notification` (`id`, `email`, `days`, `deactivate`, `updated`, `bcc`, `subject`, `content_type`, `body`, `use_variables`) VALUES
(1, '30 day renewal reminder', 30, '0', '0000-00-00 00:00:00', '', 'Listing renewal notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nYour listing "LISTING_TITLE" will expire in DAYS_INTERVAL days.\r\n\r\nYou can renew immediately by logging in to your account and submitting a credit card payment through the Payment area by the link below.\r\n\r\nDEFAULT_URL/MEMBERS_URL/billing/index.php\r\n\r\nFor further assistance please contact SITEMGR_EMAIL\r\n\r\nThank you for being a member of the Directory.\r\n\r\nRegards,\r\nThe EDIRECTORY_TITLE Team', 'ACCOUNT_NAME,ACCOUNT_USERNAME,LISTING_TITLE,LISTING_RENEWAL_DATE,DAYS_INTERVAL,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,MEMBERS_URL'),
(2, '15 day renewal reminder', 15, '0', '0000-00-00 00:00:00', '', 'Listing renewal notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nYour listing "LISTING_TITLE" will expire in DAYS_INTERVAL days.\r\n\r\nYou can renew immediately by logging in to your account and submitting a credit card payment through the Payment area by the link below.\r\n\r\nDEFAULT_URL/MEMBERS_URL/billing/index.php\r\n\r\nFor further assistance please contact SITEMGR_EMAIL\r\n\r\nThank you for being a member of the Directory.\r\n\r\nRegards,\r\nThe EDIRECTORY_TITLE Team', 'ACCOUNT_NAME,ACCOUNT_USERNAME,LISTING_TITLE,LISTING_RENEWAL_DATE,DAYS_INTERVAL,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,MEMBERS_URL'),
(3, '7 day renewal reminder', 7, '0', '0000-00-00 00:00:00', '', 'Listing renewal notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nYour listing "LISTING_TITLE" will expire in DAYS_INTERVAL days.\r\n\r\nYou can renew immediately by logging in to your account and submitting a credit card payment through the Payment area by the link below.\r\n\r\nDEFAULT_URL/MEMBERS_URL/billing/index.php\r\n\r\nFor further assistance please contact SITEMGR_EMAIL\r\n\r\nThank you for being a member of the Directory.\r\n\r\nRegards,\r\nThe EDIRECTORY_TITLE Team', 'ACCOUNT_NAME,ACCOUNT_USERNAME,LISTING_TITLE,LISTING_RENEWAL_DATE,DAYS_INTERVAL,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,MEMBERS_URL'),
(4, '1 day renewal reminder', 1, '0', '0000-00-00 00:00:00', '', 'Listing renewal notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nYour listing "LISTING_TITLE" will expire in DAYS_INTERVAL days.\r\n\r\nYou can renew immediately by logging in to your account and submitting a credit card payment through the Payment area by the link below.\r\n\r\nDEFAULT_URL/MEMBERS_URL/billing/index.php\r\n\r\nFor further assistance please contact SITEMGR_EMAIL\r\n\r\nThank you for being a member of the Directory.\r\n\r\nRegards,\r\nThe EDIRECTORY_TITLE Team', 'ACCOUNT_NAME,ACCOUNT_USERNAME,LISTING_TITLE,LISTING_RENEWAL_DATE,DAYS_INTERVAL,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,MEMBERS_URL'),
(5, 'Sponsor Account Create (sitemgr area)', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Account', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nThank you for signing up for an account in DEFAULT_URL\r\nLogin to manage your account with the username and password below.\r\n\r\nDEFAULT_URL/MEMBERS_URL\r\n\r\nUsername: ACCOUNT_USERNAME\r\nPassword: ACCOUNT_PASSWORD\r\n\r\nDEFAULT_URL', 'ACCOUNT_NAME,ACCOUNT_USERNAME,ACCOUNT_PASSWORD,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,MEMBERS_URL'),
(6, 'Sponsor Account Update (sitemgr area)', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Account', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nYour account is updated in DEFAULT_URL\r\nLogin to manage your account with the username and password below.\r\n\r\nDEFAULT_URL/MEMBERS_URL\r\n\r\nUsername: ACCOUNT_USERNAME\r\nPassword: ACCOUNT_PASSWORD\r\n\r\nDEFAULT_URL', 'ACCOUNT_NAME,ACCOUNT_USERNAME,ACCOUNT_PASSWORD,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,MEMBERS_URL'),
(7, 'Visitor Account Create (sitemgr area)', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Account', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nThank you for signing up for an account in DEFAULT_URL\r\n\r\nUsername: ACCOUNT_USERNAME\r\nPassword: ACCOUNT_PASSWORD\r\n\r\nDEFAULT_URL', 'ACCOUNT_NAME,ACCOUNT_USERNAME,ACCOUNT_PASSWORD,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE'),
(8, 'Visitor Account Update (sitemgr area)', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Account', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nYour account is updated in DEFAULT_URL\r\n\r\nUsername: ACCOUNT_USERNAME\r\nPassword: ACCOUNT_PASSWORD\r\n\r\nDEFAULT_URL', 'ACCOUNT_NAME,ACCOUNT_USERNAME,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE'),
(9, 'Forgotten Password', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE account information', 'text/plain', 'As requested, here is the information regarding your EDIRECTORY_TITLE account.\r\nTo change your forgotten password please click the link below and enter a new password.\r\nKEY_ACCOUNT\r\n\r\nFor further assistance please contact SITEMGR_EMAIL.\r\n\r\nThank you for being a member of the EDIRECTORY_TITLE.', 'ACCOUNT_NAME,ACCOUNT_USERNAME,KEY_ACCOUNT,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE'),
(10, 'New Listing', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nYou have just added one listing in our directory.\r\n\r\nWhat do I do next?\r\n\r\nPlease login to the directory at the link below and click on Make your Payment link on the left menu options to pay for your listing.\r\nDEFAULT_URL/MEMBERS_URL\r\n\r\nOnce this has been completed a directory administrator will review your listing and set it live within the directory.\r\n\r\nSubmissions are reviewed within 2 working days following payment.\r\n\r\nThank you for being a member of the EDIRECTORY_TITLE!\r\n\r\nRegards\r\nEDIRECTORY_TITLE Advertising Team\r\n', 'ACCOUNT_NAME,ACCOUNT_USERNAME,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,MEMBERS_URL'),
(11, 'New Event', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nThank you for adding your event to our directory calendar.\r\n\r\nAn administrator will review your event and make it live within the directory within 2 working days.\r\n\r\nYou can see your event in DEFAULT_URL/MEMBERS_URL/"Dashboard" link.\r\n\r\nThank you for being a member of the EDIRECTORY_TITLE!\r\n\r\nRegards\r\nEDIRECTORY_TITLE Advertising Team', 'ACCOUNT_NAME,ACCOUNT_USERNAME,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,MEMBERS_URL'),
(12, 'New Banner', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nYou have just added one banner in our directory.\r\n\r\nWhat do I do next?\r\n\r\nPlease login to the directory at the link below and click on Make your Payment link on the left menu options to pay for your banner.\r\nDEFAULT_URL/MEMBERS_URL\r\n\r\nOnce this has been completed a directory administrator will review your banner and set it live within the directory.\r\n\r\nSubmissions are reviewed within 2 working days following payment.\r\n\r\nThank you for being a member of the EDIRECTORY_TITLE!\r\n\r\nRegards\r\nEDIRECTORY_TITLE Advertising Team\r\n', 'ACCOUNT_NAME,ACCOUNT_USERNAME,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,MEMBERS_URL'),
(13, 'New Classified', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nYou have just added one classified in our directory.\r\n\r\nWhat do I do next?\r\n\r\nPlease login to the directory at the link below and click on Make your Payment link on the left menu options to pay for your classified.\r\nDEFAULT_URL/MEMBERS_URL\r\n\r\nOnce this has been completed a directory administrator will review your classified and set it live within the directory.\r\n\r\nSubmissions are reviewed within 2 working days following payment.\r\n\r\nThank you for being a member of the EDIRECTORY_TITLE!\r\n\r\nRegards\r\nEDIRECTORY_TITLE Advertising Team\r\n', 'ACCOUNT_NAME,ACCOUNT_USERNAME,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,MEMBERS_URL'),
(14, 'New Article', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nYou have just added one article in our directory.\r\n\r\nWhat do I do next?\r\n\r\nPlease login to the directory at the link below and click on Make your Payment link on the left menu options to pay for your article.\r\nDEFAULT_URL/MEMBERS_URL\r\n\r\nOnce this has been completed a directory administrator will review your article and set it live within the directory.\r\n\r\nSubmissions are reviewed within 2 working days following payment.\r\n\r\nThank you for being a member of the EDIRECTORY_TITLE!\r\n\r\nRegards\r\nEDIRECTORY_TITLE Advertising Team\r\n', 'ACCOUNT_NAME,ACCOUNT_USERNAME,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,MEMBERS_URL'),
(15, 'Custom Invoice', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Invoice', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nYour invoice is ready for payment at the following link:\r\n\r\nDEFAULT_URL/MEMBERS_URL/billing/index.php\r\n\r\nAmount Due: CUSTOM_INVOICE_AMOUNT CUSTOM_INVOICE_TAX\r\n\r\nEDIRECTORY_TITLE\r\n', 'ACCOUNT_NAME,ACCOUNT_USERNAME,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,CUSTOM_INVOICE_AMOUNT,CUSTOM_INVOICE_TAX,MEMBERS_URL'),
(16, 'Active Listing', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Activation Notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nYour listing is live and active on EDIRECTORY_TITLE\r\n\r\nLISTING_DEFAULT_URL/\r\n\r\nThank you for being a member of the EDIRECTORY_TITLE!\r\n\r\nRegards\r\nEDIRECTORY_TITLE Team\r\n', 'ACCOUNT_NAME,ACCOUNT_USERNAME,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,LISTING_DEFAULT_URL'),
(17, 'Active Event', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Activation Notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nYour event is live and active on EDIRECTORY_TITLE\r\n\r\nEVENT_DEFAULT_URL/\r\n\r\nThank you for being a member of the EDIRECTORY_TITLE!\r\n\r\nRegards\r\nEDIRECTORY_TITLE Team\r\n', 'ACCOUNT_NAME,ACCOUNT_USERNAME,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,EVENT_DEFAULT_URL '),
(18, 'Active Banner', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Activation Notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nYour banner is live and active on EDIRECTORY_TITLE\r\n\r\nDEFAULT_URL/\r\n\r\nThank you for being a member of the EDIRECTORY_TITLE!\r\n\r\nRegards\r\nEDIRECTORY_TITLE Team\r\n', 'ACCOUNT_NAME,ACCOUNT_USERNAME,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE'),
(19, 'Active Classified', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Activation Notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nYour classified is live and active on EDIRECTORY_TITLE\r\n\r\nCLASSIFIED_DEFAULT_URL/\r\n\r\nThank you for being a member of the EDIRECTORY_TITLE!\r\n\r\nRegards\r\nEDIRECTORY_TITLE Team\r\n', 'ACCOUNT_NAME,ACCOUNT_USERNAME,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,CLASSIFIED_DEFAULT_URL'),
(20, 'Active Article', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Activation Notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nYour article is live and active on EDIRECTORY_TITLE\r\n\r\nARTICLE_DEFAULT_URL/\r\n\r\nThank you for being a member of the EDIRECTORY_TITLE!\r\n\r\nRegards\r\nEDIRECTORY_TITLE Team\r\n', 'ACCOUNT_NAME,ACCOUNT_USERNAME,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,ARTICLE_DEFAULT_URL'),
(21, 'Email to Friend', 0, '0', '0000-00-00 00:00:00', '', 'Here''s information on ITEM_TITLE from EDIRECTORY_TITLE', 'text/plain', 'Hello, I found this item in EDIRECTORY_TITLE and I think it would be great for you.\r\n\r\nYou can view it at ITEM_URL\r\n', 'EDIRECTORY_TITLE,ITEM_TITLE,ITEM_URL'),
(22, 'Listing Signup', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Signup Notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\nThank you for signing up for an account in EDIRECTORY_TITLE (DEFAULT_URL).\r\nLogin to manage your account with the information below.\r\n\r\nACCOUNT_LOGIN_INFORMATION\r\n\r\nYou can see:\r\nYour account in DEFAULT_URL/MEMBERS_URL/ "Account" link.\r\nAnd\r\nYour item in DEFAULT_URL/MEMBERS_URL/ "Dashboard" link.', 'ACCOUNT_NAME,ACCOUNT_LOGIN_INFORMATION,DEFAULT_URL,EDIRECTORY_TITLE,MEMBERS_URL'),
(23, 'Event Signup', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Signup Notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\nThank you for signing up for an account in EDIRECTORY_TITLE (DEFAULT_URL).\r\nLogin to manage your account with the information below.\r\n\r\nACCOUNT_LOGIN_INFORMATION\r\n\r\nYou can see:\r\nYour account in DEFAULT_URL/MEMBERS_URL/ "Account" link.\r\nAnd\r\nYour item in DEFAULT_URL/MEMBERS_URL/ "Dashboard" link.', 'ACCOUNT_NAME,ACCOUNT_LOGIN_INFORMATION,DEFAULT_URL,EDIRECTORY_TITLE,MEMBERS_URL'),
(24, 'Banner Signup', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Signup Notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\nThank you for signing up for an account in EDIRECTORY_TITLE (DEFAULT_URL).\r\nLogin to manage your account with the information below.\r\n\r\nACCOUNT_LOGIN_INFORMATION\r\n\r\nYou can see:\r\nYour account in DEFAULT_URL/MEMBERS_URL/ "Account" link.\r\nAnd\r\nYour item in DEFAULT_URL/MEMBERS_URL/ "Dashboard" link.', 'ACCOUNT_NAME,ACCOUNT_LOGIN_INFORMATION,DEFAULT_URL,EDIRECTORY_TITLE,MEMBERS_URL'),
(25, 'Classified Signup', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Signup Notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\nThank you for signing up for an account in EDIRECTORY_TITLE (DEFAULT_URL).\r\nLogin to manage your account with the information below.\r\n\r\nACCOUNT_LOGIN_INFORMATION\r\n\r\nYou can see:\r\nYour account in DEFAULT_URL/MEMBERS_URL/ "Account" link.\r\nAnd\r\nYour item in DEFAULT_URL/MEMBERS_URL/ "Dashboard" link.', 'ACCOUNT_NAME,ACCOUNT_LOGIN_INFORMATION,DEFAULT_URL,EDIRECTORY_TITLE,MEMBERS_URL'),
(26, 'Article Signup', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Signup Notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\nThank you for signing up for an account in EDIRECTORY_TITLE (DEFAULT_URL).\r\nLogin to manage your account with the information below.\r\n\r\nACCOUNT_LOGIN_INFORMATION\r\n\r\nYou can see:\r\nYour account in DEFAULT_URL/MEMBERS_URL/ "Account" link.\r\nAnd\r\nYour item in DEFAULT_URL/MEMBERS_URL/ "Dashboard" link.', 'ACCOUNT_NAME,ACCOUNT_LOGIN_INFORMATION,DEFAULT_URL,EDIRECTORY_TITLE,MEMBERS_URL'),
(27, 'Claimer Signup', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Claimer Signup Notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\nThank you for signing up for an account in EDIRECTORY_TITLE (DEFAULT_URL).\r\nLogin to manage your account with the information below.\r\n\r\nACCOUNT_LOGIN_INFORMATION\r\n\r\nYou can see your account in DEFAULT_URL/MEMBERS_URL/ "Account" link.', 'ACCOUNT_NAME,ACCOUNT_LOGIN_INFORMATION,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,MEMBERS_URL'),
(28, 'Claim Automatically Approved', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Claim Approved Notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\nYour claim to the item "LISTING_TITLE" was "automatically approved".\r\nYou can see it in DEFAULT_URL/MEMBERS_URL/ "Dashboard" link.', 'ACCOUNT_NAME,ACCOUNT_USERNAME,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,MEMBERS_URL'),
(29, 'Claim Approved', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Claim Approved Notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\nYour claim to the item "LISTING_TITLE" was "approved" by site manager.\r\nYou can see your item in DEFAULT_URL/MEMBERS_URL/ "Dashboard" link.', 'ACCOUNT_NAME,ACCOUNT_USERNAME,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,MEMBERS_URL'),
(30, 'Claim Denied', 0, '0', '0000-00-00 00:00:00', '', 'EDIRECTORY_TITLE Claim Denied Notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\nYour claim to the item "LISTING_TITLE" from DEFAULT_URL was "denied" by site manager.', 'ACCOUNT_NAME,ACCOUNT_USERNAME,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE'),
(31, 'Approve Reply', 0, '0', '0000-00-00 00:00:00', '', '[EDIRECTORY_TITLE] Reply Approved Notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nYour reply was approved by site manager.\r\nYou can see it in DEFAULT_URL/MEMBERS_URL/ \r\n  \r\nThank you for being a member of the EDIRECTORY_TITLE! \r\n  \r\nRegards \r\nEDIRECTORY_TITLE Team', 'ACCOUNT_NAME,ACCOUNT_USERNAME,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,MEMBERS_URL'),
(32, 'Approve Review', 0, '0', '0000-00-00 00:00:00', '', '[EDIRECTORY_TITLE] Review Approved Notification', 'text/plain', 'Dear ACCOUNT_NAME, \r\n  \r\nYour item has a new review approved by site manager.\r\nYou can see it in DEFAULT_URL/MEMBERS_URL/ \r\n  \r\nRegards \r\nEDIRECTORY_TITLE Team ', 'ACCOUNT_NAME,ACCOUNT_USERNAME,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,MEMBERS_URL'),
(33, 'New Review', 0, '0', '0000-00-00 00:00:00', '', '[EDIRECTORY_TITLE] New Review Notification', 'text/plain', 'Dear ACCOUNT_NAME, \r\n  \r\nYour item on EDIRECTORY_TITLE has a new review. \r\nYou can see it in DEFAULT_URL/MEMBERS_URL/\r\n  \r\nThank you for being a member of the EDIRECTORY_TITLE!\r\n\r\nRegards \r\n EDIRECTORY_TITLE Team', 'ACCOUNT_NAME,ACCOUNT_USERNAME,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,MEMBERS_URL'),
(34, 'Invoice Notification', 0, '0', '0000-00-00 00:00:00', NULL, '[EDIRECTORY_TITLE] Invoice Notification', 'text/plain', 'Dear ACCOUNT_NAME, \r\n\r\nYou can see your invoice in DEFAULT_URL \r\n\r\nRegards \r\nEDIRECTORY_TITLE Team', 'ACCOUNT_NAME,ACCOUNT_USERNAME,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE'),
(35, 'Visitor Account Create', 0, '0', '0000-00-00 00:00:00', '', 'Activate your EDIRECTORY_TITLE Account', 'text/plain', 'Dear ACCOUNT_NAME,\r\nThank you for creating your profile in EDIRECTORY_TITLE.\r\nClick on the link below to activate your account.\r\n\r\nLINK_ACTIVATE_ACCOUNT\r\n\r\nAfter that, you can login to manage your account with the information below.\r\n\r\nACCOUNT_LOGIN_INFORMATION\r\n\r\nRegards \r\nEDIRECTORY_TITLE Team', 'ACCOUNT_NAME,LINK_ACTIVATE_ACCOUNT,ACCOUNT_LOGIN_INFORMATION,DEFAULT_URL,EDIRECTORY_TITLE'),
(36, 'Sponsor Stats & Engagement E-mail', 0, '0', '0000-00-00 00:00:00', NULL, 'Activity for your listing on EDIRECTORY_TITLE', 'text/plain', 'Dear \r\n\r\nACCOUNT_NAME,\r\n\r\nHere is the activity for LISTING_TITLE for the last 30 days:\r\n\r\n[TABLE_STATS]\r\n\r\nUpdate your listing and get more visibility by signing in here:\r\n\r\nDEFAULT_URL/MEMBERS_URL/\r\n\r\nRegards,\r\nThe EDIRECTORY_TITLE Team\r\n\r\nTo stop receiving this email please login and change your email settings, or send an email to \r\n\r\nSITEMGR_EMAIL', 'ACCOUNT_NAME,LISTING_TITLE,TABLE_STATS,DEFAULT_URL,EDIRECTORY_TITLE,SITEMGR_EMAIL,MEMBERS_URL'),
(37, 'Deal Redeemed (Owner)', 0, '0', '0000-00-00 00:00:00', NULL, '[EDIRECTORY_TITLE] Deal Redeemed Notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nYour deal on EDIRECTORY_TITLE was redeemed. \r\nYou can see it in DEFAULT_URL/MEMBERS_URL/\r\n  \r\nThank you for being a member of the EDIRECTORY_TITLE!\r\n\r\nRegards\r\nEDIRECTORY_TITLE Team', 'ACCOUNT_NAME,ACCOUNT_USERNAME,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,MEMBERS_URL'),
(38, 'Deal Redeemed (Visitor)', 0, '0', '0000-00-00 00:00:00', NULL, '[EDIRECTORY_TITLE] Deal Redeemed', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nThank you for redeeming a deal in EDIRECTORY_TITLE (DEFAULT_URL).\r\nYou can see it by logging in your visitor area.\r\n\r\nYour code: REDEEM_CODE\r\n\r\nRegards \r\nEDIRECTORY_TITLE Team', 'ACCOUNT_NAME,DEFAULT_URL,EDIRECTORY_TITLE,REDEEM_CODE'),
(39, 'Account Activation', 0, '0', '0000-00-00 00:00:00', NULL, '[EDIRECTORY_TITLE] Account Activation', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nTo activate your account, please click on the link below.\r\n\r\nLINK_ACTIVATE_ACCOUNT\r\n\r\nFor further assistance please contact SITEMGR_EMAIL.\r\n\r\nRegards\r\n\r\nEDIRECTORY_TITLE Team', 'ACCOUNT_NAME,LINK_ACTIVATE_ACCOUNT,SITEMGR_EMAIL,EDIRECTORY_TITLE'),
(40, 'New Lead', 0, '0', '0000-00-00 00:00:00', '', '[EDIRECTORY_TITLE] New Lead Notification', 'text/plain', 'Dear ACCOUNT_NAME,\r\n\r\nYour item on EDIRECTORY_TITLE received the following lead.\r\n\r\nLEAD_MESSAGE\r\n\r\nYou can see it in DEFAULT_URL/MEMBERS_URL/\r\n\r\nThank you for being a member of the EDIRECTORY_TITLE!\r\n\r\nRegards\r\n\r\nEDIRECTORY_TITLE Team', 'ACCOUNT_NAME,ACCOUNT_USERNAME,DEFAULT_URL,SITEMGR_EMAIL,EDIRECTORY_TITLE,LEAD_MESSAGE,MEMBERS_URL');

-- --------------------------------------------------------

--
-- Table structure for table `Email_Notification_Default`
--

CREATE TABLE IF NOT EXISTS `Email_Notification_Default` (
  `id` int(11) NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body_text` text COLLATE utf8_unicode_ci NOT NULL,
  `body_html` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Email_Notification_Default`
--

INSERT INTO `Email_Notification_Default` (`id`, `subject`, `body_text`, `body_html`) VALUES
(1, 'Listing renewal notification', 'Dear ACCOUNT_NAME,\r\n\r\nYour listing "LISTING_TITLE" will expire in DAYS_INTERVAL days.\r\n\r\nYou can renew immediately by logging in to your account and submitting a credit card payment through the Payment area by the link below.\r\n\r\nDEFAULT_URL/MEMBERS_URL/billing/index.php\r\n\r\nFor further assistance please contact SITEMGR_EMAIL\r\n\r\nThank you for being a member of the Directory.\r\n\r\nRegards,\r\nThe EDIRECTORY_TITLE Team', '<html>\r\n<head>\r\n<style type="text/css">\r\n.default_text_settings {\r\nfont-family: Verdana, Arial, Helvetica, sans-serif;\r\nfont-size: 8pt;\r\ncolor: #003365;\r\nmargin: 0;\r\npadding: 5px;\r\nbackground: #FBFBFB;\r\nborder: 1px solid #E9E9E9;\r\ntext-align: justify;\r\n};\r\n</style>\r\n</head>\r\n<body>\r\n<div class="default_text_settings">\r\nDear ACCOUNT_NAME,<br /><br />\r\nYour listing "LISTING_TITLE" will expire in DAYS_INTERVAL days.<br /><br />\r\nYou can renew immediately by logging in to your account and submitting a credit card payment through the Payment area by the link below. <br /><br />\r\n<a href="DEFAULT_URL/MEMBERS_URL/billing/index.php">DEFAULT_URL/MEMBERS_URL/billing/index.php</a><br /><br />\r\nFor further assistance please contact <a href="mailto:SITEMGR_EMAIL">SITEMGR_EMAIL</a> <br /><br />\r\nThank you for being a member of the Directory. <br /><br />\r\nRegards,<br />\r\nThe EDIRECTORY_TITLE Team\r\n</div>\r\n</body>\r\n</html>'),
(2, 'Listing renewal notification', 'Dear ACCOUNT_NAME,\r\n\r\nYour listing "LISTING_TITLE" will expire in DAYS_INTERVAL days.\r\n\r\nYou can renew immediately by logging in to your account and submitting a credit card payment through the Payment area by the link below.\r\n\r\nDEFAULT_URL/MEMBERS_URL/billing/index.php\r\n\r\nFor further assistance please contact SITEMGR_EMAIL\r\n\r\nThank you for being a member of the Directory.\r\n\r\nRegards,\r\nThe EDIRECTORY_TITLE Team', '<html>\r\n<head>\r\n<style type="text/css">\r\n.default_text_settings {\r\nfont-family: Verdana, Arial, Helvetica, sans-serif;\r\nfont-size: 8pt;\r\ncolor: #003365;\r\nmargin: 0;\r\npadding: 5px;\r\nbackground: #FBFBFB;\r\nborder: 1px solid #E9E9E9;\r\ntext-align: justify;\r\n};\r\n</style>\r\n</head>\r\n<body>\r\n<div class="default_text_settings">\r\nDear ACCOUNT_NAME,<br /><br />\r\nYour listing "LISTING_TITLE" will expire in DAYS_INTERVAL days.<br /><br />\r\nYou can renew immediately by logging in to your account and submitting a credit card payment through the Payment area by the link below. <br /><br />\r\n<a href="DEFAULT_URL/MEMBERS_URL/billing/index.php">DEFAULT_URL/MEMBERS_URL/billing/index.php</a><br /><br />\r\nFor further assistance please contact <a href="mailto:SITEMGR_EMAIL">SITEMGR_EMAIL</a> <br /><br />\r\nThank you for being a member of the Directory. <br /><br />\r\nRegards,<br />\r\nThe EDIRECTORY_TITLE Team\r\n</div>\r\n</body>\r\n</html>'),
(3, 'Listing renewal notification', 'Dear ACCOUNT_NAME,\r\n\r\nYour listing "LISTING_TITLE" will expire in DAYS_INTERVAL days.\r\n\r\nYou can renew immediately by logging in to your account and submitting a credit card payment through the Payment area by the link below.\r\n\r\nDEFAULT_URL/MEMBERS_URL/billing/index.php\r\n\r\nFor further assistance please contact SITEMGR_EMAIL\r\n\r\nThank you for being a member of the Directory.\r\n\r\nRegards,\r\nThe EDIRECTORY_TITLE Team', '<html>\r\n<head>\r\n<style type="text/css">\r\n.default_text_settings {\r\nfont-family: Verdana, Arial, Helvetica, sans-serif;\r\nfont-size: 8pt;\r\ncolor: #003365;\r\nmargin: 0;\r\npadding: 5px;\r\nbackground: #FBFBFB;\r\nborder: 1px solid #E9E9E9;\r\ntext-align: justify;\r\n};\r\n</style>\r\n</head>\r\n<body>\r\n<div class="default_text_settings">\r\nDear ACCOUNT_NAME,<br /><br />\r\nYour listing "LISTING_TITLE" will expire in DAYS_INTERVAL days.<br /><br />\r\nYou can renew immediately by logging in to your account and submitting a credit card payment through the Payment area by the link below. <br /><br />\r\n<a href="DEFAULT_URL/MEMBERS_URL/billing/index.php">DEFAULT_URL/MEMBERS_URL/billing/index.php</a><br /><br />\r\nFor further assistance please contact <a href="mailto:SITEMGR_EMAIL">SITEMGR_EMAIL</a> <br /><br />\r\nThank you for being a member of the Directory. <br /><br />\r\nRegards,<br />\r\nThe EDIRECTORY_TITLE Team\r\n</div>\r\n</body>\r\n</html>'),
(4, 'Listing renewal notification', 'Dear ACCOUNT_NAME,\r\n\r\nYour listing "LISTING_TITLE" will expire in DAYS_INTERVAL days.\r\n\r\nYou can renew immediately by logging in to your account and submitting a credit card payment through the Payment area by the link below.\r\n\r\nDEFAULT_URL/MEMBERS_URL/billing/index.php\r\n\r\nFor further assistance please contact SITEMGR_EMAIL\r\n\r\nThank you for being a member of the Directory.\r\n\r\nRegards,\r\nThe EDIRECTORY_TITLE Team', '<html>\r\n<head>\r\n<style type="text/css">\r\n.default_text_settings {\r\nfont-family: Verdana, Arial, Helvetica, sans-serif;\r\nfont-size: 8pt;\r\ncolor: #003365;\r\nmargin: 0;\r\npadding: 5px;\r\nbackground: #FBFBFB;\r\nborder: 1px solid #E9E9E9;\r\ntext-align: justify;\r\n};\r\n</style>\r\n</head>\r\n<body>\r\n<div class="default_text_settings">\r\nDear ACCOUNT_NAME,<br /><br />\r\nYour listing "LISTING_TITLE" will expire in DAYS_INTERVAL days.<br /><br />\r\nYou can renew immediately by logging in to your account and submitting a credit card payment through the Payment area by the link below. <br /><br />\r\n<a href="DEFAULT_URL/MEMBERS_URL/billing/index.php">DEFAULT_URL/MEMBERS_URL/billing/index.php</a><br /><br />\r\nFor further assistance please contact <a href="mailto:SITEMGR_EMAIL">SITEMGR_EMAIL</a> <br /><br />\r\nThank you for being a member of the Directory. <br /><br />\r\nRegards,<br />\r\nThe EDIRECTORY_TITLE Team\r\n</div>\r\n</body>\r\n</html>'),
(5, 'EDIRECTORY_TITLE Account', 'Dear ACCOUNT_NAME,\n\nThank you for signing up for an account in DEFAULT_URL\nLogin to manage your account with the username and password below.\n\nDEFAULT_URL/MEMBERS_URL\n\nUsername: ACCOUNT_USERNAME\nPassword: ACCOUNT_PASSWORD\n\nDEFAULT_URL', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nDear ACCOUNT_NAME,<br /><br />\nThank you for signing up for an account in <a href="DEFAULT_URL">DEFAULT_URL</a><br />\nLogin to manage your account with the username and password below.<br /><br />\n<a href="DEFAULT_URL/MEMBERS_URL">DEFAULT_URL/MEMBERS_URL</a><br /><br />\nUsername: ACCOUNT_USERNAME<br />\nPassword: ACCOUNT_PASSWORD<br /><br />\n<a href="DEFAULT_URL">DEFAULT_URL</a></div>\n</body>\n</html>'),
(6, 'EDIRECTORY_TITLE Account', 'Dear ACCOUNT_NAME,\n\nYour account is updated in DEFAULT_URL\nLogin to manage your account with the username and password below.\n\nDEFAULT_URL/MEMBERS_URL\n\nUsername: ACCOUNT_USERNAME\nPassword: ACCOUNT_PASSWORD\n\nDEFAULT_URL', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nDear ACCOUNT_NAME,<br /><br />\nYour account is updated in <a href="DEFAULT_URL">DEFAULT_URL</a><br />\nLogin to manage your account with the username and password below.<br /><br />\n<a href="DEFAULT_URL/MEMBERS_URL">DEFAULT_URL/MEMBERS_URL</a><br /><br />\nUsername: ACCOUNT_USERNAME<br />\nPassword: ACCOUNT_PASSWORD<br /><br />\n<a href="DEFAULT_URL">DEFAULT_URL</a></div>\n</body>\n</html>'),
(7, 'EDIRECTORY_TITLE Account', 'Dear ACCOUNT_NAME,\n\nThank you for signing up for an account in DEFAULT_URL\n\nUsername: ACCOUNT_USERNAME\nPassword: ACCOUNT_PASSWORD\n\nDEFAULT_URL', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nDear ACCOUNT_NAME,<br /><br />\nThank you for signing up for an account in <a href="DEFAULT_URL">DEFAULT_URL</a><br />\nUsername: ACCOUNT_USERNAME<br />\nPassword: ACCOUNT_PASSWORD<br /><br />\n<a href="DEFAULT_URL">DEFAULT_URL</a></div>\n</body>\n</html>'),
(8, 'EDIRECTORY_TITLE Account', 'Dear ACCOUNT_NAME,\n\nYour account is updated in DEFAULT_URL\n\nDEFAULT_URL/MEMBERS_URL\n\nUsername: ACCOUNT_USERNAME\nPassword: ACCOUNT_PASSWORD\n\nDEFAULT_URL', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nDear ACCOUNT_NAME,<br /><br />\nYour account is updated in <a href="DEFAULT_URL">DEFAULT_URL</a><br />\nUsername: ACCOUNT_USERNAME<br />\nPassword: ACCOUNT_PASSWORD<br /><br />\n<a href="DEFAULT_URL">DEFAULT_URL</a></div>\n</body>\n</html>'),
(9, 'EDIRECTORY_TITLE account information', 'As requested, here is the information regarding your EDIRECTORY_TITLE account.\nTo change your forgotten password please click the link below and enter a new password.\nKEY_ACCOUNT\n\nFor further assistance please contact SITEMGR_EMAIL.\n\nThank you for being a member of the EDIRECTORY_TITLE.', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nAs requested, here is the information regarding your EDIRECTORY_TITLE account.<br />\nTo change your forgotten password please click the link below and enter a new password.<br />\nKEY_ACCOUNT<br /><br />\nFor further assistance please contact SITEMGR_EMAIL.<br /><br />\nThank you for being a member of the EDIRECTORY_TITLE.</div>\n</body>\n</html>'),
(10, 'EDIRECTORY_TITLE Notification', 'Dear ACCOUNT_NAME,\r\n\r\nYou have just added one listing in our directory.\r\n\r\nWhat do I do next?\r\n\r\nPlease login to the directory at the link below and click on Make your Payment link on the left menu options to pay for your listing.\r\nDEFAULT_URL/MEMBERS_URL\r\n\r\nOnce this has been completed a directory administrator will review your listing and set it live within the directory.\r\n\r\nSubmissions are reviewed within 2 working days following payment.\r\n\r\nThank you for being a member of the EDIRECTORY_TITLE!\r\n\r\nRegards\r\nEDIRECTORY_TITLE Advertising Team\r\n', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nDear ACCOUNT_NAME,<br /><br />\nYou have just added one listing in our directory.<br /><br />\nWhat do I do next? <br /><br />\nPlease login to the directory at the link below and click on Make your Payment link on the left menu options to pay for your listing. <br />\n<a href="DEFAULT_URL/MEMBERS_URL" class="email_style_settings">DEFAULT_URL/MEMBERS_URL</a> <br /><br />\nOnce this has been completed a directory administrator will review your listing and set it live within the directory.<br /><br />\nSubmissions are reviewed within 2 working days following payment.<br /><br />\nThank you for being a member of the EDIRECTORY_TITLE!<br /><br />\nRegards<br />\nEDIRECTORY_TITLE Advertising Team</div>\n</body>\n</html>'),
(11, 'EDIRECTORY_TITLE Notification', 'Dear ACCOUNT_NAME,\r\n\r\nThank you for adding your event to our directory calendar.\r\n\r\nAn administrator will review your event and make it live within the directory within 2 working days.\r\n\r\nYou can see your event in DEFAULT_URL/MEMBERS_URL/"Dashboard" link.\r\n\r\nThank you for being a member of the EDIRECTORY_TITLE!\r\n\r\nRegards\r\nEDIRECTORY_TITLE Advertising Team', '<html>\r\n<head>\r\n<style type="text/css">\r\n.default_text_settings {\r\nfont-family: Verdana, Arial, Helvetica, sans-serif;\r\nfont-size: 8pt;\r\ncolor: #003365;\r\nmargin: 0;\r\npadding: 5px;\r\nbackground: #FBFBFB;\r\nborder: 1px solid #E9E9E9;\r\ntext-align: justify;\r\n};\r\n</style>\r\n</head>\r\n<body>\r\n<div class="default_text_settings">\r\nDear ACCOUNT_NAME,<br /><br />\r\nThank you for adding your event to our directory calendar.<br /><br />\r\nAn administrator will review your event and make it live within the directory within 2 working days.<br /><br />\r\nYou can see your event in DEFAULT_URL/MEMBERS_URL/"Dashboard" link.<br /><br />\r\nThank you for being a member of the EDIRECTORY_TITLE!<br /><br />\r\nRegards<br />\r\nEDIRECTORY_TITLE Advertising Team</div>\r\n</body>\r\n</html>'),
(12, 'EDIRECTORY_TITLE Notification', 'Dear ACCOUNT_NAME,\n\nYou have just added one banner in our directory.\n\nWhat do I do next?\n\nPlease login to the directory at the link below and click on Make your Payment link on the left menu options to pay for your banner.\nDEFAULT_URL/MEMBERS_URL\n\nOnce this has been completed a directory administrator will review your banner and set it live within the directory.\n\nSubmissions are reviewed within 2 working days following payment.\n\nThank you for being a member of the EDIRECTORY_TITLE!\n\nRegards\nEDIRECTORY_TITLE Advertising Team\n', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nDear ACCOUNT_NAME,<br /><br />\nYou have just added one banner in our directory.<br /><br />\nWhat do I do next? <br /><br />\nPlease login to the directory at the link below and click on Make your Payment link on the left menu options to pay for your banner. <br />\n<a href="DEFAULT_URL/MEMBERS_URL" class="email_style_settings">DEFAULT_URL/MEMBERS_URL</a> <br /><br />\nOnce this has been completed a directory administrator will review your banner and set it live within the directory.<br /><br />\nSubmissions are reviewed within 2 working days following payment.<br /><br />\nThank you for being a member of the EDIRECTORY_TITLE!<br /><br />\nRegards<br />\nEDIRECTORY_TITLE Advertising Team</div>\n</body>\n</html>'),
(13, 'EDIRECTORY_TITLE Notification', 'Dear ACCOUNT_NAME,\n\nYou have just added one classified in our directory.\n\nWhat do I do next?\n\nPlease login to the directory at the link below and click on Make your Payment link on the left menu options to pay for your classified.\nDEFAULT_URL/MEMBERS_URL\n\nOnce this has been completed a directory administrator will review your classified and set it live within the directory.\n\nSubmissions are reviewed within 2 working days following payment.\n\nThank you for being a member of the EDIRECTORY_TITLE!\n\nRegards\nEDIRECTORY_TITLE Advertising Team\n', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nDear ACCOUNT_NAME,<br /><br />\nYou have just added one classified in our directory.<br /><br />\nWhat do I do next? <br /><br />\nPlease login to the directory at the link below and click on Make your Payment link on the left menu options to pay for your classified. <br />\n<a href="DEFAULT_URL/MEMBERS_URL" class="email_style_settings">DEFAULT_URL/MEMBERS_URL</a> <br /><br />\nOnce this has been completed a directory administrator will review your classified and set it live within the directory.<br /><br />\nSubmissions are reviewed within 2 working days following payment.<br /><br />\nThank you for being a member of the EDIRECTORY_TITLE!<br /><br />\nRegards<br />\nEDIRECTORY_TITLE Advertising Team</div>\n</body>\n</html>'),
(14, 'EDIRECTORY_TITLE Notification', 'Dear ACCOUNT_NAME,\n\nYou have just added one article in our directory.\n\nWhat do I do next?\n\nPlease login to the directory at the link below and click on Make your Payment link on the left menu options to pay for your article.\nDEFAULT_URL/MEMBERS_URL\n\nOnce this has been completed a directory administrator will review your article and set it live within the directory.\n\nSubmissions are reviewed within 2 working days following payment.\n\nThank you for being a member of the EDIRECTORY_TITLE!\n\nRegards\nEDIRECTORY_TITLE Advertising Team\n', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nDear ACCOUNT_NAME,<br /><br />\nYou have just added one article in our directory.<br /><br />\nWhat do I do next? <br /><br />\nPlease login to the directory at the link below and click on Make your Payment link on the left menu options to pay for your article. <br />\n<a href="DEFAULT_URL/MEMBERS_URL" class="email_style_settings">DEFAULT_URL/MEMBERS_URL</a> <br /><br />\nOnce this has been completed a directory administrator will review your article and set it live within the directory.<br /><br />\nSubmissions are reviewed within 2 working days following payment.<br /><br />\nThank you for being a member of the EDIRECTORY_TITLE!<br /><br />\nRegards<br />\nEDIRECTORY_TITLE Advertising Team</div>\n</body>\n</html>'),
(15, '[EDIRECTORY_TITLE] Invoice', 'Dear ACCOUNT_NAME,\r\n\r\nYour invoice is ready for payment at the following link:\r\n\r\nDEFAULT_URL/MEMBERS_URL/billing/index.php\r\n\r\nAmount Due: CUSTOM_INVOICE_AMOUNT CUSTOM_INVOICE_TAX\r\n\r\nEDIRECTORY_TITLE\r\n', '<html>\r\n<head>\r\n<style type="text/css">\r\n.default_text_settings {\r\nfont-family: Verdana, Arial, Helvetica, sans-serif;\r\nfont-size: 8pt;\r\ncolor: #003365;\r\nmargin: 0;\r\npadding: 5px;\r\nbackground: #FBFBFB;\r\nborder: 1px solid #E9E9E9;\r\ntext-align: justify;\r\n};\r\n</style>\r\n</head>\r\n<body>\r\n<div class="default_text_settings">\r\nDear ACCOUNT_NAME,<br /><br />\r\nYour invoice is ready for payment at the following link:<br /><br />\r\nDEFAULT_URL/MEMBERS_URL/billing/index.php<br /><br />\r\nAmount Due: CUSTOM_INVOICE_AMOUNT CUSTOM_INVOICE_TAX<br /><br />\r\nEDIRECTORY_TITLE</div>\r\n</body>\r\n</html>'),
(16, 'EDIRECTORY_TITLE Activation Notification', 'Dear ACCOUNT_NAME,\n\nYour listing is live and active on EDIRECTORY_TITLE\n\nLISTING_DEFAULT_URL/\n\nThank you for being a member of the EDIRECTORY_TITLE!\n\nRegards\nEDIRECTORY_TITLE Team\n', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nDear ACCOUNT_NAME,<br /><br />\nYour listing is live and active on EDIRECTORY_TITLE<br /><br />\nLISTING_DEFAULT_URL/<br /><br />\nThank you for being a member of the EDIRECTORY_TITLE!<br /><br />\nRegards<br />\nEDIRECTORY_TITLE Team</div>\n</body>\n</html>'),
(17, 'EDIRECTORY_TITLE Activation Notification', 'Dear ACCOUNT_NAME,\n\nYour event is live and active on EDIRECTORY_TITLE\n\nEVENT_DEFAULT_URL/\n\nThank you for being a member of the EDIRECTORY_TITLE!\n\nRegards\nEDIRECTORY_TITLE Team\n', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nDear ACCOUNT_NAME,<br /><br />\nYour event is live and active on EDIRECTORY_TITLE<br /><br />\nEVENT_DEFAULT_URL/<br /><br />\nThank you for being a member of the EDIRECTORY_TITLE!<br /><br />\nRegards<br />\nEDIRECTORY_TITLE Team</div>\n</body>\n</html>'),
(18, 'EDIRECTORY_TITLE Activation Notification', 'Dear ACCOUNT_NAME,\n\nYour banner is live and active on EDIRECTORY_TITLE\n\nDEFAULT_URL/\n\nThank you for being a member of the EDIRECTORY_TITLE!\n\nRegards\nEDIRECTORY_TITLE Team\n', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nDear ACCOUNT_NAME,<br /><br />\nYour banner is live and active on EDIRECTORY_TITLE<br /><br />\nDEFAULT_URL/<br /><br />\nThank you for being a member of the EDIRECTORY_TITLE!<br /><br />\nRegards<br />\nEDIRECTORY_TITLE Team</div>\n</body>\n</html>'),
(19, 'EDIRECTORY_TITLE Activation Notification', 'Dear ACCOUNT_NAME,\n\nYour classified is live and active on EDIRECTORY_TITLE\n\nCLASSIFIED_DEFAULT_URL/\n\nThank you for being a member of the EDIRECTORY_TITLE!\n\nRegards\nEDIRECTORY_TITLE Team\n', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nDear ACCOUNT_NAME,<br /><br />\nYour classified is live and active on EDIRECTORY_TITLE<br /><br />\nCLASSIFIED_DEFAULT_URL/<br /><br />\nThank you for being a member of the EDIRECTORY_TITLE!<br /><br />\nRegards<br />\nEDIRECTORY_TITLE Team</div>\n</body>\n</html>'),
(20, 'EDIRECTORY_TITLE Activation Notification', 'Dear ACCOUNT_NAME,\n\nYour article is live and active on EDIRECTORY_TITLE\n\nARTICLE_DEFAULT_URL/\n\nThank you for being a member of the EDIRECTORY_TITLE!\n\nRegards\nEDIRECTORY_TITLE Team\n', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nDear ACCOUNT_NAME,<br /><br />\nYour article is live and active on EDIRECTORY_TITLE<br /><br />\nARTICLE_DEFAULT_URL/<br /><br />\nThank you for being a member of the EDIRECTORY_TITLE!<br /><br />\nRegards<br />\nEDIRECTORY_TITLE Team</div>\n</body>\n</html>'),
(21, 'Here''s information on ITEM_TITLE from EDIRECTORY_TITLE', 'Hello, I found this item in EDIRECTORY_TITLE and I think it would be great for you.\n\nYou can view it at ITEM_URL\n', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nHello, I found this item in EDIRECTORY_TITLE and I think it would be great for you.<br /><br />\nYou can view it at ITEM_URL</div>\n</body>\n</html>'),
(22, 'EDIRECTORY_TITLE Signup Notification', 'Dear ACCOUNT_NAME,\r\nThank you for signing up for an account in EDIRECTORY_TITLE (DEFAULT_URL).\r\nLogin to manage your account with the information below.\r\n\r\nACCOUNT_LOGIN_INFORMATION\r\n\r\nYou can see:\r\nYour account in DEFAULT_URL/MEMBERS_URL/ "Account" link.\r\nAnd\r\nYour item in DEFAULT_URL/MEMBERS_URL/ "Dashboard" link.', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nDear ACCOUNT_NAME,<br />\nThank you for signing up for an account in EDIRECTORY_TITLE (DEFAULT_URL).<br />\nLogin to manage your account with the information below.<br /><br />\nACCOUNT_LOGIN_INFORMATION<br /><br />\nYou can see:<br />\nYour account in DEFAULT_URL/MEMBERS_URL/ "Account" link.<br />\nAnd<br />\nYour item in DEFAULT_URL/MEMBERS_URL/ "Dashboard" link.\n</div>\n</body>\n</html>'),
(23, 'EDIRECTORY_TITLE Signup Notification', 'Dear ACCOUNT_NAME,\r\nThank you for signing up for an account in EDIRECTORY_TITLE (DEFAULT_URL).\r\nLogin to manage your account with the information below.\r\n\r\nACCOUNT_LOGIN_INFORMATION\r\n\r\nYou can see:\r\nYour account in DEFAULT_URL/MEMBERS_URL/ "Account" link.\r\nAnd\r\nYour item in DEFAULT_URL/MEMBERS_URL/ "Dashboard" link.', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nDear ACCOUNT_NAME,<br />\nThank you for signing up for an account in EDIRECTORY_TITLE (DEFAULT_URL).<br />\nLogin to manage your account with the information below.<br /><br />\nACCOUNT_LOGIN_INFORMATION<br /><br />\nYou can see:<br />\nYour account in DEFAULT_URL/MEMBERS_URL/ "Account" link.<br />\nAnd<br />\nYour item in DEFAULT_URL/MEMBERS_URL/ "Dashboard" link.\n</div>\n</body>\n</html>'),
(24, 'EDIRECTORY_TITLE Signup Notification', 'Dear ACCOUNT_NAME,\r\nThank you for signing up for an account in EDIRECTORY_TITLE (DEFAULT_URL).\r\nLogin to manage your account with the information below.\r\n\r\nACCOUNT_LOGIN_INFORMATION\r\n\r\nYou can see:\r\nYour account in DEFAULT_URL/MEMBERS_URL/ "Account" link.\r\nAnd\r\nYour item in DEFAULT_URL/MEMBERS_URL/ "Dashboard" link.', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nDear ACCOUNT_NAME,<br />\nThank you for signing up for an account in EDIRECTORY_TITLE (DEFAULT_URL).<br />\nLogin to manage your account with the information below.<br /><br />\nACCOUNT_LOGIN_INFORMATION<br /><br />\nYou can see:<br />\nYour account in DEFAULT_URL/MEMBERS_URL/ "Account" link.<br />\nAnd<br />\nYour item in DEFAULT_URL/MEMBERS_URL/ "Dashboard" link.\n</div>\n</body>\n</html>'),
(25, 'EDIRECTORY_TITLE Signup Notification', 'Dear ACCOUNT_NAME,\r\nThank you for signing up for an account in EDIRECTORY_TITLE (DEFAULT_URL).\r\nLogin to manage your account with the information below.\r\n\r\nACCOUNT_LOGIN_INFORMATION\r\n\r\nYou can see:\r\nYour account in DEFAULT_URL/MEMBERS_URL/ "Account" link.\r\nAnd\r\nYour item in DEFAULT_URL/MEMBERS_URL/ "Dashboard" link.', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nDear ACCOUNT_NAME,<br />\nThank you for signing up for an account in EDIRECTORY_TITLE (DEFAULT_URL).<br />\nLogin to manage your account with the information below.<br /><br />\nACCOUNT_LOGIN_INFORMATION<br /><br />\nYou can see:<br />\nYour account in DEFAULT_URL/MEMBERS_URL/ "Account" link.<br />\nAnd<br />\nYour item in DEFAULT_URL/MEMBERS_URL/ "Dashboard" link.\n</div>\n</body>\n</html>'),
(26, 'EDIRECTORY_TITLE Signup Notification', 'Dear ACCOUNT_NAME,\r\nThank you for signing up for an account in EDIRECTORY_TITLE (DEFAULT_URL).\r\nLogin to manage your account with the information below.\r\n\r\nACCOUNT_LOGIN_INFORMATION\r\n\r\nYou can see:\r\nYour account in DEFAULT_URL/MEMBERS_URL/ "Account" link.\r\nAnd\r\nYour item in DEFAULT_URL/MEMBERS_URL/ "Dashboard" link.', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nDear ACCOUNT_NAME,<br />\nThank you for signing up for an account in EDIRECTORY_TITLE (DEFAULT_URL).<br />\nLogin to manage your account with the information below.<br /><br />\nACCOUNT_LOGIN_INFORMATION<br /><br />\nYou can see:<br />\nYour account in DEFAULT_URL/MEMBERS_URL/ "Account" link.<br />\nAnd<br />\nYour item in DEFAULT_URL/MEMBERS_URL/ "Dashboard" link.\n</div>\n</body>\n</html>'),
(27, 'EDIRECTORY_TITLE Claimer Signup Notification', 'Dear ACCOUNT_NAME,\r\nThank you for signing up for an account in EDIRECTORY_TITLE (DEFAULT_URL).\r\nLogin to manage your account with the information below.\r\n\r\nACCOUNT_LOGIN_INFORMATION\r\n\r\nYou can see your account in DEFAULT_URL/MEMBERS_URL/ "Account" link.', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nDear ACCOUNT_NAME,<br />\nThank you for signing up for an account in EDIRECTORY_TITLE (DEFAULT_URL).<br />\nLogin to manage your account with the information below.<br /><br />\nACCOUNT_LOGIN_INFORMATION<br /><br />\nYou can see your account in DEFAULT_URL/MEMBERS_URL/ "Account" link.\n</div>\n</body>\n</html>'),
(28, 'EDIRECTORY_TITLE Claim Approved Notification', 'Dear ACCOUNT_NAME,\r\nYour claim to the item "LISTING_TITLE" was "automatically approved". \r\nYou can see it in DEFAULT_URL/MEMBERS_URL/ "Dashboard" link.', '<html>\r\n<head>\r\n<style type="text/css">\r\n.default_text_settings {\r\nfont-family: Verdana, Arial, Helvetica, sans-serif;\r\nfont-size: 8pt;\r\ncolor: #003365;\r\nmargin: 0;\r\npadding: 5px;\r\nbackground: #FBFBFB;\r\nborder: 1px solid #E9E9E9;\r\ntext-align: justify;\r\n};\r\n</style>\r\n</head>\r\n<body>\r\n<div class="default_text_settings">\r\nDear ACCOUNT_NAME,<br />\r\nYour claim to the item "LISTING_TITLE" was "automatically approved".<br />\r\nYou can see it in DEFAULT_URL/MEMBERS_URL/ "Dashboard" link.\r\n</div>\r\n</body>\r\n</html>'),
(29, 'EDIRECTORY_TITLE Claim Approved Notification', 'Dear ACCOUNT_NAME,\r\nYour claim to the item "LISTING_TITLE" was "approved" by site manager.\r\nYou can see your item in DEFAULT_URL/MEMBERS_URL/ "Dashboard" link.', '<html>\r\n<head>\r\n<style type="text/css">\r\n.default_text_settings {\r\nfont-family: Verdana, Arial, Helvetica, sans-serif;\r\nfont-size: 8pt;\r\ncolor: #003365;\r\nmargin: 0;\r\npadding: 5px;\r\nbackground: #FBFBFB;\r\nborder: 1px solid #E9E9E9;\r\ntext-align: justify;\r\n};\r\n</style>\r\n</head>\r\n<body>\r\n<div class="default_text_settings">\r\nDear ACCOUNT_NAME,<br />\r\nYour claim to the item "LISTING_TITLE" was "approved" by site manager.<br />\r\nYou can see your item in DEFAULT_URL/MEMBERS_URL/ "Dashboard" link.\r\n</div>\r\n</body>\r\n</html>'),
(30, 'EDIRECTORY_TITLE Claim Denied Notification', 'Dear ACCOUNT_NAME,\r\nYour claim to the item "LISTING_TITLE" from DEFAULT_URL was "denied" by site manager.', '<html>\r\n<head>\r\n<style type="text/css">\r\n.default_text_settings {\r\nfont-family: Verdana, Arial, Helvetica, sans-serif;\r\nfont-size: 8pt;\r\ncolor: #003365;\r\nmargin: 0;\r\npadding: 5px;\r\nbackground: #FBFBFB;\r\nborder: 1px solid #E9E9E9;\r\ntext-align: justify;\r\n};\r\n</style>\r\n</head>\r\n<body>\r\n<div class="default_text_settings">\r\nDear ACCOUNT_NAME,<br />\r\nYour claim to the item "LISTING_TITLE" from DEFAULT_URL was "denied" by site manager.\r\n</div>\r\n</body>\r\n</html>'),
(31, '[EDIRECTORY_TITLE] Reply Approved Notification', 'Your reply was approved by site manager.\r\nYou can see it in DEFAULT_URL/MEMBERS_URL/ \r\n  \r\nThank you for being a member of the EDIRECTORY_TITLE! \r\n  \r\nRegards \r\nEDIRECTORY_TITLE Team\r\n', '<html>\r\n<head>\r\n<style type="text/css">\r\n.default_text_settings {\r\nfont-family: Verdana, Arial, Helvetica, sans-serif;\r\nfont-size: 8pt;\r\ncolor: #003365;\r\nmargin: 0;\r\npadding: 5px;\r\nbackground: #FBFBFB;\r\nborder: 1px solid #E9E9E9;\r\ntext-align: justify;\r\n};\r\n</style>\r\n</head>\r\n<body>\r\n<div class="default_text_settings">\r\nYour reply was approved by site manager.<br />\r\nYou can see it in DEFAULT_URL/MEMBERS_URL/<br /><br />\r\nThank you for being a member of the EDIRECTORY_TITLE!<br /><br />\r\nRegards<br />\r\nEDIRECTORY_TITLE Team</div>\r\n</body>\r\n</html>'),
(32, '[EDIRECTORY_TITLE] Review Approved Notification', 'Dear ACCOUNT_NAME,\r\n\r\nYour item has a new review approved by site manager.\r\nYou can see it in DEFAULT_URL/MEMBERS_URL/\r\n\r\nRegards\r\nEDIRECTORY_TITLE Team\r\n', '<html>\r\n<head>\r\n<style type="text/css">\r\n.default_text_settings {\r\nfont-family: Verdana, Arial, Helvetica, sans-serif;\r\nfont-size: 8pt;\r\ncolor: #003365;\r\nmargin: 0;\r\npadding: 5px;\r\nbackground: #FBFBFB;\r\nborder: 1px solid #E9E9E9;\r\ntext-align: justify;\r\n};\r\n</style>\r\n</head>\r\n<body>\r\n<div class="default_text_settings">\r\nDear ACCOUNT_NAME,<br /><br />\r\nYour item has a new review approved by site manager.<br />\r\nYou can see it in DEFAULT_URL/MEMBERS_URL/<br /><br />\r\nRegards<br />\r\nEDIRECTORY_TITLE Team</div>\r\n</body>\r\n</html>'),
(33, '[EDIRECTORY_TITLE] New Review Notification', 'Dear ACCOUNT_NAME,\r\n\r\nYour item on EDIRECTORY_TITLE has a new review. \r\nYou can see it in DEFAULT_URL/MEMBERS_URL/\r\n  \r\nThank you for being a member of the EDIRECTORY_TITLE!\r\n\r\nRegards\r\nEDIRECTORY_TITLE Team\r\n', '<html>\r\n<head>\r\n<style type="text/css">\r\n.default_text_settings {\r\nfont-family: Verdana, Arial, Helvetica, sans-serif;\r\nfont-size: 8pt;\r\ncolor: #003365;\r\nmargin: 0;\r\npadding: 5px;\r\nbackground: #FBFBFB;\r\nborder: 1px solid #E9E9E9;\r\ntext-align: justify;\r\n};\r\n</style>\r\n</head>\r\n<body>\r\n<div class="default_text_settings">\r\nDear ACCOUNT_NAME,<br /><br />\r\nYour item on EDIRECTORY_TITLE has a new review.<br />\r\nYou can see it in DEFAULT_URL/MEMBERS_URL/ <br /><br />\r\nThank you for being a member of the EDIRECTORY_TITLE!<br /><br />\r\nRegards<br />\r\nEDIRECTORY_TITLE Team</div>\r\n</body>\r\n</html>'),
(34, '[EDIRECTORY_TITLE] Invoice Notification', 'Dear ACCOUNT_NAME, \r\n\r\nYou can see your invoice in DEFAULT_URL\r\n\r\nRegards\r\nEDIRECTORY_TITLE Team', '<html>\r\n<head>\r\n<style type="text/css"> .default_text_settings {\r\nfont-family: Verdana, Arial, Helvetica, sans-serif; \r\nfont-size: 8pt; \r\ncolor: #003365; \r\nmargin: 0; \r\npadding: 5px; \r\nbackground: #FBFBFB; \r\nborder: 1px solid #E9E9E9; \r\ntext-align: justify; \r\n};\r\n</style>\r\n</head>\r\n<body>\r\n<div class="default_text_settings"> \r\nDear ACCOUNT_NAME, <br />\r\nYou can see your invoice in DEFAULT_URL <br /><br /> Regards<br /> EDIRECTORY_TITLE Team\r\n</div>\r\n</body>\r\n</html>'),
(35, 'EDIRECTORY_TITLE Profile', 'Dear ACCOUNT_NAME,\r\nThank you for creating your profile in EDIRECTORY_TITLE.\r\nClick on the link below to activate your account.\r\n\r\nLINK_ACTIVATE_ACCOUNT\r\n\r\nAfter that, you can login to manage your account with the information below.\r\n\r\nACCOUNT_LOGIN_INFORMATION\r\n\r\nRegards \r\nEDIRECTORY_TITLE Team', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nDear ACCOUNT_NAME,<br />\nThank you for creating your profile in EDIRECTORY_TITLE.<br />\nClick on the link below to activate your account.<br />\nLINK_ACTIVATE_ACCOUNT<br />\nAfter that, you can login to manage your account with the e-mail and password below.<br /><br />\nACCOUNT_LOGIN_INFORMATION<br /><br />\r\nRegards<br />\r\nEDIRECTORY_TITLE Team</div>\r\n</body>\n</html>'),
(36, 'Activity for your listing on EDIRECTORY_TITLE', 'Dear ACCOUNT_NAME, \r\n\r\nHere is the activity for LISTING_TITLE for the last 30 days:\r\n\r\n[TABLE_STATS]\r\n\r\nUpdate your listing and get more visibility by signing in here:\r\n\r\nDEFAULT_URL/MEMBERS_URL/\r\n\r\nRegards,\r\n\r\nThe EDIRECTORY_TITLE Team\r\n\r\nTo stop receiving this email please login and change your email settings, or send an email to SITEMGR_EMAIL', '<html>\n<head>\n<style type="text/css">\n.default_text_settings {\nfont-family: Verdana, Arial, Helvetica, sans-serif;\nfont-size: 8pt;\ncolor: #003365;\nmargin: 0;\npadding: 5px;\nbackground: #FBFBFB;\nborder: 1px solid #E9E9E9;\ntext-align: justify;\n};\n</style>\n</head>\n<body>\n<div class="default_text_settings">\nDear ACCOUNT_NAME,<br /><br />\nHere is the activity for LISTING_TITLE for the last 30 days:<br /><br />\n[TABLE_STATS]<br /><br />\nUpdate your listing and get more visibility by signing in here:<br /><br />\nDEFAULT_URL/MEMBERS_URL/<br /><br />\nRegards<br />\nEDIRECTORY_TITLE Team<br /><br />To stop receiving this email please login and change your email settings, or send an email to SITEMGR_EMAIL</div>\n</body>\n</html>'),
(37, '[EDIRECTORY_TITLE] Deal Redeemed Notification', 'Dear ACCOUNT_NAME,\r\n\r\nYour deal on EDIRECTORY_TITLE was redeemed. \r\nYou can see it in DEFAULT_URL/MEMBERS_URL/\r\n  \r\nThank you for being a member of the EDIRECTORY_TITLE!\r\n\r\nRegards\r\nEDIRECTORY_TITLE Team', '<html>\r\n<head>\r\n<style type="text/css">\r\n.default_text_settings {\r\nfont-family: Verdana, Arial, Helvetica, sans-serif;\r\nfont-size: 8pt;\r\ncolor: #003365;\r\nmargin: 0;\r\npadding: 5px;\r\nbackground: #FBFBFB;\r\nborder: 1px solid #E9E9E9;\r\ntext-align: justify;\r\n};\r\n</style>\r\n</head>\r\n<body>\r\n<div class="default_text_settings">\r\nDear ACCOUNT_NAME,<br /><br />\r\nYour deal on EDIRECTORY_TITLE was redeemed.<br />\r\nYou can see it in DEFAULT_URL/MEMBERS_URL/ <br /><br />\r\nThank you for being a member of the EDIRECTORY_TITLE!<br /><br />\r\nRegards<br />\r\nEDIRECTORY_TITLE Team</div>\r\n</body>\r\n</html>'),
(38, '[EDIRECTORY_TITLE] Deal Redeemed', 'Dear ACCOUNT_NAME,\r\n\r\nThank you for redeeming a deal in EDIRECTORY_TITLE (DEFAULT_URL).\r\nYou can see it by logging in your visitor area.\r\n\r\nYour code: REDEEM_CODE\r\n\r\nRegards \r\nEDIRECTORY_TITLE Team', '<html>\r\n<head>\r\n<style type="text/css">\r\n.default_text_settings {\r\nfont-family: Verdana, Arial, Helvetica, sans-serif;\r\nfont-size: 8pt;\r\ncolor: #003365;\r\nmargin: 0;\r\npadding: 5px;\r\nbackground: #FBFBFB;\r\nborder: 1px solid #E9E9E9;\r\ntext-align: justify;\r\n};\r\n</style>\r\n</head>\r\n<body>\r\n<div class="default_text_settings">\r\nDear ACCOUNT_NAME,<br />\r\nThank you for redeeming a deal in EDIRECTORY_TITLE (DEFAULT_URL).<br />\r\nYou can see it by logging in your visitor area.<br /><br />\r\nYour code: REDEEM_CODE<br /><br />\r\nRegards<br />\r\nEDIRECTORY_TITLE Team</div>\r\n</body>\r\n</html>'),
(39, '[EDIRECTORY_TITLE] Account Activation', 'Dear ACCOUNT_NAME,\r\n\r\nTo activate your account, please click on the link below.\r\n\r\nLINK_ACTIVATE_ACCOUNT\r\n\r\nFor further assistance please contact SITEMGR_EMAIL.\r\n\r\nRegards\r\n\r\nEDIRECTORY_TITLE Team', '<html>\r\n<head>\r\n<style type="text/css">\r\n.default_text_settings {\r\nfont-family: Verdana, Arial, Helvetica, sans-serif;\r\nfont-size: 8pt;\r\ncolor: #003365;\r\nmargin: 0;\r\npadding: 5px;\r\nbackground: #FBFBFB;\r\nborder: 1px solid #E9E9E9;\r\ntext-align: justify;\r\n};\r\n</style>\r\n</head>\r\n<body>\r\n<div class="default_text_settings">\r\nDear ACCOUNT_NAME,<br /><br />\r\nTo activate your account, please click on the link below.<br />\r\nLINK_ACTIVATE_ACCOUNT<br />\r\nFor further assistance please contact SITEMGR_EMAIL.<br /><br />\r\nRegards<br />\r\nEDIRECTORY_TITLE Team</div>\r\n</body>\r\n</html>'),
(40, '[EDIRECTORY_TITLE] New Lead Notification', 'Dear ACCOUNT_NAME,\r\n\r\nYour item on EDIRECTORY_TITLE received the following lead.\r\n\r\nLEAD_MESSAGE\r\n\r\nYou can see it in DEFAULT_URL/MEMBERS_URL/\r\n\r\nThank you for being a member of the EDIRECTORY_TITLE!\r\n\r\nRegards\r\n\r\nEDIRECTORY_TITLE Team', '<html>\r\n<head>\r\n<style type="text/css">\r\n.default_text_settings {\r\nfont-family: Verdana, Arial, Helvetica, sans-serif;\r\nfont-size: 8pt;\r\ncolor: #003365;\r\nmargin: 0;\r\npadding: 5px;\r\nbackground: #FBFBFB;\r\nborder: 1px solid #E9E9E9;\r\ntext-align: justify;\r\n};\r\n</style>\r\n</head>\r\n<body>\r\n<div class="default_text_settings">\r\nDear ACCOUNT_NAME,<br /><br />\r\nYour item on EDIRECTORY_TITLE received the following lead.<br /><br />\r\nLEAD_MESSAGE<br /><br />\r\nYou can see it in DEFAULT_URL/MEMBERS_URL/ <br /><br />\r\nThank you for being a member of the EDIRECTORY_TITLE!<br /><br />\r\nRegards<br />\r\nEDIRECTORY_TITLE Team</div>\r\n</body>\r\n</html>');

-- --------------------------------------------------------

--
-- Table structure for table `Event`
--

CREATE TABLE IF NOT EXISTS `Event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `importID` int(11) NOT NULL,
  `account_id` int(11) NOT NULL DEFAULT '0',
  `discount_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_id` int(11) NOT NULL DEFAULT '0',
  `thumb_id` int(11) NOT NULL DEFAULT '0',
  `location_1` int(11) NOT NULL DEFAULT '0',
  `location_2` int(11) NOT NULL DEFAULT '0',
  `location_3` int(11) NOT NULL DEFAULT '0',
  `location_4` int(11) NOT NULL DEFAULT '0',
  `location_5` int(11) NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `long_description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `entered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` date NOT NULL DEFAULT '0000-00-00',
  `has_start_time` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `start_time` time NOT NULL DEFAULT '00:00:00',
  `end_date` date NOT NULL DEFAULT '0000-00-00',
  `has_end_time` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `end_time` time NOT NULL DEFAULT '00:00:00',
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zip_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `zip5` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `latitude` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `maptuning` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `maptuning_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `renewal_date` date NOT NULL DEFAULT '0000-00-00',
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `suspended_sitemgr` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `level` tinyint(3) NOT NULL DEFAULT '0',
  `random_number` bigint(15) NOT NULL DEFAULT '0',
  `cat_1_id` int(11) NOT NULL DEFAULT '0',
  `parcat_1_level1_id` int(11) NOT NULL DEFAULT '0',
  `parcat_1_level2_id` int(11) NOT NULL DEFAULT '0',
  `parcat_1_level3_id` int(11) NOT NULL DEFAULT '0',
  `parcat_1_level4_id` int(11) NOT NULL DEFAULT '0',
  `cat_2_id` int(11) NOT NULL DEFAULT '0',
  `parcat_2_level1_id` int(11) NOT NULL DEFAULT '0',
  `parcat_2_level2_id` int(11) NOT NULL DEFAULT '0',
  `parcat_2_level3_id` int(11) NOT NULL DEFAULT '0',
  `parcat_2_level4_id` int(11) NOT NULL DEFAULT '0',
  `cat_3_id` int(11) NOT NULL DEFAULT '0',
  `parcat_3_level1_id` int(11) NOT NULL DEFAULT '0',
  `parcat_3_level2_id` int(11) NOT NULL DEFAULT '0',
  `parcat_3_level3_id` int(11) NOT NULL DEFAULT '0',
  `parcat_3_level4_id` int(11) NOT NULL DEFAULT '0',
  `cat_4_id` int(11) NOT NULL DEFAULT '0',
  `parcat_4_level1_id` int(11) NOT NULL DEFAULT '0',
  `parcat_4_level2_id` int(11) NOT NULL DEFAULT '0',
  `parcat_4_level3_id` int(11) NOT NULL DEFAULT '0',
  `parcat_4_level4_id` int(11) NOT NULL DEFAULT '0',
  `cat_5_id` int(11) NOT NULL DEFAULT '0',
  `parcat_5_level1_id` int(11) NOT NULL DEFAULT '0',
  `parcat_5_level2_id` int(11) NOT NULL DEFAULT '0',
  `parcat_5_level3_id` int(11) NOT NULL DEFAULT '0',
  `parcat_5_level4_id` int(11) NOT NULL DEFAULT '0',
  `fulltextsearch_keyword` text COLLATE utf8_unicode_ci NOT NULL,
  `fulltextsearch_where` text COLLATE utf8_unicode_ci NOT NULL,
  `video_snippet` text COLLATE utf8_unicode_ci NOT NULL,
  `video_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `recurring` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `day` int(3) NOT NULL DEFAULT '0',
  `dayofweek` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `week` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `month` int(3) NOT NULL DEFAULT '0',
  `until_date` date DEFAULT '0000-00-00',
  `repeat_event` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `number_views` int(11) NOT NULL DEFAULT '0',
  `map_zoom` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `package_price` decimal(10,2) NOT NULL,
  `custom_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `start_date` (`start_date`),
  KEY `end_date` (`end_date`),
  KEY `account_id` (`account_id`),
  KEY `latitude` (`latitude`),
  KEY `longitude` (`longitude`),
  KEY `country_id` (`location_1`),
  KEY `state_id` (`location_2`),
  KEY `region_id` (`location_3`),
  KEY `status` (`status`),
  KEY `level` (`level`),
  KEY `city_id` (`location_4`),
  KEY `area_id` (`location_5`),
  KEY `title` (`title`),
  KEY `friendly_url` (`friendly_url`),
  KEY `random_number` (`random_number`),
  KEY `cat_1_id` (`cat_1_id`),
  KEY `parcat_1_level1_id` (`parcat_1_level1_id`),
  KEY `cat_2_id` (`cat_2_id`),
  KEY `parcat_2_level1_id` (`parcat_2_level1_id`),
  KEY `cat_3_id` (`cat_3_id`),
  KEY `parcat_3_level1_id` (`parcat_3_level1_id`),
  KEY `cat_4_id` (`cat_4_id`),
  KEY `parcat_4_level1_id` (`parcat_4_level1_id`),
  KEY `cat_5_id` (`cat_5_id`),
  KEY `parcat_5_level1_id` (`parcat_5_level1_id`),
  FULLTEXT KEY `fulltextsearch_keyword` (`fulltextsearch_keyword`),
  FULLTEXT KEY `fulltextsearch_where` (`fulltextsearch_where`),
  FULLTEXT KEY `fulltextsearch_title` (`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `Event`
--

INSERT INTO `Event` (`id`, `importID`, `account_id`, `discount_id`, `title`, `seo_title`, `friendly_url`, `image_id`, `thumb_id`, `location_1`, `location_2`, `location_3`, `location_4`, `location_5`, `description`, `seo_description`, `long_description`, `keywords`, `seo_keywords`, `updated`, `entered`, `start_date`, `has_start_time`, `start_time`, `end_date`, `has_end_time`, `end_time`, `location`, `address`, `zip_code`, `zip5`, `latitude`, `longitude`, `maptuning`, `maptuning_date`, `url`, `contact_name`, `phone`, `email`, `renewal_date`, `status`, `suspended_sitemgr`, `level`, `random_number`, `cat_1_id`, `parcat_1_level1_id`, `parcat_1_level2_id`, `parcat_1_level3_id`, `parcat_1_level4_id`, `cat_2_id`, `parcat_2_level1_id`, `parcat_2_level2_id`, `parcat_2_level3_id`, `parcat_2_level4_id`, `cat_3_id`, `parcat_3_level1_id`, `parcat_3_level2_id`, `parcat_3_level3_id`, `parcat_3_level4_id`, `cat_4_id`, `parcat_4_level1_id`, `parcat_4_level2_id`, `parcat_4_level3_id`, `parcat_4_level4_id`, `cat_5_id`, `parcat_5_level1_id`, `parcat_5_level2_id`, `parcat_5_level3_id`, `parcat_5_level4_id`, `fulltextsearch_keyword`, `fulltextsearch_where`, `video_snippet`, `video_url`, `recurring`, `day`, `dayofweek`, `week`, `month`, `until_date`, `repeat_event`, `number_views`, `map_zoom`, `package_id`, `package_price`, `custom_id`) VALUES
(1, 0, 0, '', 'event sliver', 'event sliver', 'event-sliver', 0, 0, 0, 0, 0, 0, 0, '', '', '', 'event sliver', 'event sliver', '2015-02-21 17:07:57', '2015-02-21 17:07:57', '2015-02-21', 'n', '00:00:00', '2016-03-25', 'n', '00:00:00', '', '', '', '0', '', '', '', '0000-00-00 00:00:00', '', '', '', '', '0000-00-00', 'A', 'n', 50, 372579178188536, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'event sliver event sliver Event Category 1 Event Category 1', '', '', '', 'N', 0, '', '', 0, '0000-00-00', 'N', 0, 0, 0, '0.00', NULL),
(2, 3, 13, '', 'Antiquities Fair', 'Antiquities Fair', 'antiquities-fair-54ea8f631fb07', 0, 0, 1, 0, 1, 1, 0, 'Sed pretium. Maecenas at erat quis quam convallis.', 'Sed pretium. Maecenas at erat quis quam convallis.', 'Sed pretium. Maecenas at erat quis quam convallis tincidunt. Nulla dictum massa sit amet risus. In auctor turpis vitae eros. Vivamus ut metus a est pretium interdum.', 'fair', 'fair', '2015-02-23 03:24:35', '2015-02-23 03:24:35', '2015-12-31', 'y', '10:00:00', '2015-12-31', 'y', '13:00:00', 'Downtown Ballroom', '2226 14th Avenue', '32960', '0', '', '', '', '0000-00-00 00:00:00', 'http://www.edirectory.com/directory_ad.php', 'Amanda Jackson', '101 742 4245', 'test_email@edirectory.com', '2015-12-31', 'A', 'n', 10, 143760681286231, 3, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Antiquities Fair fair Fair Antiquities Sed pretium. Maecenas at erat quis quam convallis.', '2226 14th Avenue 32960 United States USA Florida FL Miami', '', '', 'N', 0, '', '', 0, '0000-00-00', 'N', 0, 0, 0, '0.00', ''),
(3, 3, 14, '', 'Jazz Fest 2011', 'Jazz Fest 2011', 'jazz-fest-2011-54ea8f6323a57', 0, 0, 1, 0, 2, 2, 0, 'Sed pretium. Maecenas at erat quis quam convallis.', 'Sed pretium. Maecenas at erat quis quam convallis.', 'Sed pretium. Maecenas at erat quis quam convallis tincidunt. Nulla dictum massa sit amet risus. In auctor turpis vitae eros. Vivamus ut metus a est pretium interdum.', 'film || festival', 'film,festival', '2015-02-23 03:24:35', '2015-02-23 03:24:35', '2015-12-31', 'y', '15:00:00', '2015-12-31', 'y', '20:30:00', '', '305 Maple Ave West', '22180', '0', '', '', '', '0000-00-00 00:00:00', 'http://www.edirectory.com/directory_ad.php', 'Julia Case', '123 898 8989', 'test_email@edirectory.com', '2015-12-31', 'P', 'n', 30, 601065902599195, 5, 4, 0, 0, 0, 6, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Jazz Fest 2011 film festival Entertainment Cinema Entertainment Parties Sed pretium. Maecenas at erat quis quam convallis.', '305 Maple Ave West 22180 United States USA California CA Alamo', '', '', 'N', 0, '', '', 0, '0000-00-00', 'N', 0, 0, 0, '0.00', ''),
(4, 3, 15, '', 'Halloween Party', 'Halloween Party', 'halloween-party-54ea8f63259b6', 0, 0, 1, 0, 1, 0, 0, 'Sed pretium. Maecenas at erat quis quam convallis.', 'Sed pretium. Maecenas at erat quis quam convallis.', 'Sed pretium. Maecenas at erat quis quam convallis tincidunt. Nulla dictum massa sit amet risus. In auctor turpis vitae eros. Vivamus ut metus a est pretium interdum.', 'jazz', 'jazz', '2015-02-23 03:24:35', '2015-02-23 03:24:35', '2012-10-31', 'y', '09:15:00', '2015-10-31', 'y', '11:00:00', 'Bristol High School', '2900 S. Bristol Street', '60462', '0', '', '', '', '0000-00-00 00:00:00', 'http://www.edirectory.com/directory_ad.php', 'Brandon C', '(815) 462-0019', 'test_email@edirectory.com', '2015-12-31', 'A', 'n', 50, 574047499870926, 7, 4, 0, 0, 0, 8, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Halloween Party jazz Entertainment Theaters Entertainment Concerts Sed pretium. Maecenas at erat quis quam convallis.', '2900 S. Bristol Street 60462 United States USA Florida FL', '', '', 'N', 0, '', '', 0, '0000-00-00', 'N', 0, 0, 0, '0.00', ''),
(5, 3, 0, '', 'Wine & Food Fest', 'Wine & Food Fest', 'wine-food-fest-54ea8f63269b4', 0, 0, 1, 0, 3, 3, 0, 'Sed pretium. Maecenas at erat quis quam convallis.', 'Sed pretium. Maecenas at erat quis quam convallis.', 'Sed pretium. Maecenas at erat quis quam convallis tincidunt. Nulla dictum massa sit amet risus. In auctor turpis vitae eros. Vivamus ut metus a est pretium interdum.', 'halloween || party', 'halloween,party', '2015-02-23 03:24:35', '2015-02-23 03:24:35', '2015-12-31', 'n', '00:00:00', '2015-12-31', 'n', '00:00:00', '', '4800 Texoma Pkwy', '75090', '0', '', '', '', '0000-00-00 00:00:00', 'http://www.edirectory.com/directory_ad.php', '', '303 345 5454', 'test_email@edirectory.com', '2015-12-31', 'S', 'n', 10, 67039822290689, 9, 4, 0, 0, 0, 10, 6, 4, 0, 0, 11, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'Wine & Food Fest halloween party Entertainment Holidays Entertainment Parties Halloween Entertainment Night Clubs Sed pretium. Maecenas at erat quis quam convallis.', '4800 Texoma Pkwy 75090 United States USA Texas TX Sherman', '', '', 'N', 0, '', '', 0, '0000-00-00', 'N', 0, 0, 0, '0.00', ''),
(6, 3, 16, '', 'San Francisco Symphony', 'San Francisco Symphony', 'san-francisco-symphony-54ea8f6329b26', 0, 0, 1, 0, 4, 4, 0, 'Sed pretium. Maecenas at erat quis quam convallis.', 'Sed pretium. Maecenas at erat quis quam convallis.', 'Sed pretium. Maecenas at erat quis quam convallis tincidunt. Nulla dictum massa sit amet risus. In auctor turpis vitae eros. Vivamus ut metus a est pretium interdum.', 'wine || food || festival || fest', 'wine,food,festival,fest', '2015-02-23 03:24:35', '2015-02-23 03:24:35', '2015-12-31', 'n', '00:00:00', '2015-12-31', 'n', '00:00:00', 'New York Theater', '520 East 88th Street', '10128', '0', '', '', '', '0000-00-00 00:00:00', 'http://www.edirectory.com/directory_ad.php', 'Daniel Willis', '586 258 4566', 'test_email@edirectory.com', '2015-12-31', 'A', 'n', 50, 613056642574311, 13, 12, 0, 0, 0, 14, 12, 0, 0, 0, 6, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'San Francisco Symphony wine food festival fest Food and Dining Drinks Food and Dining Festivals Entertainment Parties Sed pretium. Maecenas at erat quis quam convallis.', '520 East 88th Street 10128 United States USA New York NY New York', '', '', 'N', 0, '', '', 0, '0000-00-00', 'N', 0, 0, 0, '0.00', '');

-- --------------------------------------------------------

--
-- Table structure for table `EventCategory`
--

CREATE TABLE IF NOT EXISTS `EventCategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `thumb_id` int(11) NOT NULL DEFAULT '0',
  `image_id` int(11) NOT NULL DEFAULT '0',
  `featured` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `summary_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `active_event` int(11) NOT NULL DEFAULT '0',
  `enabled` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `count_sub` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `active_event` (`active_event`),
  KEY `title1` (`title`),
  KEY `friendly_url1` (`friendly_url`),
  FULLTEXT KEY `keywords` (`keywords`,`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Dumping data for table `EventCategory`
--

INSERT INTO `EventCategory` (`id`, `title`, `category_id`, `thumb_id`, `image_id`, `featured`, `summary_description`, `seo_description`, `page_title`, `friendly_url`, `keywords`, `seo_keywords`, `content`, `active_event`, `enabled`, `count_sub`) VALUES
(1, 'Event Category 1', 0, 0, 0, 'y', '', '', 'Event Category 1', 'event-category-1', 'Event Category 1', '', '', 1, 'y', 0),
(2, 'Fair', 0, 0, 0, 'y', '', '', 'Fair', 'fair', '', '', NULL, 1, 'y', 0),
(3, 'Antiquities', 2, 0, 0, 'y', '', '', 'Antiquities', 'antiquities', '', '', NULL, 1, 'y', 0),
(4, 'Entertainment', 0, 0, 0, 'y', '', '', 'Entertainment', 'entertainment', '', '', NULL, 2, 'y', 0),
(5, 'Cinema', 4, 0, 0, 'y', '', '', 'Cinema', 'cinema', '', '', NULL, 0, 'y', 0),
(6, 'Parties', 4, 0, 0, 'y', '', '', 'Parties', 'parties', '', '', NULL, 1, 'y', 0),
(7, 'Theaters', 4, 0, 0, 'y', '', '', 'Theaters', 'theaters', '', '', NULL, 1, 'y', 0),
(8, 'Concerts', 4, 0, 0, 'y', '', '', 'Concerts', 'concerts', '', '', NULL, 1, 'y', 0),
(9, 'Holidays', 4, 0, 0, 'y', '', '', 'Holidays', 'holidays', '', '', NULL, 0, 'y', 0),
(10, 'Halloween', 6, 0, 0, 'y', '', '', 'Halloween', 'halloween', '', '', NULL, 0, 'y', 0),
(11, 'Night Clubs', 4, 0, 0, 'y', '', '', 'Night Clubs', 'night-clubs', '', '', NULL, 0, 'y', 0),
(12, 'Food and Dining', 0, 0, 0, 'y', '', '', 'Food and Dining', 'food-and-dining', '', '', NULL, 1, 'y', 0),
(13, 'Drinks', 12, 0, 0, 'y', '', '', 'Drinks', 'drinks', '', '', NULL, 1, 'y', 0),
(14, 'Festivals', 12, 0, 0, 'y', '', '', 'Festivals', 'festivals', '', '', NULL, 1, 'y', 0);

-- --------------------------------------------------------

--
-- Table structure for table `EventLevel`
--

CREATE TABLE IF NOT EXISTS `EventLevel` (
  `value` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `defaultlevel` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `detail` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `images` int(3) NOT NULL DEFAULT '0',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `active` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `popular` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `featured` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `theme` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  PRIMARY KEY (`value`,`theme`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `EventLevel`
--

INSERT INTO `EventLevel` (`value`, `name`, `defaultlevel`, `detail`, `images`, `price`, `active`, `popular`, `featured`, `content`, `theme`) VALUES
(50, 'silver', 'n', 'n', 0, '0.00', 'y', '', '', '', 'default'),
(30, 'gold', 'n', 'n', 0, '25.00', 'y', '', '', '', 'default'),
(10, 'diamond', 'y', 'y', 3, '50.00', 'y', 'y', 'y', '', 'default'),
(50, 'silver', 'n', 'y', 0, '0.00', 'y', '', 'y', '', 'realestate'),
(30, 'gold', 'n', 'y', 0, '25.00', 'y', '', 'y', '', 'realestate'),
(10, 'diamond', 'y', 'y', 3, '50.00', 'y', 'y', 'y', '', 'realestate'),
(50, 'silver', 'n', 'n', 0, '0.00', 'y', '', '', '', 'diningguide'),
(30, 'gold', 'n', 'n', 0, '25.00', 'y', '', '', '', 'diningguide'),
(10, 'diamond', 'y', 'y', 3, '50.00', 'y', 'y', 'y', '', 'diningguide'),
(50, 'silver', 'n', 'n', 0, '0.00', 'y', '', '', '', 'contractors'),
(30, 'gold', 'n', 'n', 0, '25.00', 'y', '', '', '', 'contractors'),
(10, 'diamond', 'y', 'y', 3, '50.00', 'y', 'y', 'y', '', 'contractors');

-- --------------------------------------------------------

--
-- Table structure for table `EventLevel_Field`
--

CREATE TABLE IF NOT EXISTS `EventLevel_Field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `theme` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `level` int(3) NOT NULL,
  `field` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `theme` (`theme`,`level`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=96 ;

--
-- Dumping data for table `EventLevel_Field`
--

INSERT INTO `EventLevel_Field` (`id`, `theme`, `level`, `field`) VALUES
(71, 'default', 10, 'summary_description'),
(70, 'default', 30, 'summary_description'),
(3, 'realestate', 10, 'summary_description'),
(4, 'realestate', 30, 'summary_description'),
(64, 'default', 10, 'email'),
(6, 'realestate', 10, 'email'),
(65, 'default', 10, 'url'),
(8, 'realestate', 10, 'url'),
(73, 'default', 10, 'contact_name'),
(10, 'realestate', 10, 'contact_name'),
(77, 'default', 10, 'end_time'),
(76, 'default', 10, 'start_time'),
(13, 'realestate', 10, 'start_time'),
(14, 'realestate', 30, 'start_time'),
(75, 'default', 30, 'end_time'),
(74, 'default', 30, 'start_time'),
(17, 'realestate', 10, 'end_time'),
(18, 'realestate', 30, 'end_time'),
(72, 'default', 10, 'long_description'),
(20, 'realestate', 10, 'long_description'),
(79, 'default', 10, 'main_image'),
(78, 'default', 30, 'main_image'),
(23, 'realestate', 10, 'main_image'),
(24, 'realestate', 30, 'main_image'),
(25, 'realestate', 50, 'main_image'),
(68, 'default', 10, 'phone'),
(67, 'default', 30, 'phone'),
(66, 'default', 50, 'phone'),
(29, 'realestate', 10, 'phone'),
(30, 'realestate', 30, 'phone'),
(31, 'realestate', 50, 'phone'),
(32, 'diningguide', 10, 'summary_description'),
(33, 'diningguide', 30, 'summary_description'),
(34, 'diningguide', 10, 'email'),
(35, 'diningguide', 10, 'url'),
(36, 'diningguide', 10, 'contact_name'),
(37, 'diningguide', 10, 'start_time'),
(38, 'diningguide', 30, 'start_time'),
(39, 'diningguide', 10, 'end_time'),
(40, 'diningguide', 30, 'end_time'),
(41, 'diningguide', 10, 'long_description'),
(42, 'diningguide', 10, 'main_image'),
(43, 'diningguide', 30, 'main_image'),
(44, 'diningguide', 10, 'phone'),
(45, 'diningguide', 30, 'phone'),
(46, 'diningguide', 10, 'video'),
(47, 'diningguide', 50, 'phone'),
(48, 'diningguide', 10, 'summary_description'),
(49, 'diningguide', 30, 'summary_description'),
(50, 'diningguide', 10, 'email'),
(51, 'diningguide', 10, 'url'),
(52, 'diningguide', 10, 'contact_name'),
(53, 'diningguide', 10, 'start_time'),
(54, 'diningguide', 30, 'start_time'),
(55, 'diningguide', 10, 'end_time'),
(56, 'diningguide', 30, 'end_time'),
(57, 'diningguide', 10, 'long_description'),
(58, 'diningguide', 10, 'main_image'),
(59, 'diningguide', 30, 'main_image'),
(60, 'diningguide', 10, 'phone'),
(61, 'diningguide', 30, 'phone'),
(62, 'diningguide', 10, 'video'),
(63, 'diningguide', 50, 'phone'),
(69, 'default', 10, 'video'),
(80, 'contractors', 10, 'summary_description'),
(81, 'contractors', 30, 'summary_description'),
(82, 'contractors', 10, 'email'),
(83, 'contractors', 10, 'url'),
(84, 'contractors', 10, 'contact_name'),
(85, 'contractors', 10, 'start_time'),
(86, 'contractors', 30, 'start_time'),
(87, 'contractors', 10, 'end_time'),
(88, 'contractors', 30, 'end_time'),
(89, 'contractors', 10, 'long_description'),
(90, 'contractors', 10, 'main_image'),
(91, 'contractors', 30, 'main_image'),
(92, 'contractors', 10, 'phone'),
(93, 'contractors', 30, 'phone'),
(94, 'contractors', 10, 'video'),
(95, 'contractors', 50, 'phone');

-- --------------------------------------------------------

--
-- Table structure for table `Event_LocationCounter`
--

CREATE TABLE IF NOT EXISTS `Event_LocationCounter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_level` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `full_friendly_url` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `Event_LocationCounter`
--

INSERT INTO `Event_LocationCounter` (`id`, `location_level`, `location_id`, `count`, `title`, `full_friendly_url`) VALUES
(11, 1, 1, 3, 'United States', 'united-states'),
(12, 3, 1, 2, 'Florida', 'united-states/florida'),
(13, 3, 4, 1, 'New York', 'united-states/new-york'),
(14, 4, 1, 1, 'Miami', 'united-states/florida/miami'),
(15, 4, 4, 1, 'New York', 'united-states/new-york/new-york');

-- --------------------------------------------------------

--
-- Table structure for table `FAQ`
--

CREATE TABLE IF NOT EXISTS `FAQ` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sitemgr` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `member` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `frontend` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `answer` text COLLATE utf8_unicode_ci NOT NULL,
  `editable` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `keyword` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `keyword` (`keyword`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=89 ;

--
-- Dumping data for table `FAQ`
--

INSERT INTO `FAQ` (`id`, `sitemgr`, `member`, `frontend`, `question`, `answer`, `editable`, `keyword`) VALUES
(1, 'y', 'n', 'n', 'When is the "Claim this ad" link available?', 'Listings without a sponsor account (No Owner) associated with a listing can use the claim feature ("Claim this ad" link will show up on the front of the directory for listings with no owner).', 'n', ''),
(2, 'y', 'n', 'n', 'Who can "Claim this ad"?', 'If the "Claim this ad" link is available on the front area, any sponsor, visitor or new user can claim the listing. After a user claims a listing the sitemgr can either deny their claim to the listing or approve it. The sitemgr''s approval gives the user full control over the listing.', 'n', ''),
(3, 'y', 'n', 'n', 'Can I turn off "Claim this ad" for a listing?', 'If the listing does not have an account and it is not available to claim, you can edit the listing and check the box "Disable claim feature for this listing". The "Claim this ad" link will be turned off for this listing.', 'n', ''),
(4, 'y', 'n', 'n', 'What is Default Image? And Where is it used?', 'If the sponsor or sitemgr forgets to upload an image to an item, to the gallery or another image field, the Default Image is used.', 'n', ''),
(5, 'y', 'n', 'n', 'My .csv file must have the same columns as the .csv sample file?', 'Make sure that your data columns match the sample .csv format provided. All fields are required (even if they have no data in them) for the import to function correctly.', 'n', ''),
(6, 'y', 'n', 'n', 'Can I delete any column on the .csv file import?', 'The same as above, if any columns are missing from the .csv file the import will not function correctly, please leave them all intact.', 'n', ''),
(7, 'y', 'n', 'n', 'Do I have to put my .csv file on the same order as the import .csv sample file?', 'Yes, you must use the format of the sample .csv. The data fields in the .csv that you upload must be in the same order as the sample template format. If the data is not in the correct order your data will not be imported correctly.', 'n', ''),
(8, 'y', 'n', 'n', 'Can I make imports with the same account? And different accounts?', 'If you want each item to have its own account then each item (row) in the .csv file will need a different account username entered. If multiple items have the same account associated with them in the .csv file they will be imported to the same account. If you plan on uploading many items into a single account we suggest doing a separate import for those imports and using the import settings to set the import to single account import.', 'n', ''),
(9, 'y', 'n', 'n', 'What happens if I misspell location on the import?', 'Please be specific when you are spelling text for your location fields. For example, if you import two items for the State of New York, and on a item you write NewYork, and on the other one you write New York, both States will appear in the advanced search dropdowns in the directory and each would need to be updated with your meta information for each location.', 'n', ''),
(10, 'y', 'n', 'n', 'What happens if I misspell a category on import?', 'Please be specific when you are spelling text for your category fields. If an item has the Arts & Entertainment category and the other one has Arts and Entertainment, both categories will appear in the item search dropdown fields, in the item browse by category area, and in the browse categories area on your item results pages.', 'n', ''),
(11, 'y', 'n', 'n', 'How many categories can I import?', 'Each item can have up to 5 categories imported. It will not import more than 5 categories.', 'n', ''),
(12, 'y', 'n', 'n', 'Can I import any username/password?', 'Invalid Usernames/Passwords will not be imported. Usernames need to be a valid e-mail between 4 and 80 characters with no spaces. Passwords need to have a minimum of 4 characters and maximum of 50 characters, we do not allow special character. If you are trying to import multiple items to the same account but are using different passwords for each row in your .csv file the import for the additional items will have problems. You may find it easier to use the Import Settings to upload multiple items to the same account.', 'n', ''),
(13, 'y', 'n', 'n', 'Do I have to import all items with an account?', 'If the username field is empty, the item will be imported without an account. That means that the item has no owner. If you are importing listings, this is a quick and easy way to use the claim feature on listings.', 'n', ''),
(14, 'y', 'n', 'n', 'What happens if I import more than one item with the same title?', 'The import will add the new items to database without deleting existing items. The friendly url will be different for each new item. <br />\r\nNOTE: If you want the data to be updated instead of creating new rows, you must check the option "Overwrite matching items" from within the import settings. The field Listing ID (or Event ID) in your CSV file must match with the value in your eDirectory database.', 'n', ''),
(15, 'y', 'n', 'n', 'What information is required to make the import work?', 'The minimum amount of information that is required is the "Title". All other fields can be blank in your .csv file.', 'n', ''),
(16, 'y', 'n', 'n', 'How can I import listings to have the "claim this ad" link for the claim feature?', 'To use the claim listing feature, do not assign a listing to a specific account. When doing the .csv import leave the columns related to member account information blank.', 'n', ''),
(18, 'y', 'n', 'n', 'Can I rollback an import?', 'You can roll back a finished or stopped import. All the accounts and items imported will be removed, but for security reasons the categories and locations imported will remain in the directory database. A task will be scheduled to roll back your data.', 'n', ''),
(19, 'y', 'n', 'n', 'How can I rollback imported data?', 'To rollback imported data, click on the Rollback button on the Import Log tab and follow the rollback process. <br />\r\nNote: Be sure to click on the Rollback button and not the delete icon (this just removes the import log record, not the data. Once the log file is removed, the data can not be rolled back, so be careful here).', 'n', ''),
(20, 'y', 'n', 'n', 'Can I delete an imported data log?', 'When using Delete Log button, data will not be removed from the directory. The specific log will be removed, but the data from the import will remain in the directory database.', 'n', ''),
(21, 'y', 'n', 'n', 'What is "Badge"?', 'Listing Badges allow you to designate listings with certain images of your choice. For example, if you want to mark a listing as "Editor''s Choice", you can upload your own icon, and mark (on the listing form) the listings you want to display the icon. You can also give members access to select the badges themselves by checking the box. For example you may want a "Pet Friendly" option that members can use to add to their listings.', 'n', ''),
(22, 'y', 'n', 'n', 'What are the listing types?', 'With the listing types you can modify the listings detail styles.<br /> PS: This feature isn''t available for themes Real Estate and Dining Guide.', 'n', ''),
(71, 'y', 'n', 'n', 'I am using the multi-domain feature, how can I see the items of the sites at the site manager area?', 'At the site manager area you can see the general data (related to all sites) at the top menu. And the data for each specific site you can see at the left menu, by changing the site at the drop-down list.', 'n', ''),
(23, 'y', 'n', 'n', 'Which type of modifications the listing types support?', 'You can charge an additional price per listing type, select the detail layout,  rename common fields (example: "Restaurant Name" instead of "Listing Title"), add new fields (checkboxes, dropdowns, text fields, short description fields and long descritpion fields) and select which of them will be required.<br /> PS: This feature isn''t available for themes Real Estate and Dining Guide.', 'n', ''),
(26, 'y', 'n', 'n', 'How do the Import Settings work?', 'On the "Import Settings" tab in the Settings area of the sitemgr, you can setup some import options:\r\n<br /> \r\n1. Import CSV comes from an Export: If you previously did an export from the import / exporter on your eDirectory installation - please check this box. \r\n<br />\r\n2. Enable all imported items as Active: Rather than having imported item in pending mode (so you can check them over and add data) this check box will make all imported items instantly active on the site. \r\n<br />\r\n3. Overwrite matching items: If you want to update an existing item in your eDirectory you must check this option.  For an item to be considered a match the following field must be identical on the imported .csv file and the database: Listing ID/Event ID.\r\n<br />\r\n4. Update friendly URL''s for matching items: It is strongly recommended that this option be left unchecked. If we find a record in the .csv file with a matching Listing ID/Event ID when compared to an existing record in the database, we will update that item with the new data (as detailed above), but with this option on we will also rewrite the URL based on the item title. If you have content that has been indexed by google, this content will cease to be reachable from the search engines until they update their data - this can take a day or months. Use Carefully. \r\n<br />\r\n5. All new categories set to featured: All categories that are imported will be automatically set to featured. \r\n<br />\r\n6. Default level for items without level specified: For all items with no level specified in the .csv file, you can choose a level here..\r\n<br />\r\n7. Import all items to the same user account: Everything in the .csv file will be attached to a single user account. After ticking this box, a dropdown will appear só that an account can be selected.', 'n', ''),
(27, 'y', 'n', 'n', 'How can I disable an Email Notification?', 'Go to the Email Notifications section and click on the active icon of the email you want to disable.', 'n', ''),
(29, 'n', 'y', 'y', 'How does the "Sign me in automatically" work?', 'The "Sign me in automatically" is optional, it saves your username and password on your computer and every time you access the page you will be automatically logged in.', 'y', ''),
(30, 'y', 'y', 'y', 'What happens if I forget my password?', 'If you forget your password, please click on the ''Forgot your Password?'' link of the front of the directory or on the sponsor login page. The password recovery email will be sent to the email address provided from your Contact Information. The email will contain a link which will redirect the user to the ''Manage Account'' section, where the password can be updated.', 'y', ''),
(31, 'n', 'y', 'y', 'How can I change my password?', 'After you are logged in, click on ''Account Settings'' link, you will see the "Current Password" field, type your current password in this field and your new password on the fields "Password" and "Retype Password", then hit the submit button.', 'y', ''),
(32, 'n', 'y', 'n', 'Can I change my username?', 'Yes, you can do that by going to ''Manage account'' > ''Account Settings'' and typing your new e-mail.', 'y', ''),
(34, 'n', 'y', 'n', 'Can I change my item level?', 'Yes, you can. After your item is expired you can choose the level (if it is free you can change the level anytime) and pay for it.', 'y', ''),
(35, 'n', 'y', 'n', 'Can I add categories to my deal?', 'No, you cannot. The deal is related to the listing categories you choose.', 'y', ''),
(81, 'y', 'n', 'n', 'What is the "Click to Call & Send to Phone" feature and how the Twilio api works?', 'With the "Click to Call & Send to Phone" feature your directory can send text messages to the users with the main information about your listing. Also, the users can contact directly the listing owners just clicking in the "Click to Call" button. You just need to create a Twilio account and upgrade it after you finish your free credits.', 'n', ''),
(37, 'n', 'n', 'y', 'Am I required to have an account to add items to the site?', 'Yes. In order to add any item, including Free items, to the directory you must have an account.', 'y', ''),
(38, 'n', 'n', 'y', 'How can I sign up for an account?', 'To sign up as a sponsor go to the ''Advertise With Us'' link at top menu, select an item and level and click in ''SIGN UP'' button. Fill out all fields, write down your username and password for future reference, choose the best payment gateway for you and follow the steps to finish the process. To sign up as a visitor go to ''Sign up | Login'' link at top menu, fill out all fields and click in ''Create Account''.', 'y', ''),
(45, 'y', 'y', 'y', 'Why am I receiving an ''Account Locked'' message?', 'If you attempt to access your account and type in an incorrect password 5 times the account will lock for 1 hour. This is for security reasons.', 'y', ''),
(46, 'y', 'n', 'n', 'How can I setup Facebook App ID and Facebook App Secret?', 'If you don''t already have a Facebook App ID and App Secret for your site, create an application with the Facebook Developer application. Note: Even if you have created an application and received an App Secret, you should review steps 4 through 7 and make sure your application settings are appropriate.<br />\r\n1. Go to https://developers.facebook.com/apps and click in + Create New App to create a new application.<br />\r\n2. Enter a name for your application in the App Display Name field.<br />\r\n3. Agree to the Facebook Platform Policies, then click Continue.<br />\r\n4. Enter the Security Check words, then click Submit.<br />\r\n5. On the Settings tab > Basic, take note of the App ID and App Secret, you''ll need this shortly.<br />\r\n6. Still on the Settings tab, click on Website and set Site URL to the top-level directory of the site which will be implementing Facebook Connect (this is usually your domain, e.g. http://www.example.com , but could also be a subdirectory). If your site is going to implement Facebook Connect across a number of subdomains of your site (for example, http://foo.example.com and bar.example.com), you need to enter a App Domain (which would be example.com in this case).<br />\r\n7. You can include a logo that appears on the Facebook Connect dialog. On the Settings tab > Basic, click Edit on the image next your App Name and browse to choose an image file. Your logo must be in JPG, GIF, or PNG format. If the image is larger than 75x75 pixels, it will be resized and converted, then click Save Changes.<br />\r\n8. Copy the App ID and App Secret and paste it on Setting > Sign In Options of your directory.', 'n', ''),
(86, 'y', 'n', 'n', 'Como habilito o PagSeguro no meu diretório?', 'Para habilitar o PagSeguro no seu diretório, você precisa de uma conta de vendedor ou empresarial no PagSeguro. Siga os passos abaixo para criar e configurar sua conta.<br /><br /> 1. Vá até a página de cadastro do PagSeguro em https://pagseguro.uol.com.br/registration/registration.jhtml;<br /> 2. Digite seu e-mail e senha e escolha um tipo de conta (Vendedor ou Empresarial);<br /> 3. Informe seus dados pessoais/empresariais, verifique os termos de contrato do PagSeguro e clique em ''Continuar'';<br /> 4. Você receberá um e-mail para confirmar sua conta;<br /> 5. Após a confirmação da conta, você precisa configurá-la para integrá-la ao seu diretório. Faça login e clique no menu <b>''Integrações''</b> - <b>''Token de segurança''</b>;<br /> 6. Clique em <b>''Gerar novo Token''</b> e tome nota do código;<br /> 7. Vá até o menu <b>''Integrações''</b> - <b>''Página de redirecionamento''</b>, selecione a opção <b>''Ativado''</b> e ative a URL: http://www.meudiretorio.com.br/<b>members/billing/processpayment.php?payment_method=pagseguro</b> , substituindo www.meudiretorio.com.br para a URL do seu diretório. Essa é a URL onde os usuários serão redirecionados após um pagamento;<br /> 8. Vá até o menu <b>''Integrações''</b> - <b>''Notificação de transações''</b>, selecione a opção <b>''Ativado''</b> e ative a URL: http://www.meudiretorio.com.br/<b>members/billing/pagseguroreturn.php</b> , substituindo www.meudiretorio.com.br para a URL do seu diretório. Certifique-se que a URL foi informada corretamente, caso contrário o PagSeguro não irá retornar os dados das transações para seu diretório;<br /> 9. Volte à interface administrativa do seu diretório e informe sua Conta (seu e-mail cadastrado no PagSeguro) e Token. Lembre-se de atualizar o campo ''Símbolo da moeda'' para <b>R$</b> e ''Moeda de Pagamento'' para <b>BRL</b>.<br /> 10. Clique em ''Enviar'' para habilitar o PagSeguro no seu diretório.<br /><br /> Obs: <br/ > -Lembre-se de atualizar o token de segurança no seu diretório sempre que gerar um novo token no Pagseguro;<br /> -Para que seus usuários possam fazer pagamentos com cartão de crédito, sua conta no PagSeguro precisa ser verificada. Leia mais em https://pagseguro.uol.com.br/account/viewCheck.jhtml ;<br /> -O PagSeguro envia os dados de uma transação para o diretório sempre que o status é alterado. Note que as formas de pagamento (cartão, boleto bancário, etc), possuem diferentes prazos para liberação do pagamento.', 'n', ''),
(68, 'y', 'n', 'n', 'What happens with the items when I disable a level?', 'In banner level cases, all items already registered of this level will keep showing on the front of the directory until their expiration. Otherwise these items will be treated as the ''default level'', however, the search order priority and the featured area priviliges will be the same as the disabled level.', 'n', ''),
(54, 'y', 'n', 'n', 'How does the "Featured Categories" work?', 'If you enable featured category on "Setting - Featured categories", only the selected module will show the featured checkbox when adding or editing a category. This box enable the category to be seen in Browse by category and it''s only available for the levels that appear on front - category and subcategory. When adding a category through import all new categories will come checked if this option is turned on in Import Settings.<br />PS: This feature isn''t available for the theme Dining Guide.', 'n', ''),
(53, 'y', 'n', 'n', 'Can I edit the maintenance page?', 'Yes. There''s a feature on Site Content in the General Section so you can edit how maintenance page will show up when the Maintenance Mode is on. ', 'n', ''),
(52, 'y', 'n', 'n', 'What happens when I enable maintenance mode?', 'When you turn on maintenance mode, all the front pages are redirected to the maintenance page.', 'n', ''),
(67, 'y', 'n', 'n', 'Can I change my Locations Options all the time?', 'No. If the directory already has items assigned to a level, it can´t be disabled. You also can´t choose a Default Location if there are items from different locations for that level.  Also, you can´t enable a Location Level if there are child levels enabled and non assigned items to that level you are trying to enable. For example: if the directory is using country, state and city, and there are items registered, you can enable neighborhood, but you can´t enable region because in this case the system will not have regions and states assigned. So, is extremely important decide your locations options before inserting data in directory. ', 'n', ''),
(66, 'y', 'n', 'n', 'My directory is using a Default Locations but now I want to use other locations. What should I do?', 'In setting locations, choose the option “No Default” in the select box that is holding the Default Location you don´t want anymore.', 'n', ''),
(65, 'y', 'n', 'n', 'What is the option “Show default” in Setting Locations?', 'When a default location is chosen, the option “show default” becomes available. When you enable it, the default location will be shown in all places that locations are shown, for example, in the summary and detail views for items. Otherwise the default location will be hidden. ', 'n', ''),
(62, 'y', 'n', 'n', 'What is Setting Locations?', 'This is where you choose the location levels you want to use within the directory. With this form you will also have the option to choose default locations and whether or not they will be illustrated on the front of the directory.', 'n', ''),
(63, 'y', 'n', 'n', 'How do I choose the locations levels I want to use?', 'There are five Locations Levels available: Country, Region, State, City and Neighborhood. Select which you want to use by checking the “enable” checkboxes for each level.', 'n', ''),
(64, 'y', 'n', 'n', 'What are Default Locations?', 'The locations chosen as default are automatically assigned to all items that will be registered in the directory. The member will not have the option to choose a different one. Example: select a default location if your directory will always show data in a specific location, e.g selecting United States as a default country will prevent you from adding Canada locations to your directory.', 'n', ''),
(69, 'y', 'n', 'n', 'What happens if I delete a site?', 'Be sure that the site you are deleting is no longer accessible, because once it been deleted, the home page, members and site manager areas do not work anymore.\r\nAfter delete the site, your data base will be preserved for one week, so you can recover it if you want. After that, all data base and files related to your site will be deleted.', 'n', ''),
(70, 'y', 'n', 'n', 'What I have to do after I create a new site?', 'After you create a new site an email will be sent to the eDirectory support team. \r\nAfter that the support team will prepare the site to work correctly in the front. \r\nThe new site only will be completed after you buy a new eDirectory licence with a discounted rate and finish the registration process for all these sites. \r\nIn the "Sites Management Page" has a column in the table to indicate the activation status of your sites. \r\nActive means the registration process was finished and the site has been activated. \r\nPending means the registration process is waiting for the licence and activation.', 'n', ''),
(72, 'y', 'n', 'n', 'I am using the multi-domain feature, can I use different locations levels for each site?', 'Yes, you can configure the location levels for each site, e.g. Country > State > City for the site 1, State > City for the site 2 and just City for the site 3.', 'n', ''),
(73, 'y', 'n', 'n', 'I am using the multi-domain feature, can I add different locations for each site?', 'Yes, you can add different location for each site (but first you must configure the locations levels for each site).', 'n', ''),
(77, 'y', 'n', 'n', 'How can I enable the Twitter Widget on my Home Page?', '1. Login on twitter and go to https://twitter.com/settings/widgets.<br />\r\n2. Click on ''''Create New''''.<br />\r\n3. Customize your Widget and click on ''''Create widget''''. We suggest you don''t change the options Height, Theme and Link Color. These will be done automatically according to your eDirectory theme in use.<br>\r\n4. Copy the code and paste on your eDirectory administration area (Site Content > General > Home Page > Twitter Widget).', 'n', ''),
(75, 'y', 'n', 'n', 'I am using the multi-domain feature, how will the import work?', 'You have to import different files for each site. When you make an import, the items and categories will be imported into the selected site; the account will be imported to a general database, which means that the member can login using the account on all sites. The locations will be imported and will appear for the selected site.', 'n', ''),
(76, 'y', 'n', 'n', 'I want to use the multi-domain feature, how can I enable it?', 'If you want to have different sites you can use the multi-domain feature and create a new site. Click on Sites > Add, fill out the new site information and click on the Submit button.', 'n', ''),
(79, 'y', 'n', 'n', 'What is the field Listing ID/Event ID?', 'If your CSV file comes from Export Section, this field will be filled with the values in your eDirectory database. Items that match (with the same Listing ID/Event ID) on import can be overwritten / replaced during the import process. This setting can be found on the import settings tab.', 'n', ''),
(80, 'y', 'n', 'n', 'What is the recommended size of my CSV file?', 'If you want to upload a CSV, we recommend small files with default maximum of 5mb. For bigger files, you must send your file to the import folder by FTP first and use the Select File by FTP option on the import window. Notice that when you use the FTP option, a task will be scheduled to prepare your file before the import, depending on your server configuration this can take some time.', 'n', ''),
(83, 'y', 'n', 'n', 'What are the fields Start Time Mode and End Time Mode?', 'If you are importing events which have start time or end time, you must fill in these fields. Use "AM" or "PM" if your clock type is 12 hours or just put "24" if you use a 24 hours clock.', 'n', ''),
(85, 'y', 'n', 'n', 'What is Favicon? And where is it used?', 'A favicon (short for favorites icon) is a file containing one small icon, most commonly 16×16 pixels, associated with a particular Web site. Browsers that provide favicon support typically display a page''s favicon in the browser''s address bar and next to the page''s name in a list of bookmarks. Also, the ones that support a tabbed document interface typically show a page''s favicon next to the page''s title on the tab. By using a favicon your users can identify your directory web site in a list of others web sites.', 'n', ''),
(82, 'y', 'n', 'n', 'How can I setup my Twilio API?', 'Go to http://www.twilio.com/pricing/ and click in "Get started". In the next page, fill in your First Name, Last Name, Email and Password and click in "Get started". You will be logged in and in the following page you will see your Account SID, Auth Token and Sandbox Number. Just copy these informations to your site manager and click in "Submit".', 'n', ''),
(87, 'y', 'n', 'n', 'O que significam os status das transações do PagSeguro?', 'O PagSeguro retorna diferentes status de transações para o diretório durante o processo de pagamento. Veja abaixo os detalhes de cada status:<br /><br />\r\n<b>-Aguardando pagamento/aprovação: </b>O comprador iniciou a transação, mas até o momento o PagSeguro não recebeu nenhuma informação sobre o pagamento;<br />\r\n<b>-Em análise: </b>O comprador optou por pagar com um cartão de crédito e o PagSeguro está analisando o risco da transação;<br />\r\n<b>-Pago: </b>A transação foi paga pelo comprador e o PagSeguro já recebeu uma confirmação da instituição financeira responsável pelo processamento;<br />\r\n<b>-Disponível: </b>A transação foi paga e chegou ao final de seu prazo de liberação sem ter sido retornada e sem que haja nenhuma disputa aberta;<br />\r\n<b>-Em disputa: </b>O comprador, dentro do prazo de liberação da transação, abriu uma disputa;<br />\r\n<b>-Devolvido: </b>O valor da transação foi devolvido para o comprador;<br />\r\n<b>-Cancelado: </b>A transação foi cancelada sem ter sido finalizada.<br />', 'n', ''),
(88, 'y', 'n', 'n', 'How do I signup for a Google Maps API?', 'Follow the steps below to create your API key.<br />\r\n1. Go to https://code.google.com/apis/console/ ;<br />\r\n2. On the Services Tab, enable the Google Maps API v3 option;<br />\r\n3. On the API Access Tab, take note of the API key in the Simple API Access option;<br />\r\n4. You can limit this API to specific domains by informing them in the ''Create new Browser key...'' option;<br />\r\n5. After save your API key on your site manager interface, you can track your API usage on the Reports tab.', 'n', '');

-- --------------------------------------------------------

--
-- Table structure for table `Gallery`
--

CREATE TABLE IF NOT EXISTS `Gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `entered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=34 ;

--
-- Dumping data for table `Gallery`
--

INSERT INTO `Gallery` (`id`, `account_id`, `title`, `entered`, `updated`) VALUES
(1, 0, 'Bronze listing test', '2015-02-21 14:28:58', '2015-02-21 14:28:58'),
(2, 0, 'Bronze listing test', '2015-02-21 16:33:51', '2015-02-21 16:33:51'),
(3, 0, 'Siliver listing test', '2015-02-21 16:36:36', '2015-02-21 16:36:36'),
(4, 0, 'Siliver listing test', '2015-02-21 16:37:08', '2015-02-21 16:37:08'),
(5, 0, 'Gold Listing test', '2015-02-21 16:45:14', '2015-02-21 16:45:14'),
(6, 0, 'Diamond', '2015-02-21 16:50:27', '2015-02-21 16:50:27'),
(7, 0, 'Gold Listing test', '2015-02-21 17:05:26', '2015-02-21 17:05:26'),
(8, 0, 'event sliver', '2015-02-21 17:07:57', '2015-02-21 17:07:57'),
(29, 13, 'Antiquities Fair', '2015-02-23 03:24:35', '2015-02-23 03:24:35'),
(28, 16, 'Skin Care Center', '2015-02-23 03:03:37', '2015-02-23 03:03:37'),
(27, 0, 'Arts Center', '2015-02-23 03:03:37', '2015-02-23 03:03:37'),
(24, 13, 'Ocean Resort', '2015-02-23 03:03:37', '2015-02-23 03:03:37'),
(25, 14, 'Coffee House', '2015-02-23 03:03:37', '2015-02-23 03:03:37'),
(26, 15, 'All Sport Fitness', '2015-02-23 03:03:37', '2015-02-23 03:03:37'),
(30, 14, 'Jazz Fest 2011', '2015-02-23 03:24:35', '2015-02-23 03:24:35'),
(31, 15, 'Halloween Party', '2015-02-23 03:24:35', '2015-02-23 03:24:35'),
(32, 0, 'Wine & Food Fest', '2015-02-23 03:24:35', '2015-02-23 03:24:35'),
(33, 16, 'San Francisco Symphony', '2015-02-23 03:24:35', '2015-02-23 03:24:35');

-- --------------------------------------------------------

--
-- Table structure for table `Gallery_Image`
--

CREATE TABLE IF NOT EXISTS `Gallery_Image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) NOT NULL DEFAULT '0',
  `image_id` int(11) NOT NULL DEFAULT '0',
  `image_caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumb_id` int(11) NOT NULL DEFAULT '0',
  `thumb_caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_default` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `gallery_id` (`gallery_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Gallery_Image`
--

INSERT INTO `Gallery_Image` (`id`, `gallery_id`, `image_id`, `image_caption`, `thumb_id`, `thumb_caption`, `image_default`, `order`) VALUES
(1, 6, 15, '', 16, '', 'y', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Gallery_Item`
--

CREATE TABLE IF NOT EXISTS `Gallery_Item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `item_id` int(11) NOT NULL DEFAULT '0',
  `gallery_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `item_type` (`item_type`),
  KEY `item_id` (`item_id`),
  KEY `gallery_id` (`gallery_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Dumping data for table `Gallery_Item`
--

INSERT INTO `Gallery_Item` (`id`, `item_type`, `item_id`, `gallery_id`) VALUES
(2, 'listing', 1, 2),
(3, 'listing', 2, 4),
(5, 'listing', 4, 6),
(7, 'listing', 3, 7),
(27, 'listing', 24, 28),
(26, 'listing', 23, 27),
(25, 'listing', 22, 26),
(24, 'listing', 21, 25),
(23, 'listing', 20, 24),
(28, 'event', 2, 29),
(29, 'event', 3, 30),
(30, 'event', 4, 31),
(31, 'event', 5, 32),
(32, 'event', 6, 33);

-- --------------------------------------------------------

--
-- Table structure for table `Gallery_Temp`
--

CREATE TABLE IF NOT EXISTS `Gallery_Temp` (
  `image_id` int(11) NOT NULL,
  `image_caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumb_id` int(11) NOT NULL,
  `thumb_caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_default` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `sess_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Gallery_Temp`
--

INSERT INTO `Gallery_Temp` (`image_id`, `image_caption`, `thumb_id`, `thumb_caption`, `image_default`, `sess_id`) VALUES
(13, '', 14, '', 'y', 'listing_194708680854e8a826e66d78.45014020');

-- --------------------------------------------------------

--
-- Table structure for table `Image`
--

CREATE TABLE IF NOT EXISTS `Image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` set('JPG','GIF','SWF','PNG') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'JPG',
  `width` smallint(6) NOT NULL DEFAULT '0',
  `height` smallint(6) NOT NULL DEFAULT '0',
  `prefix` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `Image`
--

INSERT INTO `Image` (`id`, `type`, `width`, `height`, `prefix`) VALUES
(1, 'PNG', 50, 50, ''),
(2, 'PNG', 50, 50, ''),
(3, 'PNG', 50, 50, ''),
(4, 'PNG', 50, 50, ''),
(5, 'PNG', 50, 50, ''),
(6, 'PNG', 50, 50, ''),
(7, 'PNG', 50, 50, ''),
(8, 'PNG', 50, 50, ''),
(9, 'PNG', 50, 50, ''),
(10, 'PNG', 944, 408, 'sitemgr_'),
(11, 'JPG', 497, 300, 'sitemgr_'),
(12, 'JPG', 222, 134, 'sitemgr_'),
(13, 'JPG', 497, 300, 'sitemgr_'),
(14, 'JPG', 222, 134, 'sitemgr_'),
(15, 'JPG', 497, 300, 'sitemgr_'),
(16, 'JPG', 222, 134, 'sitemgr_'),
(17, 'PNG', 664, 409, 'sitemgr_');

-- --------------------------------------------------------

--
-- Table structure for table `ImportLog`
--

CREATE TABLE IF NOT EXISTS `ImportLog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL DEFAULT '0000-00-00',
  `time` time NOT NULL DEFAULT '00:00:00',
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linesadded` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phisicalname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `action` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `progress` char(5) COLLATE utf8_unicode_ci NOT NULL,
  `totallines` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `errorlines` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `itens_added` int(11) NOT NULL,
  `accounts_added` int(11) NOT NULL,
  `history` longtext COLLATE utf8_unicode_ci NOT NULL,
  `update_itens` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `from_export` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `active_item` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `update_friendlyurl` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `featured_categs` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `default_level` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `same_account` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `account_id` int(11) NOT NULL,
  `delimiter` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `mysqlerror` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'listing',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `ImportLog`
--

INSERT INTO `ImportLog` (`id`, `date`, `time`, `filename`, `linesadded`, `phisicalname`, `status`, `action`, `progress`, `totallines`, `errorlines`, `itens_added`, `accounts_added`, `history`, `update_itens`, `from_export`, `active_item`, `update_friendlyurl`, `featured_categs`, `default_level`, `same_account`, `account_id`, `delimiter`, `mysqlerror`, `type`) VALUES
(1, '2015-02-23', '01:06:55', 'edirectory_sample.csv', '0', 'wShkZeoJFaQy88SSMs02.csv', 'D', 'RI', '0%', '5', '0', 0, 0, 'LANG_SITEMGR_IMPORT_SUCCESSUPLOADED||LANG_MSG_IMPORT_TOTALLINESREADY[5].||LANG_MSG_IMPORT_TOTALLINESERROR[0].||LANG_MSG_IMPORT_CSVIMPORTEDTOTEMPORARYTABLE||', 'n', 'n', 'n', 'n', 'y', '30', 'n', 0, ',', '', 'listing'),
(2, '2015-02-23', '01:12:00', 'edirectory_sample_event.csv', '0', 'IJSNNhYHbsvY6YO6qkHG.csv', 'D', 'RI', '0%', '5', '0', 0, 0, 'LANG_SITEMGR_IMPORT_SUCCESSUPLOADED||LANG_MSG_IMPORT_TOTALLINESREADY[5].||LANG_MSG_IMPORT_TOTALLINESERROR[0].||LANG_MSG_IMPORT_CSVIMPORTEDTOTEMPORARYTABLE||', 'n', 'n', 'n', 'n', 'y', '30', 'n', 0, ',', '', 'event'),
(3, '2015-02-23', '02:02:01', 'edirectory_sample_event.csv', '5', 'yVMGQk7lL3b5RSlkFz00.csv', 'F', 'D', '100%', '5', '0', 5, 0, 'LANG_SITEMGR_IMPORT_SUCCESSUPLOADED||LANG_MSG_IMPORT_TOTALLINESREADY[5].||LANG_MSG_IMPORT_TOTALLINESERROR[0].||LANG_MSG_IMPORT_CSVIMPORTEDTOTEMPORARYTABLE||LANG_SITEMGR_IMPORT_FEATURED_CATEGS||LANG_SITEMGR_IMPORT_DEFAULTLEVELSETTO[Diamond].||', 'n', 'n', 'n', 'n', 'y', '10', 'n', 0, ',', '', 'event'),
(4, '2015-02-23', '02:02:31', 'edirectory_sample.csv', '5', 'Ku7wgv70fx1WMxH9EbNU.csv', 'C', 'D', '100%', '5', '0', 5, 4, 'LANG_SITEMGR_IMPORT_SUCCESSUPLOADED||LANG_MSG_IMPORT_TOTALLINESREADY[5].||LANG_MSG_IMPORT_TOTALLINESERROR[0].||LANG_MSG_IMPORT_CSVIMPORTEDTOTEMPORARYTABLE||LANG_SITEMGR_IMPORT_FEATURED_CATEGS||LANG_SITEMGR_IMPORT_DEFAULTLEVELSETTO[Diamond].||LANG_SITEMGR_IMPORT_PROCCESSCANCELLED||5[LANG_MSG_IMPORT_ITEM_ROLLEDBACK_PLURAL].||4[LANG_MSG_IMPORT_ACCOUNT_ROLLEDBACK_PLURAL].||LANG_SITEMGR_IMPORT_ROLLBACKDONE||', 'n', 'n', 'n', 'n', 'y', '10', 'n', 0, ',', '', 'listing'),
(5, '2015-02-23', '02:49:33', 'edirectory_sample.csv', '5', 'nORJB4k7M19kdhA1Z62f.csv', 'C', 'D', '100%', '5', '0', 5, 4, 'LANG_SITEMGR_IMPORT_SUCCESSUPLOADED||LANG_MSG_IMPORT_TOTALLINESREADY[5].||LANG_MSG_IMPORT_TOTALLINESERROR[0].||LANG_MSG_IMPORT_CSVIMPORTEDTOTEMPORARYTABLE||LANG_SITEMGR_IMPORT_FEATURED_CATEGS||LANG_SITEMGR_IMPORT_DEFAULTLEVELSETTO[Diamond].||LANG_SITEMGR_IMPORT_PROCCESSCANCELLED||5[LANG_MSG_IMPORT_ITEM_ROLLEDBACK_PLURAL].||4[LANG_MSG_IMPORT_ACCOUNT_ROLLEDBACK_PLURAL].||LANG_SITEMGR_IMPORT_ROLLBACKDONE||', 'n', 'n', 'n', 'n', 'y', '10', 'n', 0, ',', '', 'listing'),
(6, '2015-02-23', '02:57:24', 'edirectory_sample.csv', '5', 'RX5QEUjbmpfjPcwLbUjU.csv', 'C', 'D', '100%', '5', '0', 5, 4, 'LANG_SITEMGR_IMPORT_SUCCESSUPLOADED||LANG_MSG_IMPORT_TOTALLINESREADY[5].||LANG_MSG_IMPORT_TOTALLINESERROR[0].||LANG_MSG_IMPORT_CSVIMPORTEDTOTEMPORARYTABLE||LANG_SITEMGR_IMPORT_FEATURED_CATEGS||LANG_SITEMGR_IMPORT_DEFAULTLEVELSETTO[Diamond].||LANG_SITEMGR_IMPORT_PROCCESSCANCELLED||5[LANG_MSG_IMPORT_ITEM_ROLLEDBACK_PLURAL].||4[LANG_MSG_IMPORT_ACCOUNT_ROLLEDBACK_PLURAL].||LANG_SITEMGR_IMPORT_ROLLBACKDONE||', 'n', 'n', 'n', 'n', 'y', '10', 'n', 0, ',', '', 'listing'),
(7, '2015-02-23', '03:01:31', 'edirectory_sample.csv', '5', 'aNEdkQ8o8LbdiV6HJtYY.csv', 'F', 'D', '100%', '5', '0', 5, 4, 'LANG_SITEMGR_IMPORT_SUCCESSUPLOADED||LANG_MSG_IMPORT_TOTALLINESREADY[5].||LANG_MSG_IMPORT_TOTALLINESERROR[0].||LANG_MSG_IMPORT_CSVIMPORTEDTOTEMPORARYTABLE||LANG_SITEMGR_IMPORT_FEATURED_CATEGS||LANG_SITEMGR_IMPORT_DEFAULTLEVELSETTO[Diamond].||', 'n', 'n', 'n', 'n', 'y', '10', 'n', 0, ',', '', 'listing');

-- --------------------------------------------------------

--
-- Table structure for table `ImportTemporary`
--

CREATE TABLE IF NOT EXISTS `ImportTemporary` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `import_log_id` int(11) NOT NULL DEFAULT '0',
  `account_username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_id` int(11) DEFAULT '0',
  `listing_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_location1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_location1_abbreviation` char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_location2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_location2_abbreviation` char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_location3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_location3_abbreviation` char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_location4` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_location4_abbreviation` char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_location5` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_location5_abbreviation` char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_latitude` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_longitude` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_long_description` text COLLATE utf8_unicode_ci,
  `listing_seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_renewal_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_level` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_category_1` text COLLATE utf8_unicode_ci,
  `listing_category_2` text COLLATE utf8_unicode_ci,
  `listing_category_3` text COLLATE utf8_unicode_ci,
  `listing_category_4` text COLLATE utf8_unicode_ci,
  `listing_category_5` text COLLATE utf8_unicode_ci,
  `listing_template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_line_number` int(11) NOT NULL DEFAULT '0',
  `inserted` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `error` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

-- --------------------------------------------------------

--
-- Table structure for table `ImportTemporary_Event`
--

CREATE TABLE IF NOT EXISTS `ImportTemporary_Event` (
  `id` bigint(255) NOT NULL AUTO_INCREMENT,
  `import_log_id` int(11) NOT NULL DEFAULT '0',
  `account_username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_address2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_fax` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_id` int(11) DEFAULT '0',
  `event_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_seo_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_locationname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_contactname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_startdate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_enddate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_starttime` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_starttime_mode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_endtime` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_endtime_mode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_location1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_location1_abbreviation` char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_location2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_location2_abbreviation` char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_location3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_location3_abbreviation` char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_location4` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_location4_abbreviation` char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_location5` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_location5_abbreviation` char(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_latitude` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_longitude` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_long_description` text COLLATE utf8_unicode_ci,
  `event_seo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_keyword` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_renewal_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_level` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_category_1` text COLLATE utf8_unicode_ci,
  `event_category_2` text COLLATE utf8_unicode_ci,
  `event_category_3` text COLLATE utf8_unicode_ci,
  `event_category_4` text COLLATE utf8_unicode_ci,
  `event_category_5` text COLLATE utf8_unicode_ci,
  `custom_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_line_number` int(11) NOT NULL DEFAULT '0',
  `inserted` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `error` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `Invoice`
--

CREATE TABLE IF NOT EXISTS `Invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL DEFAULT '0',
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `status` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subtotal_amount` decimal(10,2) DEFAULT NULL,
  `tax_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `amount` decimal(10,2) DEFAULT NULL,
  `currency` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `expire_date` date DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `hidden` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `date` (`date`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Invoice_Article`
--

CREATE TABLE IF NOT EXISTS `Invoice_Article` (
  `invoice_id` int(11) NOT NULL DEFAULT '0',
  `article_id` int(11) NOT NULL DEFAULT '0',
  `article_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `discount_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `level` tinyint(4) DEFAULT NULL,
  `level_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `renewal_date` date DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  KEY `invoice_id` (`invoice_id`),
  KEY `article_id` (`article_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Invoice_Banner`
--

CREATE TABLE IF NOT EXISTS `Invoice_Banner` (
  `invoice_id` int(11) NOT NULL DEFAULT '0',
  `banner_id` int(11) NOT NULL DEFAULT '0',
  `banner_caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `discount_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `level` tinyint(4) DEFAULT NULL,
  `level_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `renewal_date` date DEFAULT NULL,
  `impressions` mediumint(9) NOT NULL DEFAULT '0',
  `amount` decimal(10,2) DEFAULT NULL,
  KEY `invoice_id` (`invoice_id`),
  KEY `banner_id` (`banner_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Invoice_Classified`
--

CREATE TABLE IF NOT EXISTS `Invoice_Classified` (
  `invoice_id` int(11) NOT NULL DEFAULT '0',
  `classified_id` int(11) NOT NULL DEFAULT '0',
  `classified_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `discount_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `level` tinyint(4) DEFAULT NULL,
  `level_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `renewal_date` date DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  KEY `invoice_id` (`invoice_id`),
  KEY `classified_id` (`classified_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Invoice_CustomInvoice`
--

CREATE TABLE IF NOT EXISTS `Invoice_CustomInvoice` (
  `invoice_id` int(11) NOT NULL DEFAULT '0',
  `custom_invoice_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `items` text COLLATE utf8_unicode_ci NOT NULL,
  `items_price` text COLLATE utf8_unicode_ci NOT NULL,
  `subtotal` decimal(10,2) NOT NULL,
  `tax` decimal(10,2) NOT NULL DEFAULT '0.00',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  KEY `invoice_id` (`invoice_id`),
  KEY `custom_invoice_id` (`custom_invoice_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Invoice_Event`
--

CREATE TABLE IF NOT EXISTS `Invoice_Event` (
  `invoice_id` int(11) NOT NULL DEFAULT '0',
  `event_id` int(11) NOT NULL DEFAULT '0',
  `event_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `discount_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `level` tinyint(4) DEFAULT NULL,
  `level_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `renewal_date` date DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  KEY `invoice_id` (`invoice_id`),
  KEY `event_id` (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Invoice_Listing`
--

CREATE TABLE IF NOT EXISTS `Invoice_Listing` (
  `invoice_id` int(11) NOT NULL DEFAULT '0',
  `listing_id` int(11) NOT NULL DEFAULT '0',
  `listing_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `discount_id` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` tinyint(4) DEFAULT NULL,
  `level_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `renewal_date` date DEFAULT NULL,
  `categories` tinyint(4) NOT NULL,
  `extra_categories` tinyint(4) NOT NULL DEFAULT '0',
  `listingtemplate_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  KEY `invoice_id` (`invoice_id`),
  KEY `listing_id` (`listing_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Invoice_Package`
--

CREATE TABLE IF NOT EXISTS `Invoice_Package` (
  `invoice_id` int(11) NOT NULL DEFAULT '0',
  `package_id` int(11) NOT NULL DEFAULT '0',
  `package_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `items` text COLLATE utf8_unicode_ci NOT NULL,
  `items_price` text COLLATE utf8_unicode_ci NOT NULL,
  `subtotal` decimal(10,2) NOT NULL,
  `tax` decimal(10,2) NOT NULL DEFAULT '0.00',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  KEY `invoice_id` (`invoice_id`),
  KEY `package_id` (`package_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ItemStatistic`
--

CREATE TABLE IF NOT EXISTS `ItemStatistic` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ItemStatistic`
--

INSERT INTO `ItemStatistic` (`name`, `value`) VALUES
('l_pending', '10'),
('l_expiring', '0'),
('l_expired', '0'),
('l_active', '10'),
('l_suspended', '10'),
('l_added30', '10'),
('e_pending', '10'),
('e_expiring', '0'),
('e_expired', '0'),
('e_active', '10'),
('e_suspended', '10'),
('e_added30', '10'),
('b_pending', '0'),
('b_expired', '0'),
('b_active', '0'),
('b_suspended', '0'),
('b_added30', '0'),
('c_pending', '0'),
('c_expiring', '0'),
('c_expired', '0'),
('c_active', '0'),
('c_suspended', '0'),
('c_added30', '0'),
('a_pending', '0'),
('a_expiring', '0'),
('a_expired', '0'),
('a_active', '0'),
('a_suspended', '0'),
('a_added30', '0');

-- --------------------------------------------------------

--
-- Table structure for table `Lang`
--

CREATE TABLE IF NOT EXISTS `Lang` (
  `id_number` int(11) NOT NULL AUTO_INCREMENT,
  `id` char(5) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lang_enabled` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `lang_default` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `lang_order` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_number`),
  KEY `name` (`name`),
  KEY `enabled` (`lang_enabled`),
  KEY `default` (`lang_default`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `Lang`
--

INSERT INTO `Lang` (`id_number`, `id`, `name`, `lang_enabled`, `lang_default`, `lang_order`) VALUES
(1, 'en_us', 'English', 'y', 'y', 0),
(2, 'pt_br', 'Português', 'n', 'n', 1),
(3, 'es_es', 'Español', 'n', 'n', 3),
(4, 'fr_fr', 'Français', 'n', 'n', 4),
(5, 'it_it', 'Italiano', 'n', 'n', 2),
(6, 'ge_ge', 'Deutsch', 'n', 'n', 5),
(7, 'tr_tr', 'Türkçe', 'n', 'n', 6);

-- --------------------------------------------------------

--
-- Table structure for table `Leads`
--

CREATE TABLE IF NOT EXISTS `Leads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'listing/event/classified/general',
  `first_name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `entered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reply_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `forward_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'P' COMMENT 'A : read / P: unread',
  `new` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y' COMMENT 'y/n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Listing`
--

CREATE TABLE IF NOT EXISTS `Listing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL DEFAULT '0',
  `image_id` int(11) NOT NULL DEFAULT '0',
  `thumb_id` int(11) NOT NULL DEFAULT '0',
  `promotion_id` int(11) NOT NULL DEFAULT '0',
  `location_1` int(11) NOT NULL DEFAULT '0',
  `location_2` int(11) NOT NULL DEFAULT '0',
  `location_3` int(11) NOT NULL DEFAULT '0',
  `location_4` int(11) NOT NULL DEFAULT '0',
  `location_5` int(11) NOT NULL DEFAULT '0',
  `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `entered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `renewal_date` date NOT NULL DEFAULT '0000-00-00',
  `discount_id` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `show_email` enum('y','n') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `zip_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `zip5` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `latitude` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `maptuning` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `maptuning_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `long_description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `attachment_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `attachment_caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `features` text COLLATE utf8_unicode_ci NOT NULL,
  `price` int(10) DEFAULT NULL COMMENT 'price range (1-4)',
  `facebook_page` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `suspended_sitemgr` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `level` tinyint(3) NOT NULL DEFAULT '0',
  `random_number` bigint(15) NOT NULL DEFAULT '0',
  `reminder` tinyint(4) NOT NULL DEFAULT '0',
  `fulltextsearch_keyword` text COLLATE utf8_unicode_ci NOT NULL,
  `fulltextsearch_where` text COLLATE utf8_unicode_ci NOT NULL,
  `video_snippet` text COLLATE utf8_unicode_ci NOT NULL,
  `video_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `video_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `importID` int(11) NOT NULL DEFAULT '0',
  `hours_work` text COLLATE utf8_unicode_ci NOT NULL,
  `locations` text COLLATE utf8_unicode_ci NOT NULL,
  `claim_disable` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `listingtemplate_id` int(11) NOT NULL DEFAULT '0',
  `custom_checkbox0` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `custom_checkbox1` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `custom_checkbox2` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `custom_checkbox3` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `custom_checkbox4` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `custom_checkbox5` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `custom_checkbox6` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `custom_checkbox7` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `custom_checkbox8` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `custom_checkbox9` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `custom_dropdown0` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_dropdown1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_dropdown2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_dropdown3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_dropdown4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_dropdown5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_dropdown6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_dropdown7` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_dropdown8` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_dropdown9` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_text0` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_text1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_text2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_text3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_text4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_text5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_text6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_text7` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_text8` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_text9` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_short_desc0` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_short_desc1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_short_desc2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_short_desc3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_short_desc4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_short_desc5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_short_desc6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_short_desc7` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_short_desc8` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_short_desc9` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_long_desc0` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_long_desc1` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_long_desc2` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_long_desc3` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_long_desc4` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_long_desc5` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_long_desc6` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_long_desc7` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_long_desc8` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_long_desc9` text COLLATE utf8_unicode_ci NOT NULL,
  `number_views` int(11) NOT NULL DEFAULT '0',
  `avg_review` int(11) NOT NULL DEFAULT '0',
  `map_zoom` int(11) NOT NULL,
  `package_id` int(11) NOT NULL,
  `package_price` decimal(10,2) NOT NULL,
  `backlink` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `backlink_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `clicktocall_number` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clicktocall_extension` int(5) DEFAULT NULL,
  `clicktocall_date` date DEFAULT NULL,
  `last_traffic_sent` date NOT NULL,
  `custom_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `random_number` (`random_number`),
  KEY `country_id` (`location_1`),
  KEY `state_id` (`location_2`),
  KEY `region_id` (`location_3`),
  KEY `account_id` (`account_id`),
  KEY `renewal_date` (`renewal_date`),
  KEY `status` (`status`),
  KEY `promotion_id` (`promotion_id`),
  KEY `latitude` (`latitude`),
  KEY `longitude` (`longitude`),
  KEY `level` (`level`),
  KEY `city_id` (`location_4`),
  KEY `area_id` (`location_5`),
  KEY `zip_code` (`zip_code`),
  KEY `friendly_url` (`friendly_url`),
  KEY `listingtemplate_id` (`listingtemplate_id`),
  KEY `image_id` (`image_id`),
  KEY `thumb_id` (`thumb_id`),
  KEY `idx_fulltextsearch_keyword` (`fulltextsearch_keyword`(3)),
  KEY `idx_fulltextsearch_where` (`fulltextsearch_where`(3)),
  KEY `updated_date` (`updated`),
  KEY `clicktocall_number` (`clicktocall_number`),
  KEY `Listing_Promotion` (`level`,`promotion_id`,`account_id`,`title`,`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Dumping data for table `Listing`
--

INSERT INTO `Listing` (`id`, `account_id`, `image_id`, `thumb_id`, `promotion_id`, `location_1`, `location_2`, `location_3`, `location_4`, `location_5`, `updated`, `entered`, `renewal_date`, `discount_id`, `title`, `seo_title`, `friendly_url`, `email`, `show_email`, `url`, `display_url`, `address`, `address2`, `zip_code`, `zip5`, `latitude`, `longitude`, `maptuning`, `maptuning_date`, `phone`, `fax`, `description`, `seo_description`, `long_description`, `keywords`, `seo_keywords`, `attachment_file`, `attachment_caption`, `features`, `price`, `facebook_page`, `status`, `suspended_sitemgr`, `level`, `random_number`, `reminder`, `fulltextsearch_keyword`, `fulltextsearch_where`, `video_snippet`, `video_url`, `video_description`, `importID`, `hours_work`, `locations`, `claim_disable`, `listingtemplate_id`, `custom_checkbox0`, `custom_checkbox1`, `custom_checkbox2`, `custom_checkbox3`, `custom_checkbox4`, `custom_checkbox5`, `custom_checkbox6`, `custom_checkbox7`, `custom_checkbox8`, `custom_checkbox9`, `custom_dropdown0`, `custom_dropdown1`, `custom_dropdown2`, `custom_dropdown3`, `custom_dropdown4`, `custom_dropdown5`, `custom_dropdown6`, `custom_dropdown7`, `custom_dropdown8`, `custom_dropdown9`, `custom_text0`, `custom_text1`, `custom_text2`, `custom_text3`, `custom_text4`, `custom_text5`, `custom_text6`, `custom_text7`, `custom_text8`, `custom_text9`, `custom_short_desc0`, `custom_short_desc1`, `custom_short_desc2`, `custom_short_desc3`, `custom_short_desc4`, `custom_short_desc5`, `custom_short_desc6`, `custom_short_desc7`, `custom_short_desc8`, `custom_short_desc9`, `custom_long_desc0`, `custom_long_desc1`, `custom_long_desc2`, `custom_long_desc3`, `custom_long_desc4`, `custom_long_desc5`, `custom_long_desc6`, `custom_long_desc7`, `custom_long_desc8`, `custom_long_desc9`, `number_views`, `avg_review`, `map_zoom`, `package_id`, `package_price`, `backlink`, `backlink_url`, `clicktocall_number`, `clicktocall_extension`, `clicktocall_date`, `last_traffic_sent`, `custom_id`) VALUES
(1, 0, 0, 0, 0, 84, 0, 712, 9120, 0, '2015-02-21 16:34:31', '2015-02-21 14:28:58', '0000-00-00', '', 'Bronze listing test', 'Bronze listing test', 'bronze-listing-test', '', 'y', '', 'Visit Website', 'street 441', 'apratment 45', '3014', '0', '-38.102737', '-57.582531', '', '2015-02-21 16:34:31', '01455587', '', '', '', '', 'test keywrod || test || bronze', 'test keywrod, test, bronze', '', '', '', 0, '', 'A', 'n', 70, 0, 0, 'Bronze listing test test keywrod test bronze Category1 Category1,Category one', 'street 441 3014 Iraq IQ Kurdistan Arbil', '', '', '', 0, '', '', 'n', 0, 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 5, 0, 0, '0.00', 'n', '', '', 0, '0000-00-00', '0000-00-00', NULL),
(2, 0, 0, 0, 0, 1, 0, 8, 5628, 0, '2015-02-21 16:43:12', '2015-02-21 16:36:36', '2016-02-21', '', 'Siliver listing test', 'Siliver listing test', 'siliver-listing-test', 'mrqaidi@gmail.com', 'y', 'http://qaidi.info/', 'Visit Website', 'none', 'none 88', '11111', '0', '40.750096', '-73.938468', '', '2015-02-21 16:36:37', '+4915211912299', '', '', '', '', 'silver listing', 'silver listing', '', '', '', 0, '', 'A', 'n', 50, 0, 0, 'Siliver listing test silver listing Category1 Category1,Category one', 'none 11111 United States USA New York Ny New York City', '', '', '', 0, '', '', 'n', 0, 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, '0.00', 'n', '', '', 0, '0000-00-00', '0000-00-00', NULL),
(3, 0, 0, 0, 0, 195, 0, 396, 8752, 0, '2015-02-21 17:05:40', '2015-02-21 16:45:14', '2016-02-21', '', 'Gold Listing test', 'Gold Listing test', 'gold-listing-test', 'mrqaidi@gmail.com', 'y', 'http://qaidi.info/', 'Visit Website', 'none 88', 'none 88', '11111', '0', '51.507351', '-0.127758', '', '2015-02-21 16:45:14', '+4915211912299', '', 'Summary Description', 'Summary Description', '', 'gold', 'gold', '', '', '', 0, '', 'A', 'n', 30, 0, 0, 'Gold Listing test gold Summary Description Category1 Category1,Category one', 'none 88 11111 United Kingdom GB London London', '', '', '', 0, '', '', 'n', 0, 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, '0.00', 'n', '', '', 0, '0000-00-00', '0000-00-00', NULL),
(4, 0, 15, 16, 1, 1, 0, 9, 8903, 0, '2015-02-21 17:05:10', '2015-02-21 16:50:27', '0000-00-00', '', 'Diamond', 'Diamond', 'diamond', 'mrqaidi@gmail.com', 'y', 'http://qaidi.info', 'Visit Website', 'none 88', 'apratment 45', '111111', '0', '33.349001', '-96.548599', '', '2015-02-21 16:50:27', '+4915211912299', '', 'Diamonds Are A Girls Best Friend', 'Diamonds Are A Girls Best Friend', 'Diamonds Are A Girls Best Friend', '', '', '', '', 'Features', 0, 'fb.com/page', 'A', 'n', 10, 428736127380967, 0, 'Diamond Diamonds Are A Girls Best Friend Category1 Category1,Category one', 'none 88 111111 United States USA Texas Tx Anna', '<iframe type=''text/html'' width=''380'' height=''295'' src=''http://www.youtube.com/embed/rH59jVKyui8'' frameborder=''0''></iframe>', 'https://www.youtube.com/watch?v=rH59jVKyui8', 'Diamonds Are A Girls Best Friend', 0, 'Sun 8:00 am - 6:00 pm\nMon 8:00 am - 9:00 pm\nTue 8:00 am - 9:00 pm', 'Reference', 'n', 0, 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 12, 0, 0, 0, '0.00', 'n', '', '', 0, '0000-00-00', '0000-00-00', NULL),
(20, 13, 0, 0, 0, 1, 0, 1, 1, 0, '2015-02-23 03:03:37', '2015-02-23 03:03:37', '2015-12-31', '', 'Ocean Resort', 'Ocean Resort', 'ocean-resort-54ea8a7938c15', 'test_email@edirectory.com', 'y', 'http://www.edirectory.com/directory_ad.php', '', '2226 14th Avenue', '', '32960', '0', '', '', '', '0000-00-00 00:00:00', '101 742 4245', '101 742 4244', 'Sed pretium. Maecenas at erat quis quam convallis.', 'Sed pretium. Maecenas at erat quis quam convallis.', 'Sed pretium. Maecenas at erat quis quam convallis tincidunt. Nulla dictum massa sit amet risus. In auctor turpis vitae eros. Vivamus ut metus a est pretium interdum.', 'resort', 'resort', '', '', '', NULL, '', 'A', 'n', 10, 712306976050424, 0, 'Ocean Resort resort Travel Resorts Travel Hotels Travel Cruises Food and Dining Restaurants Sed pretium. Maecenas at erat quis quam convallis.', '2226 14th Avenue 32960 United States USA Florida FL Miami', '', '', '', 7, '', '', 'n', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 2, 0, 0, 0, '0.00', 'n', '', NULL, NULL, NULL, '0000-00-00', ''),
(21, 14, 0, 0, 0, 1, 0, 2, 2, 0, '2015-02-23 03:03:37', '2015-02-23 03:03:37', '2015-12-31', '', 'Coffee House', 'Coffee House', 'coffee-house-54ea8a7940cff', 'test_email@edirectory.com', 'y', 'http://www.edirectory.com/directory_ad.php', '', '305 Maple Ave West', '', '22180', '0', '', '', '', '0000-00-00 00:00:00', '123 898 8989', '123 898 8999', 'Sed pretium. Maecenas at erat quis quam convallis.', 'Sed pretium. Maecenas at erat quis quam convallis.', 'Sed pretium. Maecenas at erat quis quam convallis tincidunt. Nulla dictum massa sit amet risus. In auctor turpis vitae eros. Vivamus ut metus a est pretium interdum.', 'coffee || hot chocolate || cappucino', 'coffee,hot chocolate,cappucino', '', '', '', NULL, '', 'P', 'n', 30, 0, 0, 'Coffee House coffee hot chocolate cappucino Food and Dining Coffee Shops Food and Dining Bagel Shops Sed pretium. Maecenas at erat quis quam convallis.', '305 Maple Ave West 22180 United States USA California CA Alamo', '', '', '', 7, '', '', 'n', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, '0.00', 'n', '', NULL, NULL, NULL, '0000-00-00', ''),
(22, 15, 0, 0, 0, 1, 0, 1, 0, 0, '2015-02-23 03:03:37', '2015-02-23 03:03:37', '2015-12-31', '', 'All Sport Fitness', 'All Sport Fitness', 'all-sport-fitness-54ea8a7948ec3', 'test_email@edirectory.com', 'y', 'http://www.edirectory.com/directory_ad.php', '', '2900 S. Bristol Street', 'Post Office Box 2294', '60462', '0', '', '', '', '0000-00-00 00:00:00', '(815) 462-0019', '', 'Sed pretium. Maecenas at erat quis quam convallis.', 'Sed pretium. Maecenas at erat quis quam convallis.', 'Sed pretium. Maecenas at erat quis quam convallis tincidunt. Nulla dictum massa sit amet risus. In auctor turpis vitae eros. Vivamus ut metus a est pretium interdum.', 'sports', 'sports', '', '', '', NULL, '', 'A', 'n', 70, 0, 0, 'All Sport Fitness sports Sports Skiing Sports Tennis Sports Water Sports Sed pretium. Maecenas at erat quis quam convallis.', '2900 S. Bristol Street 60462 United States USA Florida FL', '', '', '', 7, '', '', 'n', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, '0.00', 'n', '', NULL, NULL, NULL, '0000-00-00', ''),
(23, 0, 0, 0, 0, 1, 0, 3, 3, 0, '2015-02-23 03:03:37', '2015-02-23 03:03:37', '2015-12-31', '', 'Arts Center', 'Arts Center', 'arts-center-54ea8a7951a23', 'test_email@edirectory.com', 'y', 'http://www.edirectory.com/directory_ad.php', '', '4800 Texoma Pkwy', '', '75090', '0', '', '', '', '0000-00-00 00:00:00', '303 345 5454', '303 345 4545', 'Sed pretium. Maecenas at erat quis quam convallis.', 'Sed pretium. Maecenas at erat quis quam convallis.', 'Sed pretium. Maecenas at erat quis quam convallis tincidunt. Nulla dictum massa sit amet risus. In auctor turpis vitae eros. Vivamus ut metus a est pretium interdum.', 'arts || dance || performing || studio', 'arts,dance,performing,studio', '', '', '', NULL, '', 'S', 'n', 10, 275326528842865, 0, 'Arts Center arts dance performing studio Entertainment Theaters Entertainment Concerts Sed pretium. Maecenas at erat quis quam convallis.', '4800 Texoma Pkwy 75090 United States USA Texas TX Sherman', '', '', '', 7, '', '', 'n', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, '0.00', 'n', '', NULL, NULL, NULL, '0000-00-00', ''),
(24, 16, 0, 0, 0, 1, 0, 4, 4, 0, '2015-02-23 03:03:37', '2015-02-23 03:03:37', '2015-12-31', '', 'Skin Care Center', 'Skin Care Center', 'skin-care-center-54ea8a795949b', 'test_email@edirectory.com', 'y', 'http://www.edirectory.com/directory_ad.php', '', '520 East 88th Street', '', '10128', '0', '', '', '', '0000-00-00 00:00:00', '586 258 4566', '145 256 9852', 'Sed pretium. Maecenas at erat quis quam convallis.', 'Sed pretium. Maecenas at erat quis quam convallis.', 'Sed pretium. Maecenas at erat quis quam convallis tincidunt. Nulla dictum massa sit amet risus. In auctor turpis vitae eros. Vivamus ut metus a est pretium interdum.', 'spa || day spa || laser clinic || nail || hair || salon || massage', 'spa,day spa,laser clinic,nail,hair,salon,massage', '', '', '', NULL, '', 'A', 'n', 50, 0, 0, 'Skin Care Center spa day spa laser clinic nail hair salon massage Beauty and Fitness Beauty Salons Beauty and Fitness Massage Beauty and Fitness Spas Sed pretium. Maecenas at erat quis quam convallis.', '520 East 88th Street 10128 United States USA New York NY New York', '', '', '', 7, '', '', 'n', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, 0, '0.00', 'n', '', NULL, NULL, NULL, '0000-00-00', '');

-- --------------------------------------------------------

--
-- Table structure for table `ListingCategory`
--

CREATE TABLE IF NOT EXISTS `ListingCategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `root_id` int(11) DEFAULT '0',
  `left` int(11) DEFAULT '0',
  `right` int(11) DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `thumb_id` int(11) NOT NULL DEFAULT '0',
  `image_id` int(11) NOT NULL DEFAULT '0',
  `featured` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `summary_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `active_listing` int(11) NOT NULL DEFAULT '0',
  `full_friendly_url` text COLLATE utf8_unicode_ci,
  `enabled` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `count_sub` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `active_listing` (`active_listing`),
  KEY `title1` (`title`),
  KEY `friendly_url1` (`friendly_url`),
  KEY `right` (`right`),
  KEY `root_id` (`root_id`),
  KEY `left` (`left`),
  KEY `cat_tree` (`root_id`,`left`,`right`),
  KEY `category_id_2` (`category_id`,`active_listing`),
  FULLTEXT KEY `keywords` (`keywords`,`title`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=21 ;

--
-- Dumping data for table `ListingCategory`
--

INSERT INTO `ListingCategory` (`id`, `root_id`, `left`, `right`, `title`, `category_id`, `thumb_id`, `image_id`, `featured`, `summary_description`, `seo_description`, `page_title`, `friendly_url`, `keywords`, `seo_keywords`, `content`, `active_listing`, `full_friendly_url`, `enabled`, `count_sub`) VALUES
(1, 1, 1, 2, 'Category1', 0, 0, 0, 'y', '', '', 'Category1', 'category1', 'Category1,Category one', '', '', 4, 'category1', 'y', 0),
(2, 2, 1, 8, 'Travel', 0, 0, 0, 'y', '', '', 'Travel', 'travel', '', '', '', 1, 'travel', 'y', 3),
(3, 2, 2, 3, 'Resorts', 2, 0, 0, 'y', '', '', 'Resorts', 'resorts', '', '', '', 1, 'travel/resorts', 'y', 0),
(4, 2, 4, 5, 'Hotels', 2, 0, 0, 'y', '', '', 'Hotels', 'hotels', '', '', '', 1, 'travel/hotels', 'y', 0),
(5, 2, 6, 7, 'Cruises', 2, 0, 0, 'y', '', '', 'Cruises', 'cruises', '', '', '', 1, 'travel/cruises', 'y', 0),
(6, 6, 1, 8, 'Food and Dining', 0, 0, 0, 'y', '', '', 'Food and Dining', 'food-and-dining', '', '', '', 1, 'food-and-dining', 'y', 3),
(7, 6, 2, 3, 'Restaurants', 6, 0, 0, 'y', '', '', 'Restaurants', 'restaurants', '', '', '', 1, 'food-and-dining/restaurants', 'y', 0),
(8, 6, 4, 5, 'Coffee Shops', 6, 0, 0, 'y', '', '', 'Coffee Shops', 'coffee-shops', '', '', '', 0, 'food-and-dining/coffee-shops', 'y', 0),
(9, 6, 6, 7, 'Bagel Shops', 6, 0, 0, 'y', '', '', 'Bagel Shops', 'bagel-shops', '', '', '', 0, 'food-and-dining/bagel-shops', 'y', 0),
(10, 10, 1, 8, 'Sports', 0, 0, 0, 'y', '', '', 'Sports', 'sports', '', '', '', 1, 'sports', 'y', 3),
(11, 10, 2, 3, 'Skiing', 10, 0, 0, 'y', '', '', 'Skiing', 'skiing', '', '', '', 1, 'sports/skiing', 'y', 0),
(12, 10, 4, 5, 'Tennis', 10, 0, 0, 'y', '', '', 'Tennis', 'tennis', '', '', '', 1, 'sports/tennis', 'y', 0),
(13, 10, 6, 7, 'Water Sports', 10, 0, 0, 'y', '', '', 'Water Sports', 'water-sports', '', '', '', 1, 'sports/water-sports', 'y', 0),
(14, 14, 1, 6, 'Entertainment', 0, 0, 0, 'y', '', '', 'Entertainment', 'entertainment', '', '', '', 0, 'entertainment', 'y', 2),
(15, 14, 2, 3, 'Theaters', 14, 0, 0, 'y', '', '', 'Theaters', 'theaters', '', '', '', 0, 'entertainment/theaters', 'y', 0),
(16, 14, 4, 5, 'Concerts', 14, 0, 0, 'y', '', '', 'Concerts', 'concerts', '', '', '', 0, 'entertainment/concerts', 'y', 0),
(17, 17, 1, 8, 'Beauty and Fitness', 0, 0, 0, 'y', '', '', 'Beauty and Fitness', 'beauty-and-fitness', '', '', '', 1, 'beauty-and-fitness', 'y', 3),
(18, 17, 2, 3, 'Beauty Salons', 17, 0, 0, 'y', '', '', 'Beauty Salons', 'beauty-salons', '', '', '', 1, 'beauty-and-fitness/beauty-salons', 'y', 0),
(19, 17, 4, 5, 'Massage', 17, 0, 0, 'y', '', '', 'Massage', 'massage', '', '', '', 1, 'beauty-and-fitness/massage', 'y', 0),
(20, 17, 6, 7, 'Spas', 17, 0, 0, 'y', '', '', 'Spas', 'spas', '', '', '', 1, 'beauty-and-fitness/spas', 'y', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ListingLevel`
--

CREATE TABLE IF NOT EXISTS `ListingLevel` (
  `value` int(3) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `defaultlevel` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `detail` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `images` int(3) NOT NULL DEFAULT '0',
  `has_promotion` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `has_review` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `has_sms` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `has_call` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `backlink` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `free_category` int(3) NOT NULL DEFAULT '0',
  `category_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `active` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `popular` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `featured` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `theme` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  PRIMARY KEY (`value`,`theme`),
  KEY `active_value` (`active`,`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ListingLevel`
--

INSERT INTO `ListingLevel` (`value`, `name`, `defaultlevel`, `detail`, `images`, `has_promotion`, `has_review`, `has_sms`, `has_call`, `backlink`, `price`, `free_category`, `category_price`, `active`, `popular`, `featured`, `content`, `theme`) VALUES
(70, 'bronze', 'n', 'n', 0, 'n', 'y', 'n', 'n', 'n', '0.00', 1, '5.00', 'y', '', '', '', 'default'),
(50, 'silver', 'n', 'n', 0, 'n', 'y', 'n', 'n', 'n', '99.00', 1, '10.00', 'y', '', '', '', 'default'),
(30, 'gold', 'n', 'n', 0, 'n', 'y', 'n', 'n', 'n', '199.00', 2, '15.00', 'y', '', '', '', 'default'),
(10, 'diamond', 'y', 'y', 9, 'y', 'y', 'y', 'y', 'y', '299.00', 3, '20.00', 'y', 'y', 'y', '', 'default'),
(70, 'bronze', 'n', 'y', 0, 'n', 'y', 'n', 'n', 'n', '0.00', 1, '5.00', 'y', '', 'y', '', 'realestate'),
(50, 'silver', 'n', 'y', 2, 'n', 'y', 'n', 'n', 'n', '99.00', 1, '10.00', 'y', '', 'y', '', 'realestate'),
(30, 'gold', 'n', 'y', 4, 'n', 'y', 'n', 'n', 'n', '199.00', 2, '15.00', 'y', '', 'y', '', 'realestate'),
(10, 'diamond', 'y', 'y', 9, 'y', 'y', 'y', 'y', 'y', '299.00', 3, '20.00', 'y', 'y', 'y', '', 'realestate'),
(70, 'bronze', 'n', 'n', 0, 'n', 'y', 'n', 'n', 'n', '0.00', 1, '5.00', 'y', '', '', '', 'diningguide'),
(50, 'silver', 'n', 'n', 0, 'n', 'y', 'n', 'n', 'n', '99.00', 1, '10.00', 'y', '', '', '', 'diningguide'),
(30, 'gold', 'n', 'n', 0, 'n', 'y', 'n', 'n', 'n', '199.00', 2, '15.00', 'y', '', '', '', 'diningguide'),
(10, 'diamond', 'y', 'y', 9, 'y', 'y', 'y', 'y', 'y', '299.00', 3, '20.00', 'y', 'y', 'y', '', 'diningguide'),
(70, 'bronze', 'n', 'n', 0, 'n', 'y', 'n', 'n', 'n', '0.00', 1, '5.00', 'y', '', '', '', 'contractors'),
(50, 'silver', 'n', 'n', 0, 'n', 'y', 'n', 'n', 'n', '99.00', 1, '10.00', 'y', '', '', '', 'contractors'),
(30, 'gold', 'n', 'n', 0, 'n', 'y', 'n', 'n', 'n', '199.00', 2, '15.00', 'y', '', '', '', 'contractors'),
(10, 'diamond', 'y', 'y', 9, 'y', 'y', 'y', 'y', 'y', '299.00', 3, '20.00', 'y', 'y', 'y', '', 'contractors');

-- --------------------------------------------------------

--
-- Table structure for table `ListingLevel_Field`
--

CREATE TABLE IF NOT EXISTS `ListingLevel_Field` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `theme` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `level` int(3) NOT NULL,
  `field` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `theme` (`theme`,`level`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=149 ;

--
-- Dumping data for table `ListingLevel_Field`
--

INSERT INTO `ListingLevel_Field` (`id`, `theme`, `level`, `field`) VALUES
(115, 'default', 10, 'summary_description'),
(102, 'default', 10, 'email'),
(105, 'default', 10, 'url'),
(111, 'default', 10, 'fax'),
(112, 'default', 10, 'video'),
(113, 'default', 10, 'attachment_file'),
(116, 'default', 10, 'long_description'),
(117, 'default', 10, 'hours_of_work'),
(120, 'default', 10, 'locations'),
(119, 'default', 10, 'badges'),
(121, 'default', 10, 'main_image'),
(114, 'default', 30, 'summary_description'),
(101, 'default', 30, 'email'),
(104, 'default', 30, 'url'),
(110, 'default', 30, 'fax'),
(118, 'default', 30, 'badges'),
(100, 'default', 50, 'email'),
(103, 'default', 50, 'url'),
(19, 'realestate', 10, 'summary_description'),
(20, 'realestate', 10, 'long_description'),
(21, 'realestate', 10, 'attachment_file'),
(22, 'realestate', 10, 'video'),
(23, 'realestate', 10, 'fax'),
(24, 'realestate', 10, 'url'),
(25, 'realestate', 10, 'email'),
(26, 'realestate', 10, 'badges'),
(27, 'realestate', 10, 'main_image'),
(28, 'realestate', 30, 'summary_description'),
(29, 'realestate', 30, 'long_description'),
(30, 'realestate', 30, 'attachment_file'),
(31, 'realestate', 30, 'fax'),
(32, 'realestate', 30, 'url'),
(33, 'realestate', 30, 'email'),
(34, 'realestate', 30, 'main_image'),
(35, 'realestate', 50, 'summary_description'),
(36, 'realestate', 50, 'long_description'),
(37, 'realestate', 50, 'fax'),
(38, 'realestate', 50, 'url'),
(39, 'realestate', 50, 'email'),
(40, 'realestate', 50, 'main_image'),
(41, 'realestate', 70, 'summary_description'),
(42, 'realestate', 70, 'long_description'),
(43, 'realestate', 70, 'email'),
(44, 'realestate', 70, 'main_image'),
(109, 'default', 10, 'phone'),
(108, 'default', 30, 'phone'),
(107, 'default', 50, 'phone'),
(106, 'default', 70, 'phone'),
(49, 'realestate', 10, 'phone'),
(50, 'realestate', 30, 'phone'),
(51, 'realestate', 50, 'phone'),
(52, 'realestate', 70, 'phone'),
(53, 'diningguide', 10, 'summary_description'),
(54, 'diningguide', 10, 'email'),
(55, 'diningguide', 10, 'phone'),
(56, 'diningguide', 10, 'url'),
(57, 'diningguide', 10, 'fax'),
(58, 'diningguide', 10, 'video'),
(59, 'diningguide', 10, 'attachment_file'),
(60, 'diningguide', 10, 'long_description'),
(61, 'diningguide', 10, 'hours_of_work'),
(62, 'diningguide', 10, 'locations'),
(63, 'diningguide', 10, 'badges'),
(64, 'diningguide', 10, 'main_image'),
(65, 'diningguide', 30, 'summary_description'),
(66, 'diningguide', 30, 'email'),
(67, 'diningguide', 30, 'phone'),
(68, 'diningguide', 30, 'url'),
(69, 'diningguide', 30, 'fax'),
(70, 'diningguide', 30, 'badges'),
(71, 'diningguide', 50, 'email'),
(72, 'diningguide', 50, 'phone'),
(73, 'diningguide', 50, 'url'),
(74, 'diningguide', 70, 'phone'),
(75, 'diningguide', 10, 'summary_description'),
(76, 'diningguide', 10, 'email'),
(77, 'diningguide', 10, 'phone'),
(78, 'diningguide', 10, 'url'),
(79, 'diningguide', 10, 'fax'),
(80, 'diningguide', 10, 'video'),
(81, 'diningguide', 10, 'attachment_file'),
(82, 'diningguide', 10, 'long_description'),
(83, 'diningguide', 10, 'hours_of_work'),
(84, 'diningguide', 10, 'locations'),
(85, 'diningguide', 10, 'badges'),
(86, 'diningguide', 10, 'main_image'),
(87, 'diningguide', 30, 'summary_description'),
(88, 'diningguide', 30, 'email'),
(89, 'diningguide', 30, 'phone'),
(90, 'diningguide', 30, 'url'),
(91, 'diningguide', 30, 'fax'),
(92, 'diningguide', 30, 'badges'),
(93, 'diningguide', 50, 'email'),
(94, 'diningguide', 50, 'phone'),
(95, 'diningguide', 50, 'url'),
(96, 'diningguide', 70, 'phone'),
(97, 'diningguide', 10, 'price'),
(98, 'diningguide', 10, 'fbpage'),
(99, 'diningguide', 10, 'features'),
(122, 'default', 10, 'fbpage'),
(123, 'default', 10, 'features'),
(124, 'contractors', 10, 'summary_description'),
(125, 'contractors', 10, 'email'),
(126, 'contractors', 10, 'phone'),
(127, 'contractors', 10, 'url'),
(128, 'contractors', 10, 'fax'),
(129, 'contractors', 10, 'video'),
(130, 'contractors', 10, 'attachment_file'),
(131, 'contractors', 10, 'long_description'),
(132, 'contractors', 10, 'hours_of_work'),
(133, 'contractors', 10, 'locations'),
(134, 'contractors', 10, 'badges'),
(135, 'contractors', 10, 'main_image'),
(136, 'contractors', 30, 'summary_description'),
(137, 'contractors', 30, 'email'),
(138, 'contractors', 30, 'phone'),
(139, 'contractors', 30, 'url'),
(140, 'contractors', 30, 'fax'),
(141, 'contractors', 30, 'badges'),
(142, 'contractors', 50, 'email'),
(143, 'contractors', 50, 'phone'),
(144, 'contractors', 50, 'url'),
(145, 'contractors', 70, 'phone'),
(147, 'contractors', 10, 'fbpage'),
(148, 'contractors', 10, 'features');

-- --------------------------------------------------------

--
-- Table structure for table `ListingTemplate`
--

CREATE TABLE IF NOT EXISTS `ListingTemplate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layout_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `updated` datetime NOT NULL,
  `entered` datetime NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `cat_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `editable` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `theme` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `ListingTemplate`
--

INSERT INTO `ListingTemplate` (`id`, `layout_id`, `title`, `updated`, `entered`, `status`, `price`, `cat_id`, `editable`, `theme`) VALUES
(9, 0, 'Real Estate', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'enabled', '0.00', '', 'n', 'realestate');

-- --------------------------------------------------------

--
-- Table structure for table `ListingTemplate_Field`
--

CREATE TABLE IF NOT EXISTS `ListingTemplate_Field` (
  `listingtemplate_id` int(11) NOT NULL,
  `field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fieldvalues` text COLLATE utf8_unicode_ci NOT NULL,
  `instructions` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `required` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `search` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `searchbykeyword` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `searchbyrange` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `show_order` int(11) NOT NULL DEFAULT '0',
  `enabled` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  PRIMARY KEY (`listingtemplate_id`,`field`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ListingTemplate_Field`
--

INSERT INTO `ListingTemplate_Field` (`listingtemplate_id`, `field`, `label`, `fieldvalues`, `instructions`, `required`, `search`, `searchbykeyword`, `searchbyrange`, `show_order`, `enabled`) VALUES
(9, 'custom_checkbox6', 'LANG_LABEL_TEMPLATE_POOL', '', '', '', '', '', '', 6, 'y'),
(9, 'custom_dropdown0', 'LANG_LABEL_TEMPLATE_BEDROOM', '1,2,3,4,5,6', '', '', 'y', '', '', 7, 'y'),
(9, 'custom_dropdown1', 'LANG_LABEL_TEMPLATE_BATHROOM', '1,1.5,2,2.5,3,3.5,4,4.5,5,5.5,6', '', '', '', '', '', 8, 'y'),
(9, 'custom_dropdown2', 'LANG_LABEL_TEMPLATE_TYPE', 'Commercial,Residential', '', '', 'y', '', '', 9, 'y'),
(9, 'custom_text0', 'LANG_LABEL_TEMPLATE_PRICE', '', 'Type only numbers, example: 100000', '', '', '', 'y', 10, 'y'),
(9, 'custom_text1', 'LANG_LABEL_TEMPLATE_ACRES', '', '', '', '', '', '', 11, 'y'),
(9, 'custom_text2', 'LANG_LABEL_TEMPLATE_TYPEBUILTIN', '', '', '', '', '', '', 12, 'y'),
(9, 'custom_text3', 'LANG_LABEL_TEMPLATE_SQUARE', '', '', '', '', '', 'y', 13, 'y'),
(9, 'custom_checkbox5', 'LANG_LABEL_TEMPLATE_OFFICE', '', '', '', '', '', '', 5, 'y'),
(9, 'custom_checkbox4', 'LANG_LABEL_TEMPLATE_LAUNDRYROOM', '', '', '', '', '', '', 4, 'y'),
(9, 'custom_checkbox0', 'LANG_LABEL_TEMPLATE_AIRCOND', '', '', '', '', '', '', 0, 'y'),
(9, 'custom_checkbox1', 'LANG_LABEL_TEMPLATE_DINING', '', '', '', '', '', '', 1, 'y'),
(9, 'custom_checkbox2', 'LANG_LABEL_TEMPLATE_GARAGE', '', '', '', '', '', '', 2, 'y'),
(9, 'custom_checkbox3', 'LANG_LABEL_TEMPLATE_GARBAGE', '', '', '', '', '', '', 3, 'y');

-- --------------------------------------------------------

--
-- Table structure for table `Listing_Category`
--

CREATE TABLE IF NOT EXISTS `Listing_Category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `listing_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `category_root_id` int(11) NOT NULL,
  `category_node_left` int(11) NOT NULL,
  `category_node_right` int(11) NOT NULL,
  `status` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `listing_id` (`listing_id`),
  KEY `category_id` (`category_id`),
  KEY `status` (`status`),
  KEY `category_status` (`category_id`,`status`),
  KEY `category_listing_id` (`category_id`,`category_root_id`,`listing_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=64 ;

--
-- Dumping data for table `Listing_Category`
--

INSERT INTO `Listing_Category` (`id`, `listing_id`, `category_id`, `category_root_id`, `category_node_left`, `category_node_right`, `status`) VALUES
(2, 1, 1, 1, 1, 2, 'A'),
(3, 2, 1, 1, 1, 2, 'A'),
(6, 4, 1, 1, 1, 2, 'A'),
(7, 3, 1, 1, 1, 2, 'A'),
(50, 20, 3, 2, 2, 3, 'A'),
(51, 20, 4, 2, 4, 5, 'A'),
(52, 20, 5, 2, 6, 7, 'A'),
(53, 20, 7, 6, 2, 3, 'A'),
(54, 21, 8, 6, 4, 5, 'P'),
(55, 21, 9, 6, 6, 7, 'P'),
(56, 22, 11, 10, 2, 3, 'A'),
(57, 22, 12, 10, 4, 5, 'A'),
(58, 22, 13, 10, 6, 7, 'A'),
(59, 23, 15, 14, 2, 3, 'S'),
(60, 23, 16, 14, 4, 5, 'S'),
(61, 24, 18, 17, 2, 3, 'A'),
(62, 24, 19, 17, 4, 5, 'A'),
(63, 24, 20, 17, 6, 7, 'A');

-- --------------------------------------------------------

--
-- Table structure for table `Listing_Choice`
--

CREATE TABLE IF NOT EXISTS `Listing_Choice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `editor_choice_id` int(11) NOT NULL DEFAULT '0',
  `listing_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `EditorChoice_has_Listing_FKIndex1` (`editor_choice_id`),
  KEY `EditorChoice_has_Listing_FKIndex2` (`listing_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

--
-- Dumping data for table `Listing_Choice`
--

INSERT INTO `Listing_Choice` (`id`, `editor_choice_id`, `listing_id`) VALUES
(22, 9, 3),
(21, 4, 3),
(20, 8, 4),
(19, 2, 4);

-- --------------------------------------------------------

--
-- Table structure for table `Listing_FeaturedTemp`
--

CREATE TABLE IF NOT EXISTS `Listing_FeaturedTemp` (
  `listing_id` int(11) NOT NULL,
  `listing_level` int(11) NOT NULL,
  `random_number` bigint(15) NOT NULL,
  `status` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  KEY `Listing_FeaturedTemp` (`listing_level`,`status`,`listing_id`,`random_number`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Listing_FeaturedTemp`
--

INSERT INTO `Listing_FeaturedTemp` (`listing_id`, `listing_level`, `random_number`, `status`) VALUES
(4, 10, 428736127380967, 'R'),
(20, 10, 712306976050424, 'R'),
(23, 10, 275326528842865, 'R');

-- --------------------------------------------------------

--
-- Table structure for table `Listing_LocationCounter`
--

CREATE TABLE IF NOT EXISTS `Listing_LocationCounter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_level` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `full_friendly_url` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=82 ;

--
-- Dumping data for table `Listing_LocationCounter`
--

INSERT INTO `Listing_LocationCounter` (`id`, `location_level`, `location_id`, `count`, `title`, `full_friendly_url`) VALUES
(67, 1, 1, 5, 'United States', 'united-states'),
(68, 1, 84, 1, '', ''),
(69, 1, 195, 1, '', ''),
(70, 3, 1, 2, 'Florida', 'united-states/florida'),
(71, 3, 4, 1, 'New York', 'united-states/new-york'),
(72, 3, 8, 1, '', ''),
(73, 3, 9, 1, '', ''),
(74, 3, 396, 1, '', ''),
(75, 3, 712, 1, '', ''),
(76, 4, 1, 1, 'Miami', 'united-states/florida/miami'),
(77, 4, 4, 1, 'New York', 'united-states/new-york/new-york'),
(78, 4, 5628, 1, '', ''),
(79, 4, 8752, 1, '', ''),
(80, 4, 8903, 1, '', ''),
(81, 4, 9120, 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `Listing_Summary`
--

CREATE TABLE IF NOT EXISTS `Listing_Summary` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `location_1` int(11) NOT NULL,
  `location_1_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location_1_abbreviation` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `location_1_friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location_2` int(11) NOT NULL,
  `location_2_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location_2_abbreviation` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `location_2_friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location_3` int(11) NOT NULL,
  `location_3_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location_3_abbreviation` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `location_3_friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location_4` int(11) NOT NULL,
  `location_4_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location_4_abbreviation` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `location_4_friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location_5` int(11) NOT NULL,
  `location_5_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location_5_abbreviation` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `location_5_friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_id` int(11) NOT NULL,
  `friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `show_email` enum('y','n') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `zip_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `zip5` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `latitude` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `maptuning` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `attachment_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `attachment_caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `renewal_date` date NOT NULL DEFAULT '0000-00-00',
  `level` tinyint(3) NOT NULL DEFAULT '0',
  `random_number` bigint(15) NOT NULL DEFAULT '0',
  `claim_disable` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `locations` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fulltextsearch_keyword` text COLLATE utf8_unicode_ci NOT NULL,
  `fulltextsearch_where` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_checkbox0` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `custom_checkbox1` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `custom_checkbox2` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `custom_checkbox3` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `custom_checkbox4` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `custom_checkbox5` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `custom_checkbox6` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `custom_checkbox7` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `custom_checkbox8` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `custom_checkbox9` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `custom_dropdown0` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_dropdown1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_dropdown2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_dropdown3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_dropdown4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_dropdown5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_dropdown6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_dropdown7` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_dropdown8` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_dropdown9` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_text0` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_text1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_text2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_text3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_text4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_text5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_text6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_text7` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_text8` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_text9` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_short_desc0` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_short_desc1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_short_desc2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_short_desc3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_short_desc4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_short_desc5` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_short_desc6` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_short_desc7` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_short_desc8` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_short_desc9` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `custom_long_desc0` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_long_desc1` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_long_desc2` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_long_desc3` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_long_desc4` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_long_desc5` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_long_desc6` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_long_desc7` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_long_desc8` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_long_desc9` text COLLATE utf8_unicode_ci NOT NULL,
  `number_views` int(11) NOT NULL DEFAULT '0',
  `avg_review` int(11) NOT NULL DEFAULT '0',
  `price` int(10) DEFAULT NULL COMMENT 'price range (1-4)',
  `promotion_start_date` date NOT NULL DEFAULT '0000-00-00',
  `promotion_end_date` date NOT NULL DEFAULT '0000-00-00',
  `thumb_id` int(11) NOT NULL,
  `thumb_type` set('JPG','GIF','SWF','PNG') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'JPG',
  `thumb_width` smallint(6) NOT NULL DEFAULT '0',
  `thumb_height` smallint(6) NOT NULL DEFAULT '0',
  `listingtemplate_id` int(11) NOT NULL DEFAULT '0',
  `template_layout_id` int(11) NOT NULL DEFAULT '0',
  `template_cat_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `template_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `template_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `template_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `updated` datetime NOT NULL,
  `entered` datetime NOT NULL,
  `promotion_id` int(11) DEFAULT NULL,
  `backlink` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `clicktocall_number` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clicktocall_extension` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `title_keywords_seokeywords` (`status`,`title`(100)),
  KEY `id_status` (`id`,`status`),
  KEY `clicktocall_number` (`clicktocall_number`),
  KEY `Listing_Promotion` (`level`,`promotion_id`,`account_id`,`title`,`id`),
  KEY `rating_filter` (`level`,`status`,`avg_review`),
  KEY `price_filter` (`level`,`status`,`price`),
  FULLTEXT KEY `fulltextsearch_keyword` (`fulltextsearch_keyword`),
  FULLTEXT KEY `fulltextsearch_where` (`fulltextsearch_where`),
  FULLTEXT KEY `fulltextsearch_title` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Listing_Summary`
--

INSERT INTO `Listing_Summary` (`id`, `account_id`, `location_1`, `location_1_title`, `location_1_abbreviation`, `location_1_friendly_url`, `location_2`, `location_2_title`, `location_2_abbreviation`, `location_2_friendly_url`, `location_3`, `location_3_title`, `location_3_abbreviation`, `location_3_friendly_url`, `location_4`, `location_4_title`, `location_4_abbreviation`, `location_4_friendly_url`, `location_5`, `location_5_title`, `location_5_abbreviation`, `location_5_friendly_url`, `title`, `image_id`, `friendly_url`, `email`, `show_email`, `url`, `display_url`, `address`, `address2`, `zip_code`, `zip5`, `latitude`, `longitude`, `maptuning`, `phone`, `fax`, `description`, `attachment_file`, `attachment_caption`, `status`, `renewal_date`, `level`, `random_number`, `claim_disable`, `locations`, `keywords`, `seo_keywords`, `fulltextsearch_keyword`, `fulltextsearch_where`, `custom_checkbox0`, `custom_checkbox1`, `custom_checkbox2`, `custom_checkbox3`, `custom_checkbox4`, `custom_checkbox5`, `custom_checkbox6`, `custom_checkbox7`, `custom_checkbox8`, `custom_checkbox9`, `custom_dropdown0`, `custom_dropdown1`, `custom_dropdown2`, `custom_dropdown3`, `custom_dropdown4`, `custom_dropdown5`, `custom_dropdown6`, `custom_dropdown7`, `custom_dropdown8`, `custom_dropdown9`, `custom_text0`, `custom_text1`, `custom_text2`, `custom_text3`, `custom_text4`, `custom_text5`, `custom_text6`, `custom_text7`, `custom_text8`, `custom_text9`, `custom_short_desc0`, `custom_short_desc1`, `custom_short_desc2`, `custom_short_desc3`, `custom_short_desc4`, `custom_short_desc5`, `custom_short_desc6`, `custom_short_desc7`, `custom_short_desc8`, `custom_short_desc9`, `custom_long_desc0`, `custom_long_desc1`, `custom_long_desc2`, `custom_long_desc3`, `custom_long_desc4`, `custom_long_desc5`, `custom_long_desc6`, `custom_long_desc7`, `custom_long_desc8`, `custom_long_desc9`, `number_views`, `avg_review`, `price`, `promotion_start_date`, `promotion_end_date`, `thumb_id`, `thumb_type`, `thumb_width`, `thumb_height`, `listingtemplate_id`, `template_layout_id`, `template_cat_id`, `template_title`, `template_status`, `template_price`, `updated`, `entered`, `promotion_id`, `backlink`, `clicktocall_number`, `clicktocall_extension`) VALUES
(1, 0, 84, 'Iraq', 'IQ', 'iraq', 0, '', '', '', 712, 'Kurdistan', '', 'kurdistan', 9120, 'Arbil', '', 'arbil', 0, '', '', '', 'Bronze listing test', 0, 'bronze-listing-test', '', 'y', '', 'Visit Website', 'street 441', 'apratment 45', '3014', '0', '-38.102737', '-57.582531', '', '01455587', '', '', '', '', 'A', '0000-00-00', 70, 0, 'n', '', 'test keywrod || test || bronze', 'test keywrod, test, bronze', 'Bronze listing test test keywrod test bronze Category1 Category1,Category one', 'street 441 3014 Iraq IQ Kurdistan Arbil', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 5, 0, '0000-00-00', '0000-00-00', 0, '', 0, 0, 0, 0, '0', '', '', '0.00', '2015-02-21 16:34:31', '2015-02-21 14:28:58', 0, 'n', '', 0),
(2, 0, 1, 'United States', 'USA', 'united-states', 0, '', '', '', 8, 'New York', 'Ny', 'new-york', 5628, 'New York City', '', 'new-york-city', 0, '', '', '', 'Siliver listing test', 0, 'siliver-listing-test', 'mrqaidi@gmail.com', 'y', 'http://qaidi.info/', 'Visit Website', 'none', 'none 88', '11111', '', '40.750096', '-73.938468', '', '+4915211912299', '', '', '', '', 'A', '2016-02-21', 50, 0, 'n', '', 'silver listing', 'silver listing', 'Siliver listing test silver listing Category1 Category1,Category one', 'none 11111 United States USA New York Ny New York City', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', 0, '', 0, 0, 0, 0, '0', '', '', '0.00', '2015-02-21 16:43:12', '2015-02-21 16:36:36', 0, 'n', '', 0),
(3, 0, 195, 'United Kingdom', 'GB', 'united-kingdom', 0, '', '', '', 396, 'London', '', 'london', 8752, 'London', '', 'london', 0, '', '', '', 'Gold Listing test', 0, 'gold-listing-test', 'mrqaidi@gmail.com', 'y', 'http://qaidi.info/', 'Visit Website', 'none 88', 'none 88', '11111', '0', '51.507351', '-0.127758', '', '+4915211912299', '', 'Summary Description', '', '', 'A', '2016-02-21', 30, 0, 'n', '', 'gold', 'gold', 'Gold Listing test gold Summary Description Category1 Category1,Category one', 'none 88 11111 United Kingdom GB London London', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', 0, '', 0, 0, 0, 0, '0', '', '', '0.00', '2015-02-21 17:05:40', '2015-02-21 16:45:14', 0, 'n', '', 0),
(4, 0, 1, 'United States', 'USA', 'united-states', 0, '', '', '', 9, 'Texas', 'Tx', 'texas', 8903, 'Anna', '', 'anna', 0, '', '', '', 'Diamond', 15, 'diamond', 'mrqaidi@gmail.com', 'y', 'http://qaidi.info', 'Visit Website', 'none 88', 'apratment 45', '111111', '0', '33.349001', '-96.548599', '', '+4915211912299', '', 'Diamonds Are A Girls Best Friend', '', '', 'A', '0000-00-00', 10, 428736127380967, 'n', 'Reference', '', '', 'Diamond Diamonds Are A Girls Best Friend Category1 Category1,Category one', 'none 88 111111 United States USA Texas Tx Anna', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 12, 0, 0, '2015-02-20', '2016-05-27', 16, 'JPG', 222, 134, 0, 0, '0', '', '', '0.00', '2015-02-21 17:05:10', '2015-02-21 16:50:27', 1, 'n', '', 0),
(24, 16, 1, 'United States', 'USA', 'united-states', 0, '', '', '', 4, 'New York', 'NY', 'new-york', 4, 'New York', '', 'new-york', 0, '', '', '', 'Skin Care Center', 0, 'skin-care-center-54ea8a795949b', 'test_email@edirectory.com', '', 'http://www.edirectory.com/directory_ad.php', '', '520 East 88th Street', '', '10128', '', '', '', '', '586 258 4566', '145 256 9852', 'Sed pretium. Maecenas at erat quis quam convallis.', '', '', 'A', '2015-12-31', 50, 0, 'n', '', '', '', 'Skin Care Center spa day spa laser clinic nail hair salon massage Beauty and Fitness Beauty Salons Beauty and Fitness Massage Beauty and Fitness Spas Sed pretium. Maecenas at erat quis quam convallis.', '520 East 88th Street 10128 United States USA New York NY New York', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', 0, '', 0, 0, 0, 0, '0', '', '', '0.00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'n', '', 0),
(23, 0, 1, 'United States', 'USA', 'united-states', 0, '', '', '', 3, 'Texas', 'TX', 'texas', 3, 'Sherman', '', 'sherman', 0, '', '', '', 'Arts Center', 0, 'arts-center-54ea8a7951a23', 'test_email@edirectory.com', '', 'http://www.edirectory.com/directory_ad.php', '', '4800 Texoma Pkwy', '', '75090', '', '', '', '', '303 345 5454', '303 345 4545', 'Sed pretium. Maecenas at erat quis quam convallis.', '', '', 'S', '2015-12-31', 10, 275326528842865, 'n', '', '', '', 'Arts Center arts dance performing studio Entertainment Theaters Entertainment Concerts Sed pretium. Maecenas at erat quis quam convallis.', '4800 Texoma Pkwy 75090 United States USA Texas TX Sherman', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', 0, '', 0, 0, 0, 0, '0', '', '', '0.00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'n', '', 0),
(22, 15, 1, 'United States', 'USA', 'united-states', 0, '', '', '', 1, 'Florida', 'FL', 'florida', 0, '', '', '', 0, '', '', '', 'All Sport Fitness', 0, 'all-sport-fitness-54ea8a7948ec3', 'test_email@edirectory.com', '', 'http://www.edirectory.com/directory_ad.php', '', '2900 S. Bristol Street', 'Post Office Box 2294', '60462', '', '', '', '', '(815) 462-0019', '', 'Sed pretium. Maecenas at erat quis quam convallis.', '', '', 'A', '2015-12-31', 70, 0, 'n', '', '', '', 'All Sport Fitness sports Sports Skiing Sports Tennis Sports Water Sports Sed pretium. Maecenas at erat quis quam convallis.', '2900 S. Bristol Street 60462 United States USA Florida FL', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', 0, '', 0, 0, 0, 0, '0', '', '', '0.00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'n', '', 0),
(20, 13, 1, 'United States', 'USA', 'united-states', 0, '', '', '', 1, 'Florida', 'FL', 'florida', 1, 'Miami', '', 'miami', 0, '', '', '', 'Ocean Resort', 0, 'ocean-resort-54ea8a7938c15', 'test_email@edirectory.com', '', 'http://www.edirectory.com/directory_ad.php', '', '2226 14th Avenue', '', '32960', '', '', '', '', '101 742 4245', '101 742 4244', 'Sed pretium. Maecenas at erat quis quam convallis.', '', '', 'A', '2015-12-31', 10, 712306976050424, 'n', '', '', '', 'Ocean Resort resort Travel Resorts Travel Hotels Travel Cruises Food and Dining Restaurants Sed pretium. Maecenas at erat quis quam convallis.', '2226 14th Avenue 32960 United States USA Florida FL Miami', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 2, 0, 0, '0000-00-00', '0000-00-00', 0, '', 0, 0, 0, 0, '0', '', '', '0.00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'n', '', 0),
(21, 14, 1, 'United States', 'USA', 'united-states', 0, '', '', '', 2, 'California', 'CA', 'california', 2, 'Alamo', '', 'alamo', 0, '', '', '', 'Coffee House', 0, 'coffee-house-54ea8a7940cff', 'test_email@edirectory.com', '', 'http://www.edirectory.com/directory_ad.php', '', '305 Maple Ave West', '', '22180', '', '', '', '', '123 898 8989', '123 898 8999', 'Sed pretium. Maecenas at erat quis quam convallis.', '', '', 'P', '2015-12-31', 30, 0, 'n', '', '', '', 'Coffee House coffee hot chocolate cappucino Food and Dining Coffee Shops Food and Dining Bagel Shops Sed pretium. Maecenas at erat quis quam convallis.', '305 Maple Ave West 22180 United States USA California CA Alamo', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '0000-00-00', '0000-00-00', 0, '', 0, 0, 0, 0, '0', '', '', '0.00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'n', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `MailAppList`
--

CREATE TABLE IF NOT EXISTS `MailAppList` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `module` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(1) CHARACTER SET utf8 DEFAULT NULL COMMENT 'P=Pending, F=Finished, R=Running, E=Error',
  `filename` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `categories` text CHARACTER SET utf8,
  `progress` int(11) DEFAULT NULL,
  `total_item_exported` int(11) NOT NULL DEFAULT '0',
  `last_account_exported` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `MailApp_Subscribers`
--

CREATE TABLE IF NOT EXISTS `MailApp_Subscribers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `subscriber_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `subscriber_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `subscriber_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `list_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Payment_Article_Log`
--

CREATE TABLE IF NOT EXISTS `Payment_Article_Log` (
  `payment_log_id` int(11) NOT NULL DEFAULT '0',
  `article_id` int(11) NOT NULL DEFAULT '0',
  `article_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `discount_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `level` tinyint(4) NOT NULL DEFAULT '0',
  `level_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `renewal_date` date NOT NULL DEFAULT '0000-00-00',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  KEY `payment_log_id` (`payment_log_id`),
  KEY `article_id` (`article_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Payment_Banner_Log`
--

CREATE TABLE IF NOT EXISTS `Payment_Banner_Log` (
  `payment_log_id` int(11) NOT NULL DEFAULT '0',
  `banner_id` int(11) NOT NULL DEFAULT '0',
  `banner_caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `discount_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `level` tinyint(4) NOT NULL DEFAULT '0',
  `level_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `renewal_date` date NOT NULL DEFAULT '0000-00-00',
  `impressions` mediumint(9) NOT NULL DEFAULT '0',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  KEY `payment_log_id` (`payment_log_id`),
  KEY `banner_id` (`banner_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Payment_Classified_Log`
--

CREATE TABLE IF NOT EXISTS `Payment_Classified_Log` (
  `payment_log_id` int(11) NOT NULL DEFAULT '0',
  `classified_id` int(11) NOT NULL DEFAULT '0',
  `classified_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `discount_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `level` tinyint(4) NOT NULL DEFAULT '0',
  `level_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `renewal_date` date NOT NULL DEFAULT '0000-00-00',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  KEY `payment_log_id` (`payment_log_id`),
  KEY `classified_id` (`classified_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Payment_CustomInvoice_Log`
--

CREATE TABLE IF NOT EXISTS `Payment_CustomInvoice_Log` (
  `payment_log_id` int(11) NOT NULL DEFAULT '0',
  `custom_invoice_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `items` text COLLATE utf8_unicode_ci NOT NULL,
  `items_price` text COLLATE utf8_unicode_ci NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  KEY `payment_log_id` (`payment_log_id`),
  KEY `custom_invoice_id` (`custom_invoice_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Payment_Event_Log`
--

CREATE TABLE IF NOT EXISTS `Payment_Event_Log` (
  `payment_log_id` int(11) NOT NULL DEFAULT '0',
  `event_id` int(11) NOT NULL DEFAULT '0',
  `event_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `discount_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `level` tinyint(4) NOT NULL DEFAULT '0',
  `level_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `renewal_date` date NOT NULL DEFAULT '0000-00-00',
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  KEY `payment_log_id` (`payment_log_id`),
  KEY `event_id` (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Payment_Listing_Log`
--

CREATE TABLE IF NOT EXISTS `Payment_Listing_Log` (
  `payment_log_id` int(11) NOT NULL DEFAULT '0',
  `listing_id` int(11) NOT NULL DEFAULT '0',
  `listing_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `discount_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `level` tinyint(4) NOT NULL DEFAULT '0',
  `level_label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `renewal_date` date NOT NULL DEFAULT '0000-00-00',
  `categories` tinyint(4) NOT NULL DEFAULT '0',
  `extra_categories` tinyint(4) NOT NULL DEFAULT '0',
  `listingtemplate_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  KEY `payment_log_id` (`payment_log_id`),
  KEY `listing_id` (`listing_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Payment_Log`
--

CREATE TABLE IF NOT EXISTS `Payment_Log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL DEFAULT '0',
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `transaction_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `transaction_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `transaction_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `transaction_subtotal` decimal(10,2) NOT NULL,
  `transaction_tax` decimal(10,2) NOT NULL DEFAULT '0.00',
  `transaction_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `transaction_currency` char(3) COLLATE utf8_unicode_ci NOT NULL,
  `system_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `recurring` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `return_fields` text COLLATE utf8_unicode_ci NOT NULL,
  `hidden` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Payment_Package_Log`
--

CREATE TABLE IF NOT EXISTS `Payment_Package_Log` (
  `payment_log_id` int(11) NOT NULL DEFAULT '0',
  `package_id` int(11) NOT NULL DEFAULT '0',
  `package_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `items` text COLLATE utf8_unicode_ci NOT NULL,
  `items_price` text COLLATE utf8_unicode_ci NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  KEY `payment_log_id` (`payment_log_id`),
  KEY `package_id` (`package_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Post`
--

CREATE TABLE IF NOT EXISTS `Post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_id` int(11) NOT NULL DEFAULT '0',
  `thumb_id` int(11) NOT NULL DEFAULT '0',
  `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `entered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `friendly_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumb_caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_abstract` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fulltextsearch_keyword` text COLLATE utf8_unicode_ci NOT NULL,
  `fulltextsearch_where` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `number_views` int(11) NOT NULL DEFAULT '0',
  `legacy_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `legacy_id` (`legacy_id`(7)),
  KEY `title` (`title`),
  KEY `status` (`status`),
  KEY `friendly_url` (`friendly_url`),
  KEY `entered` (`entered`),
  KEY `updated` (`updated`),
  FULLTEXT KEY `fulltextsearch_keyword` (`fulltextsearch_keyword`),
  FULLTEXT KEY `fulltextsearch_where` (`fulltextsearch_where`),
  FULLTEXT KEY `fulltextsearch_title` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Promotion`
--

CREATE TABLE IF NOT EXISTS `Promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL DEFAULT '0',
  `image_id` int(11) NOT NULL DEFAULT '0',
  `thumb_id` int(11) NOT NULL DEFAULT '0',
  `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `entered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `long_description` text COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `seo_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fulltextsearch_keyword` text COLLATE utf8_unicode_ci NOT NULL,
  `fulltextsearch_where` text COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL DEFAULT '0000-00-00',
  `end_date` date NOT NULL DEFAULT '0000-00-00',
  `random_number` bigint(15) NOT NULL DEFAULT '0',
  `conditions` text COLLATE utf8_unicode_ci NOT NULL,
  `number_views` int(11) NOT NULL DEFAULT '0',
  `visibility_start` int(11) NOT NULL,
  `visibility_end` int(11) NOT NULL,
  `realvalue` double(8,2) NOT NULL,
  `dealvalue` double(8,2) NOT NULL,
  `deal_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'monetary value' COMMENT 'percentage/monetary value',
  `amount` int(8) NOT NULL,
  `avg_review` int(11) NOT NULL,
  `friendly_url` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `listing_id` int(11) NOT NULL,
  `listing_status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `listing_level` tinyint(3) NOT NULL,
  `listing_location1` int(11) DEFAULT NULL,
  `listing_location2` int(11) DEFAULT NULL,
  `listing_location3` int(11) DEFAULT NULL,
  `listing_location4` int(11) DEFAULT NULL,
  `listing_location5` int(11) DEFAULT NULL,
  `listing_address` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_address2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_zipcode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `listing_zip5` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `listing_latitude` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `listing_longitude` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `start_date` (`start_date`),
  KEY `end_date` (`end_date`),
  KEY `name` (`name`),
  KEY `random_number` (`random_number`),
  KEY `listing_level` (`listing_level`),
  KEY `listing_id` (`listing_id`,`listing_status`),
  FULLTEXT KEY `fulltextsearch_keyword` (`fulltextsearch_keyword`),
  FULLTEXT KEY `fulltextsearch_where` (`fulltextsearch_where`),
  FULLTEXT KEY `fulltextsearch_title` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Promotion`
--

INSERT INTO `Promotion` (`id`, `account_id`, `image_id`, `thumb_id`, `updated`, `entered`, `name`, `seo_name`, `description`, `long_description`, `seo_description`, `keywords`, `seo_keywords`, `fulltextsearch_keyword`, `fulltextsearch_where`, `start_date`, `end_date`, `random_number`, `conditions`, `number_views`, `visibility_start`, `visibility_end`, `realvalue`, `dealvalue`, `deal_type`, `amount`, `avg_review`, `friendly_url`, `listing_id`, `listing_status`, `listing_level`, `listing_location1`, `listing_location2`, `listing_location3`, `listing_location4`, `listing_location5`, `listing_address`, `listing_address2`, `listing_zipcode`, `listing_zip5`, `listing_latitude`, `listing_longitude`) VALUES
(1, 0, 11, 12, '2015-02-21 17:05:10', '2015-02-21 16:41:06', 'test deal', 'test deal', 'test deal Summary Description', 'test deal  Description....Description.....', 'test deal Summary Description', 'deal test', 'deal test', 'test deal Diamond Diamonds Are A Girls Best Friend Category1 Category1,Category one deal test test deal Summary Description', 'none 88 111111 United States USA Texas Tx Anna', '2015-02-20', '2016-05-27', 239711746796697, 'There is a limit of 1 deal per person. The promotional value of this deal expires in 3 months. Deal must be presented in order to receive discount. This deal is not valid for cash back, can only be used once, does not cover tax or gratuities. This deal can not be combined with other offers.', 6, 24, 24, 75.00, 75.00, 'monetary value', 50, 0, 'test-deal', 4, 'A', 10, 1, 0, 9, 8903, 0, 'none 88', 'apratment 45', '111111', '0', '33.349001', '-96.548599');

-- --------------------------------------------------------

--
-- Table structure for table `Promotion_LocationCounter`
--

CREATE TABLE IF NOT EXISTS `Promotion_LocationCounter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_level` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `full_friendly_url` varchar(255) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Dumping data for table `Promotion_LocationCounter`
--

INSERT INTO `Promotion_LocationCounter` (`id`, `location_level`, `location_id`, `count`, `title`, `full_friendly_url`) VALUES
(16, 1, 1, 1, 'United States', 'united-states'),
(17, 3, 9, 1, '', ''),
(18, 4, 8903, 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `Promotion_Redeem`
--

CREATE TABLE IF NOT EXISTS `Promotion_Redeem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `profile_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `promotion_id` int(11) NOT NULL,
  `twittered` int(11) NOT NULL,
  `facebooked` int(11) NOT NULL,
  `network_response` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `redeem_code` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `datetime` datetime NOT NULL,
  `used` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `promotion_info` (`account_id`,`promotion_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Quicklist`
--

CREATE TABLE IF NOT EXISTS `Quicklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_type` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Report_Article`
--

CREATE TABLE IF NOT EXISTS `Report_Article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL DEFAULT '0',
  `report_type` tinyint(1) NOT NULL DEFAULT '0',
  `report_amount` int(11) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `report_info` (`article_id`,`report_type`,`date`),
  KEY `article_id` (`article_id`),
  KEY `report_type` (`report_type`),
  KEY `date` (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Report_Article_Daily`
--

CREATE TABLE IF NOT EXISTS `Report_Article_Daily` (
  `article_id` int(11) NOT NULL DEFAULT '0',
  `day` date NOT NULL DEFAULT '0000-00-00',
  `summary_view` int(11) NOT NULL DEFAULT '0',
  `detail_view` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`article_id`,`day`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Report_Article_Monthly`
--

CREATE TABLE IF NOT EXISTS `Report_Article_Monthly` (
  `article_id` int(11) NOT NULL DEFAULT '0',
  `day` date NOT NULL,
  `summary_view` int(11) NOT NULL DEFAULT '0',
  `detail_view` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`article_id`,`day`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Report_Banner`
--

CREATE TABLE IF NOT EXISTS `Report_Banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banner_id` int(11) NOT NULL DEFAULT '0',
  `report_type` tinyint(1) NOT NULL DEFAULT '0',
  `report_amount` int(11) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `report_info` (`banner_id`,`report_type`,`date`),
  KEY `banner_id` (`banner_id`),
  KEY `report_type` (`report_type`),
  KEY `date` (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Report_Banner_Daily`
--

CREATE TABLE IF NOT EXISTS `Report_Banner_Daily` (
  `banner_id` int(11) NOT NULL DEFAULT '0',
  `day` date NOT NULL DEFAULT '0000-00-00',
  `view` int(11) NOT NULL DEFAULT '0',
  `click_thru` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`banner_id`,`day`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Report_Banner_Monthly`
--

CREATE TABLE IF NOT EXISTS `Report_Banner_Monthly` (
  `banner_id` int(11) NOT NULL DEFAULT '0',
  `day` date NOT NULL,
  `view` int(11) NOT NULL DEFAULT '0',
  `click_thru` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`banner_id`,`day`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Report_Classified`
--

CREATE TABLE IF NOT EXISTS `Report_Classified` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classified_id` int(11) NOT NULL DEFAULT '0',
  `report_type` tinyint(1) NOT NULL DEFAULT '0',
  `report_amount` int(11) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `report_info` (`classified_id`,`report_type`,`date`),
  KEY `classified_id` (`classified_id`),
  KEY `report_type` (`report_type`),
  KEY `date` (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Report_Classified_Daily`
--

CREATE TABLE IF NOT EXISTS `Report_Classified_Daily` (
  `classified_id` int(11) NOT NULL DEFAULT '0',
  `day` date NOT NULL DEFAULT '0000-00-00',
  `summary_view` int(11) NOT NULL DEFAULT '0',
  `detail_view` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`classified_id`,`day`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Report_Classified_Monthly`
--

CREATE TABLE IF NOT EXISTS `Report_Classified_Monthly` (
  `classified_id` int(11) NOT NULL DEFAULT '0',
  `day` date NOT NULL,
  `summary_view` int(11) NOT NULL DEFAULT '0',
  `detail_view` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`classified_id`,`day`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Report_Event`
--

CREATE TABLE IF NOT EXISTS `Report_Event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL DEFAULT '0',
  `report_type` tinyint(1) NOT NULL DEFAULT '0',
  `report_amount` int(11) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `report_info` (`event_id`,`report_type`,`date`),
  KEY `event_id` (`event_id`),
  KEY `report_type` (`report_type`),
  KEY `date` (`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `Report_Event`
--

INSERT INTO `Report_Event` (`id`, `event_id`, `report_type`, `report_amount`, `date`) VALUES
(9, 6, 1, 1, '2015-02-25'),
(8, 4, 1, 3, '2015-02-25'),
(7, 2, 1, 5, '2015-02-25'),
(6, 4, 1, 1, '2015-02-24'),
(5, 2, 1, 5, '2015-02-24'),
(10, 1, 1, 1, '2015-02-25');

-- --------------------------------------------------------

--
-- Table structure for table `Report_Event_Daily`
--

CREATE TABLE IF NOT EXISTS `Report_Event_Daily` (
  `event_id` int(11) NOT NULL DEFAULT '0',
  `day` date NOT NULL DEFAULT '0000-00-00',
  `summary_view` int(11) NOT NULL DEFAULT '0',
  `detail_view` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`event_id`,`day`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Report_Event_Daily`
--

INSERT INTO `Report_Event_Daily` (`event_id`, `day`, `summary_view`, `detail_view`) VALUES
(1, '2015-02-23', 1, 0),
(2, '2015-02-23', 2, 0),
(4, '2015-02-23', 1, 0),
(6, '2015-02-23', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `Report_Event_Monthly`
--

CREATE TABLE IF NOT EXISTS `Report_Event_Monthly` (
  `event_id` int(11) NOT NULL DEFAULT '0',
  `day` date NOT NULL,
  `summary_view` int(11) NOT NULL DEFAULT '0',
  `detail_view` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`event_id`,`day`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Report_Listing`
--

CREATE TABLE IF NOT EXISTS `Report_Listing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `listing_id` int(11) NOT NULL DEFAULT '0',
  `report_type` tinyint(1) NOT NULL DEFAULT '0',
  `report_amount` int(11) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `report_info` (`listing_id`,`report_type`,`date`),
  KEY `listing_id` (`listing_id`),
  KEY `report_type` (`report_type`),
  KEY `date` (`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Dumping data for table `Report_Listing`
--

INSERT INTO `Report_Listing` (`id`, `listing_id`, `report_type`, `report_amount`, `date`) VALUES
(23, 4, 2, 6, '2015-02-24'),
(22, 20, 1, 81, '2015-02-24'),
(21, 4, 1, 80, '2015-02-24'),
(32, 4, 2, 1, '2015-02-25'),
(31, 1, 1, 5, '2015-02-25'),
(30, 22, 1, 3, '2015-02-25'),
(29, 24, 1, 4, '2015-02-25'),
(28, 2, 1, 4, '2015-02-25'),
(27, 3, 1, 4, '2015-02-25'),
(26, 20, 2, 1, '2015-02-25'),
(25, 4, 1, 33, '2015-02-25'),
(24, 20, 1, 33, '2015-02-25');

-- --------------------------------------------------------

--
-- Table structure for table `Report_Listing_Daily`
--

CREATE TABLE IF NOT EXISTS `Report_Listing_Daily` (
  `listing_id` int(11) NOT NULL DEFAULT '0',
  `day` date NOT NULL DEFAULT '0000-00-00',
  `summary_view` int(11) NOT NULL DEFAULT '0',
  `detail_view` int(11) NOT NULL DEFAULT '0',
  `click_thru` int(11) NOT NULL DEFAULT '0',
  `email_sent` int(11) NOT NULL DEFAULT '0',
  `phone_view` int(11) NOT NULL DEFAULT '0',
  `fax_view` int(11) NOT NULL DEFAULT '0',
  `send_phone` int(11) NOT NULL DEFAULT '0',
  `click_call` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`listing_id`,`day`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Report_Listing_Daily`
--

INSERT INTO `Report_Listing_Daily` (`listing_id`, `day`, `summary_view`, `detail_view`, `click_thru`, `email_sent`, `phone_view`, `fax_view`, `send_phone`, `click_call`) VALUES
(1, '2015-02-21', 13, 0, 0, 0, 0, 0, 0, 0),
(2, '2015-02-21', 2, 0, 1, 0, 0, 0, 0, 0),
(3, '2015-02-21', 3, 0, 0, 0, 0, 0, 0, 0),
(4, '2015-02-21', 23, 1, 0, 0, 0, 0, 0, 0),
(1, '2015-02-22', 4, 0, 0, 0, 0, 0, 0, 0),
(2, '2015-02-22', 3, 0, 0, 0, 0, 0, 0, 0),
(3, '2015-02-22', 2, 0, 0, 0, 0, 0, 0, 0),
(4, '2015-02-22', 101, 3, 0, 0, 0, 0, 0, 0),
(1, '2015-02-23', 4, 0, 0, 0, 0, 0, 0, 0),
(2, '2015-02-23', 3, 0, 0, 0, 0, 0, 0, 0),
(3, '2015-02-23', 3, 0, 0, 0, 0, 0, 0, 0),
(4, '2015-02-23', 18, 1, 0, 0, 0, 0, 0, 0),
(20, '2015-02-23', 10, 1, 0, 0, 0, 0, 0, 0),
(22, '2015-02-23', 3, 0, 0, 0, 0, 0, 0, 0),
(24, '2015-02-23', 3, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `Report_Listing_Monthly`
--

CREATE TABLE IF NOT EXISTS `Report_Listing_Monthly` (
  `listing_id` int(11) NOT NULL DEFAULT '0',
  `day` date NOT NULL,
  `summary_view` int(11) NOT NULL DEFAULT '0',
  `detail_view` int(11) NOT NULL DEFAULT '0',
  `click_thru` int(11) NOT NULL DEFAULT '0',
  `email_sent` int(11) NOT NULL DEFAULT '0',
  `phone_view` int(11) NOT NULL DEFAULT '0',
  `fax_view` int(11) NOT NULL DEFAULT '0',
  `send_phone` int(11) NOT NULL DEFAULT '0',
  `click_call` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`listing_id`,`day`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Report_Login`
--

CREATE TABLE IF NOT EXISTS `Report_Login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=34 ;

--
-- Dumping data for table `Report_Login`
--


-- --------------------------------------------------------

--
-- Table structure for table `Report_Post`
--

CREATE TABLE IF NOT EXISTS `Report_Post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL DEFAULT '0',
  `report_type` tinyint(1) NOT NULL DEFAULT '0',
  `report_amount` int(11) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `report_info` (`post_id`,`report_type`,`date`),
  KEY `post_id` (`post_id`),
  KEY `report_type` (`report_type`),
  KEY `date` (`date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Report_Post_Daily`
--

CREATE TABLE IF NOT EXISTS `Report_Post_Daily` (
  `post_id` int(11) NOT NULL DEFAULT '0',
  `day` date NOT NULL DEFAULT '0000-00-00',
  `summary_view` int(11) NOT NULL DEFAULT '0',
  `detail_view` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`post_id`,`day`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Report_Post_Monthly`
--

CREATE TABLE IF NOT EXISTS `Report_Post_Monthly` (
  `post_id` int(11) NOT NULL DEFAULT '0',
  `day` date NOT NULL,
  `summary_view` int(11) NOT NULL DEFAULT '0',
  `detail_view` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`post_id`,`day`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Report_Promotion`
--

CREATE TABLE IF NOT EXISTS `Report_Promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promotion_id` int(11) NOT NULL DEFAULT '0',
  `report_type` tinyint(1) NOT NULL DEFAULT '0',
  `report_amount` int(11) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `report_info` (`promotion_id`,`report_type`,`date`),
  KEY `promotion_id` (`promotion_id`),
  KEY `report_type` (`report_type`),
  KEY `date` (`date`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `Report_Promotion`
--

INSERT INTO `Report_Promotion` (`id`, `promotion_id`, `report_type`, `report_amount`, `date`) VALUES
(8, 1, 2, 1, '2015-02-24'),
(7, 1, 1, 82, '2015-02-24'),
(10, 1, 2, 1, '2015-02-25'),
(9, 1, 1, 33, '2015-02-25');

-- --------------------------------------------------------

--
-- Table structure for table `Report_Promotion_Daily`
--

CREATE TABLE IF NOT EXISTS `Report_Promotion_Daily` (
  `promotion_id` int(11) NOT NULL DEFAULT '0',
  `day` date NOT NULL DEFAULT '0000-00-00',
  `summary_view` int(11) NOT NULL DEFAULT '0',
  `detail_view` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`promotion_id`,`day`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Report_Promotion_Daily`
--

INSERT INTO `Report_Promotion_Daily` (`promotion_id`, `day`, `summary_view`, `detail_view`) VALUES
(1, '2015-02-21', 25, 2),
(1, '2015-02-22', 100, 1),
(1, '2015-02-23', 16, 1);

-- --------------------------------------------------------

--
-- Table structure for table `Report_Promotion_Monthly`
--

CREATE TABLE IF NOT EXISTS `Report_Promotion_Monthly` (
  `promotion_id` int(11) NOT NULL DEFAULT '0',
  `day` date NOT NULL,
  `summary_view` int(11) NOT NULL DEFAULT '0',
  `detail_view` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`promotion_id`,`day`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Report_Statistic`
--

CREATE TABLE IF NOT EXISTS `Report_Statistic` (
  `search_date` datetime NOT NULL,
  `module` char(1) CHARACTER SET utf8 NOT NULL,
  `keyword` varchar(50) CHARACTER SET utf8 NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `location_1` int(11) NOT NULL DEFAULT '0',
  `location_2` int(11) NOT NULL DEFAULT '0',
  `location_3` int(11) NOT NULL DEFAULT '0',
  `location_4` int(11) NOT NULL DEFAULT '0',
  `location_5` int(11) NOT NULL DEFAULT '0',
  `search_where` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  KEY `search_date` (`search_date`),
  KEY `module` (`module`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Report_Statistic`
--

INSERT INTO `Report_Statistic` (`search_date`, `module`, `keyword`, `category_id`, `location_1`, `location_2`, `location_3`, `location_4`, `location_5`, `search_where`) VALUES
('2015-02-25 18:51:25', 'l', '', 0, 1, 0, 4, 4, 0, ''),
('2015-02-25 18:35:19', 'l', '', 0, 1, 0, 1, 1, 0, ''),
('2015-02-25 14:30:52', 'l', '', 1, 0, 0, 0, 0, 0, ''),
('2015-02-25 14:25:56', 'd', '', 1, 0, 0, 0, 0, 0, ''),
('2015-02-24 18:12:02', 'l', '', 2, 0, 0, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `Report_Statistic_Daily`
--

CREATE TABLE IF NOT EXISTS `Report_Statistic_Daily` (
  `day` date NOT NULL,
  `module` char(1) CHARACTER SET utf8 NOT NULL,
  `key` varchar(255) CHARACTER SET utf8 NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 NOT NULL,
  `quantity` int(11) NOT NULL,
  KEY `key` (`key`),
  KEY `module` (`module`),
  KEY `day` (`day`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Report_Statistic_Daily`
--

INSERT INTO `Report_Statistic_Daily` (`day`, `module`, `key`, `value`, `quantity`) VALUES
('2015-02-21', 'l', 'categories', 'Category1', 12),
('2015-02-21', 'd', 'categories', 'Category1', 1),
('2015-02-23', 'l', 'categories', 'Food and Dining', 2),
('2015-02-23', 'e', 'locations', 'United States >> Florida >> Miami', 1),
('2015-02-23', 'l', 'categories', 'Sports', 1),
('2015-02-23', 'l', 'categories', 'Travel', 1),
('2015-02-23', 'l', 'categories', 'Beauty and Fitness', 1),
('2015-02-23', 'd', 'categories', 'Category1', 1),
('2015-02-23', 'l', 'categories', 'Category1', 1),
('2015-02-23', 'l', 'locations', 'United States >> Florida >> Miami', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Review`
--

CREATE TABLE IF NOT EXISTS `Review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `item_id` int(11) NOT NULL DEFAULT '0',
  `member_id` int(11) NOT NULL,
  `added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `review_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `review` text COLLATE utf8_unicode_ci,
  `reviewer_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reviewer_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reviewer_location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rating` tinyint(2) NOT NULL DEFAULT '0',
  `approved` int(1) NOT NULL DEFAULT '0',
  `response` text COLLATE utf8_unicode_ci NOT NULL,
  `responseapproved` int(1) NOT NULL DEFAULT '0',
  `like` int(11) NOT NULL,
  `dislike` int(11) NOT NULL,
  `like_ips` text COLLATE utf8_unicode_ci NOT NULL,
  `dislike_ips` text COLLATE utf8_unicode_ci NOT NULL,
  `new` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y' COMMENT 'y/n',
  PRIMARY KEY (`id`),
  KEY `approved` (`approved`),
  KEY `item_info` (`item_type`,`item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `Setting`
--

CREATE TABLE IF NOT EXISTS `Setting` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Setting`
--

INSERT INTO `Setting` (`name`, `value`) VALUES
('custom_classified_feature', 'on'),
('custom_blog_feature', 'on'),
('custom_banner_feature', 'on'),
('default_url', 'edir.qaidi.info'),
('custom_article_feature', 'on'),
('todo_theme', 'yes'),
('todo_paymentgateway', 'yes'),
('todo_pricing', 'yes'),
('todo_invoice', 'yes'),
('todo_email', 'yes'),
('todo_emailnotification', 'yes'),
('phpMailer_error', '1'),
('todo_googleads', 'yes'),
('todo_googlemaps', 'yes'),
('todo_googleanalytics', 'yes'),
('todo_headerlogo', 'yes'),
('todo_noimage', 'yes'),
('todo_claim', 'yes'),
('todo_langcenter', 'yes'),
('todo_levels', 'yes'),
('todo_emailconfig', 'yes'),
('sitemgr_email', ''),
('sitemgr_send_email', 'on'),
('sitemgr_listing_email', ''),
('sitemgr_event_email', ''),
('sitemgr_banner_email', ''),
('sitemgr_classified_email', ''),
('sitemgr_article_email', ''),
('sitemgr_account_email', ''),
('sitemgr_contactus_email', ''),
('sitemgr_support_email', ''),
('sitemgr_payment_email', ''),
('sitemgr_rate_email', ''),
('sitemgr_claim_email', ''),
('invoice_company', 'Your Company, Inc.'),
('invoice_address', '123 Main Street'),
('invoice_city', 'Arlington'),
('invoice_state', 'VA'),
('invoice_zipcode', '22207'),
('invoice_phone', '703.914.0770'),
('invoice_country', 'USA'),
('invoice_fax', '703.914.0770'),
('invoice_email', ''),
('invoice_notes', ''),
('invoice_image', ''),
('invoice_payableto', 'Your Company, Inc.'),
('review_listing_enabled', 'on'),
('review_article_enabled', 'on'),
('review_approve', 'on'),
('review_manditory', 'on'),
('claim_approve', 'on'),
('claim_deny', 'on'),
('claim_approveemail', 'on'),
('claim_denyemail', 'on'),
('import_enable_listing_active', ''),
('import_defaultlevel', '10'),
('import_sameaccount', ''),
('import_account_id', ''),
('import_from_export', ''),
('last_report_rollup', '2015-02-24'),
('blog_featuredcategory', 'on'),
('import_featured_categs', '1'),
('last_listing_reminder', '0'),
('edir_default_language', 'en_us'),
('edir_languages', 'en_us'),
('edir_languagenames', 'English'),
('edir_language', 'en_us'),
('emailconf_method', 'smtp'),
('emailconf_host', ''),
('emailconf_port', '465'),
('emailconf_auth', 'secure'),
('emailconf_email', ''),
('emailconf_username', ''),
('emailconf_password', ''),
('foreignaccount_openid', 'on'),
('foreignaccount_facebook', 'on'),
('foreignaccount_facebook_apikey', ''),
('last_datetime_cronmgr', '0000-00-00 00:00:00'),
('last_datetime_dailymaintenance', '2015-02-24 17:17:33'),
('last_datetime_import', '2015-02-23 03:03:37'),
('last_datetime_randomizer', '2015-02-23 16:05:58'),
('last_datetime_renewalreminder', '2015-02-23 16:05:59'),
('last_datetime_reportrollup', '2015-02-24 17:17:34'),
('last_datetime_sitemap', '2015-02-24 17:17:34'),
('last_datetime_statisticreport', '2015-02-24 17:17:35'),
('last_cronmgr_run', '0'),
('featuredcategory', 'on'),
('listing_featuredcategory', 'on'),
('article_featuredcategory', 'on'),
('classified_featuredcategory', 'on'),
('event_featuredcategory', 'on'),
('article_approve_free', 'on'),
('article_approve_paid', 'on'),
('listing_approve_updated', 'on'),
('banner_approve_paid', 'on'),
('event_approve_updated', 'on'),
('classified_approve_paid', 'on'),
('banner_approve_free', 'on'),
('classified_approve_updated', 'on'),
('event_approve_free', 'on'),
('event_approve_paid', 'on'),
('article_approve_updated', 'on'),
('classified_approve_free', 'on'),
('banner_approve_updated', 'on'),
('new_listing_email', 'on'),
('update_listing_email', 'on'),
('new_event_email', 'on'),
('update_event_email', 'on'),
('new_classified_email', 'on'),
('update_classified_email', 'on'),
('new_article_email', 'on'),
('update_article_email', 'on'),
('new_banner_email', 'on'),
('update_banner_email', 'on'),
('todo_approvalconfig', 'yes'),
('foreignaccount_facebook_apisecret', '4188275bd07967fd4289213cedc94772'),
('todo_locations', 'done'),
('emailconf_protocol', 'ssl'),
('payment_tax_status', 'on'),
('payment_tax_value', '10'),
('maintenance_mode', 'off'),
('custom_event_feature', 'on'),
('custom_promotion_feature', 'on'),
('promotion_force_redeem_by_facebook', '0'),
('foreignaccount_twitter_apikey', ''),
('last_listing_randomizer_domain', '0'),
('foreignaccount_twitter_apisecret', ''),
('foreignaccount_twitter_mobile_apikey', ''),
('foreignaccount_twitter_mobile_apisecret', ''),
('last_listing_traffic', '0'),
('last_promotion_randomizer_domain', '0'),
('last_event_randomizer_domain', '0'),
('last_banner_randomizer_domain', '0'),
('last_classified_randomizer_domain', '0'),
('last_article_randomizer_domain', '0'),
('custom_has_promotion', 'on'),
('foreignaccount_facebook_apiid', '490613937707212'),
('import_account_id_event', ''),
('listing_approve_paid', 'on'),
('listing_approve_free', 'on'),
('twitter_account', ''),
('setting_linkedin_link', ''),
('setting_facebook_link', ''),
('twilio_enabled_sms', ''),
('twilio_enabled_call', ''),
('twilio_account_sid', ''),
('twilio_auth_token', ''),
('twilio_number', ''),
('twilio_clicktocall_message', 'You are currently dialing [ITEM_TITLE], please hold while we connect you.'),
('email_traffic_listing_10', 'on'),
('review_promotion_enabled', 'on'),
('commenting_edir', 'on'),
('commenting_fb', 'on'),
('slider_feature', 'on'),
('commenting_fb_user_id', '696951136'),
('commenting_fb_number_comments', ''),
('review_blog_enabled', 'on'),
('import_automatic_start', '1'),
('import_automatic_start_event', '1'),
('last_favicon_id', '1'),
('foreignaccount_google', 'on'),
('scheme_custom', 'on'),
('scheme_realestate_customized', 'off'),
('scheme_updatefile', 'off'),
('scheme_change_images', 'off'),
('import_sameaccount_event', ''),
('import_update_listings', ''),
('import_update_friendlyurl', ''),
('import_from_export_event', ''),
('import_update_events', ''),
('import_update_friendlyurl_event', ''),
('import_featured_categs_event', '1'),
('import_enable_event_active', ''),
('import_defaultlevel_event', '10'),
('button_share_facebook', 'on'),
('button_share_google', 'on'),
('button_share_pinterest', 'on'),
('pendingReviews_per_page', '2'),
('sitemgr_blog_email', ''),
('sitemgr_import_email', ''),
('listing_price_symbol', '$'),
('listing_price_1_from', '0'),
('listing_price_1_to', '15'),
('listing_price_2_from', '16'),
('listing_price_2_to', '30'),
('listing_price_3_from', '31'),
('listing_price_3_to', '40'),
('listing_price_4_from', '41'),
('listing_price_4_to', ''),
('foreignaccount_twitter_oauthtoken', ''),
('foreignaccount_twitter_oauthsecret', ''),
('contact_address', ''),
('contact_zipcode', ''),
('contact_country', ''),
('contact_state', ''),
('contact_city', ''),
('contact_phone', ''),
('contact_email', ''),
('contact_mapzoom', ''),
('contact_latitude', ''),
('contact_longitude', ''),
('scheme_default_customized', 'on'),
('arcamailer_customer_id', '09da52e2db59f657d8acf0662d7c307c'),
('arcamailer_customer_name', 'Ayman Qaidi'),
('arcamailer_customer_email', 'mrqaidi@gmail.com'),
('arcamailer_customer_country', 'Iraq'),
('arcamailer_customer_timezone', '(GMT) Dublin, Edinburgh, Lisbon, London'),
('arcamailer_enable_list', 'on'),
('arcamailer_list_label', 'Signup for our Newsletter'),
('arcamailer_customer_listname', 'MrQaidi  Directory Newsletter'),
('arcamailer_customer_listid', '047f9276c30a3d16de6223587c1619b6'),
('mailapp_via_cron', ''),
('gmaps_scroll', ''),
('gmaps_max_markers', '1000'),
('edirectory_api_enabled', 'on'),
('twitter_widget', '<a class="twitter-timeline" href="https://twitter.com/MrQaidi988" data-widget-id="487364081871908864">Tweets by @MrQaidi988</a>\r\n<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?''http'':''https'';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>'),
('appbuilder_app_name', 'Demo Directory'),
('appbuilder_step_1', 'done'),
('edirectory_api_key', '6269-997c-c8ea-29ec-01f7-dbf2-15a3-2b2c'),
('appbuilder_step_2', 'done'),
('appbuilder_colorscheme', '16A085-2C3E50'),
('appbuilder_step_3', 'done'),
('appbuilder_about_email', ''),
('appbuilder_about_phone', ''),
('appbuilder_about_website', 'http://edir.qaidi.info'),
('appbuilder_about_text', ''),
('appbuilder_step_4', 'done'),
('appbuilder_step_6', 'done'),
('appbuilder_previewpassword', '94173'),
('appbuilder_splash_id', '2'),
('appbuilder_splash_extension', 'png'),
('appbuilder_icon_id', '2'),
('appbuilder_icon_extension', 'png'),
('appbuilder_pendingdownload', 'no'),
('appbuilder_pendingdownload_total', '0'),
('appbuilder_build_done', 'yes'),
('last_datetime_listingtraffic', '2015-02-23 16:05:58'),
('last_datetime_location_update', '2015-02-25 14:49:43'),
('last_datetime_import_events', '2015-02-23 03:24:35'),
('last_datetime_rollback_import', '2015-02-23 02:59:54');

-- --------------------------------------------------------

--
-- Table structure for table `Setting_Google`
--

CREATE TABLE IF NOT EXISTS `Setting_Google` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `Setting_Google`
--

INSERT INTO `Setting_Google` (`id`, `name`, `value`) VALUES
(1, 'google_ad_client', ''),
(2, 'google_maps_key', ''),
(3, 'google_analytics_account', ''),
(4, 'google_analytics_front', ''),
(5, 'google_analytics_members', ''),
(6, 'google_analytics_sitemgr', ''),
(7, 'google_ad_channel', ''),
(8, 'google_ad_status', 'off'),
(9, 'google_maps_status', 'on'),
(10, 'google_ad_type', 'text_image'),
(11, 'google_tag_status', 'off'),
(12, 'google_tag_client', '');

-- --------------------------------------------------------

--
-- Table structure for table `Setting_Location`
--

CREATE TABLE IF NOT EXISTS `Setting_Location` (
  `id` tinyint(4) NOT NULL,
  `default_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_plural` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `show` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Setting_Location`
--

INSERT INTO `Setting_Location` (`id`, `default_id`, `name`, `name_plural`, `enabled`, `show`) VALUES
(1, 0, 'COUNTRY', 'COUNTRIES', 'y', 'n'),
(2, 0, 'REGION', 'REGIONS', 'n', 'b'),
(3, 0, 'STATE', 'STATES', 'y', 'b'),
(4, 0, 'CITY', 'CITIES', 'y', 'b'),
(5, 0, 'NEIGHBORHOOD', 'NEIGHBORHOODS', 'n', 'b');

-- --------------------------------------------------------

--
-- Table structure for table `Setting_Navigation`
--

CREATE TABLE IF NOT EXISTS `Setting_Navigation` (
  `order` int(11) NOT NULL,
  `label` varchar(100) CHARACTER SET utf8 NOT NULL,
  `link` varchar(255) CHARACTER SET utf8 NOT NULL,
  `area` varchar(20) CHARACTER SET utf8 NOT NULL,
  `custom` char(1) CHARACTER SET utf8 NOT NULL,
  `theme` varchar(30) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`order`,`area`,`theme`),
  KEY `label` (`label`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Setting_Navigation`
--

INSERT INTO `Setting_Navigation` (`order`, `label`, `link`, `area`, `custom`, `theme`) VALUES
(0, 'Home', 'NON_SECURE_URL', 'header', 'n', 'default'),
(1, 'Listings', 'LISTING_DEFAULT_URL', 'header', 'n', 'default'),
(2, 'Events', 'EVENT_DEFAULT_URL', 'header', 'n', 'default'),
(3, 'Classifieds', 'CLASSIFIED_DEFAULT_URL', 'header', 'n', 'default'),
(4, 'Articles', 'ARTICLE_DEFAULT_URL', 'header', 'n', 'default'),
(5, 'Deals', 'PROMOTION_DEFAULT_URL', 'header', 'n', 'default'),
(6, 'Blog', 'BLOG_DEFAULT_URL', 'header', 'n', 'default'),
(7, 'Advertise With Us', 'ALIAS_ADVERTISE_URL_DIVISOR', 'header', 'n', 'default'),
(8, 'Contact Us', 'ALIAS_CONTACTUS_URL_DIVISOR', 'header', 'n', 'default'),
(7, 'My Favorites', 'favorites', 'tabbar', 'n', 'default'),
(6, 'Classifieds', 'classifieds', 'tabbar', 'n', 'default'),
(5, 'Articles', 'articles', 'tabbar', 'n', 'default'),
(4, 'Events', 'events', 'tabbar', 'n', 'default'),
(3, 'Reviews', 'reviews', 'tabbar', 'n', 'default'),
(2, 'Deals', 'deals', 'tabbar', 'n', 'default'),
(1, 'Nearby', 'nearby', 'tabbar', 'n', 'default'),
(0, 'Listings', 'listings', 'tabbar', 'n', 'default'),
(8, 'My Account', 'account', 'tabbar', 'n', 'default'),
(9, 'About Us', 'about', 'tabbar', 'n', 'default');

-- --------------------------------------------------------

--
-- Table structure for table `Setting_Payment`
--

CREATE TABLE IF NOT EXISTS `Setting_Payment` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Setting_Payment`
--

INSERT INTO `Setting_Payment` (`name`, `value`) VALUES
('PAYPAL_ACCOUNT', ''),
('PAYPALAPI_USERNAME', ''),
('PAYPALAPI_PASSWORD', ''),
('PAYPALAPI_SIGNATURE', ''),
('PAYFLOW_LOGIN', ''),
('PAYFLOW_PARTNER', ''),
('TWOCHECKOUT_LOGIN', ''),
('PSIGATE_STOREID', ''),
('PSIGATE_PASSPHRASE', ''),
('WORLDPAY_INSTID', ''),
('ITRANSACT_VENDORID', ''),
('AUTHORIZE_LOGIN', ''),
('AUTHORIZE_TXNKEY', ''),
('LINKPOINT_CONFIGFILE', ''),
('LINKPOINT_KEYFILE', ''),
('SIMPLEPAY_ACCESSKEY', ''),
('SIMPLEPAY_SECRETKEY', ''),
('PAYPAL_STATUS', 'on'),
('PAYPALAPI_STATUS', 'off'),
('PAYFLOW_STATUS', 'off'),
('TWOCHECKOUT_STATUS', 'off'),
('PSIGATE_STATUS', 'off'),
('WORLDPAY_STATUS', 'off'),
('ITRANSACT_STATUS', 'off'),
('AUTHORIZE_STATUS', 'on'),
('LINKPOINT_STATUS', 'off'),
('SIMPLEPAY_STATUS', 'on'),
('SIMPLEPAY_RECURRING', 'off'),
('PAYPAL_RECURRING', 'off'),
('LINKPOINT_RECURRING', 'off'),
('AUTHORIZE_RECURRING', 'off'),
('SIMPLEPAY_RECURRINGCYCLE', ' '),
('SIMPLEPAY_RECURRINGUNIT', ' '),
('SIMPLEPAY_RECURRINGTIMES', ' '),
('PAYPAL_RECURRINGCYCLE', ' '),
('PAYPAL_RECURRINGTIMES', ' '),
('PAYPAL_RECURRINGUNIT', ' '),
('LINKPOINT_RECURRINGTYPE', ''),
('AUTHORIZE_RECURRINGLENGTH', ' '),
('AUTHORIZE_RECURRINGUNIT', 'months'),
('CURRENCY_SYMBOL', '$'),
('LISTING_RENEWAL_PERIOD', '1Y'),
('EVENT_RENEWAL_PERIOD', '1M'),
('BANNER_RENEWAL_PERIOD', '1Y'),
('CLASSIFIED_RENEWAL_PERIOD', '30D'),
('ARTICLE_RENEWAL_PERIOD', '1Y'),
('INVOICEPAYMENT_FEATURE', 'on'),
('MANUALPAYMENT_FEATURE', 'on'),
('PAYMENT_CURRENCY', 'USD'),
('PAGSEGURO_STATUS', 'off'),
('PAGSEGURO_EMAIL', ''),
('PAGSEGURO_TOKEN', '');

-- --------------------------------------------------------

--
-- Table structure for table `Setting_Search_Tag`
--

CREATE TABLE IF NOT EXISTS `Setting_Search_Tag` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Setting_Social_Network`
--

CREATE TABLE IF NOT EXISTS `Setting_Social_Network` (
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Setting_Social_Network`
--

INSERT INTO `Setting_Social_Network` (`name`, `value`, `label`) VALUES
('general_see_profile', 'no', 'LANG_SITEMGR_SN_SEE_PROFILE'),
('listing_see_results', 'no', 'LANG_SITEMGR_SN_SEE_RESULTS'),
('listing_send_email_to_friend', 'no', 'LANG_SITEMGR_SN_SEND_EMAIL_TO_FRIEND'),
('listing_print', 'no', 'LANG_SITEMGR_SN_PRINT'),
('listing_see_comments', 'no', 'LANG_SITEMGR_SN_SEE_COMMENTS'),
('listing_rate', 'yes', 'LANG_SITEMGR_SN_RATE'),
('listing_see_detail', 'no', 'LANG_SITEMGR_SN_SEE_DETAIL'),
('event_see_results', 'no', 'LANG_SITEMGR_SN_SEE_RESULTS'),
('event_send_email_to_friend', 'no', 'LANG_SITEMGR_SN_SEND_EMAIL_TO_FRIEND'),
('event_print', 'no', 'LANG_SITEMGR_SN_PRINT'),
('event_see_detail', 'no', 'LANG_SITEMGR_SN_SEE_DETAIL'),
('classified_see_results', 'no', 'LANG_SITEMGR_SN_SEE_RESULTS'),
('classified_send_email_to_friend', 'no', 'LANG_SITEMGR_SN_SEND_EMAIL_TO_FRIEND'),
('classified_print', 'no', 'LANG_SITEMGR_SN_PRINT'),
('classified_see_detail', 'no', 'LANG_SITEMGR_SN_SEE_DETAIL'),
('article_see_results', 'no', 'LANG_SITEMGR_SN_SEE_RESULTS'),
('article_send_email_to_friend', 'no', 'LANG_SITEMGR_SN_SEND_EMAIL_TO_FRIEND'),
('article_print', 'no', 'LANG_SITEMGR_SN_PRINT'),
('article_see_comments', 'no', 'LANG_SITEMGR_SN_SEE_COMMENTS'),
('article_rate', 'yes', 'LANG_SITEMGR_SN_RATE'),
('article_see_detail', 'no', 'LANG_SITEMGR_SN_SEE_DETAIL'),
('promotion_see_results', 'no', 'LANG_SITEMGR_SN_SEE_RESULTS'),
('promotion_rate', 'yes', 'LANG_SITEMGR_SN_RATE'),
('promotion_see_detail', 'no', 'LANG_SITEMGR_SN_SEE_DETAIL'),
('promotion_see_comments', 'no', 'LANG_SITEMGR_SN_SEE_COMMENTS');

-- --------------------------------------------------------

--
-- Table structure for table `Slider`
--

CREATE TABLE IF NOT EXISTS `Slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `summary` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alternative_text` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title_text` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slide_order` int(11) NOT NULL,
  `target` varchar(10) CHARACTER SET utf8 NOT NULL DEFAULT 'self',
  PRIMARY KEY (`id`),
  KEY `order` (`slide_order`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `Slider`
--

INSERT INTO `Slider` (`id`, `image_id`, `title`, `summary`, `alternative_text`, `title_text`, `link`, `price`, `slide_order`, `target`) VALUES
(1, 17, 'Village Florist', 'Flowers are gifts! Visit our floriculture and find the perfect gift for that special person.', '', '', '#', '', 1, 'self'),
(2, 0, '', '', '', '', '', '', 2, 'blank'),
(3, 0, '', '', '', '', '', '', 3, 'blank'),
(4, 0, '', '', '', '', '', '', 4, 'blank');

-- --------------------------------------------------------

--
-- Table structure for table `Timeline`
--

CREATE TABLE IF NOT EXISTS `Timeline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `item_id` int(11) NOT NULL,
  `action` set('new','edit') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'new',
  `datetime` datetime NOT NULL,
  `new` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'y',
  PRIMARY KEY (`id`),
  KEY `item_type` (`item_type`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
