<?php

	function get_domain($url)
	{
	  $pieces = parse_url($url);
	  $domain = isset($pieces['host']) ? $pieces['host'] : '';
	  if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
		return $regs['domain'];
	  }
	  return false;
	}
	function create_config($post_details)
	{
		$fp = fopen('../conf/my_config.php', 'w');
		
		if ($fp)
		{
			$content = "<?php \n".
				" \n".
				"/* Database info */ \n".
			
				" define('_DIRECTORYDB_HOST',  '" . $post_details['db_host'] . "'); \n".
				" \n".
				
				" define('_DIRECTORYDB_USER',  '" . $post_details['db_username'] . "'); \n".
				" \n".
				
				" define('_DIRECTORYDB_PASS',  '" . $post_details['db_password'] . "'); \n".
				" \n".
				
				" define('_DIRECTORYDB_NAME',  '" . $post_details['db_name'] . "'); \n".
				" \n".
				
							
				" define('EDIRECTORY_FOLDER',  '" . $post_details['subfolder'] . "'); \n".
				" \n".
											
				" define('DB_NAME_PREFIX',  '" . $post_details['db_name_prefix'] . "'); \n".
				" \n".
				
				" define('qaidi_key',  '" . $post_details['lic_key'] . "'); \n".
				" \n".
				
				" define('qaidi_installed',  true); \n".
				" \n".
				
				
				"?>";

			fputs($fp, $content); 
			fclose($fp); 
			
			$output['display'] = '<div class="msg-win">The configuration file was created successfully.</div>';
			$output['result'] = true;
		}
		else 
		{
			$output['display'] = '<div class="msg-error"><b>Error: Could not create the configuration file.</b><br> '.
				'Please make sure the <b>includes</b> folder has writing permissions and the <b>conf/config.php</b> file doesn\'t already exist.</div>';
			$output['result'] = false;		
		}
		
		return $output;

	}

		
	function create_domain_file($url)
	{
		$fp = fopen('../custom/domain/domain.inc.php', 'w');

		if ($fp)
		{
			$content = "<?php \n".
			
			'$domainInfo["' . $url . '"] = 1; '.
				" \n".
				
				"?>";

			fputs($fp, $content); 
			fclose($fp); 
			
			$output['display'] = '<div class="msg-win">The domain file was created successfully.</div>';
			$output['result'] = true;
		}
		else 
		{
			$output['display'] = '<div class="msg-error"><b>Error: Could not create the domain file.</b><br> '.
				'Please make sure the <b>includes</b> folder has writing permissions and the <b>custom/domain/domain.inc.php</b> file doesn\'t already exist.</div>';
			$output['result'] = false;		
		}
		
		return $output;

	}


	// Function For Run Multiple Query From .SQL File
	function MultiQuery($sqlfile, $sqldelimiter = ';') {
	set_time_limit(0);

	if (is_file($sqlfile) === true) {
	$sqlfile = fopen($sqlfile, 'r');

	if (is_resource($sqlfile) === true) {
	$query = array();

	while (feof($sqlfile) === false) {
	$query[] = fgets($sqlfile);

	if (preg_match('~' . preg_quote($sqldelimiter, '~') . '\s*$~iS', end($query)) === 1) {
	$query = trim(implode('', $query));

	if (mysql_query($query) === false) {
	} else {
	if(preg_match_all('/((CREATE TABLE IF NOT EXISTS) `(.*)`)/', $query, $matches)) {
		$tables = array_unique($matches[3]);
			$query= $tables[0];
			echo '<div class="msg-win"><b>' . $query . '</b>  Table successfully Created  </div>';
// send to browser
ob_flush();

	}
	}

	while (ob_get_level() ) {
	ob_end_flush();
	}

	flush();
	}

	if (is_string($query) === true) {
	$query = array();
	}
	}

	return fclose($sqlfile);
	}
	}

	return false;
	}

		
	function apache_module_exists($module)
{
    return in_array($module, apache_get_modules());
}
		
				function db_formatString($string, $default = "", $import = false, $simpleQuotes = true) {
        if ($import){
            if (!$string){
               $string = "'".$string."'"; 
            } elseif (is_string($string)) {
                if ((string_strpos($string,"\'") !== false) || (string_strpos($string,"\\") !== false) || (string_strpos($string,"\\\"") !== false) || !get_magic_quotes_gpc()){
                    $string = stripslashes($string);
                }
                $string  = addslashes($string);
                $string = "'".$string."'";
            } elseif (is_numeric($string)) {
                return $string;
            } else {
                $string = "'".$string."'";
            }
            return $string;
            
        } else {
            
            if (empty($string) && $string != "0") {
                $string = $default;
            }
            if (($string[0]=="'" && $string[string_strlen($string)-1]=="'") || ($string[0]=='"' && $string[string_strlen($string)-1]=='"')) {
                $string = string_substr($string, 1, string_strlen($string)-2);
            }
            if (db_stringNeedsAddslashes($string)) {
                $string = addslashes($string);
            }
            if ($simpleQuotes){
                return "'".$string."'";
            } else {
                return $string;
            }
        }
		
	}
	function db_stringNeedsAddslashes($str) {
		if (($qp = string_strpos($str,"'")) !== false || ($qp = string_strpos($str,"\"")) !== false) {
		if ($str[$qp-1] != "\\")
			return true;
		else
			return db_stringNeedsAddslashes(string_substr($str,$qp+1,string_strlen($str)));
		}
		return false;
	}
	
		function string_strpos($haystack, $needle, $offset = 0, $charset = EDIR_CHARSET) {
		if (isset($haystack) && isset($needle) || (is_numeric($haystack) && is_numeric($needle))) {
			if (is_array($haystack)) $haystack = implode(",", $haystack);
			if (function_exists('mb_strpos')) {
				$position = mb_strpos($haystack, $needle, $offset, $charset);
			} else {
				$position = strpos($haystack, $needle, $offset);
			}
		} else {
			$position = false;
		}
		return $position;
	}
		
		function setup_header(){
		global $site_url ;
	  $put= '
	<!doctype html> 
	<html prefix="og: http://ogp.me/ns#"> 
	 <html dir="ltr" lang="en-US">  
	<head>  
	<meta http-equiv="content-type" content="text/html;charset=UTF-8">
	<title>eDirectory v.10.4.00 Installer By Mr Qaidi</title>
	<meta charset="UTF-8">  
	<link rel="stylesheet" type="text/css" href="'.$site_url.ADMINCP.'/css/style.css" media="screen" />
	<link href="'.$site_url.ADMINCP.'/css/bootstrap-combined.min.css" rel="stylesheet" />
		<link href=\'http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800\' rel=\'stylesheet\' type=\'text/css\'>
		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
	<script type="text/javascript" src="'.$site_url.ADMINCP.'/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="'.$site_url.ADMINCP.'/js/bootstrap-modal.js"></script>
	<script type="text/javascript" src="'.$site_url.ADMINCP.'/js/jquery.validation.js"></script>
	<script type="text/javascript" src="'.$site_url.ADMINCP.'/js/jquery.validationEngine-en.js"></script> 	
	<script type="text/javascript" src="'.$site_url.ADMINCP.'/js/jquery.form.js"></script>
	<script type="text/javascript" src="'.$site_url.ADMINCP.'/js/scripts.js"></script>
	
	</head>
	<body style="padding:4%; padding-top: 10px;   width: 80%;margin-left: auto;margin-right: auto;"><div id="" class="ns-row head-title"><span ="menu-title">eDirectory  v.10.4.00 Installer By Mr Qaidi</span></div><div id="wrapper" class="container-fluid"><div id="content"><div class="row-fluid">';
	return $put;
		}
// NOTE: When accessing a element from the above phpinfo_array(), you can do:


if($_POST["phpini"]=="yes"){

// check if not windows server
if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
    die( 'This is a server using Windows!  and this script not gonna work with windows server');
}

define( 'ABSPATH', str_replace( '\\', '/',  dirname(dirname( __FILE__ ) ))  );
define( 'INSTALL_PATH', ABSPATH.'/setup' );	
	if(!function_exists('system')) { 
	echo '<div class="msg-warning">PHP  system() Funcation disabled  without this fucnation this script will not work</div>';
	}elseif(!file_exists(ABSPATH."/php.ini")){
    exec("cp ".php_ini_loaded_file()." ".ABSPATH."/php.ini");
	attach_loader(INSTALL_PATH,ABSPATH);
	edit_htaccess(ABSPATH);
	fix_cron(ABSPATH);
	
	echo "php.ini genereted please refresh";

	}else{
	attach_loader(INSTALL_PATH,ABSPATH);
	edit_htaccess(ABSPATH);
	fix_cron(ABSPATH);
	
	echo "php.ini generated please refresh";

	}

}	


function attach_loader($installpath,$ABSPATH){
	$uname=php_uname("m");
	chmod ($installpath."/loader/qaididecoder-".$uname."-php".get_php_version().".so", 0755);
	copy($installpath."/loader/qaididecoder-".$uname."-php".get_php_version().".so",
	$ABSPATH."/includes/loader/qaididecoder-".$uname."-php".get_php_version().".so");
	$append = "extension = ".$ABSPATH."/includes/loader/qaididecoder-".$uname."-php".get_php_version().".so";
	file_put_contents($ABSPATH."/php.ini", $append.PHP_EOL , FILE_APPEND);

}

function edit_htaccess($ABSPATH){
    if( strpos(file_get_contents($ABSPATH."/.htaccess"),"SetEnv") ==false ) {  
		prepend("SetEnv PHPRC ".$ABSPATH, $ABSPATH."/.htaccess");
    }	
	$phpinfo = parse_phpinfo();

	if($phpinfo["Environment"]["PWD"] !=="" or !$phpinfo["Environment"]["PWD"]){
	    if( strpos(file_get_contents($ABSPATH."/.htaccess"),"suPHP_ConfigPath") ==false ) {  
		prepend("suPHP_ConfigPath ".$ABSPATH, $ABSPATH."/.htaccess");
		}	
	}
}	

function prepend($string, $filename) {
  $context = stream_context_create();
  $fp = fopen($filename, 'r', 1, $context);
  $tmpname = md5($string);
  file_put_contents($tmpname,$string.PHP_EOL);
  file_put_contents($tmpname, $fp, FILE_APPEND);
  fclose($fp);
  unlink($filename);
  rename($tmpname, $filename);
}

function fix_cron($ABSPATH) {
$file_contents = file_get_contents($ABSPATH."/cron/cron_manager.php");
$file_contents = str_replace("/home/user/public_html/php.ini",$ABSPATH."/php.ini",$file_contents);
file_put_contents($ABSPATH."/cron/cron_manager.php",$file_contents);
}

function my_error($msg) {
global $site_url;
header("Location: ".$site_url."?error&error_msg=".base64_encode($msg));
die();
}
function my_redirect($param) {
global $site_url;
header("Location: ".$site_url."?".$param);
die();
}

function check_php_ini(){
if (strpos(get_cfg_var('cfg_file_path'), mrqaidi_get_current_user()) === false){
return true;
}
return false;
}
function get_php_version (){
if (strpos(PHP_VERSION, '5.5')!==false) {
		return "55";
}if (strpos(PHP_VERSION, '5.4') !==false) {
		return "54";
}if (strpos(PHP_VERSION, '5.3')!==false) {
		return "53";
}
if (strpos(PHP_VERSION, '5.2')!==false) {
		return "52";
}else{
	return false;
	}
}
	function mrqaidi_get_current_user() {
	
		if(get_current_user ==""){
		$doc_root=$_SERVER["DOCUMENT_ROOT"];
		$doc_user= explode("/",$doc_root);
		return $doc_user[2];
		}else{
		return get_current_user();
		}
}
function parse_phpinfo() {
    ob_start(); phpinfo(INFO_ENVIRONMENT); $s = ob_get_contents(); ob_end_clean();
    $s = strip_tags($s, '<h2><th><td>');
    $s = preg_replace('/<th[^>]*>([^<]+)<\/th>/', '<info>\1</info>', $s);
    $s = preg_replace('/<td[^>]*>([^<]+)<\/td>/', '<info>\1</info>', $s);
    $t = preg_split('/(<h2[^>]*>[^<]+<\/h2>)/', $s, -1, PREG_SPLIT_DELIM_CAPTURE);
    $r = array(); $count = count($t);
    $p1 = '<info>([^<]+)<\/info>';
    $p2 = '/'.$p1.'\s*'.$p1.'\s*'.$p1.'/';
    $p3 = '/'.$p1.'\s*'.$p1.'/';
    for ($i = 1; $i < $count; $i++) {
        if (preg_match('/<h2[^>]*>([^<]+)<\/h2>/', $t[$i], $matchs)) {
            $name = trim($matchs[1]);
            $vals = explode("\n", $t[$i + 1]);
            foreach ($vals AS $val) {
                if (preg_match($p2, $val, $matchs)) { // 3cols
                    $r[$name][trim($matchs[1])] = array(trim($matchs[2]), trim($matchs[3]));
                } elseif (preg_match($p3, $val, $matchs)) { // 2cols
                    $r[$name][trim($matchs[1])] = trim($matchs[2]);
                }
            }
        }
    }
    return $r;
}
	
?>