<?php
if(!defined('MrQaidi')) {die('Direct access not permitted');}

	// physical path of your root
	define( 'ABSPATH', str_replace( '\\', '/',  dirname(dirname( __FILE__ ) ))  );
	// physical path of includes directory
	define( 'CONFIG_PATH', ABSPATH.'/conf' );	
	define( 'INSTALL_PATH', ABSPATH.'/setup' );	

	//Config include
	require_once(CONFIG_PATH."/my_config.php");

	//Check if config exists
	if(!file_exists(CONFIG_PATH.'/my_config.php')){
	echo '<h1>Hold on! Configuration file is missing! </h1><br />';
	die();
	}	

	$base_href_path = pathinfo($_SERVER['SCRIPT_NAME'], PATHINFO_DIRNAME);
    $EDIRECTORY_FOLDER = str_replace("setup","",$base_href_path);
    $EDIRECTORY_FOLDER = rtrim($EDIRECTORY_FOLDER, "/");
	$base_href_protocol = ( array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] !== 'off' ? 'https' : 'http' ).'://';
	if( array_key_exists('HTTP_HOST', $_SERVER) && !empty($_SERVER['HTTP_HOST']) )
	{
		$base_href_host = $_SERVER['HTTP_HOST'];
	}
	elseif( array_key_exists('SERVER_NAME', $_SERVER) && !empty($_SERVER['SERVER_NAME']) )
	{
		$base_href_host = $_SERVER['SERVER_NAME'].( $_SERVER['SERVER_PORT'] !== 80 ? ':'.$_SERVER['SERVER_PORT'] : '' );
	}
	$base_href = rtrim( $base_href_protocol.$base_href_host.$base_href_path, "/" ).'/';
	$site_link = parse_url($base_href);
	$site_link = $site_link['host'];
	if (strpos($site_link, "www.") !== false) {
			$site_link = str_replace("www.", "", $site_link);
			}
	$site_url = $base_href;

	define( 'ADMINCP',"/");		
	require_once( INSTALL_PATH.'/functions.php' );


?>