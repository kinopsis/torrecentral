
function showPhone(listingid) {
	try {
		xmlhttp = new XMLHttpRequest();
	} catch (exc) {
		try {
			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (ex) {
			try {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
				xmlhttp = false;
			}
		}
	}
	if (xmlhttp) {
		xmlhttp.open("GET", DEFAULT_URL+'/countphoneclick.php?listing_id='+listingid, true);
		xmlhttp.send(null);
	}
	document.getElementById("phoneLink"+listingid).className = "hide";
	document.getElementById("phoneNumber"+listingid).className = "show-inline";
}

function showFax(listingid) {
	try {
		xmlhttp = new XMLHttpRequest();
	} catch (exc) {
		try {
			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (ex) {
			try {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
				xmlhttp = false;
			}
		}
	}
	if (xmlhttp) {
		xmlhttp.open("GET", DEFAULT_URL+'/countfaxclick.php?listing_id='+listingid, true);
		xmlhttp.send(null);
	}
	document.getElementById("faxLink"+listingid).className = "hide";
	document.getElementById("faxNumber"+listingid).className = "show-inline";
}