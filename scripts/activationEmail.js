function sendEmailActivation(acc_id) {
    $("#loadingEmail").css("display", "");
    $.get(DEFAULT_URL + "/activationEmail.php", {
        acc_id: acc_id
    }, function (response) {
        $("#loadingEmail").css("display", "none");
        if (response == "ok") {
            $("#messageEmail").css("display", "");
            $("#messageEmailError").css("display", "none");
        } else {
            $("#messageEmail").css("display", "none");
            $("#messageEmailError").html(response);
            $("#messageEmailError").css("display", "");
        }
    });
}