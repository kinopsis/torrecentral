<?
    /*
    * # Admin Panel for eDirectory
    * @copyright Copyright 2014 Arca Solutions, Inc.
    * @author Basecode - Arca Solutions, Inc.
    */

    # ----------------------------------------------------------------------------------------------------
	# * FILE: /includes/modals/modal-crop.php
	# ----------------------------------------------------------------------------------------------------

?>

    <div class="modal fade" id="modal-crop" tabindex="-1" role="dialog" aria-labelledby="modal-crop" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?=system_showText(LANG_CLOSE);?></span></button>
                    <h4 class="modal-title"><?=system_showText(LANG_LABEL_IMAGE_CROP)?></h4>
                </div>
                
                <form name="form-crop" id="form-crop" action="<?=system_getFormAction($_SERVER["PHP_SELF"])?>" method="get">
                    <input type="hidden" name="domain_id" value="<?=SELECTED_DOMAIN_ID?>">
                    <input type="hidden" name="edit_action" id="edit-action" value="crop">
                    <input type="hidden" name="edit_image_id" id="edit-image_id" value="">
                    <input type="hidden" name="edit_temp" id="edit-temp" value="">
                    <input type="hidden" name="edit_image_type" id="edit_image_type" value="">
                    <div class="modal-body">
                        
                        <? if (string_strpos($_SERVER["PHP_SELF"], "deal.php") === false) { ?>
                        <ul class="nav nav-pills" role="tablist">
                            <li class="active"><a href="#cropping" data-toggle="tab" role="tab" onclick="$('#edit-action').attr('value', 'crop');"><?=system_showText(LANG_LABEL_IMAGE_CROPPING);?></a></li>
                            <li class=""><a href="#crop_description" data-toggle="tab" role="tab" onclick="$('#edit-action').attr('value', 'description');"><?=system_showText(LANG_LABEL_DESCRIPTION);?></a></li>
                        </ul>
                        <? } ?>

                        <div class="row tab-content content-pills">
                            <div class="tab-pane active" id="cropping">
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <img src="" id="crop_image">
                                        <? // Crop Tool Inputs ?>
                                        <input type="hidden" name="x" id="x">
                                        <input type="hidden" name="y" id="y">
                                        <input type="hidden" name="x2" id="x2">
                                        <input type="hidden" name="y2" id="y2">
                                        <input type="hidden" name="w" id="w">
                                        <input type="hidden" name="h" id="h">
                                    </div>

                                </div>
                            </div>
                            <? if (string_strpos($_SERVER["PHP_SELF"], "deal.php") === false) { ?>
                            <div class="tab-pane" id="crop_description">
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label for="crop_image_title"><?=string_ucwords(system_showText(LANG_LABEL_IMAGE_TITLE))?></label>
                                        <input type="text" class="form-control" id="crop_image_title" name="image_title" value="" maxlength="100">
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="crop_image_description"><?=string_ucwords(LANG_LABEL_IMAGE_DESCRIPTION)?></label>
                                        <input type="text" class="form-control" id="crop_image_description" name="image_description" value="" maxlength="100">
                                    </div>
                                </div>
                            </div>
                            <? } ?>
                        </div>
                          
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?=system_showText(LANG_CANCEL);?></button>
                        <button type="button" class="btn btn-primary action-save" id="button-edit-img" data-loading-text="<?=system_showText(LANG_LABEL_FORM_WAIT);?>" onclick="saveImage();"><?=system_showText(LANG_SITEMGR_SAVE_CHANGES);?></button>
                    </div>

                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->