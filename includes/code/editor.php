<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /includes/code/editor.php
	# ----------------------------------------------------------------------------------------------------

	# ----------------------------------------------------------------------------------------------------
	# SUBMIT
	# ----------------------------------------------------------------------------------------------------
	if ($_SERVER["REQUEST_METHOD"] == "POST" && !DEMO_LIVE_MODE  && $submitAction == "csseditor") {

        $errorFolder = false;
        $errorMessage = "";
       
        if ($fileType == "css") {
            $filePath = HTMLEDITOR_THEMEFILE_DIR."/".EDIR_THEME;
            $file = "editor_".(EDIR_SCHEME != "custom" ? EDIR_SCHEME : EDIR_THEME)."_".$file;
        } else {
            $filePath = HTMLEDITOR_FOLDER;
        }
        
        if (!is_dir($filePath)) {
            if (!mkdir($filePath)) {
                $errorFolder = true;
            }
        }
        
        if (get_magic_quotes_gpc()) {
            $text = stripslashes($text);
        }
                
        if ($errorFolder) {
            $errorMessage = system_showText(LANG_SITEMGR_EDITOR_PERMERROR);
        } elseif (!$text){
            $errorMessage = system_showText(LANG_SITEMGR_EDITOR_EMPTYERROR);
        }
       
        if (!$errorMessage || $revert) {
            
            if ($revert) {
                if (file_exists($filePath."/".$file)) {
                    unlink($filePath."/".$file);
                }
                
                if (CACHE_FULL_FEATURE == "on") {
                    cachefull_forceExpiration();
                }
                
                $message = 1;
            } else {
                $message = 0;
                
                if (!$errorMessage) {
                    file_put_contents($filePath."/".$file, $text);

                    if (CACHE_FULL_FEATURE == "on") {
                        cachefull_forceExpiration();
                    }
                }
            }

            $file = str_replace("editor_".(EDIR_SCHEME != "custom" ? EDIR_SCHEME : EDIR_THEME)."_", "", $file);
            
            if (!$errorMessage){
                header("Location: ".DEFAULT_URL."/".SITEMGR_ALIAS."/design/layout-editor/index.php?file=$file&message=$message");
                exit;
            } else {
                unset($message);
            }
        } else {
            $file = str_replace("editor_".(EDIR_SCHEME != "custom" ? EDIR_SCHEME : EDIR_THEME)."_", "", $file);
        }
	} elseif($_SERVER["REQUEST_METHOD"] == "POST" && DEMO_LIVE_MODE) {
        $errorMessage = system_showText(LANG_SITEMGR_THEME_DEMO_MESSAGE2);
    }
    
    # ----------------------------------------------------------------------------------------------------
	# FORMS DEFINES
	# ----------------------------------------------------------------------------------------------------
    $langPart = explode("_", $sitemgr_language);
    $editorLang = $langPart[0];
    if ($editorLang == "ge") {
        $editorLang = "de";
    }
    $editorSyntax = "html";
    $availableFiles = system_getEditorAvailable();
    $nameArray = array();
    $valueArray = array();
    
    $nameArray[] = "-- ".system_showText(LANG_SITEMGR_EDITOR_CSSFILES)." --";
    $valueArray[] = "";
    
    foreach ($availableFiles as $avlFile) {
        $nameArray[] = $avlFile;
        $valueArray[] = $avlFile;
    }

    if (!in_array($file, $availableFiles) || !$file) {
        $file = "plans.css";
    }

    if ($_SERVER["REQUEST_METHOD"] != "POST") {
        $fileType = "css";
        $text = file_get_contents(system_getStylePath($file, EDIR_THEME, true));
        $editorSyntax = "css";
    }
    
    $editorSelect = html_selectBox("selectFile", $nameArray, $valueArray, $file, "onchange=\"setEditor(this.value);\"");