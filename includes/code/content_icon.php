<?

    /* ==================================================================*\
      ######################################################################
      #                                                                    #
      # Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
      #                                                                    #
      # This file may not be redistributed in whole or part.               #
      # eDirectory is licensed on a per-domain basis.                      #
      #                                                                    #
      # ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
      #                                                                    #
      # http://www.edirectory.com | http://www.edirectory.com/license.html #
      ######################################################################
      \*================================================================== */

    # ----------------------------------------------------------------------------------------------------
    # * FILE: /includes/code/content_icon.php
    # ----------------------------------------------------------------------------------------------------

    if ( $_FILES['favicon_file']['name'] )
    {
        $arr_favicon         = explode( ".", $_FILES['favicon_file']['name'] );
        $favicon_extension   = $arr_favicon[count( $arr_favicon ) - 1];

        if( string_strtolower( $favicon_extension ) == "ico" )
        {
            setting_get( "last_favicon_id", $last_favicon_id );
            $last_favicon_id or ( $last_favicon_id = "1" and setting_new( "last_favicon_id", "1" ) );

            // FAVICON FILE UPLOAD
            if ( file_exists( $_FILES['favicon_file']['tmp_name'] ) && filesize( $_FILES['favicon_file']['tmp_name'] ) )
            {
                if ( file_exists( EDIRECTORY_ROOT."/custom/domain_".SELECTED_DOMAIN_ID."/content_files/favicon_".$last_favicon_id.".ico" ) )
                {
                    @unlink( EDIRECTORY_ROOT."/custom/domain_".SELECTED_DOMAIN_ID."/content_files/favicon_".$last_favicon_id.".ico" );
                }
                $last_favicon_id++;

                $file_path = EDIRECTORY_ROOT."/custom/domain_".SELECTED_DOMAIN_ID."/content_files/favicon_".$last_favicon_id.".ico";
                copy( $_FILES['favicon_file']['tmp_name'], $file_path );
                setting_set( "last_favicon_id", $last_favicon_id );
            }
            else
            {
                MessageHandler::registerError( system_showText( LANG_MSGERROR_ERRORUPLOADINGIMAGE ) );
            }
        }
        else
        {
            MessageHandler::registerError( system_showText( LANG_UPLOAD_MSG_NOTALLOWED_WRONGFILETYPE ) );
        }
    }