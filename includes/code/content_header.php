<?

    /* ==================================================================*\
      ######################################################################
      #                                                                    #
      # Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
      #                                                                    #
      # This file may not be redistributed in whole or part.               #
      # eDirectory is licensed on a per-domain basis.                      #
      #                                                                    #
      # ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
      #                                                                    #
      # http://www.edirectory.com | http://www.edirectory.com/license.html #
      ######################################################################
      \*================================================================== */

    # ----------------------------------------------------------------------------------------------------
    # * FILE: /includes/code/content_header.php
    # ----------------------------------------------------------------------------------------------------

    /* fixing urls if needed. */
    if ( $setting_facebook_link = trim( $setting_facebook_link ) && string_strpos( $setting_facebook_link, "://" ) === false )
    {
        $setting_facebook_link = "http://".$setting_facebook_link;
    }

    if ( $setting_linkedin_link = trim( $setting_linkedin_link ) && string_strpos( $setting_linkedin_link, "://" ) === false )
    {
        $setting_linkedin_link = "http://".$setting_linkedin_link;
    }

    customtext_set( "header_title", $header_title )                   or customtext_new( "header_title", $header_title )                   or MessageHandler::registerError( system_showText( LANG_SITEMGR_SETTINGS_PAYMENTS_GATEWAY_ERROR_DATABASE ) );
    customtext_set( "header_author", $header_author )                 or customtext_new( "header_author", $header_author )                 or MessageHandler::registerError( system_showText( LANG_SITEMGR_SETTINGS_PAYMENTS_GATEWAY_ERROR_DATABASE ) );
    customtext_set( "header_description", $header_description )       or customtext_new( "header_description", $header_description )       or MessageHandler::registerError( system_showText( LANG_SITEMGR_SETTINGS_PAYMENTS_GATEWAY_ERROR_DATABASE ) );
    customtext_set( "header_keywords", $header_keywords )             or customtext_new( "header_keywords", $header_keywords )             or MessageHandler::registerError( system_showText( LANG_SITEMGR_SETTINGS_PAYMENTS_GATEWAY_ERROR_DATABASE ) );
    customtext_set( "footer_copyright", $copyright )                  or customtext_new( "footer_copyright", $copyright )                  or MessageHandler::registerError( system_showText( LANG_SITEMGR_SETTINGS_PAYMENTS_GATEWAY_ERROR_DATABASE ) );
    customtext_set( "setting_facebook_link", $setting_facebook_link ) or customtext_new( "setting_facebook_link", $setting_facebook_link ) or MessageHandler::registerError( system_showText( LANG_SITEMGR_SETTINGS_PAYMENTS_GATEWAY_ERROR_DATABASE ) );
    customtext_set( "setting_linkedin_link", $setting_linkedin_link ) or customtext_new( "setting_linkedin_link", $setting_linkedin_link ) or MessageHandler::registerError( system_showText( LANG_SITEMGR_SETTINGS_PAYMENTS_GATEWAY_ERROR_DATABASE ) );
    customtext_set( "twitter_account", $twitter_account )             or customtext_new( "twitter_account", $twitter_account )             or MessageHandler::registerError( system_showText( LANG_SITEMGR_SETTINGS_PAYMENTS_GATEWAY_ERROR_DATABASE ) );

    /* Writes new header title to config file. */
    if ( !MessageHandler::haveErrors() && ( $header_title = trim( $header_title ) ) )
    {
        $fileConstPath       = EDIRECTORY_ROOT."/custom/domain_".SELECTED_DOMAIN_ID."/conf/constants.inc.php";
        system_writeConstantsFile( $fileConstPath, SELECTED_DOMAIN_ID, array( "name" => $header_title ) ) or MessageHandler::registerError( system_showText( LANG_SITEMGR_SETTINGS_PAYMENTS_GATEWAY_ERROR_DATABASE ) );
    }

    if ( $_FILES["header_image"]["tmp_name"] && $_FILES["header_image"]["error"] == 0 )
    {
        $filename     = EDIRECTORY_ROOT.IMAGE_HEADER_PATH;
        $image_upload = image_uploadForHeader( $filename, $_FILES["header_image"]["tmp_name"] );
        $image_upload["success"] or MessageHandler::registerError( system_showText( LANG_SITEMGR_MSGERROR_ALERTUPLOADIMAGE1 ) );
    }

