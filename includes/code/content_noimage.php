<?

    /* ==================================================================*\
      ######################################################################
      #                                                                    #
      # Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
      #                                                                    #
      # This file may not be redistributed in whole or part.               #
      # eDirectory is licensed on a per-domain basis.                      #
      #                                                                    #
      # ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
      #                                                                    #
      # http://www.edirectory.com | http://www.edirectory.com/license.html #
      ######################################################################
      \*================================================================== */

    # ----------------------------------------------------------------------------------------------------
    # * FILE: /includes/code/content_noimage.php
    # ----------------------------------------------------------------------------------------------------

    /* noimage image file */
    if ( $_FILES["noimage_image"]["tmp_name"] && $_FILES["noimage_image"]["error"] == 0 )
    {
        $filename = EDIRECTORY_ROOT.NOIMAGE_PATH."/".NOIMAGE_NAME.".".NOIMAGE_IMGEXT;

        $image_upload = image_uploadForNoImage( $filename, $_FILES["noimage_image"]["tmp_name"] );
        $image_upload["success"] or MessageHandler::registerError( system_showText( LANG_SITEMGR_MSGERROR_ALERTUPLOADIMAGE1 ) );
    }