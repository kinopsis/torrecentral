<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /includes/code/content_contactus.php
	# ----------------------------------------------------------------------------------------------------

    if ($_SERVER['REQUEST_METHOD'] == "POST" && !DEMO_LIVE_MODE) {

        if ($contact_email) {
            if (!validate_email($contact_email)) {
                $errorMessage = "&#149;&nbsp;".system_showText(LANG_MSG_ENTER_VALID_EMAIL_ADDRESS);
            }
        }

        if (!$errorMessage) {

            $contactInfo = array(
                "contact_company",
                "contact_address",
                "contact_zipcode",
                "contact_country",
                "contact_state",
                "contact_city",
                "contact_phone",
                "contact_fax",
                "contact_email",
                "contact_mapzoom",
                "contact_latitude",
                "contact_longitude"
            );

            foreach ($contactInfo as $info) {
                if (!setting_set($info, $$info)) {
                    if (!setting_new($info, $$info)) {
                        $error = true;
                    }
                }
            }
        }
    }
    
    if ($_SERVER["REQUEST_METHOD"] != "POST") {
        setting_get("contact_company", $contact_company);
        setting_get("contact_address", $contact_address);
        setting_get("contact_zipcode", $contact_zipcode);
        setting_get("contact_country", $contact_country);
        setting_get("contact_state", $contact_state);
        setting_get("contact_city", $contact_city);
        setting_get("contact_phone", $contact_phone);
        setting_get("contact_fax", $contact_fax);
        setting_get("contact_email", $contact_email);
        setting_get("contact_latitude", $contact_latitude);
        setting_get("contact_longitude", $contact_longitude);
        setting_get("contact_mapzoom", $contact_mapzoom);
        $map_zoom = $contact_mapzoom;
    }

    //Map Control
    $loadMap = false;
    $mapObj = new GoogleSettings(GOOGLE_MAPS_STATUS, $_SERVER["HTTP_HOST"]);
    if (GOOGLE_MAPS_ENABLED == "on" && $mapObj->getString("value") == "on") {
        $loadMap = true; 
     
        $hasValidCoord = false;
      
        if ($contact_latitude && $contact_longitude && is_numeric($contact_latitude) && is_numeric($contact_longitude)) {
            $hasValidCoord = true;
        }
    }
?>