<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /includes/code/layout_editor.php
	# ----------------------------------------------------------------------------------------------------

    # ----------------------------------------------------------------------------------------------------
	# SUBMIT
	# ----------------------------------------------------------------------------------------------------
	extract($_POST);
	extract($_GET);

    //Ajax requests
    if ($_GET["action"] == "ajax") {
        
        //Upload background image
        if ($_GET["type"] == "uploadBackground") {
            
            $return = "";
            $error = false;
            
            if (isset($_POST) && $_SERVER["REQUEST_METHOD"] == "POST") {

                if ($_FILES) {

                    $i = 1;
                    $image_errors = array();

                    $maxImageSize = ((UPLOAD_MAX_SIZE * 10) + 1)."00000";

                    foreach ($_FILES as $key => $value) {

                        if (strlen($value["tmp_name"]) > 0) {
                            if (image_upload_check($value["tmp_name"])) {
                                if (strlen($value["name"])) {
                                    $_POST[$key] = $value["name"];
                                }
                            } else {
                                $image_errors[] = "&#149;&nbsp; ".system_showText(LANG_SITEMGR_MSGERROR_FILEEXTENSIONNOTALLOWED);
                            }	

                            if ($value["size"] > $maxImageSize) {
                                $image_errors[] = "&#149;&nbsp; ".system_showText(LANG_SITEMGR_MSGERROR_MAXFILESIZEALLOWEDIS." ".UPLOAD_MAX_SIZE."MB.");
                            }
                        }
                        $i++;
                    }

                    if (count($image_errors) == 0) {
                        if ($_FILES["file_background_image"]["error"] == 0) {

                            $imageObj = image_upload($_FILES["file_background_image"]["tmp_name"], IMAGE_THEME_BACKGROUND_W, IMAGE_THEME_BACKGROUND_H, 'sitemgr_', false);
                            if ($imageObj) {
                                $return = "<input type='hidden' name='background_image_id' value='".$imageObj->getNumber("id")."'>";
                                $return .= $imageObj->getTag();
                            }

                        } else {
                            $error = true;
                            $return = system_showText(LANG_MSGERROR_ERRORUPLOADINGIMAGE);
                        }        
                    } else {
                        $error = true;
                        foreach ($image_errors as $imgError) {
                            $return .= $imgError."<br />";
                        }
                    }

                } else {
                    $error = true;
                    $return = system_showText(LANG_SITEMGR_IMAGE_EMPTY);
                }
            }

            echo ($error ? "error" : "ok")."||".$return;
            
        //Save background image / Listing options (Dining Guide)
        } elseif ($_GET["type"] == "general") {
            
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
        
                $error = false;

                //Listing options (Dining Guide)
                if ($_POST["form_id"] == "pricing_levels_form") {
                    if (is_numeric($_POST["listing_price_1_to"]) && 
                    is_numeric($_POST["listing_price_2_from"]) && 
                    is_numeric($_POST["listing_price_2_to"]) && 
                    is_numeric($_POST["listing_price_3_from"]) && 
                    is_numeric($_POST["listing_price_3_to"]) && 
                    is_numeric($_POST["listing_price_4_from"]) && $_POST["symbol"]) {

                        if (($_POST["symbol"] == "custom") && (!$_POST["custom_symbol"])) {
                            $msg = system_showText(LANG_SITEMGR_PRICING_CHECK_FIELDS);
                            $error = true;
                        } elseif (($_POST["listing_price_1_to"] < $_POST["listing_price_2_from"]) &&
                        ($_POST["listing_price_1_to"] < $_POST["listing_price_2_to"]) &&
                        ($_POST["listing_price_1_to"] < $_POST["listing_price_3_from"]) &&
                        ($_POST["listing_price_1_to"] < $_POST["listing_price_3_to"]) &&
                        ($_POST["listing_price_1_to"] < $_POST["listing_price_4_from"])) {

                                if (($_POST["listing_price_2_from"] < $_POST["listing_price_2_to"]) &&
                                ($_POST["listing_price_2_from"] < $_POST["listing_price_3_from"]) &&
                                ($_POST["listing_price_2_from"] < $_POST["listing_price_3_to"]) &&
                                ($_POST["listing_price_2_from"] < $_POST["listing_price_4_from"])) {

                                if (($_POST["listing_price_2_to"] < $_POST["listing_price_3_from"]) &&
                                        ($_POST["listing_price_2_to"] < $_POST["listing_price_3_to"]) &&
                                        ($_POST["listing_price_2_to"] < $_POST["listing_price_4_from"])) {

                                        if (($_POST["listing_price_3_from"] < $_POST["listing_price_3_to"]) &&
                                        ($_POST["listing_price_3_from"] < $_POST["listing_price_4_from"])) {

                                            if (($_POST["listing_price_3_to"] < $_POST["listing_price_4_from"])) {

                                                for ($i = 1; $i <= LISTING_PRICE_LEVELS; $i++) {

                                                    if (!setting_set("listing_price_{$i}_from", ($_POST["listing_price_".$i."_from"] ? $_POST["listing_price_".$i."_from"] : "0"))) {
                                                        setting_new("listing_price_{$i}_from", ($_POST["listing_price_".$i."_from"] ? $_POST["listing_price_".$i."_from"] : "0"));
                                                    }

                                                    if (!setting_set("listing_price_{$i}_to", ($_POST["listing_price_".$i."_to"] ? $_POST["listing_price_".$i."_to"] : ""))) {
                                                        setting_new("listing_price_{$i}_to", ($_POST["listing_price_".$i."_to"] ? $_POST["listing_price_".$i."_to"] : ""));
                                                    }

                                                }

                                                if ($_POST["symbol"] == "custom") {

                                                    if (!setting_set("listing_price_symbol", $_POST["custom_symbol"])) {
                                                        setting_new("listing_price_symbol", $_POST["custom_symbol"]);
                                                    }

                                                } else {

                                                    if (!setting_set("listing_price_symbol", $_POST["symbol"])) {
                                                        setting_new("listing_price_symbol", $_POST["symbol"]);
                                                    }
                                                }

                                                $msg = system_showText(LANG_SITEMGR_PRICING_SUCCESSFULLY_UPDATED);

                                            } else {
                                                $msg = system_showText(LANG_SITEMGR_PRICING_RANGE_FIELDS);
                                                $error = true;
                                            }
                                        } else {
                                            $msg = system_showText(LANG_SITEMGR_PRICING_RANGE_FIELDS);
                                            $error = true;
                                        }

                                } else {
                                    $msg = system_showText(LANG_SITEMGR_PRICING_RANGE_FIELDS);
                                    $error = true;
                                }

                                } else {
                                    $msg = system_showText(LANG_SITEMGR_PRICING_RANGE_FIELDS);
                                    $error = true;
                                }
                        } else {
                            $msg = system_showText(LANG_SITEMGR_PRICING_RANGE_FIELDS);
                            $error = true;
                        }

                    } else {
                        $msg = system_showText(LANG_SITEMGR_PRICING_CHECK_FIELDS);
                        $error = true;
                    }

                //Save background image
                } elseif ($_POST["form_id"] == "theme_background_image") {

                    $buttonReset = "";

                    if ($_POST["reset_form"] == "reset" || $_POST["background_image_id"]) {

                        //Remove old image
                        setting_get("diningguide_background_image_id", $image_id);

                        if ($image_id != $_POST["background_image_id"]) {
                            $imgObj = new Image($image_id);
                            if ($imgObj->getNumber("id")) {
                                $imgObj->delete();
                            }
                        }

                        if ($_POST["reset_form"] == "reset") {
                            $buttonReset = "hide";
                            $_POST["background_image_id"] = "";
                            system_backgroundImageStyle("clean");

                            //clear dimension
                            if (!setting_set("background_image_height", "")) {
                                setting_new("background_image_height", "");
                            }
                        } else {
                            $buttonReset = "show";
                        }

                        if (!setting_set("diningguide_background_image_id", $_POST["background_image_id"])) {
                            setting_new("diningguide_background_image_id", $_POST["background_image_id"]);
                        }

                        $newImageReturn = "<input type='hidden' name='background_image_id' value=''>".front_getBackground($customimage);

                        if ($_POST["background_image_id"] && $customimage) {
                            if ($_POST["dimensionY"]) {
                                //generate new css file
                                system_backgroundImageStyle("new", $_POST["dimensionY"]);

                                //save dimension
                                if (!setting_set("background_image_height", $_POST["dimensionY"])) {
                                    setting_new("background_image_height", $_POST["dimensionY"]);
                                }
                            } else {
                                //remove css file
                                system_backgroundImageStyle("clean");

                                //clear dimension
                                if (!setting_set("background_image_height", "")) {
                                    setting_new("background_image_height", "");
                                }
                            }
                        }

                        $msg = system_showText(LANG_SITEMGR_BACKGROUND_UPDATED);

                    } elseif ($_POST["curr_image_id"]) {

                        //generate new css file
                        system_backgroundImageStyle("new", $_POST["dimensionY"]);

                        //save dimension
                        if (!setting_set("background_image_height", $_POST["dimensionY"])) {
                            setting_new("background_image_height", $_POST["dimensionY"]);
                        }

                        $msg = system_showText(LANG_SITEMGR_BACKGROUND_UPDATED);
                    } else {
                        $msg = system_showText(LANG_SITEMGR_IMAGE_EMPTY);
                        $error = true;
                    }
                }

                echo ($error ? "error" : "success")."||".$msg.($buttonReset ? "||".$buttonReset."||".$newImageReturn : "");
            }
            
        } else {
        
            //Create dining guide categories
            setting_get("theme_create_categories", $theme_create_categories);

            if (THEME_CATEGORY_IMAGE && $theme_create_categories != "done" && !DEMO_LIVE_MODE && !DEMO_DEV_MODE) {

                if (EDIR_LANGUAGE != "pt_br") {

                    //English
                    $categArray = array();
                    $categArray[] = "Seafood";
                    $categArray[] = "Barbecue";
                    $categArray[] = "Spanish";
                    $categArray[] = "Vegetarian";
                    $categArray[] = "American";
                    $categArray[] = "Chinese";
                    $categArray[] = "Japanese";
                    $categArray[] = "Mexican";
                    $categArray[] = "Italian";
                    $categArray[] = "French";
                    $categArray[] = "Asian";
                    $categArray[] = "Brazilian";
                    $categArray[] = "Coffeehouse";
                    $categArray[] = "Deli";
                    $categArray[] = "Dessert";
                    $categArray[] = "German";
                    $categArray[] = "Hamburgers";
                    $categArray[] = "Hawaiian";
                    $categArray[] = "Health Food";
                    $categArray[] = "Indian";
                    $categArray[] = "Korean";
                    $categArray[] = "Mediterranean";
                    $categArray[] = "Pizza";
                    $categArray[] = "Sandwiches";
                    $categArray[] = "Steakhouse";
                    $categArray[] = "Sushi";
                    $categArray[] = "Tapas";
                    $categArray[] = "Thai";
                    $categArray[] = "Turkish";
                    $categArray[] = "Vietnamese";
                    $categArray[] = "Bakery";

                } else {

                    //Portuguese
                    $categArray[] = "Bares";
                    $categArray[] = "Brasileira";
                    $categArray[] = "Cafeterias";
                    $categArray[] = "Cantinas";
                    $categArray[] = "Chinesa";
                    $categArray[] = "Churrascaria";
                    $categArray[] = "Moderna";
                    $categArray[] = "Americana";
                    $categArray[] = "Frutos do Mar";
                    $categArray[] = "Indiana";
                    $categArray[] = "Italiana";
                    $categArray[] = "Japonesa";
                    $categArray[] = "Lanchonetes";
                    $categArray[] = "Nordestina";
                    $categArray[] = "Padarias";
                    $categArray[] = "Pizzarias";
                    $categArray[] = "Self-Service";
                    $categArray[] = "Sorveterias";
                    $categArray[] = "Vegetariana";
                }

                foreach ($categArray as $category) {

                    $imgName = strtolower(str_replace(" ", "-", $category));

                    $srcImgFull = EDIRECTORY_ROOT."/".SITEMGR_ALIAS."/assets/img/categories_diningguide/".$imgName."_full.png";
                    $srcImgThumb = EDIRECTORY_ROOT."/".SITEMGR_ALIAS."/assets/img/categories_diningguide/".$imgName."_thumb.png";

                    if (file_exists($srcImgFull) && file_exists($srcImgThumb)) {

                        //Generate main image record
                        $imgFull = getimagesize($srcImgFull);
                        $arrayImg = array();
                        $arrayImg["type"] = "PNG";
                        $arrayImg["width"] = $imgFull[0];
                        $arrayImg["height"] = $imgFull[1];
                        $arrayImg["prefix"] = "sitemgr_";
                        $imgMainObj = new Image($arrayImg);
                        $imgMainObj->save();

                        //Generate thumb image record
                        $imgThumb = getimagesize($srcImgThumb);
                        $arrayImg = array();
                        $arrayImg["type"] = "PNG";
                        $arrayImg["width"] = $imgThumb[0];
                        $arrayImg["height"] = $imgThumb[1];
                        $arrayImg["prefix"] = "sitemgr_";
                        $imgThumbObj = new Image($arrayImg);
                        $imgThumbObj->save();

                        //Create category
                        $categ = array();
                        $categ["title"] = $category;
                        $categ["category_id"] = 0;
                        $categ["thumb_id"] = $imgThumbObj->getNumber("id");
                        $categ["image_id"] = $imgMainObj->getNumber("id");
                        $categ["featured"] = "y";
                        $categ["page_title"] = $category;
                        $categ["friendly_url"] = system_generateFriendlyURL($category);
                        $categ["enabled"] = "y";
                        $catObj = new ListingCategory($categ);
                        $catObj->save();

                        //Move images
                        $dstImgFull = $imgMainObj->getPath(false);
                        $dstImgThumb = $imgThumbObj->getPath(false);
                        copy($srcImgFull, $dstImgFull);
                        copy($srcImgThumb, $dstImgThumb);

                    }

                }

                if (!setting_set("theme_create_categories", "done")) {
                    setting_new("theme_create_categories", "done");
                }

            }
        }
        exit;
    }

	$filethemeConfigPath    = EDIRECTORY_ROOT."/custom/domain_".$_POST["domain_id"]."/theme/theme.inc.php";
	$folderthemesPath       = EDIRECTORY_ROOT."/theme";
    unset($array);

	// Default CSS class for message
	$message_style = "success";

	if ($_SERVER["REQUEST_METHOD"] == "POST" && !DEMO_LIVE_MODE && $submitAction == "changetheme") {
        
        if ($select_theme) {
			$status = "success";

			$src = EDIRECTORY_ROOT."/theme/$select_theme";
			$dst = EDIRECTORY_ROOT."/custom/domain_".SELECTED_DOMAIN_ID."/theme/".$select_theme;
			if (!is_dir($dst)) {
				$domain = new Domain(SELECTED_DOMAIN_ID);
				$domain->copyThemeToDomain($src, $dst);
			} 

			if (!$filethemeConfig = fopen($filethemeConfigPath, "w+")) {
				$status = "error";
			} else {
				                
                if (CACHE_PARTIAL_FEATURE == "on") {
                    cachepartial_removecache("event_calendar");
                }

                if (CACHE_FULL_FEATURE == "on") {
                    cachefull_forceExpiration();
                }

				$buffer  = "<?php".PHP_EOL."\$edir_theme=\"$select_theme\";".PHP_EOL;

				if (!fwrite($filethemeConfig, $buffer, strlen($buffer))) {
					$status = "error";
				}

                if ($select_theme == EDIR_THEME) {
                    $auxCurValues = unserialize(EDIR_CURR_SCHEME_VALUES);

                    $array["colorBackground"] = $auxCurValues[$scheme]["colorBackground"];
                    $array["colorHeader"] = $auxCurValues[$scheme]["colorHeader"];
                    $array["colorText"] = $auxCurValues[$scheme]["colorText"];
                    $array["colorLink"] = $auxCurValues[$scheme]["colorLink"];
                    $array["colorNavbar"] = $auxCurValues[$scheme]["colorNavbar"];
                    $array["colorNavbarLink"] = $auxCurValues[$scheme]["colorNavbarLink"];
                    $array["colorNavbarLinkActive"] = $auxCurValues[$scheme]["colorNavbarLinkActive"];
                    $array["colorFooter"] = $auxCurValues[$scheme]["colorFooter"];
                    $array["colorFooterText"] = $auxCurValues[$scheme]["colorFooterText"];
                    $array["colorFooterLink"] = $auxCurValues[$scheme]["colorFooterLink"];
                    $array["color1"] = $auxCurValues[$scheme]["color1"];
                    $array["color2"] = $auxCurValues[$scheme]["color2"];
                    $array["color3"] = $auxCurValues[$scheme]["color3"];
                    $array["color4"] = $auxCurValues[$scheme]["color4"];
                    $array["color5"] = $auxCurValues[$scheme]["color5"];
                    $array["color6"] = $auxCurValues[$scheme]["color6"];
                    $array["color7"] = $auxCurValues[$scheme]["color7"];
                    $array["fontOption"] = $auxCurValues[$scheme]["fontOption"];

                    colorscheme_themeSchemeFile($array, $scheme, $select_theme, $scheme, $status);
                } else {
                   @include_once(EDIRECTORY_ROOT."/custom/domain_".SELECTED_DOMAIN_ID."/theme/".$select_theme."_scheme.inc.php"); 
                   $scheme = $edir_scheme;
                }

                setting_get("scheme_".$scheme."_customized", $aux_value);

                if (!setting_set("scheme_custom", $aux_value)) {
                    if (!setting_new("scheme_custom", $aux_value)) {
                        $error = true;
                    }
                }

                if ($aux_value) {
                    if (!setting_set("scheme_updatefile", "on")) {
                        if (!setting_new("scheme_updatefile", "on")) {
                            $error = true;
                        }
                    }
                }
			}
            
            setting_get("theme_create_categories", $theme_create_categories);
            
            if ($theme_create_categories != "done" && $import_categories == "yes") {
                $ajaxCategory = "&loadCateg=1";
            } else {
                $ajaxCategory = "";
            }

		} else {
			$status = "error";
		}

		header("Location: ".DEFAULT_URL."/".SITEMGR_ALIAS."/design/layout-editor/index.php?status=$status$ajaxCategory");
		exit;
	} elseif ($_SERVER["REQUEST_METHOD"] == "POST" && !DEMO_LIVE_MODE && $submitAction == "changecolors") {
	
		if ($action == "submit") {

			$array["colorBackground"] = $colorBackground;
			$array["colorHeader"] = $colorHeader;
			$array["colorText"] = $colorText;
			$array["colorLink"] = $colorLink;
			$array["colorNavbar"] = $colorNavbar;
			$array["colorNavbarLink"] = $colorNavbarLink;
			$array["colorNavbarLinkActive"] = $colorNavbarLinkActive;
			$array["colorFooter"] = $colorFooter;
			$array["colorFooterText"] = $colorFooterText;
			$array["colorFooterLink"] = $colorFooterLink;
			$array["color1"] = $color1;
			$array["color2"] = $color2;
			$array["color3"] = $color3;
			$array["color4"] = $color4;
			$array["color5"] = $color5;
			$array["color6"] = $color6;
			$array["color7"] = $color7;
			$array["fontOption"] = $font;

            colorscheme_themeSchemeFile($array, $scheme, EDIR_THEME, ($aux_action ? $scheme : EDIR_SCHEME), $status);

            if ($scheme == EDIR_SCHEME || $aux_action){
                if (!setting_set("scheme_updatefile", "on")) {
                    if (!setting_new("scheme_updatefile", "on")) {
                        $error = true;
                    }
                }
            }

            if (!setting_set("scheme_custom", "on")) {
                if (!setting_new("scheme_custom", "on")) {
                    $error = true;
                }
            }

            if (!setting_set("scheme_".$scheme."_customized", "on")) {
                if (!setting_new("scheme_".$scheme."_customized", "on")) {
                    $error = true;
                }
            }

            header("Location: ".DEFAULT_URL."/".SITEMGR_ALIAS."/design/layout-editor/index.php?status=successcolors");
            exit;
            
		} elseif ($action == "reset") {
			
			$arrayDefault = unserialize(ARRAY_DEFAULT_COLORS);
			$arrayCurValues = unserialize(EDIR_CURR_SCHEME_VALUES);
			
			$array["colorBackground"] = $arrayDefault[$theme][$scheme]["colorBackground"] ? $arrayDefault[$theme][$scheme]["colorBackground"] : "SCHEME_EMPTY";
			$colorBackground = $arrayDefault[$theme][$scheme]["colorBackground"] ? $arrayDefault[$theme][$scheme]["colorBackground"] : false;
			
			$array["colorHeader"] = $arrayDefault[$theme][$scheme]["colorHeader"] ? $arrayDefault[$theme][$scheme]["colorHeader"] : "SCHEME_EMPTY";
			$colorHeader = $arrayDefault[$theme][$scheme]["colorHeader"] ? $arrayDefault[$theme][$scheme]["colorHeader"] : false;
						
			$array["colorText"] = $arrayDefault[$theme][$scheme]["colorText"] ? $arrayDefault[$theme][$scheme]["colorText"] : "SCHEME_EMPTY";
			$colorText = $arrayDefault[$theme][$scheme]["colorText"] ? $arrayDefault[$theme][$scheme]["colorText"] : false;
			
			$array["colorLink"] = $arrayDefault[$theme][$scheme]["colorLink"] ? $arrayDefault[$theme][$scheme]["colorLink"] : "SCHEME_EMPTY";
			$colorLink = $arrayDefault[$theme][$scheme]["colorLink"] ? $arrayDefault[$theme][$scheme]["colorLink"] : false;
			
			$array["colorNavbar"] = $arrayDefault[$theme][$scheme]["colorNavbar"] ? $arrayDefault[$theme][$scheme]["colorNavbar"] : "SCHEME_EMPTY";
			$colorNavbar = $arrayDefault[$theme][$scheme]["colorNavbar"] ? $arrayDefault[$theme][$scheme]["colorNavbar"] : false;
			
			$array["colorNavbarLink"] = $arrayDefault[$theme][$scheme]["colorNavbarLink"] ? $arrayDefault[$theme][$scheme]["colorNavbarLink"] : "SCHEME_EMPTY";
			$colorNavbarLink = $arrayDefault[$theme][$scheme]["colorNavbarLink"] ? $arrayDefault[$theme][$scheme]["colorNavbarLink"] : false;
			
			$array["colorNavbarLinkActive"] = $arrayDefault[$theme][$scheme]["colorNavbarLinkActive"] ? $arrayDefault[$theme][$scheme]["colorNavbarLinkActive"] : "SCHEME_EMPTY";
			$colorNavbarLinkActive = $arrayDefault[$theme][$scheme]["colorNavbarLinkActive"] ? $arrayDefault[$theme][$scheme]["colorNavbarLinkActive"] : false;
			
			$array["colorFooter"] = $arrayDefault[$theme][$scheme]["colorFooter"] ? $arrayDefault[$theme][$scheme]["colorFooter"] : "SCHEME_EMPTY";
			$colorFooter = $arrayDefault[$theme][$scheme]["colorFooter"] ? $arrayDefault[$theme][$scheme]["colorFooter"] : false;
			
			$array["colorFooterText"] = $arrayDefault[$theme][$scheme]["colorFooterText"] ? $arrayDefault[$theme][$scheme]["colorFooterText"] : "SCHEME_EMPTY";
			$colorFooterText = $arrayDefault[$theme][$scheme]["colorFooterText"] ? $arrayDefault[$theme][$scheme]["colorFooterText"] : false;
			
			$array["colorFooterLink"] = $arrayDefault[$theme][$scheme]["colorFooterLink"] ? $arrayDefault[$theme][$scheme]["colorFooterLink"] : "SCHEME_EMPTY";
			$colorFooterLink = $arrayDefault[$theme][$scheme]["colorFooterLink"] ? $arrayDefault[$theme][$scheme]["colorFooterLink"] : false;
            
            $array["color1"] = $arrayDefault[$theme][$scheme]["color1"] ? $arrayDefault[$theme][$scheme]["color1"] : "SCHEME_EMPTY";
			$color1 = $arrayDefault[$theme][$scheme]["color1"] ? $arrayDefault[$theme][$scheme]["color1"] : false;
            
            $array["color2"] = $arrayDefault[$theme][$scheme]["color2"] ? $arrayDefault[$theme][$scheme]["color2"] : "SCHEME_EMPTY";
			$color2 = $arrayDefault[$theme][$scheme]["color2"] ? $arrayDefault[$theme][$scheme]["color2"] : false;
            
            $array["color3"] = $arrayDefault[$theme][$scheme]["color3"] ? $arrayDefault[$theme][$scheme]["color3"] : "SCHEME_EMPTY";
			$color3 = $arrayDefault[$theme][$scheme]["color3"] ? $arrayDefault[$theme][$scheme]["color3"] : false;
            
            $array["color4"] = $arrayDefault[$theme][$scheme]["color4"] ? $arrayDefault[$theme][$scheme]["color4"] : "SCHEME_EMPTY";
			$color4 = $arrayDefault[$theme][$scheme]["color4"] ? $arrayDefault[$theme][$scheme]["color4"] : false;
            
            $array["color5"] = $arrayDefault[$theme][$scheme]["color5"] ? $arrayDefault[$theme][$scheme]["color5"] : "SCHEME_EMPTY";
			$color5 = $arrayDefault[$theme][$scheme]["color5"] ? $arrayDefault[$theme][$scheme]["color5"] : false;
            
            $array["color6"] = $arrayDefault[$theme][$scheme]["color6"] ? $arrayDefault[$theme][$scheme]["color6"] : "SCHEME_EMPTY";
			$color6 = $arrayDefault[$theme][$scheme]["color6"] ? $arrayDefault[$theme][$scheme]["color6"] : false;
            
            $array["color7"] = $arrayDefault[$theme][$scheme]["color7"] ? $arrayDefault[$theme][$scheme]["color7"] : "SCHEME_EMPTY";
			$color7 = $arrayDefault[$theme][$scheme]["color7"] ? $arrayDefault[$theme][$scheme]["color7"] : false;
					
			$array["fontOption"] = $arrayDefault[$theme][$scheme]["fontOption"] ? $arrayDefault[$theme][$scheme]["fontOption"] : "SCHEME_EMPTY";
            $font = 1;

			colorscheme_themeSchemeFile($array, $scheme, EDIR_THEME, EDIR_SCHEME, $status);
			
			if (!setting_set("scheme_custom", "off")) {
				if (!setting_new("scheme_custom", "off")) {
					$error = true;
				}
			}
			
			if (!setting_set("scheme_".$scheme."_customized", "off")) {
				if (!setting_new("scheme_".$scheme."_customized", "off")) {
					$error = true;
				}
			}

			header("Location: ".DEFAULT_URL."/".SITEMGR_ALIAS."/design/layout-editor/index.php?status=successcolors");
            exit;
		}
	} elseif ($_SERVER["REQUEST_METHOD"] == "POST" && $submitAction == "changeExtraFields" && USING_THEME_TEMPLATE && THEME_TEMPLATE_ID) {

        if (validate_listingtemplate($_POST, $message_listingtemplate, false)) {

			$listingtemplate = new ListingTemplate($_POST);

			$template_id = $listingtemplate->getNumber("id");
			$listingtemplate->clearListingTemplateFields();

			$show_order = 0;
			foreach ($label as $fieldname=>$labelname) {
				if (trim($labelname)) {
					$ltf["field"] = $fieldname;
					$ltf["label"] = $labelname;
					unset($aux_fieldvalues);
					if ($fieldvalues[$fieldname]) {
						$auxfieldvalues = explode("\n", $fieldvalues[$fieldname]);
						foreach ($auxfieldvalues as $fieldvalue) {
							$fieldvalue = str_replace("\n", "", $fieldvalue);
							$fieldvalue = str_replace("\r", "", $fieldvalue);
							if (string_strlen(trim($fieldvalue))) {
								$aux_fieldvalues[] = $fieldvalue;
							}
						}
					}
					if ($aux_fieldvalues) $ltf["fieldvalues"] = implode(",", $aux_fieldvalues);
					else $ltf["fieldvalues"] = "";
					$ltf["instructions"] = $instructions[$fieldname];
					$ltf["required"] = $required[$fieldname];
					$ltf["search"] = $search[$fieldname];
					$ltf["searchbykeyword"] = $searchbykeyword[$fieldname];
					$ltf["searchbyrange"] = $searchbyrange[$fieldname];
					$ltf["show_order"] = $show_order;
					$ltf["enabled"] = $enabled[$fieldname];
					$listingtemplate->addListingTemplateField($ltf);
					$show_order++;
				}
			}

			header("Location: ".DEFAULT_URL."/".SITEMGR_ALIAS."/design/layout-editor/index.php?status=success&extrafields=1");
            exit;

		}
        
        // removing slashes added if required
		$_POST = format_magicQuotes($_POST);
		$_GET  = format_magicQuotes($_GET);
		extract($_POST);
		extract($_GET);
        
    }
    
    # ----------------------------------------------------------------------------------------------------
	# FORMS DEFINES
	# ----------------------------------------------------------------------------------------------------
	setting_get("sitemgr_language", $sitemgr_language);
    
    unset($folders);
	$folderthemes = opendir($folderthemesPath);
	$folders = array();
	while ($folder = readdir($folderthemes)) {
		if ($folder != "sample" && $folder != "." && $folder != "..") {
			$folders[] = $folder;
		}
	}
	unset($valuesArray);
	unset($namesArray);

	$_valuesArray = explode(",", EDIR_THEMES);
	$_namesArray  = explode(",", EDIR_THEMENAMES);
    $availableThemes = array();
    $countThemes = 0;
	for ($i = 0; $i < count($_valuesArray); $i++) {
		if (in_array($_valuesArray[$i], $folders)) {
			if ($_namesArray[$i]) {
                $availableThemes[$countThemes]["name"] = $_namesArray[$i];
                $availableThemes[$countThemes]["value"] = $_valuesArray[$i];
                
                switch ($_valuesArray[$i]) {
                    case "default"      :   $availableThemes[$countThemes]["preview_url"] = ($sitemgr_language == "pt_br" ? "http://demodirectory.com.br/" : "http://demodirectory.com/");
                                            break;
                    case "realestate"   :   $availableThemes[$countThemes]["preview_url"] = ($sitemgr_language == "pt_br" ? "http://guiadeimobiliaria.demodirectory.com.br/" : "http://realestate.demodirectory.com/");
                                            break;
                    case "diningguide"  :   $availableThemes[$countThemes]["preview_url"] = ($sitemgr_language == "pt_br" ? "http://guiaderestaurante.demodirectory.com.br/" : "http://diningguide.demodirectory.com/");
                                            break;
                    case "contractors"  :   $availableThemes[$countThemes]["preview_url"] = "http://contractors.demodirectory.com/";
                                            break;
                }
                
                $countThemes++;
                
			}
		}
	}
    
    $availableTabs["default"] = "background,color,css";
    $availableTabs["diningguide"] = "background,price,css";
    $availableTabs["realestate"] = "extrafields,css";
    $availableTabs["contractors"] = "color,css";

	$edir_theme = EDIR_THEME == "" ? "edirectory" : EDIR_THEME;
    
    front_getBackground($customimage, false);
    
    //Current image height
    setting_get("background_image_height", $background_image_height);
    //Current image id
    setting_get("diningguide_background_image_id", $curr_image_id);
    
    setting_get("theme_create_categories", $theme_create_categories);
    if ($theme_create_categories != "done") {
        $onclickJs = "JS_submit(false, true, this);";
    } else {
        $onclickJs = "JS_submit(false, false, this);";
    }
	//Messages
	if ($status == "success") {
		$message = system_showText(LANG_SITEMGR_SETTINGS_THEMES_THEMEWASCHANGED);
		$message_style = "success";
	} elseif ($status == "failed") {
		$message = system_showText(LANG_SITEMGR_MSGERROR_SYSTEMERROR);
		$message_style = "warning";
	} elseif ($status == "successcolors"){
        $message = system_showText(LANG_SITEMGR_COLOR_SAVED);
		$message_style = "success";
    }
    
    if ($_SERVER['REQUEST_METHOD'] != "POST") {
		
		if (!DEMO_LIVE_MODE) {
			$arrayCurValues = unserialize(EDIR_CURR_SCHEME_VALUES);
		} else {
			$arrayDefault = unserialize(ARRAY_DEFAULT_COLORS);
			$arrayCurValues = $arrayDefault[EDIR_THEME];
		}
			
		$colorBackground = $arrayCurValues[EDIR_SCHEME]["colorBackground"];
		$colorHeader = $arrayCurValues[EDIR_SCHEME]["colorHeader"];
		$colorText = $arrayCurValues[EDIR_SCHEME]["colorText"];
		$colorLink = $arrayCurValues[EDIR_SCHEME]["colorLink"];
		$colorNavbar = $arrayCurValues[EDIR_SCHEME]["colorNavbar"];
		$colorNavbarLink = $arrayCurValues[EDIR_SCHEME]["colorNavbarLink"];
		$colorNavbarLinkActive = $arrayCurValues[EDIR_SCHEME]["colorNavbarLinkActive"];
		$colorFooter = $arrayCurValues[EDIR_SCHEME]["colorFooter"];
		$colorFooterText = $arrayCurValues[EDIR_SCHEME]["colorFooterText"];
		$colorFooterLink = $arrayCurValues[EDIR_SCHEME]["colorFooterLink"];
        $color1 = $arrayCurValues[EDIR_SCHEME]["color1"];
        $color2 = $arrayCurValues[EDIR_SCHEME]["color2"];
        $color3 = $arrayCurValues[EDIR_SCHEME]["color3"];
        $color4 = $arrayCurValues[EDIR_SCHEME]["color4"];
        $color5 = $arrayCurValues[EDIR_SCHEME]["color5"];
        $color6 = $arrayCurValues[EDIR_SCHEME]["color6"];
        $color7 = $arrayCurValues[EDIR_SCHEME]["color7"];
		$fontOption = $arrayCurValues[EDIR_SCHEME]["fontOption"];
	
	} else {
		$fontOption = $font;
	}
    
    $arrayDefault = unserialize(ARRAY_DEFAULT_COLORS);
	$arrayAuxCurValues = $arrayDefault[EDIR_THEME];

	unset($arrayFont);
	unset($arrayNameFont);
	unset($arrayValueFont);	
	
	$arrayNameFont[] = $arrayAuxCurValues[EDIR_SCHEME]["fontName"];
	$arrayNameFont[] = "Arial, Helvetica, Sans-serif";
	$arrayNameFont[] = "Courier New, Courier, monospace";
	$arrayNameFont[] = "Georgia, Times New Roman, Times, serif";
	$arrayNameFont[] = "Tahoma, Geneva, sans-serif";
	$arrayNameFont[] = "Trebuchet MS, Arial, Helvetica, sans-serif";
	$arrayNameFont[] = "Verdana, Geneva, sans-serif";
	
	$arrayValueFont[] = 1; //Open Sans (Default and Contractors)
	$arrayValueFont[] = 2; //Arial, Helvetica, Sans-serif
	$arrayValueFont[] = 3; //'Courier New', Courier, monospace
	$arrayValueFont[] = 4; //Georgia, 'Times New Roman', Times, serif
	$arrayValueFont[] = 5; //Tahoma, Geneva, sans-serif
	$arrayValueFont[] = 6; //'Trebuchet MS', Arial, Helvetica, sans-serif
	$arrayValueFont[] = 7; //Verdana, Geneva, sans-serif

	$arrayFont = html_selectBox("font", $arrayNameFont, $arrayValueFont, $fontOption, "", "", "");
	
    $table_colors_1 = array(0 => "Text", 1 => "Link", 2 => "NavbarLink", 3 => "NavbarLinkActive", 4 => "FooterLink", 5 => "FooterText");
    $table_colors_2 = array(0 => "Header", 1 => "Background", 2 => "Navbar", 3 => "Footer");
    $table_colors_3 = array(0 => "1", 1 => "2", 2 => "3", 3 => "4", 4 => "5", 5 => "6", 6 => "7");
    
    /**
    * Pricing Levels
    */
    setting_get("listing_price_symbol", $listing_price_symbol);
    for ($i = 1; $i <= LISTING_PRICE_LEVELS; $i++) {
        setting_get("listing_price_{$i}_from", ${"listing_price_".$i."_from"});
        setting_get("listing_price_{$i}_to", ${"listing_price_".$i."_to"});
    }
    
    if (USING_THEME_TEMPLATE && THEME_TEMPLATE_ID) {
        $listingtemplate = new ListingTemplate(THEME_TEMPLATE_ID);
		$listingtemplate->extract();
		$template_fields = $listingtemplate->getListingTemplateFields();
		if ($template_fields) {
			foreach ($template_fields as $template_field) {
				$label[$template_field["field"]] = $template_field["label"];
				$fieldvalues[$template_field["field"]] = str_replace(",", "\n", $template_field["fieldvalues"]);
				$instructions[$template_field["field"]] = $template_field["instructions"];
				$required[$template_field["field"]] = $template_field["required"];
				$search[$template_field["field"]] = $template_field["search"];
				$searchbykeyword[$template_field["field"]] = $template_field["searchbykeyword"];
				$searchbyrange[$template_field["field"]] = $template_field["searchbyrange"];
                $enabled[$template_field["field"]] = $template_field["enabled"];
			}
		}
    }