<?
	# ----------------------------------------------------------------------------------------------------
	# * FILE: /includes/code/domain.php
	# ----------------------------------------------------------------------------------------------------

    # ----------------------------------------------------------------------------------------------------
	# SUBMIT
	# ----------------------------------------------------------------------------------------------------
	
	extract($_POST);

	unset($_GET);
	var_dump($_POST["event_feature"]);
	if ($_SERVER['REQUEST_METHOD'] == "POST") {
		if (validate_form("domain", $_POST, $message_domain)) {

			$domainObj = new Domain();
			if ($_POST["server"] == "default") {
				$_db_host = explode(":", _DIRECTORYDB_HOST);
				$sINFO["database_host"] = $_db_host[0];
				$sINFO["database_port"] = $_db_host[1];
				$sINFO["database_username"] = _DIRECTORYDB_USER;
				$sINFO["database_password"] = _DIRECTORYDB_PASS;
				$has_server = true;
			} else {
				$sINFO = explode(" : ", $_POST["server"]);

				if ($sINFO[0]) {
					$where = " WHERE database_host = '".$sINFO[0]."' ";
				}
				if ($sINFO[1]) {
					$where .= " AND database_port = '".$sINFO[1]."' ";
				}

				$dbObj = db_getDBObject(DEFAULT_DB, true);
				$sql = "SELECT database_host, database_port, database_name, database_username, database_password FROM Domain ".($where? $where: "")." ORDER BY id LIMIT 1";
				$result = $dbObj->query($sql);
				if (mysql_num_rows($result) > 0) {
					$row = mysql_fetch_assoc($result);

					unset($sINFO);
					$sINFO["database_host"] = $row["database_host"];
					$sINFO["database_port"] = $row["database_port"];
					$sINFO["database_username"] = $row["database_username"];
					$sINFO["database_password"] = $row["database_password"];
				} 
			}
			$domainObj->setString("article_feature", $_POST["article_feature"]? "on": "off");
			$domainObj->setString("banner_feature", $_POST["banner_feature"]? "on": "off");
			$domainObj->setString("classified_feature", $_POST["classified_feature"]? "on": "off");
			$domainObj->setString("event_feature", $_POST["event_feature"]? "on": "off");
			$domainObj->Save();

			$domain_id = $domainObj->getNumber("id");

			$domain_id = $domain_id;
			$selected_server = $server;
			$is_valid = true;
		}
	}
 
	# ----------------------------------------------------------------------------------------------------
	# FORMS DEFINES
	# ----------------------------------------------------------------------------------------------------
	//Check user privileges. If one of them is missing, user can not add a new domain
	$deniedOperation = false;
	
	$folderPerm = true;

	//if (system_checkPerm(EDIRECTORY_ROOT."/custom") != PERMISSION_CUSTOM_FOLDER) { 
	if ((int)system_checkPerm(EDIRECTORY_ROOT."/custom") < (int)PERMISSION_CUSTOM_FOLDER) {
		$folderPerm = system_checkPerm(EDIRECTORY_ROOT."/custom");
		$deniedOperation = true;
	}


	if (ini_get("safe_mode")) { 
		$safeMode = true;
		$deniedOperation = true;
	} else {
		$safeMode = false;
	}

	$dbObj = db_getDBObject(DEFAULT_DB, true);

	$sqlDomain = "SELECT DISTINCT `database_host`, `database_port`, `database_username`, `database_password` FROM `Domain` WHERE `status` = 'A'";
	$resDomain = $dbObj->Query($sqlDomain);

	if (mysql_num_rows($resDomain) > 0) {
		unset($servers);

		$_aux_conf = explode(":", _DIRECTORYDB_HOST);
		$_db_host = $_aux_conf[0];
		$_db_port = $_aux_conf[1];

		$domainCheck = new Domain();
		$privileges = $domainCheck->checkUserProvilegies(false);

		$hasPrivilegies = false;
		if (count($privileges["denied"]) <= 0){
			$servers[] = system_showText(LANG_SITEMGR_DOMAIN_CURRENT_SERVER);
			$hasPrivilegies = true;
		}

		while ($rowDomain = mysql_fetch_assoc($resDomain)) {
			if ($rowDomain["database_host"] != $_db_host || $rowDomain["database_port"] != $_db_port) {

				$host_link = $rowDomain["database_host"];
				$rowDomain["database_port"] ? $host_link .= ":".$rowDomain["database_port"] : "";
				$link = mysql_connect($host_link, $rowDomain["database_username"], $rowDomain["database_password"]);
				$privileges = $domainCheck->checkUserProvilegies($link);

				if (count($privileges["denied"]) <= 0){
					$servers[] = $rowDomain["database_host"].($rowDomain["database_port"]? " : ".$rowDomain["database_port"]: "");
					$hasPrivilegies = true;
				}
			}
		}

		if (!$hasPrivilegies) {
			$deniedOperation= true;
		}
	}
	
?>