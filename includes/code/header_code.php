<?php

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /includes/code/header_code.php
	# ----------------------------------------------------------------------------------------------------

    header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", FALSE);
    header("Pragma: no-cache");
    header("Content-Type: text/html; charset=".EDIR_CHARSET, TRUE);

    //Contact Us info
    setting_get("contact_address", $contact_address);
    setting_get("contact_zipcode", $contact_zipcode);
    setting_get("contact_country", $contact_country);
    setting_get("contact_state", $contact_state);
    setting_get("contact_city", $contact_city);
    setting_get("contact_phone", $contact_phone);
    

    //This function returns the variables to fill in the meta tags content below. Do not change this line.
    front_getHeaderTag($headertag_title, $headertag_author, $headertag_description, $headertag_keywords);

?>