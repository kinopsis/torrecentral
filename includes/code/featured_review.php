<?
	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /includes/code/featured_review.php
	# ----------------------------------------------------------------------------------------------------

    if (ACTUAL_MODULE_FOLDER == LISTING_FEATURE_FOLDER || ACTUAL_MODULE_FOLDER == "" || (!defined("ACTUAL_MODULE_FOLDER") && string_strpos($_SERVER["REQUEST_URI"], "results.php") !== false)) {
        $module_review = "listing";
        $levelsWithReview = system_retrieveLevelsWithInfoEnabled("has_review");
        $levelObj = new ListingLevel();
    } elseif (ACTUAL_MODULE_FOLDER == PROMOTION_FEATURE_FOLDER) {
        $module_review = "promotion";
        $levelsWithReview = true;
        $levelObj = new ListingLevel();
        $levels = $levelObj->getLevelValues();
        unset($allowed_levels);
        foreach ($levels as $each_level) {
            if ( ($levelObj->getActive($each_level) == 'y') && ($levelObj->getHasPromotion($each_level) == 'y' ) ) {
                $allowed_levels[] = $each_level;
            }
        }

        $search_levels = ($allowed_levels ? implode(",", $allowed_levels) : "0");
    } elseif (ACTUAL_MODULE_FOLDER == ARTICLE_FEATURE_FOLDER) {
        $module_review = "article";
        $levelsWithReview = true;
    }

	setting_get("commenting_edir", $commenting_edir);
    setting_get("review_{$module_review}_enabled", $review_enabled);
    
    $featuredReviews = array();
    $count = 0;

    if ($review_enabled == "on" && $commenting_edir && $levelsWithReview) {
        
		# ----------------------------------------------------------------------------------------------------
		# LIMIT
		# ----------------------------------------------------------------------------------------------------

		$lastItemStyle = 0;
		$numberOfReviews = ($numberOfReviews ? $numberOfReviews : 3);
		$reviewMaxSize = 120;

		# ----------------------------------------------------------------------------------------------------
		# CODE
		# ----------------------------------------------------------------------------------------------------

		$sql = "SELECT item_id,
					member_id,
					added,
					reviewer_name,
					reviewer_location,
					review_title,
					review,
					rating,
					Account.image_id,
					Account.facebook_image,
					Account.has_profile,";
                    
        if ($module_review == "listing") {
            
            $sql .= "".(FORCE_SECOND ? "Listing_Summary" : "Listing").".id,
                            ".(FORCE_SECOND ? "Listing_Summary" : "Listing").".title,
                            ".(FORCE_SECOND ? "Listing_Summary" : "Listing").".friendly_url,
                            ".(FORCE_SECOND ? "Listing_Summary" : "Listing").".level
            FROM Review
            INNER JOIN  ".(FORCE_SECOND ? "Listing_Summary" : "Listing")." ON Review.item_id = ".(FORCE_SECOND ? "Listing_Summary" : "Listing").".id
            LEFT JOIN AccountProfileContact Account ON (Account.account_id = member_id) 
            WHERE item_type = 'listing' AND 
                  approved = 1 AND 
                  ".(FORCE_SECOND ? "Listing_Summary" : "Listing").".status = 'A' AND 
                  ".(FORCE_SECOND ? "Listing_Summary" : "Listing").".level IN (".implode(",", $levelsWithReview).") ORDER BY ".($randomReview ? "RAND()" : "added DESC")." LIMIT ".$numberOfReviews;
            
        } elseif ($module_review == "article") {
            
            $sql .= "   Article.id,
                        Article.title,
                        Article.friendly_url
                    FROM Review
                    INNER JOIN  Article ON Review.item_id = Article.id
                    LEFT JOIN AccountProfileContact Account ON (Account.account_id = member_id)
                    WHERE item_type = 'article' AND approved = 1 AND Article.status = 'A' ORDER BY added DESC LIMIT ".$numberOfReviews;
            
        } elseif ($module_review == "promotion") {

            $visibility_start = date('H')*60+date('i');
            $visibility_end = date('H')*60+date('i');
            
            $sql .= "   Promotion.id,
                        Promotion.name AS title,
                        Promotion.friendly_url
                    FROM Review
                    INNER JOIN  Promotion ON Review.item_id = Promotion.id
                    LEFT JOIN AccountProfileContact Account ON (Account.account_id = member_id)
                    WHERE item_type = 'promotion' AND 
                    approved = 1 AND 
                    Promotion.listing_id > 0 AND 
                    Promotion.listing_status = 'A' AND 
                    Promotion.end_date >= DATE_FORMAT(NOW(), '%Y-%m-%d') AND 
                    Promotion.start_date <= DATE_FORMAT(NOW(), '%Y-%m-%d') AND
                    ((Promotion.visibility_start <= $visibility_start AND Promotion.visibility_end >= $visibility_end ) OR (Promotion.visibility_start = 24 AND Promotion.visibility_end = 24)) AND
                    Promotion.listing_level IN ($search_levels)
                    ORDER BY added DESC LIMIT ".$numberOfReviews;

        }
        
        $dbObj = db_getDBObject();
		$result = $dbObj->query($sql);
        
        if ($getTotalReviews) {
            $sqlTotal = str_replace("ORDER BY RAND()", "", $sql);
            $sqlTotal = str_replace("LIMIT ".$numberOfReviews, "", $sqlTotal);
            $totalReviews = mysql_num_rows($dbObj->query($sqlTotal));
        }

		if (mysql_numrows($result)) {

			while ($row = mysql_fetch_array($result)) {

				$lastItemStyle++;

				if ($lastItemStyle == 1) {
					$itemStyle = "first";
				} elseif ($lastItemStyle == 3) {
					$itemStyle = "last";
				} else {
					$itemStyle = "";
				}
                
                $featuredReviews[$count]["style"] = $itemStyle;

				if (SOCIALNETWORK_FEATURE == "on") {
					if ($row["member_id"] && $row["has_profile"] == "y") {
						$imgTag = socialnetwork_writeLink($row["member_id"], "profile", "general_see_profile", $row["image_id"], false, false);
                        $featuredReviews[$count]["image"] = $imgTag;
						if (!$imgTag){
							$featuredReviews[$count]["image"] = "<span class=\"no-image no-link\"></span>";
						}
					} else {
						$featuredReviews[$count]["image"] = "<span class=\"no-image no-link\"></span>";
					}
				}

				$rate_stars = "";
				if ($row["rating"]) {
					for ($x=0 ; $x < 5 ;$x++) {
						if ($row["rating"] > $x) $rate_stars .= "<img src=\"".THEMEFILE_URL."/".EDIR_THEME."/images/iconography/img_rateMiniStarOn.png\" alt=\"Star On\" align=\"bottom\" />";
						else $rate_stars .= "<img src=\"".THEMEFILE_URL."/".EDIR_THEME."/images/iconography/img_rateMiniStarOff.png\" alt=\"Star Off\" align=\"bottom\" />";
					}
				}
                
                $featuredReviews[$count]["stars"] = $rate_stars;
                
                $featuredReviews[$count]["avg_review"] = $row["rating"];

                $detailLink = "".constant(strtoupper($module_review)."_DEFAULT_URL")."/".ALIAS_REVIEW_URL_DIVISOR."/".$row["friendly_url"];
                
                if ($module_review == "listing") {
                    if ($levelObj->getDetail($row["level"]) == "y") {
                        $detailItemLink = "".LISTING_DEFAULT_URL."/".$row["friendly_url"].".html";
                    } else {
                        $detailItemLink = "".LISTING_DEFAULT_URL."/results.php?id=".$row["id"];
                    }
                } else {
                    $detailItemLink = "".constant(strtoupper($module_review)."_DEFAULT_URL")."/".$row["friendly_url"].".html";
                }
                
                $featuredReviews[$count]["detailItemLink"] = $detailItemLink;
                $featuredReviews[$count]["detailLink"] = $detailLink;
                $featuredReviews[$count]["title"] = string_htmlentities($row["title"]);
                
				$review = "";
				if (string_strlen(trim($row["review"])) > 0) {
					$review .= ($fullReview ? $row["review"] : system_showTruncatedText($row["review"], $reviewMaxSize));
				}
                
                $featuredReviews[$count]["review"] = $review;

                $str_time = format_getTimeString($row["added"]);

                $membersStr = "";
                if ($row["member_id"]) {
                    $membersStr = socialnetwork_writeLink($row["member_id"], "profile", "general_see_profile");
                    if ($membersStr) {
                        $featuredReviews[$count]["reviewer_name"] = (($row["reviewer_name"]) ? socialnetwork_writeLink($row["member_id"], "profile", "general_see_profile") : system_showText(LANG_NA));
                    } else {
                        $featuredReviews[$count]["reviewer_name"] = (($row["reviewer_name"]) ? string_htmlentities($row["reviewer_name"]) : system_showText(LANG_NA));
                    }
                } else {
                    $featuredReviews[$count]["reviewer_name"] = (($row["reviewer_name"]) ? string_htmlentities($row["reviewer_name"]) : system_showText(LANG_NA));
                }
                $featuredReviews[$count]["reviewer_location"] = (($row["reviewer_location"]) ? string_htmlentities($row["reviewer_location"]) : system_showText(LANG_NA));
                $featuredReviews[$count]["date"] = format_date($row["added"], DEFAULT_DATE_FORMAT, "datetime")." - ".$str_time;
                $featuredReviews[$count]["date_notime"] = format_date($row["added"], DEFAULT_DATE_FORMAT, "datetime");
						
                $count++;

			}
		}
    }
?>