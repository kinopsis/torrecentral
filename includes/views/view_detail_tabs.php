<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /includes/views/view_detail_tabs.php
	# ----------------------------------------------------------------------------------------------------

    if (string_strpos(ACTUAL_MODULE_FOLDER, LISTING_FEATURE_FOLDER) !== false || $signUpListing) {

        $item_type = "listing";
        $item_id = $listingtemplate_id;
        $tabReview = $listingtemplate_review;
        $tabMenu = (THEME_LISTING_MENU ? $listingtemplate_attachment_file : false);
        $tabVideo = $listingtemplate_video_snippet;
        $tabDeal = $hasDeal;
        $signUpListing = false;
        
    } elseif (string_strpos(ACTUAL_MODULE_FOLDER, CLASSIFIED_FEATURE_FOLDER) !== false || $signUpClassified) {
        
        $tabReview = false;
        $tabMenu = false;
        $tabVideo = false;
        $tabDeal = false;
        $signUpClassified = false;
        
    } elseif (string_strpos(ACTUAL_MODULE_FOLDER, EVENT_FEATURE_FOLDER) !== false || $signUpEvent) {

        $tabReview = false;
        $tabMenu = false;
        $tabVideo = $event_video_snippet;
        $signUpEvent = false;
        $tabDeal = false;
        
    } elseif (string_strpos(ACTUAL_MODULE_FOLDER, ARTICLE_FEATURE_FOLDER) !== false || $signUpArticle) {
        
        $tabReview = $detail_review;
        $tabMenu = false;
        $tabVideo = false;
        $signUpArticle = false;
        $tabDeal = false;
        
    }

?>

    <div class="navbar detail-navbar">
        
        <div class="navbar-inner">
            
            <ul class="nav">
                
                <? if ($tabOverview) { ?>
                <li class="tab-overview <?=$tabActiveOverview ? "active" : ""?>">
                    <a href="<?=($user ? "#content_overview" : "javascript: void(0);")?>">
                        <?=system_showText(LANG_LABEL_OVERVIEW);?>
                    </a>
                </li>
                <? } ?>
                
                <? if ($tabMenu) { ?>
                <li class="tab-menu <?=$tabActiveMenu ? "active" : ""?>">
                    <a href="<?=($user ? "#content_menu" : "javascript: void(0);")?>">
                        <?=system_showText(LANG_LABEL_MENU);?>
                    </a>
                </li> 
                <? } ?>
                
                <? if ($tabVideo) { ?>
                <li class="tab-video <?=$tabActiveVideo ? "active" : ""?>">
                    <a href="<?=($user ? "#content_video" : "javascript: void(0);")?>">
                        <?=system_showText(LANG_LABEL_VIDEO);?>
                    </a>
                </li>
                <? } ?>
                
                <? if ($tabDeal) { ?>
                <li class="tab-deal <?=$tabActiveDeal ? "active" : ""?>">
                    <a href="<?=($user ? "#content_deal" : "javascript: void(0);")?>">
                        <?=ucfirst(system_showText(LANG_PROMOTION));?>
                    </a>
                </li>
                <? } ?>
                
                <? if ($tabReview) { ?>
                <li class="tab-review <?=$tabActiveReview ? "active" : ""?>">
                    <a href="<?=($user ? "#content_review" : "javascript: void(0);")?>">
                        <?=system_showText(LANG_REVIEW_PLURAL);?>
                    </a>
                </li>
                <? } ?>
                
            </ul>

        </div>
        
    </div>