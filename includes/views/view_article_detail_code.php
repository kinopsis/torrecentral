<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /includes/views/view_article_detail_code.php
	# ----------------------------------------------------------------------------------------------------

?>

    <div class="detail-article" itemscope itemtype="http://schema.org/Article">

        <div class="row-fluid">

            <div class="top-info">

                <h3 itemprop="name"><?=$article_title?></h3>

                <div class="row-fluid">

                    <div class="span8" <?=$summary_review && count($reviewsArr) > 0 ? "itemprop=\"aggregateRating\" itemscope itemtype=\"http://schema.org/AggregateRating\"" : ""?>>
                        <? if ($summary_review) { ?>
                            <?=$summary_review;?>
                        <? } ?>
                        <? if ($summary_review && count($reviewsArr) > 0) { ?>
                            <meta itemprop="ratingValue" content="<?=$rate_avg;?>" />
                            <meta itemprop="ratingCount" content="<?=count($reviewsArr);?>" />
                        <? } ?>
                    </div>

                    <div class="span4 <?=($summary_review ? "text-right" : "text-left")?>">
                        <?=$article_icon_navbar?>
                    </div>

                </div>

            </div>

        </div>

        <div class="row-fluid">

            <? if ($article_category_tree) { ?>
                <div class="span12 top-info">
                    <?=$article_category_tree?>
                </div>
            <? } ?>

        </div>

        <div class="row-fluid middle-info overview">

            <? if ($imageTag || $articleGallery) { ?>            
           
            <div class="span7">

                <section <?=($onlyMain && !$isNoImage ? "class=\"gallery-overview detailfeatures\"" : "class=\"gallery-overview\"")?> >

                    <? if (($imageTag && !$articleGallery && $onlyMain) || ($tPreview && $imageTag)) { ?>

                        <div class="image">
                            <?=$imageTag?>
                        </div>

                    <? } ?>

                    <? if ($articleGallery) { ?>

                        <div <?=$tPreview ? "class=\"ad-gallery gallery\"" : ""?>>
                            <?=$articleGallery?>
                        </div>

                    <? } ?>

                </section>

            </div>

            <? } ?>

            <? if ($article_publicationDate) { ?>

                <h6><?=system_showText(LANG_ARTICLE_PUBLISHED)?></h6>
                <?=$article_publicationDate?>

            <? } ?>

            <? if ($article_author) { ?>

                <?=system_showText(LANG_BY)?> <span itemprop="author"><?=$article_authorStr?></span>

            <? } elseif ($article_name) { ?>

                <?=system_showText(LANG_BY)?> <?=$article_name?>

            <? } ?>

        </div>

        <?
        $tabOverview = false;
        if ($article_content) { ?>
        
            <div id="content_overview">
                <?
                $tabActiveOverview = true;
                $tabOverview = true;
                include(INCLUDES_DIR."/views/view_detail_tabs.php");
                $tabActiveOverview = false;
                ?>
            </div>
        
            <div class="long"><?=($article_content)?></div>

        <? } ?>

        <? if ($detail_review) { ?>

        <div id="content_review" class="area-content">
            <?
            $tabActiveReview = true;
            include(INCLUDES_DIR."/views/view_detail_tabs.php");
            $tabActiveReview = false;
            ?>
        </div>

        <div class="row-fluid">

            <div class="span12">
                <div class="span8 large-rating">
                    <div class="rate">
                        <div class="stars-rating">
                            <div class="rate-<?=(is_numeric($rate_avg) ? $rate_avg : "0")?>"></div>
                        </div>
                    </div>
                </div>

                <div class="span4 pull-right">
                    <a rel="nofollow" class="btn-review btn btn-success <?=$class;?>" href="<?=($user ? $linkReviewFormPopup : "javascript:void(0);");?>"><?=system_showText(LANG_REVIEWRATEIT);?></a>
                </div>

            </div>

        </div>

        <div id="loading_reviews">
            <img src="<?=DEFAULT_URL."/theme/".EDIR_THEME."/images/iconography/icon-loading-location.gif"?>" alt="<?=system_showText(LANG_WAITLOADING)?>"/>
        </div>

        <div id="all_reviews" class="row-fluid"></div>
        
        <script type="text/javascript">
            $('document').ready(function() {
                loadReviews('<?=$item_type?>', <?=($user ? $item_id : "'preview'")?>, 1, 'tab');
            });
        </script>

        <? } ?>

    </div>