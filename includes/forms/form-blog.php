<?
    /*
    * # Admin Panel for eDirectory
    * @copyright Copyright 2014 Arca Solutions, Inc.
    * @author Basecode - Arca Solutions, Inc.
    */

    # ----------------------------------------------------------------------------------------------------
	# * FILE: /includes/forms/form-blog.php
	# ----------------------------------------------------------------------------------------------------
?>

    <script type="text/javascript" src="<?=DEFAULT_URL?>/includes/tiny_mce/tiny_mce_src.js"></script>
    
    <div class="col-md-7">

        <!-- Item Name is separated from all informations -->
        <div class="form-group" id="tour-title">
            <? system_fieldsGuide($arrayTutorial, $counterTutorial, system_showText(LANG_BLOG_TITLE), "tour-title"); ?>
            <label for="name" class="label-lg"><?=system_showText(LANG_BLOG_TITLE);?></label>
            <input type="text" class="form-control input-lg" name="title" id="name" value="<?=$title?>" maxlength="100" <?=(!$id) ? " onblur=\"easyFriendlyUrl(this.value, 'friendly_url', '".FRIENDLYURL_VALIDCHARS."', '".FRIENDLYURL_SEPARATOR."');\" " : ""?> placeholder="<?=system_showText(LANG_HOLDER_BLOGTITLE)?>">
            <input type="hidden" name="friendly_url" id="friendly_url" value="<?=$friendly_url?>">
        </div>
        
        <!-- Panel Basic Informartion  -->
        <div class="panel panel-form">           
            
            <div class="panel-heading"><?=system_showText(LANG_BASIC_INFO)?></div>

            <div class="panel-body">

                <div class="form-group row" id="tour-categories">
                    
                    <? system_fieldsGuide($arrayTutorial, $counterTutorial, system_showText(LANG_LABEL_CATEGORY_PLURAL), "tour-categories"); ?>
                    <div class="col-xs-12">
                        <label for="categories"><?=system_showText(LANG_LABEL_CATEGORY_PLURAL);?></label>
                    </div>
                    
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="categories" placeholder="<?=system_showText(LANG_SELECT_CATEGORIES);?>">
                    </div>
                    
                    <input type="hidden" name="return_categories" value="">
                    
                    <?=str_replace("<select", "<select class=\"hidden\"", $feedDropDown);?>
                            
                    <div class="col-sm-3">
                        <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal-categories" id="action-categoryList"><?=system_showText(LANG_ADD_CATEGORIES);?> <i class="ionicons ion-ios7-photos-outline"></i></button>
                    </div>
                    
                </div>
                
                <div class="form-group row">
                    <? system_fieldsGuide($arrayTutorial, $counterTutorial, system_showText(LANG_LABEL_STATUS), "tour-status"); ?>
                    <div class="col-sm-4" id="tour-status">
                        <label for="status"><?=system_showText(LANG_LABEL_STATUS);?></label>
                        <?=($statusDropDown)?>
                    </div>
                </div>
                                
                <? system_fieldsGuide($arrayTutorial, $counterTutorial, system_showText(LANG_LABEL_POST_CONTENT), "tour-content"); ?>
                <div class="form-group" id="tour-content">
                    <label><?=system_showText(LANG_LABEL_POST_CONTENT)?></label>
                    <div class="table-responsive">
                    <? 
                    // TinyMCE Editor Init
                    //fix ie bug with images
                    if (!($content)) $content =  "&nbsp;".$content;

                    // calling TinyMCE
                    system_addTinyMCE("", "none", "advanced", "content", "30", "15", "100%", $content, false);
                    ?>
                    </div>
                </div>
                
                <? system_fieldsGuide($arrayTutorial, $counterTutorial, system_showText(LANG_LABEL_KEYWORDS_FOR_SEARCH), "tour-keywords"); ?>
                <div class="form-group" id="tour-keywords">
                    <label for="keywords"><?=system_showText(LANG_LABEL_KEYWORDS_FOR_SEARCH)?></label>
                    <input type="text" name="keywords" id="keywords" class="form-control tag-input <?=($highlight == "additional" && !$keywords ? "highlight" : "")?>" placeholder="<?=system_showText(LANG_HOLDER_KEYWORDS);?>" value="<?=$keywords?>">
                    <p class="help-block small"><?=ucfirst(system_showText(LANG_LABEL_MAX));?> <?=MAX_KEYWORDS?> <?=system_showText(LANG_LABEL_KEYWORDS);?></p>
                </div>

            </div>

        </div>
        
    </div>

    <div class="col-md-5">

        <!-- Images-->
        <div class="panel panel-form-media" id="tour-image">
            <?
            system_fieldsGuide($arrayTutorial, $counterTutorial, system_showText(LANG_LABEL_IMAGE_PLURAL), "tour-image");
            ?>
            <div class="panel-heading">
                <?=system_showText(LANG_LABEL_IMAGE);?>
                <div class="pull-right">
                    <input id="upload-images" type="file" name="files[]" class="filestyle upload-files">
                </div>
            </div>
            
            <div class="panel-body">
                          
                <div id="filesImages" class="files uploaded-files"></div>

                <div id="no-filesImages" class="no-files center-block text-center">
                    <i class="icon-images9"></i>
                    <p class="text-muted"><?=system_showText(LANG_MSG_DROP_IMAGE);?></p>
                </div>
                
            </div>
            
        </div>       

    </div>

    <!-- ######################## -->
    <!-- Modal Categories -->
    <!-- ######################## -->

    <div class="modal fade" id="modal-categories" tabindex="-1" role="dialog" aria-labelledby="modal-categories" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title"><?=system_showText(LANG_CATEGORIES_SUBCATEGS)?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="multiple-categories">
                                <ul id="blog_categorytree_id_0">&nbsp;</ul>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <p><?=system_showText(LANG_CATEGORIES_CATEGORIESMAXTIP1)." <strong>".system_showText(MAX_CATEGORY_ALLOWED)."</strong> ".system_showText(LANG_CATEGORIES_CATEGORIESMAXTIP2)?></p>
                            <br>
                            <input type="text" id="category-select" class="form-control" placeholder="<?=str_replace("[max]", MAX_CATEGORY_ALLOWED, system_showText(LANG_SELECTMAX_CATEGORIES));?>" value="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="action-categories"><?=system_showText(LANG_BUTTON_OK);?></button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <? system_displayTinyMCE('content'); ?>