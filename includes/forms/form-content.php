<?php
    /*
    * # Admin Panel for eDirectory
    * @copyright Copyright 2014 Arca Solutions, Inc.
    * @author Basecode - Arca Solutions, Inc.
    */

    # ----------------------------------------------------------------------------------------------------
	# * FILE: /includes/forms/form-content.php
	# ----------------------------------------------------------------------------------------------------
?>

    <!-- Panel Basic Description and SEO CENTER  -->
    <? if ((($auxContent->getString("section") == "general") || (string_strpos($auxContent->type, "Advertisement") === false)) 
    && (string_strpos($auxContent->type, "Bottom") === false) && (string_strpos($auxContent->type, "Terms") === false) && ($auxContent->getString("section") != "member") && (string_strpos($auxContent->type,"Packages") === false )) { ?>
    <div class="panel panel-form">

        <div class="panel-heading">
            <?=system_showText(LANG_LABEL_DESCRIPTION)?>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="title"><?=system_showText(LANG_LABEL_TITLE)?></label>
                <input id="title" type="text" name="title" value="<?=$contentObj->title?>" class="form-control" />
            </div>
            <div class="form-group">
                <label for="description"><?=system_showText(LANG_LABEL_DESCRIPTION)?></label>
                <textarea id="description" name="description" rows="3" class="form-control textarea-counter" data-chars="250" data-msg="<?=system_showText(LANG_MSG_CHARS_LEFT)?>"><?=$contentObj->description?></textarea>
            </div>
            <div class="form-group">
                <label for="kewywords"><?=system_showText(LANG_SITEMGR_LABEL_KEYWORDS)?></label>
                <input type="text" class="form-control tag-input" id="keywords" name="keywords" value="<?=$contentObj->keywords;?>" placeholder="<?=system_showText(LANG_HOLDER_KEYWORDS);?>">
            </div>
        </div>

    </div>
    <? } ?>

    <!-- Panel HTML Content -->
    <? if ($allowContent) { ?>
    <div class="panel panel-form">

        <div class="panel-heading">
            <?=system_showText(LANG_LABEL_CONTENT)?>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <? // TinyMCE Editor Init
                    // getting content
                    $content = $contentObj->getString("content");
                    //fix ie bug with images
                    if (!$content) $content = "&nbsp;".$content;
                    // calling TinyMCE
                    system_addTinyMCE("", "exact", "advanced", "content_html", "30", "25", "100%", $content);
                ?>
            </div>
        </div>

    </div>
    <? } ?>

    <? if (THEME_HOMEPAGE_FIELDS && $auxContent->getString("type") == "Home Page") { ?>
                
        <div class="panel panel-form">

            <div class="panel-heading">
                <?=system_showText(LANG_SITEMGR_EXTRA_CONTENT)?>
            </div>
            <div class="panel-body">
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="front_text_top"><?=system_showText(LANG_SITEMGR_EXTRA_ADVTEXT_TOP)?></label>
                        <input type="text" class="form-control" name="front_text_top" id="front_text_top" value="<?=htmlspecialchars($front_text_top);?>" maxlength="50" placeholder="<?=system_showText(LANG_SITEMGR_EXTRA_ADVTEXT_TOP_TIP);?>">
                    </div>
                    <div class="form-group">
                        <label for="front_text_sidebar"><?=system_showText(LANG_SITEMGR_EXTRA_ADVTEXT_SIDEBAR)?></label>
                        <input type="text" class="form-control" name="front_text_sidebar" id="front_text_sidebar" value="<?=htmlspecialchars($front_text_sidebar);?>" maxlength="50" placeholder="<?=htmlspecialchars(system_showText(LANG_SITEMGR_EXTRA_ADVTEXT_SIDEBAR_TIP));?>">
                    </div>
                    <div class="form-group">
                        <label for="front_text_sidebar2"><?=system_showText(LANG_SITEMGR_EXTRA_ADVTEXT_SIDEBAR2)?></label>
                        <input type="text" class="form-control" name="front_text_sidebar2" id="front_text_sidebar2" value="<?=htmlspecialchars($front_text_sidebar2);?>" maxlength="50" placeholder="<?=htmlspecialchars(system_showText(LANG_SITEMGR_EXTRA_ADVTEXT_SIDEBAR2_TIP));?>">
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="front_itunes_url"><?=system_showText(LANG_SITEMGR_EXTRA_IOS)?></label>
                            <input type="text" class="form-control" name="front_itunes_url" id="front_itunes_url" value="<?=htmlspecialchars($front_itunes_url);?>" placeholder="<?=htmlspecialchars(system_showText(LANG_SITEMGR_EXTRA_IOS_TIP));?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="front_gplay_url"><?=system_showText(LANG_SITEMGR_EXTRA_ANDROID)?></label>
                            <input type="text" class="form-control" name="front_gplay_url" id="front_gplay_url" value="<?=htmlspecialchars($front_gplay_url);?>" placeholder="<?=htmlspecialchars(system_showText(LANG_SITEMGR_EXTRA_ANDROID_TIP));?>">
                        </div>
                    </div>
                    <? if ($review_enabled == "on" && $commenting_edir) { ?>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="front_review_counter" value="on" <?=($front_review_counter == "on" ? "checked=\"checked\"" : "")?> />
                                <?=system_showText(LANG_SITEMGR_EXTRA_REVIEW);?>
                                <p class="small text-muted"><?=system_showText(LANG_SITEMGR_EXTRA_REVIEW_TIP);?></p>
                            </label>
                        </div>
                    </div>
                    <? } ?>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="front_testimonial"><?=system_showText(LANG_SITEMGR_EXTRA_ADVTEXT_TESTIMONIAL)?></label>
                        <textarea name="front_testimonial" id="front_testimonial" rows="4" class="form-control textarea-counter" data-chars="200" data-msg="<?=system_showText(LANG_MSG_CHARS_LEFT);?>" placeholder="<?=htmlspecialchars(system_showText(LANG_SITEMGR_EXTRA_ADVTEXT_TESTIMONIAL_TIP));?>"><?=$front_testimonial?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="front_testimonial_author"><?=system_showText(LANG_SITEMGR_EXTRA_ADVTEXT_TESTIMONIAL_AUTHOR)?></label>
                        <input type="text" class="form-control" name="front_testimonial_author" id="front_testimonial_author" value="<?=htmlspecialchars($front_testimonial_author);?>" maxlength="60" placeholder="<?=htmlspecialchars(system_showText(LANG_SITEMGR_EXTRA_ADVTEXT_TESTIMONIAL_TIP2));?>">
                    </div>
                    <div class="form-group row">
                        <div class="col-xs-6">
                            <label for="image"><?=system_showText(LANG_SITEMGR_EXTRA_ADVTEXT_TESTIMONIAL_IMAGE)?></label>
                            <input type="file" name="image" id="image" class="filestyle upload-files">
                        </div>
                        <? if (file_exists(EDIRECTORY_ROOT.IMAGE_TESTIMONIAL_PATH)) { ?>
                            <div class="col-xs-6 text-right" id="div_remove">
                                <br>
                                <span onclick="$('#div_remove').addClass('hidden'); $('#remove_image').attr('value', '1');" class="btn btn-icon btn-primary  pull-right action-delete-image"><i class="icon-ion-ios7-trash-outline"></i></span>
                                <input type="hidden" name="remove_image" id="remove_image" value="" />
                            </div>
                        <? } ?>
                    </div>
                </div>
            </div>

        </div>

    <? } ?>