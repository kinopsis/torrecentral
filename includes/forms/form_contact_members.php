<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /includes/forms/form_contact_members.php
	# ----------------------------------------------------------------------------------------------------

    include(INCLUDES_DIR."/code/newsletter.php");
?>

    <div id="contact-info">

        <div class="left textright">
            <h2><?=system_showText(LANG_LABEL_CONTACT_INFORMATION);?></h2>
            <span><?=system_showText(LANG_LABEL_ACCOUNT_CONTACT_TIP);?></span>
        </div>

        <div class="right">

            <div class="cont_50">
                <label><?=system_showText(LANG_LABEL_FIRST_NAME);?> <a href="javascript: void(0);">* <span><?=system_showText(LANG_LABEL_REQUIRED_FIELD);?></span></a></label>
                <input type="text" name="first_name" value="<?=$first_name?>" />
            </div>

            <div class="cont_50">
                <label><?=system_showText(LANG_LABEL_LAST_NAME);?> <a href="javascript: void(0);">* <span><?=system_showText(LANG_LABEL_REQUIRED_FIELD);?></span></a></label>
                <input type="text" name="last_name" value="<?=$last_name?>" />
            </div>

            <div class="cont_100">
                <label><?=system_showText(LANG_LABEL_COMPANY)?></label>
                <input type="text" name="company" value="<?=$company?>" />
            </div>

            <div class="cont_100">
                <label><?=system_showText(LANG_LABEL_ADDRESS1)?> <em><?=system_showText(LANG_ADDRESS_EXAMPLE)?></em></label>
                <input type="text" name="address" value="<?=$address?>" maxlength="50" />
            </div>

            <div class="cont_100">
                <label><?=system_showText(LANG_LABEL_ADDRESS2)?> <em><?=system_showText(LANG_ADDRESS2_EXAMPLE)?></em></label>
                <input type="text" name="address2" value="<?=$address2?>" maxlength="50" />
            </div>

            <div class="cont_30">
                <label><?=system_showText(LANG_LABEL_COUNTRY)?></label>
                <input type="text" name="country" value="<?=$country?>" />
            </div>

            <div class="cont_30">
                <label><?=system_showText(LANG_LABEL_STATE)?></label>
                <input type="text" name="state" value="<?=$state?>" />
            </div>

            <div class="cont_30">
                <label><?=system_showText(LANG_LABEL_CITY)?></label>
                <input type="text" name="city" value="<?=$city?>" />	
            </div>

            <div class="cont_30">
                <label><?=string_ucwords(ZIPCODE_LABEL)?></label>
                <input type="text" name="zip" value="<?=$zip?>" />
            </div>

            <div class="cont_30">
                <label><?=system_showText(LANG_LABEL_PHONE)?></label>
                <input type="text" name="phone" value="<?=$phone?>" />
            </div>

            <div class="cont_30">
                <label><?=system_showText(LANG_LABEL_FAX)?></label>
                <input type="text" name="fax" value="<?=$fax?>" />
            </div>

            <? if (($id || $account_id) && $isForeignAcc) { ?>
            <div class="cont_100">
                <label><?=system_showText(LANG_LABEL_EMAIL)?> <a href="javascript: void(0);">* <span><?=system_showText(LANG_LABEL_REQUIRED_FIELD);?></span></a></label>
                <input type="text" name="email" id="email" value="<?=$email?>" />
            </div>
            <? } ?>

            <div class="cont_100">
                <label><?=system_showText(LANG_LABEL_URL)?></label>
                <?=$dropdown_protocol;?>
                <input class="input-httpSelect" type="text" name="url" value="<?=str_replace($protocol_replace, "", $url)?>" />
            </div>

            <? if (($id || $account_id) && $isForeignAcc) { ?>
                <input type="hidden" name="isforeignAcc" value="y" />
                <input type="hidden" name="foreignaccount" value="y" />
            <? } else { ?>
                <input type="hidden" name="email" id="email" value="<?=$email?>" />
            <? } ?>
        </div>

    </div>

    <? if (SOCIALNETWORK_FEATURE == "on" || $is_sponsor == "y") { ?>

    <div id="settings">

        <div class="left textright">
            <h2><?=system_showText(LANG_LABEL_CONTACT_SETTINGS);?></h2> 				
            <span><?=system_showText(LANG_LABEL_CONTACT_SETTINGS_TIP);?></span>
        </div>

        <div class="right">

            <? if (SOCIALNETWORK_FEATURE == "on") { ?>
            <div class="cont_checkbox" style="<?=$has_profile == "y" || !$has_profile ? "" : "display: none;";?>">	 				
                <input id="inputpublish" type="checkbox" name="publish_contact" <?=($publish_contact == "y" || $publish_contact == "on") ? "checked=\"checked\"": "" ?> />
                <label for="inputpublish"><?=system_showText(LANG_LABEL_PUBLISH_MY_CONTACT);?></label>
            </div>
            <? } ?>

            <? if ($is_sponsor == "y") { ?>
            <div class="cont_checkbox">	 				
                <input type="checkbox" name="notify_traffic_listing" id="notify_traffic_listing" <?=($notify_traffic_listing == "y" || $notify_traffic_listing == "on" || (!$id && $_SERVER["REQUEST_METHOD"] != "POST")) ? "checked=\"checked\"": "" ?> />
                <label for="notify_traffic_listing" ><?=system_showText(LANG_LABEL_NOTIFY_TRAFFIC);?></label>
            </div>
            <? } ?>
            
            <? if ($showNewsletter) { ?>
            <div class="cont_checkbox">
                <input id="inputnewsletter" type="checkbox" class="checkbox" name="newsletter" value="y" <?=($newsletter == "y" || $newsletter == "on") ? "checked=\"checked\"": "" ?> />
                <label for="inputnewsletter"><?=$signupLabel?></label>
            </div>
            <? } ?>

        </div>
        
    </div>

    <?    
    if (GEOIP_FEATURE == "on") {
    $location_GeoIP = geo_GeoIP();

    if ($location && $location_GeoIP && $facebook_uid) { ?>

    <div id="locationPrefs">

        <div class="left textright">
            <h2><?=system_showText(LANG_LABEL_LOCATIONPREF);?></h2> 				
            <span><?=system_showText(LANG_LABEL_CHOOSELOCATIONPREF);?></span>
        </div>

        <div class="right">
            <div class="cont_checkbox">

                <input id="radio_1" type="radio" name="usefacebooklocation" value="1" style="width:auto" <?=$usefacebooklocation?" checked=\"checked\" ":""?> /> <label for="radio_1"><?=system_showText(LANG_LABEL_USEFACEBOOKLOCATION)?>: <strong> <?=$location?></strong></label>
                <input id="radio_2" type="radio" name="usefacebooklocation" value="0" style="width:auto" <?=!$usefacebooklocation?" checked=\"checked\" ":""?> /> <label for="radio_2" ><?=system_showText(LANG_LABEL_USECURRENTLOCATION)?>: <strong> <?=$location_GeoIP?></strong></label>
                <input type="hidden" name="location" id="location" value="<?=$location?>" />
            </div>     
        </div>

    </div>
    <? }
    } ?>

    <? } ?>