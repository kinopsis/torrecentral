<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /includes/forms/form_account_members.php
	# ----------------------------------------------------------------------------------------------------

    $accountID = sess_getAccountIdFromSession();
                    
    $readonly = "";
    if (DEMO_LIVE_MODE && ($username == "demo@demodirectory.com")) {
        $readonly = "readonly"; 
    }

    $isForeignAcc = false;

    if ((string_strpos($username, "facebook::") !== false || string_strpos($username, "google::") !== false)) {
        $isForeignAcc = true;
    }

    $dropdown_protocol = html_protocolDropdown($url, "url_protocol", false, $protocol_replace);

    ?>
        
    <script type="text/javascript" src="<?=DEFAULT_URL?>/scripts/checkusername.js"></script>
    <script type="text/javascript" src="<?=DEFAULT_URL?>/scripts/activationEmail.js"></script>
                          
    <? if ((string_strpos($username, "facebook::") === false && string_strpos($username, "google::") === false)) { ?>
    <div id="change-email">

        <div class="left textright">
            <h2><?=system_showText(LANG_LABEL_ACCOUNT_USERNAME);?></h2>
            <span><?=system_showText(LANG_LABEL_ACCOUNT_USERNAME_TIP);?></span>
        </div>

        <div class="right">

            <div class="cont_100">

                <label><?=system_showText(LANG_LABEL_USERNAME)?> <a href="javascript: void(0);">* <span><?=system_showText(LANG_LABEL_REQUIRED_FIELD);?></span></a></label>

                <div class="checking">

                    <input type="text" name="username" value="<?=$username?>" maxlength="<?=USERNAME_MAX_LEN?>" onblur="checkUsername(this.value, '<?=DEFAULT_URL;?>', 'members', <?=($accountID ? $accountID : 0);?>); populateField(this.value,'email');"/>
                    <label id="checkUsername">&nbsp;</label>

                    <? if ($active == "y") { ?>
                        <span class="positive"> <img src="<?=DEFAULT_URL?>/images/ico-approve.png"/> <?=system_showText(LANG_LABEL_ACCOUNT_ACT);?></span>
                    <? } else { ?>
                        <span class="negative"> <img src="<?=DEFAULT_URL?>/images/ico-deny.png"/> <?=system_showText(LANG_LABEL_ACCOUNT_NOTACT);?> <a href="javascript: void(0);" onclick="sendEmailActivation(<?=$accountID?>);"><?=system_showText(LANG_LABEL_ACTIVATE_ACC);?></a></span>
                    <? } ?>
                    <img id="loadingEmail" src="<?=DEFAULT_URL?>/images/img_loading.gif" width="15px;" style="display: none;" />
                    <input type="hidden" name="active" value="<?=$active?>" />

                </div>

            </div>

        </div>

    </div>

    <? } else { ?>

    <input type="hidden" name="username" value="<?=$username?>" />

    <? } ?>

    <? if (!$isForeignAcc) { ?>

    <div id="change-password">

        <div class="left textright">

            <h2><?=system_showText(LANG_LABEL_ACCOUNT_CHANGEPASS);?></h2>
            <span><?=system_showText(LANG_LABEL_ACCOUNT_CHANGEPASS_TIP);?></span>

        </div>

        <div class="right">

            <div class="cont_100">

                <label><?=system_showText(LANG_LABEL_CURRENT_PASSWORD)?></label>

                <div class="checking">
                    <input type="password" name="current_password" class="input-form-account" <?=$readonly?> />
                </div>

            </div>

            <div class="cont_50">
                <label><?=system_showText(LANG_LABEL_NEW_PASSWORD);?></label>
                <input type="password" name="password" maxlength="<?=PASSWORD_MAX_LEN?>" <?=$readonly?> />
            </div>

            <div class="cont_50">
                <label><?=system_showText(LANG_LABEL_RETYPE_NEW_PASSWORD);?></label>
                <input type="password" name="retype_password" <?=$readonly?> />
            </div>

        </div>

    </div>

    <? } ?>    