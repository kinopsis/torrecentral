<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /includes/forms/form_listing.php
	# ----------------------------------------------------------------------------------------------------

	setting_get("payment_tax_status", $payment_tax_status);
	setting_get("payment_tax_value", $payment_tax_value);
	customtext_get("payment_tax_label", $payment_tax_label);

?>
        
<? // Other Listing Javascript without AJAX /////////////////////////////////////////// ?>
<script type="text/javascript">
    
	<? if (!$members) { ?>
        
        function changeListingLevel(){
             
            var auxLevel = $('#level').val();
            var auxTemplate = $('#listingtemplate_id').val();
            var url = "<?=DEFAULT_URL.str_replace(EDIRECTORY_FOLDER, "", $_SERVER["PHP_SELF"]);?>?level="+auxLevel+"&listingtemplate_id="+auxTemplate+"<?=($id ? "&id=".$id : "")?>";
            var currLevel = '<?=$level ? $level : ''?>';
            var currTemplateId = '<?=$listingtemplate_id ? $listingtemplate_id : ''?>';
            
            dialogBox('confirm_redirect', '<?=system_showText(LANG_CONFIRM_CHANGE_LEVEL)?>','','','200','<?=system_showText(LANG_SITEMGR_OK);?>','<?=system_showText(LANG_SITEMGR_CANCEL);?>', url, currLevel, currTemplateId);
            
        }
        
    <? } ?>
	
    function isInt(elm) {
		if (elm.value == "") {
			return false;
		}
		for (var i = 0; i < elm.value.length; i++) {
			if (elm.value.charAt(i) < "0" || elm.value.charAt(i) > "9") {
				return false;
			}
		}
		return true;
	}

	// ---------------------------------- //

	function JS_addCategory(id) {

		seed = document.listing.seed;
		feed = document.listing.feed;
		cat_add = <?=$listingLevelObj->getFreeCategory($level)?>;
		var text = unescapeHTML($("#liContent"+id).html());
		<?
		if ($listing->needToCheckOut() || (string_strpos($url_base, "/".SITEMGR_ALIAS."")) || !$listing->getNumber("id") || $listing->getNumber("package_id") == 0){
		?>
			check_cat = 0;
		<?} else {?>
			check_cat = 1;
		<? } ?>

		if (feed.length == cat_add && check_cat == 1){
            fancy_alert('<?=system_showText(str_replace("[max]",$listingLevelObj->getFreeCategory($level),$listingLevelObj->getFreeCategory($level) == 1 ? LANG_ITEM_ALREADY_HAS_PAID2 : LANG_ITEM_ALREADY_HAS_PAID));?>', 'errorMessage', false, 500, 100, false);
		} else {
			var flag=true;
			for (i=0;i<feed.length;i++)
				if (feed.options[i].value==id)
					flag=false;

			if(text && id && flag){
				feed.options[feed.length] = new Option(text, id);
				$('#categoryAdd'+id).after("<span class=\"categorySuccessMessage\"><?=system_showText(LANG_MSG_CATEGORY_SUCCESSFULLY_ADDED)?></span>").css('display', 'none');
				$('.categorySuccessMessage').fadeOut(5000);
			} else {
				if (!flag) $('#categoryAdd'+id).after("</a> <span class=\"categoryErrorMessage\"><?=system_showText(LANG_MSG_CATEGORY_ALREADY_INSERTED)?></span> </li>");
				else ('#categoryAdd'+id).after("</a> <span class=\"categoryErrorMessage\"><?=system_showText(LANG_MSG_SELECT_VALID_CATEGORY)?></span> </li>");
			}
		}

        $('#removeCategoriesButton').show(); 

	}

	// ---------------------------------- //

	function JS_submit() {

		feed = document.listing.feed;
		return_categories = document.listing.return_categories;
		if(return_categories.value.length > 0) return_categories.value="";

		for (i=0;i<feed.length;i++) {
			if (!isNaN(feed.options[i].value)) {
				if(return_categories.value.length > 0)
				return_categories.value = return_categories.value + "," + feed.options[i].value;
				else
			return_categories.value = return_categories.value + feed.options[i].value;
			}
		}

		document.listing.submit();
	}
    
    // ---------------------------------- //
    <? if (!THEME_NEW_UPLOADER) { ?>

	function makeMain(image_id,thumb_id,item_id,temp,item_type) {

		$.get(DEFAULT_URL + "/makemainimage.php", {
			image_id: image_id,
			thumb_id: thumb_id,
			item_id: item_id,
			temp: temp,
			item_type: item_type,
			gallery_hash: '<?=$gallery_hash?>',
			domain_id: <?=SELECTED_DOMAIN_ID?>
		}, function () {
			<? if ($members) { ?>
				loadGallery(item_id, "y", "<?=MEMBERS_ALIAS?>", "", "true");
			<? } else { ?>
				loadGallery(item_id, "y", "<?=SITEMGR_ALIAS?>", "", "true");
			<? } ?>
		});
	}

	// ---------------------------------- //

	function changeMain(image_id,thumb_id,item_id,temp,gallery_id,item_type) {

		$.get(DEFAULT_URL + "/changemainimage.php", {
			image_id: image_id,
			thumb_id: thumb_id,
			item_id: item_id,
			gallery_id: gallery_id,
			temp: temp,
			item_type: item_type,
			level: <?=$level?>,
			gallery_hash: '<?=$gallery_hash?>',
			domain_id: <?=SELECTED_DOMAIN_ID?>
		}, function (response) {
			if (response == "error"){
				fancy_alert('<?=system_showText(LANG_ITEM_ALREADY_HAD_MAX_IMAGE)?>', 'errorMessage', false, 500, 100, false);
			}
			<? if ($members) { ?>
				loadGallery(item_id, "y", "<?=MEMBERS_ALIAS?>", "", "true");
			<? } else { ?>
				loadGallery(item_id, "y", "<?=SITEMGR_ALIAS?>", "", "true");
			<? } ?>
		});
		}

	// ---------------------------------- //

	function loadGallery(id, new_image, sess, del, main) {

		$("#galleryF").fadeIn(0);
		$.get(DEFAULT_URL + "/includes/code/returngallery.php", {
			sess: sess,
			module: 'listing',
			id: id,
			new_image: new_image,
			main: main,
			gallery_hash: '<?=$gallery_hash?>',
			domain_id: <?=SELECTED_DOMAIN_ID?>,
            level: <?=$level?>
		}, function (ret) {
			$("#galleryF").html(ret);

            if (del != "edit" && del != "editFe"){
                if (del == "n"){
                    $("#addImage").css("display", "none");
                    $("#galleryF").css("display", "");
                } else {
                    <? if ($hasImage){ ?>
                        if (del) {
                            $("#addImage").css("display", "none");
                            $("#galleryF").css("display", "");
                        } else {
                            $("#addImage").css("display", "");
                            $("#galleryF").css("display", "none");
                        }
                    <? } else { ?>
                        $("#addImage").css("display", "");
                        $("#galleryF").css("display", "none");
                    <? } ?>
                }
            } else {
                if (del == "edit" || del == "editFe")
                    $("#galleryF").css("display", "");
            }

            if (main == "true"){
                $("#galleryF").css("display", "");
            }
            <? if ($hasImage){ ?>
                $("#galleryF").css("display", "");
            <? } ?>

            if (ret == "no image"){
                $("#galleryF").css("display", "none");
            }
            
            $("a.fancy_window_imgAdd").fancybox({
                
                <? if ($members) { ?>
                        
                'type'                  : 'iframe',
                'width'                 : <?=FANCYBOX_UPIMAGE_WIDTH?>,
                'minHeight'             : <?=FANCYBOX_UPIMAGE_HEIGHT?>,
                <? if (THEME_FLAT_FANCYBOX) { ?>
                'closeBtn'              : false,
                <? } ?>
                'padding'               : 0,
                'margin'                : 0  
                
                <? } else { ?>
                
                'overlayShow'			: true,
                'overlayOpacity'		: 0.4,
                'autoDimensions'        : false,
                'width'                 : <?=FANCYBOX_UPIMAGE_WIDTH?>,
                'height'                : <?=FANCYBOX_UPIMAGE_HEIGHT?>,
                'titleShow'             : false
                
                <? } ?>
            });
            
            $("a.fancy_window_imgCaptions").fancybox({
                
                <? if ($members) { ?>
                    
                'type'                  : 'iframe',
                'width'                 : <?=FANCYBOX_IMAGECAPTIONS_WIDTH?>,
                'minHeight'             : <?=FANCYBOX_IMAGECAPTIONS_HEIGHT?>,
                <? if (THEME_FLAT_FANCYBOX) { ?>
                'closeBtn'              : false,
                <? } ?>
                'padding'               : 0,
                'margin'                : 0    
                
                <? } else { ?>
                    
                'overlayShow'			: true,
                'overlayOpacity'		: 0.75,
                'autoDimensions'        : false,
                'width'                 : <?=FANCYBOX_IMAGECAPTIONS_WIDTH?>,
                'height'                : <?=FANCYBOX_IMAGECAPTIONS_HEIGHT?>,
                'titleShow'             : false
                
                <? } ?>

            });

            $("a.fancy_window_imgDelete").fancybox({
                
                <? if ($members) { ?>
                'type'                  : 'iframe',
                'width'                 : <?=FANCYBOX_DELIMAGE_WIDTH?>,
                'minHeight'             : <?=FANCYBOX_DELIMAGE_HEIGHT?>,
                <? if (THEME_FLAT_FANCYBOX) { ?>
                'closeBtn'              : false,
                <? } ?>
                'padding'               : 0,
                'margin'                : 0    
                <? } else { ?>
                
                'overlayShow'			: true,
                'overlayOpacity'		: 0.4,
                'autoDimensions'        : false,
                'width'                 : <?=FANCYBOX_DELIMAGE_WIDTH?>,
                'height'                : <?=FANCYBOX_DELIMAGE_HEIGHT?>,               
                'titleShow'             : false
                <? } ?>
            });
		});
	}
    
    <? } ?>
	
	<? if(is_array($array_fields) && in_array("summary_description", $array_fields)){ ?>
	$(document).ready(function(){
		
        var field_name = 'summarydesc';
        var count_field_name = 'summarydesc_remLen';

        var options = {
                    'maxCharacterSize': 250,
                    'originalStyle': 'originalTextareaInfo',
                    'warningStyle' : 'warningTextareaInfo',
                    'warningNumber': 40,
                    'displayFormat' : '<span><input readonly="readonly" type="text" id="'+count_field_name+'" name="'+count_field_name+'" size="3" maxlength="3" value="#left" class="textcounter" disabled="disabled" /> <?=system_showText(LANG_MSG_CHARS_LEFT)?> <?=system_showText(LANG_MSG_INCLUDING_SPACES_LINE_BREAKS)?></span>' 
            };
        $('#'+field_name).textareaCount(options);
        
        <?
        if($message_listing){
            ?>
            if(feed.length == 0){
                $('#removeCategoriesButton').hide(); 
            }
            <?
        }
        ?>

	});
	<? } ?>
</script>
<? //////////////////////////////////////////////////////////////////////////////////// ?>

<? // Account Search Javascript /////////////////////////////////////////////////////// ?>

<script type="text/javascript" src="<?=DEFAULT_URL?>/scripts/accountsearch.js"></script>

<?
if ($message_listing) {
	echo "<p class='errorMessage'>";
	echo $message_listing;
	echo "</p>";
}

// Display Level //////////////////////////////////////////////////////////////////
$template_name = "Default";
$level_name = "";
if ($listingtemplate_id) {
	$templateObj = new ListingTemplate($listingtemplate_id);
	if ($templateObj->getString("title") != "") {
		$template_name = $templateObj->getString("title");
	}
}

if ($level) {
	$levelObjTMP = new ListingLevel();
	$level_name = $levelObjTMP->getLevel($level);
}

?>

<table class="standard-table onebackground">
	
	<tr class="bigger">
		<th class="wrap">
			<i><span><?=system_showText(LANG_LABEL_REQUIRED_FIELD);?></span></i>
			<?
			if (LISTINGTEMPLATE_FEATURE == "on" && CUSTOM_LISTINGTEMPLATE_FEATURE == "on") {
				echo "".(($template_title_field!==false) ? $template_title_field[0]["label"] : "".system_showText(LANG_LISTING_TITLE)).":";
			} else {
				echo "".system_showText(LANG_LISTING_TITLE).":";
			}
			?>
		</th>
		<td>
			<input type="text" name="title" value="<?=$title?>" maxlength="100" <?=(!$id) ? " onblur=\"easyFriendlyUrl(this.value, 'friendly_url', '".FRIENDLYURL_VALIDCHARS."', '".FRIENDLYURL_SEPARATOR."');\" " : ""?> />
			<input type="hidden" name="friendly_url" id="friendly_url" value="<?=$friendly_url?>" maxlength="150" />
			<?
			if (LISTINGTEMPLATE_FEATURE == "on" && CUSTOM_LISTINGTEMPLATE_FEATURE == "on") {
				echo (($template_title_field!==false) ? "<span>".$template_title_field[0]["instructions"]."</span>" : "");
			}
			?>
		</td>
	</tr>
    
    <? if (!$members) { ?>
    
        <? if (LISTINGTEMPLATE_FEATURE == "on" && CUSTOM_LISTINGTEMPLATE_FEATURE == "on" && !USING_THEME_TEMPLATE) { ?>
        <tr>
            <th>
                <?=system_showText(LANG_LISTING_TEMPLATE);?>:
            </th>
            <td>
                <select name="listingtemplate_id" id="listingtemplate_id" onchange="changeListingLevel();">
                    <option value=""><?=system_showText(LANG_BUSINESS);?></option>
                    <?
                    $dbMain = db_getDBObject(DEFAULT_DB, true);
                    $dbObjLT = db_getDBObjectByDomainID(SELECTED_DOMAIN_ID, $dbMain);
                    $sqlLT = "SELECT id FROM ListingTemplate WHERE status = 'enabled' AND editable = 'y' ORDER BY title";
                    $resultLT = $dbObjLT->query($sqlLT);
                    while ($rowLT = mysql_fetch_assoc($resultLT)) {
                        $listingtemplate = new ListingTemplate($rowLT["id"]);
                        echo "<option value=\"".$listingtemplate->getNumber("id")."\"";
                        if ($listingtemplate_id == $listingtemplate->getNumber("id")) {
                            echo " selected";
                        }
                        echo ">".$listingtemplate->getString("title");
                        if ($listingtemplate->getString("price") > 0) echo " (+".CURRENCY_SYMBOL.$listingtemplate->getString("price").")";
                        echo "</option>";
                    }
                    ?>
                </select>
            </td>
        </tr>
        <? } ?>
        
        <tr>
            <th>
                <?=system_showText(LANG_LISTING_LEVEL)?>:
            </th>
            <td>
                <select name="level" id="level" onchange="changeListingLevel();">
                <?
                $levelObjAux = new ListingLevel();
                $levelvalues = $levelObjAux->getLevelValues();
                foreach ($levelvalues as $levelvalue) { ?>
                    <option value="<?=$levelvalue?>" <?=(($levelArray[$levelObjAux->getLevel($levelvalue)]) ? "selected" : "")?>>
                        <?=$levelObjAux->showLevel($levelvalue);?>
                    </option>
                <? } ?>
                </select>
            </td>
        </tr>
        <?
    }
    ?>
</table>

<? if (is_array($array_fields) && in_array("summary_description", $array_fields)) { ?>
    <table class="standard-table">
        <tr>            	
            <th>
                <?=system_showText(LANG_LABEL_SUMMARY_DESCRIPTION)?> <span>(<?=string_strtolower(system_showText(LANG_MSG_MAX_250_CHARS))?>)</span>
            </th>
            <td>
                <textarea id="summarydesc" <?=($highlight == "description" && !$description ? "class=\"highlight\"" : "")?> name="description" rows="5" cols="1"><?=$description;?></textarea>
            </td>
        </tr>
    </table>
<? } ?>

<? if (is_array($array_fields) && in_array("long_description", $array_fields)) { ?>
    <table class="standard-table">
        <tr>
            <th><?=system_showText(LANG_LABEL_DESCRIPTION)?></th>
            <td>
                <textarea name="long_description" <?=($highlight == "description" && !$long_description ? "class=\"highlight\"" : "")?> rows="5"><?=$long_description?></textarea>
            </td>
        </tr>
    </table>
<? } ?>

    <table class="standard-table">
        <tr>
			<th>
				<?=system_showText(LANG_LABEL_KEYWORDS_FOR_SEARCH)?> <span>(<?=system_showText(LANG_LABEL_MAX);?> <?=MAX_KEYWORDS?> <?=system_showText(LANG_LABEL_KEYWORDS);?>)</span>
			</th>
            <td>
                <textarea name="keywords" <?=($highlight == "additional" && !$keywords ? "class=\"highlight\"" : "")?> rows="5"><?=$keywords?></textarea>
                <span><?=system_showText(LANG_MSG_KEYWORD_PER_LINE)?></span>
            </td>
        </tr>
	</table>

<? // Account Search ////////////////////////////////////////////////////////////////// ?>
<? if (!$members) { ?>

	<table class="standard-table" style="margin-bottom: 0;">
		<tr>
			<th colspan="2" class="standard-tabletitle"><?=system_showText(LANG_SITEMGR_LABEL_ACCOUNT)?> <span><?=system_showText(LANG_SITEMGR_SETTINGS_IMPORT_TOACCOUNT_SPAN);?></span></th>
		</tr>
	</table>

	<?
    $acct_search_table_title = system_showText(LANG_SITEMGR_ACCOUNTSEARCH_SELECT_DEFAULT);
	$acct_search_field_name = "account_id";
	$acct_search_field_value = $account_id;
	$acct_search_required_mark = false;
	$acct_search_form_width = "95%";
	$acct_search_cell_width = "";
	$return = system_generateAjaxAccountSearch($acct_search_table_title, $acct_search_field_name, $acct_search_field_value, $acct_search_required_mark, $acct_search_form_width, $acct_search_cell_width);
	echo $return;
	?>

<? } ?>
<? //////////////////////////////////////////////////////////////////////////////////// ?>

<? if ((!$members) && (CLAIM_FEATURE == "on")) { ?>
	<table class="standard-table">
		<tr>
			<th><input type="checkbox" name="claim_disable" value="y" <? if ($claim_disable == "y") { echo "checked"; } ?> class="inputCheck" /></th>
			<td><?=system_showText(LANG_SITEMGR_ACCOUNTSEARCH_DISABLECLAIM)?></td>
		</tr>
	</table>
<? } ?>


<table class="standard-table">  
    <tr>
		<th colspan="2" class="standard-tabletitle"><?=system_showText(LANG_LABEL_CONTACT_INFORMATION)?></th>
	</tr>
    <? if(is_array($array_fields) && in_array("email", $array_fields)){ ?>
        <tr>
			<th><?=system_showText(LANG_LABEL_EMAIL)?>:</th>
			<td><input type="text" <?=($highlight == "description" && !$email ? "class=\"highlight\"" : "")?> name="email" value="<?=$email?>" maxlength="50" /></td>
		</tr>    
    <? } ?>
    
    <? if(is_array($array_fields) && in_array("url", $array_fields)){ ?>
        <tr>
			<th><?=system_showText(LANG_LABEL_URL)?>:</th>
			<td>
			<select name="url_protocol" class="httpSelect">
			<?
			$url_protocols = explode(",", URL_PROTOCOL);
			$sufix = "://";
			$protocol_replace = "" ;
			for ($i=0; $i<count($url_protocols); $i++) {
				$selected = false;
				$protocol = $url_protocols[$i];
				if (isset($url) || isset($url_protocol)) {
					if ($url && !$url_protocol) {
						$_protocol = explode($sufix, $url);
						$_protocol = $_protocol[0];
					} else if ($url_protocol) {
						$_protocol = str_replace($sufix, "", $url_protocol);
					}
					if ($_protocol == $protocol) {
						$selected = true;
						$protocol_replace = $_protocol.$sufix;
					}
				} else if (!isset($id) && !$i) {
					$selected = true;
					$protocol_replace = $url_protocols[$i];
					$protocol_replace = $protocol_replace.$sufix;
				}
				$protocol .= $sufix;
			?>

			<option value="<?=$protocol?>"  <?=($selected==true  ? "selected=\"selected\"" : "")?> ><?=$protocol?></option>
			<?
			}
			?>
			</select>
			<input type="text" class="httpInput <?=($highlight == "additional" && !$url ? "highlight" : "")?>" name="url" value="<?=str_replace($protocol_replace, "", $url)?>" maxlength="255" />
			</td>
		</tr>
		<tr>
			<th><?=system_showText(LANG_LABEL_DISPLAY_URL)?>:</th>
			<td>
                <input type="text" name="display_url" value="<?=$display_url?>" maxlength="255" />
                <span><?=system_showText(LANG_LABEL_DISPLAY_URL_TIP);?></span>
            </td>
		</tr>
    <? } ?>

    <? if (is_array($array_fields) && in_array("phone", $array_fields)) { ?>
	<tr>
		<th><?=system_showText(LANG_LABEL_PHONE)?>:</th>
		<td><input type="text" <?=($highlight == "description" && !$phone ? "class=\"highlight\"" : "")?> name="phone" value="<?=$phone?>" /></td>
	</tr>
    <? } ?>

    <? if(is_array($array_fields) && in_array("fax", $array_fields)){ ?>
        <tr>
			<th><?=system_showText(LANG_LABEL_FAX)?>:</th>
			<td><input type="text" <?=($highlight == "additional" && !$fax ? "class=\"highlight\"" : "")?> name="fax" value="<?=$fax?>" /></td>
		</tr>
    <? } ?>
        
    <? if (THEME_LISTING_PRICE && is_array($array_fields) && in_array("price", $array_fields)){ ?>
        <tr>
			<th <?=($highlight == "additional" && !$price ? "class=\"highlight\"" : "")?>><?=system_showText(LANG_LABEL_PRICE)?>:</th>
			<td <?=($highlight == "additional" && !$price ? "class=\"highlight\"" : "")?>>
                <table border="0" cellpadding="0" cellspacing="0" class="table-pricing">
                    <tr>
                    <?
                    setting_get("listing_price_symbol",$listing_price_symbol);
                    for ($i = 1; $i <= LISTING_PRICE_LEVELS; $i++) {

                        setting_get("listing_price_{$i}_from", ${"listing_price_".$i."_from"});
                        setting_get("listing_price_{$i}_to", ${"listing_price_".$i."_to"});

                        if (${"listing_price_".$i."_from"} >= 0) { ?>

                            <td style="width:100px;">
                                <input type="radio" name="price" value="<?=$i?>" <?=($price == $i ? "checked=\"checked\"" : "")?> style="width: auto; border: 0;" />

                                <? for ($j = 1; $j <= $i; $j++) {
                                        echo $listing_price_symbol;
                                } ?>

                                <span><?=system_showListingPrice($i)?></span>
                            </td>

                        <? }
                    }

                    ?>
                    </tr>
                </table>
            </td>
		</tr>
    <? } ?>
        
    <? if (THEME_LISTING_FBPAGE && is_array($array_fields) && in_array("fbpage", $array_fields)) { ?>
        <tr>
			<th><?=system_showText(LANG_LABEL_FBPAGE)?>:</th>
			<td>
                <input type="text" name="facebook_page" <?=($highlight == "additional" && !$facebook_page ? "class=\"highlight\"" : "")?> value="<?=$facebook_page?>" />
                <span>Ex: http://www.facebook.com/fanpage</span>
            </td>
		</tr>
    <? } ?>
</table>

<table class="standard-table noMargin">
	<tr>
		<th colspan="2" class="standard-tabletitle"><?=system_showText(LANG_LABEL_LOCATIONS)?></th>
	</tr>
	<tr>
		<th class="wrap">
			<?
			if (LISTINGTEMPLATE_FEATURE == "on" && CUSTOM_LISTINGTEMPLATE_FEATURE == "on") {
				echo (($template_address_field!==false) ? $template_address_field[0]["label"] : system_showText(LANG_LABEL_ADDRESS1)).":";
			} else {
				echo system_showText(LANG_LABEL_ADDRESS1).":";
			}
			?>
		</th>
		<td>
			<input type="text" <?=($highlight == "description" && !$address ? "class=\"highlight\"" : "")?> name="address" id="address" <?=($loadMap ? "onblur=\"loadMap(document.listing);\"" : "")?> value="<?=$address?>" maxlength="50" />
			<span>
				<?
				if (LISTINGTEMPLATE_FEATURE == "on" && CUSTOM_LISTINGTEMPLATE_FEATURE == "on") {
					echo (($template_address_field!==false) ? $template_address_field[0]["instructions"] : system_showText(LANG_ADDRESS_EXAMPLE));
				} else {
					echo system_showText(LANG_ADDRESS_EXAMPLE);
				}
				?>
			</span>
		</td>
	</tr>
	<tr>
		<th class="wrap">
			<?
			if (LISTINGTEMPLATE_FEATURE == "on" && CUSTOM_LISTINGTEMPLATE_FEATURE == "on") {
				echo (($template_address2_field!==false) ? $template_address2_field[0]["label"] : system_showText(LANG_LABEL_ADDRESS2)).":";
			} else {
				echo system_showText(LANG_LABEL_ADDRESS2).":";
			}
			?>
		</th>
		<td>
			<input type="text" <?=($highlight == "description" && !$address2 ? "class=\"highlight\"" : "")?> name="address2" value="<?=$address2?>" maxlength="50" />
			<span>
				<?
				if (LISTINGTEMPLATE_FEATURE == "on" && CUSTOM_LISTINGTEMPLATE_FEATURE == "on") {
					echo (($template_address2_field!==false) ? "<span>".$template_address2_field[0]["instructions"]."</span>" : system_showText(LANG_ADDRESS2_EXAMPLE));
				} else {
					echo system_showText(LANG_ADDRESS2_EXAMPLE);
				}
				?>
			</span>
		</td>
	</tr>

	<tr>
		<th><?=string_ucwords(ZIPCODE_LABEL)?>:</th>
		<td><input type="text" <?=($highlight == "description" && !$zip_code ? "class=\"highlight\"" : "")?> name="zip_code" id="zip_code" <?=($loadMap ? "onblur=\"loadMap(document.listing);\"" : "")?> value="<?=$zip_code?>" maxlength="10" /></td>
	</tr>
    
    <? if (is_array($array_fields) && in_array("locations", $array_fields)) { ?>
    
    <tr>
        <th>
            <?=system_showText(LANG_LABEL_REFERENCE).":"?>
        </th>
        <td>
            <textarea name="locations" <?=($highlight == "description" && !$locations ? "class=\"highlight\"" : "")?> rows="5"><?=$locations?></textarea>
            <span><?=system_showText(LANG_LABEL_LOCATIONS_TIP);?></span>
        </td>
    </tr>
    
    <? } ?>
    
    <? if ($loadMap) { ?>
    
    <tr <?=($members ? "style=\"display: none\"" : "" )?>>
		<th><?=system_showText(LANG_LABEL_LATITUDE)?>:</th>
		<td>
            <input type="text" name="latitude" id="latitude" <?=($loadMap ? "onblur=\"loadMap(document.listing, true);\"" : "")?> value="<?=$latitude?>" maxlength="10" />
            <span>Ex: 38.830391</span>
        </td>
	</tr>
    
    <tr <?=($members ? "style=\"display: none\"" : "" )?>>
		<th><?=system_showText(LANG_LABEL_LONGITUDE)?>:</th>
		<td>
            <input type="text" name="longitude" id="longitude" <?=($loadMap ? "onblur=\"loadMap(document.listing, true);\"" : "")?> value="<?=$longitude?>" maxlength="10" />
            <span>Ex: -77.196370</span>
        </td>
	</tr>
    
    <? } ?>
</table>

<? 
    include(EDIRECTORY_ROOT."/includes/code/load_location.php");

    if ($loadMap) {
               
        include(EDIRECTORY_ROOT."/includes/code/maptuning_forms.php");
 
?>
    <table class="standard-table noMargin" id="tableMapTuning" <?=($hasValidCoord ? "" : "style=\"display: none\"" )?>>
        <tr>
            <th colspan="2" class="standard-tabletitle">
                <?=system_showText(LANG_LABEL_MAP_TUNING)?> 
                <div id="tipsMap">
                    <span style="text-align: justify;"><?=system_showText(LANG_MSG_USE_CONTROLS_TO_ADJUST)?></span>
                    <br />
                    <span style="text-align: justify;"><?=system_showText(LANG_MSG_USE_ARROWS_TO_NAVIGATE)?></span>
                    <br />
                    <span style="text-align: justify;"><?=system_showText(LANG_MSG_DRAG_AND_DROP_MARKER)?></span>
                </div>
            </th>
        </tr>

        <tr>
            <td>
                <div id="map" class="googleBase" style="border: 1px solid #000;">&nbsp;</div>
                <input type="hidden" name="latitude_longitude" id="myLatitudeLongitude" value="<?=$latitude_longitude?>" />
                <input type="hidden" name="map_zoom" id="map_zoom" value="<?=$map_zoom?>" />
                <input type="hidden" name="maptuning_done" id="maptuning_done" value="<?=$maptuning_done?>" />
            </td>
        </tr>

    </table>

<? }
    /* Images */
    if ((is_array($array_fields) && in_array("main_image", $array_fields)) || $levelMaxImages > 0) {
        
        if (THEME_NEW_UPLOADER) {
            require CLASSES_DIR ."/autoload.php";
            $imageUploader = new NewImageUploader("listing", $gallery_hash, $gallery_id, $levelMaxImages, SELECTED_DOMAIN_ID);
            $imageUploader->render();
        } else { ?>

        <table class="standard-table">
            <tr>
                <th colspan="3" class="standard-tabletitle"><?=($onlyMainImage ? system_showText(LANG_LABEL_IMAGE) : system_showText(LANG_LABEL_IMAGE_PLURAL))?> <span>(<?=IMAGE_LISTING_FULL_WIDTH?>px x <?=IMAGE_LISTING_FULL_HEIGHT?>px) (JPG, GIF <?=system_showText(LANG_OR);?> PNG)</span></th>
            </tr>

            <tr id="table_gallery">
                <th colspan="3" class="Full">
                    <div id="galleryF"></div>
                </th>
            </tr>

            <?
            $gallery_id = $listing->getGalleries();
            if ($onlyMainImage) { ?>
                <tr id="addImage" style="display:<?=($image_id ? "none" : "");?>">
            <? } else { ?>
                <tr>
            <? } ?>
                <td class="alignTop width100">
<!--                    <p class="successMessage highlight">Add some images to your listing and get more visibility for you business!</p>-->
                    <? if ($members) { ?>
                        <a id="uploadimage" href="<?=DEFAULT_URL?>/popup/popup.php?domain_id=<?=SELECTED_DOMAIN_ID?>&pop_type=uploadimage&gallery_hash=<?=$gallery_hash?>&item_type=listing&item_id=<?=$listing->getNumber("id")?>&galleryid=<?=$gallery_id[0]?>&photos=<?=$levelMaxImages?>&level=<?=$level?>" class="addImageForm input-button-form iframe fancy_window_imgAdd"><?=system_showText(LANG_LABEL_ADDIMAGE)?></a>
                    <? } else { ?>
                        <a id="uploadimage" href="<?=DEFAULT_URL?>/<?=SITEMGR_ALIAS?>/uploadimage.php?gallery_hash=<?=$gallery_hash?>&item_type=listing&item_id=<?=$listing->getNumber("id")?>&galleryid=<?=$gallery_id[0]?>&photos=<?=$levelMaxImages?>&level=<?=$level?>" class="addImageForm input-button-form iframe fancy_window_imgAdd"><b><?=system_showText(LANG_LABEL_ADDIMAGE)?></b></a>
                    <? } ?>
                    <span><?=system_showText(LANG_MSG_MAX_FILE_SIZE)?> <?=UPLOAD_MAX_SIZE;?> MB. <?=system_showText(LANG_MSG_ANIMATEDGIF_NOT_SUPPORTED);?></span>
                    <? if ($levelMaxImages > 0){ ?>
                        <p class="informationMessage"><?=system_showText(LANG_MSG_LISTING_WILL_SHOW)?> <?=(($levelMaxImages == -1) ? (system_showText(LANG_LABEL_UNLIMITED)) : (system_showText(LANG_MSG_THE_MAX_OF)." ".$levelMaxImages))." ".(($levelMaxImages == 1) ? (LANG_MSG_GALLERY_PHOTO) : (LANG_MSG_GALLERY_PHOTOS)) ?> <?=system_showText(LANG_MSG_PER_GALLERY)?><?=($hasMainImage ? " ".system_showText(LANG_MSG_PLUS_MAINIMAGE) : ".")?></p>
                    <? } ?>
                </td>
            </tr>		
        </table>

        <? }       
        
    }

    if(is_array($array_fields) && in_array("video", $array_fields)){ ?>
        
        <table class="standard-table">
            <tr>
                <th colspan="2" class="standard-tabletitle">
                    <?=system_showText(LANG_LABEL_VIDEO_SNIPPET_CODE)?>
                    <span style="display: block; white-space: normal;">(<?=system_showText(LANG_MSG_VIDEO_SNIPPET_CODE)?>)</span>
                </th>
            </tr>
            <tr>
                <th class="alignTop"><?=system_showText(LANG_LABEL_CODE)?>:</th>
                <td>
                    <input type="text" name="video_snippet" <?=($highlight == "media" && !$video_snippet ? "class=\"highlight\"" : "")?> value="<?=$video_snippet?>" class="videoSnippetInput" />
                    <span><?=system_showText(LANG_MSG_MAX_VIDEO_CODE_SIZE);?>: <?=DETAIL_VIDEO_WIDTH?>px x <?=DETAIL_VIDEO_HEIGHT?>px.<br /><?=system_showText(LANG_MSG_VIDEO_MODIFIED);?></span>
                    <? if($members == 1) {?>
                        <span><a href="<?=DEFAULT_URL?>/<?=MEMBERS_ALIAS?>/faq.php?keyword=video" target="_blank"><?=system_showText(LANG_VIDEO_SNIPPETTIP)?></a></span>
                    <? } else { ?>
                        <span><a href="<?=DEFAULT_URL?>/<?=SITEMGR_ALIAS?>/faq/faq.php?keyword=video" target="_blank"><?=system_showText(LANG_VIDEO_SNIPPETTIP)?></a></span>
                    <? } ?>
                </td>
            </tr>
            <? if (THEME_LISTING_VIDEO_DESC) { ?>
            <tr>
                <th><?=system_showText(LANG_LABEL_CAPTION)?>:</th>
                <td>
                    <input type="text" maxlength="250" name="video_description" value="<?=$video_description?>" />
                </td>
            </tr>
            <? } ?>
        </table>
    <? } ?>
    
    <? if(is_array($array_fields) && in_array("attachment_file", $array_fields)){ ?>
        <table class="standard-table">
            <tr>
                <th colspan="2" class="standard-tabletitle"><?=system_showText(THEME_LISTING_MENU ? LANG_LABEL_ATTACH_MENU : LANG_LABEL_ATTACH_ADDITIONAL_FILE)?> <span style="display:block;white-space:normal;">(<?=system_showText(LANG_LABEL_ATTACH_ADDITIONAL_FILE_TIP);?> - PDF, DOC, TXT, JPG, GIF <?=system_showText(LANG_OR);?> PNG)</span></th>
            </tr>
            <?
            if ($listing->getString("attachment_file")) {
                if (file_exists(EXTRAFILE_DIR."/".$listing->getString("attachment_file"))) {
                    ?>
                    <tr>
                        <th class="alignTop">&nbsp;</th>

                        <td colspan="2">

                            <a href="<?=EXTRAFILE_URL?>/<?=$listing->getString("attachment_file")?>" style="text-decoration: underline;" target="_blank">
                                <?
                                if ($listing->getString("attachment_caption")) {
                                    echo $listing->getString("attachment_caption");
                                } else {
                                    echo system_showText(LANG_MSG_ATTACHMENT_HAS_NO_CAPTION);
                                }
                                ?>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <th><input type="checkbox" name="remove_attachment" value="1" class="inputCheck"/></th>
                        <td><?=system_showText(LANG_MSG_CLICK_TO_REMOVE_ATTACHMENT);?></td>
                    </tr>
                    <?
                }
            }
            ?>
            <tr>
                <th class="alignTop"><?=system_showText(LANG_LABEL_SOURCE)?>:</th>
                <td class="columnFile">
                    <input type="file" name="attachment_file" <?=($highlight == "additional" && !$attachment_file ? "class=\"highlight\"" : "")?> size="50" /><span><?=system_showText(LANG_MSG_MAX_FILE_SIZE)?> <?=UPLOAD_MAX_SIZE;?> MB</span>
                </td>
            </tr>
            <tr>
                <th><?=system_showText(LANG_LABEL_CAPTION)?>:</th>
                <td>
                    <input type="text" maxlength="250" name="attachment_caption" value="<?=$attachment_caption?>" />
                </td>
            </tr>
        </table>
    <? } ?>

    <? if ((is_array($array_fields) && in_array("hours_of_work", $array_fields)) || (THEME_LISTING_FEATURES && is_array($array_fields) && in_array("features", $array_fields))) { ?>
	
    <table class="standard-table">
        <tr>
            <th colspan="2" class="standard-tabletitle"><?=system_showText(LANG_EXTRA_FIELDS)?></th>
        </tr>

        <? if (THEME_LISTING_FEATURES && is_array($array_fields) && in_array("features", $array_fields)) { ?>
            <tr>
            	<th>
                    <?=system_showText(LANG_LABEL_FEATURES)?>
                </th>
                <td>
                    <textarea name="features" <?=($highlight == "additional" && !$features ? "class=\"highlight\"" : "")?> rows="5" cols="1"><?=$features;?></textarea>
                </td>
            </tr>
        <? }
        
        if (is_array($array_fields) && in_array("hours_of_work", $array_fields)) { ?>

		<tr>
			<th>
				<?=system_showText(LANG_LABEL_HOURS_OF_WORK)?>
			</th>
			<td>
				<textarea name="hours_work" <?=($highlight == "additional" && !$hours_work ? "class=\"highlight\"" : "")?> rows="5"><?=$hours_work?></textarea>
				<span><?=system_showText(LANG_MSG_PHRASE_PER_LINE)?></span>
			</td>
		</tr>
	
        <? } ?>
    </table>
    <? }
    
if (LISTINGTEMPLATE_FEATURE == "on" && CUSTOM_LISTINGTEMPLATE_FEATURE == "on") {
	include(INCLUDES_DIR."/forms/form_listing_extra_fields.php");
}
?>

<table class="standard-table">
	<tr>
		<th colspan="2" class="standard-tabletitle">			
			<?=system_showText(LANG_CATEGORIES_SUBCATEGS)?>
			<?
			if (((!$listing->getNumber("id")) || $listing->getNumber("package_id")>0 || (($listing) && ($listing->needToCheckOut())) || (string_strpos($url_base, "/".SITEMGR_ALIAS."")) || (($listing) && ($listing->getPrice() <= 0))) && ($process != "signup")) {
				?><span style="display: block; white-space: normal;"><?=(($listingLevelObj->getFreeCategory($level) > 1) ? system_showText(LANG_LABEL_CATEGORY_PLURAL) : system_showText(LANG_LABEL_CATEGORY))?> <strong><?=system_showText(LANG_LABEL_FREE)?>: <?=$listingLevelObj->getFreeCategory($level)?></strong>. <?=system_showText(LANG_MSG_EXTRA_CATEGORIES_COST)?> <strong><?=system_showText(LANG_MSG_ADDITIONAL)?> <?=CURRENCY_SYMBOL?> <?=$listingLevelObj->getCategoryPrice($level)?></strong> <?=system_showText(LANG_MSG_BE_SEEN)?></span><?
				?><span><?=system_showText(LANG_CATEGORIES_CATEGORIESMAXTIP1)." <strong>".system_showText(LISTING_MAX_CATEGORY_ALLOWED)."</strong> ".system_showText(LANG_CATEGORIES_CATEGORIESMAXTIP2)?></span><?
			}
			?>
		</th>
	</tr>
	<tr>
		<td class="standard-tableContent">
			<p class="warningBOXtext"><?=system_showText(LANG_MSG_ONLY_SELECT_SUBCATEGS)?><br /><br /></p>
		</td>
	</tr>
	<? if (((!$listing->getNumber("id")) || $listing->getNumber("package_id")>0 || (($listing) && ($listing->needToCheckOut())) || (string_strpos($url_base, "/".SITEMGR_ALIAS."")) || (($listing) && ($listing->getPrice() <= 0) || $listing->getNumber("package_id") > 0)) && ($process != "signup")) { ?>
		<input type="hidden" name="return_categories" value="">
		<tr>
			<td class="treeView">
				<ul id="listing_categorytree_id_0" class="categoryTreeview">&nbsp;</ul>

				<table width="100%" border="0" cellpadding="2" class="tableCategoriesADDED" cellspacing="0">
					<tr>
						<th colspan="2" class="tableCategoriesTITLE alignLeft"><i> * <span><?=system_showText(LANG_LABEL_REQUIRED_FIELD);?></span></i> <strong><?=system_showText(LANG_LISTING_CATEGORIES);?>:</strong></th>
					</tr>
					<tr id="msgback2" class="informationMessageShort">
						<td colspan="2" style="width: auto;"><p><img width="16" height="14" src="<?=DEFAULT_URL?>/images/icon_atention.gif" alt="<?=LANG_LABEL_ATTENTION?>" title="<?=LANG_LABEL_ATTENTION?>" /> <?=system_showText(LANG_MSG_CLICKADDTOSELECTCATEGORIES);?></p></td>
					</tr>
					<tr id="msgback" class="informationMessageShort" style="display:none">
						<td colspan="2"><i> * <span><?=system_showText(LANG_LABEL_REQUIRED_FIELD);?></span></i> <div><img width="16" height="14" src="<?=DEFAULT_URL?>/images/icon_atention.gif" alt="<?=LANG_LABEL_ATTENTION?>" title="<?=LANG_LABEL_ATTENTION?>" /></div><p><?=system_showText(LANG_MSG_CLICKADDTOADDCATEGORIES);?></p></td>
					</tr>
					<tr>
						<td colspan="2" class="tableCategoriesCONTENT"><?=$feedDropDown?></td>
					</tr>
					<tr id="removeCategoriesButton" <?=(($listing->getNumber("id") || $message_listing) ? "" :"style='display:none'")?>>
						<td class="tableCategoriesBUTTONS" colspan="2">
							<center>
								<button class="input-button-form" type="button" value="<?=system_showText(LANG_BUTTON_REMOVESELECTEDCATEGORY)?>" onclick="JS_removeCategory(document.listing.feed, false);"><?=system_showText(LANG_BUTTON_REMOVESELECTEDCATEGORY)?></button>
							</center>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	<? } else { ?>
		<input type="hidden" name="return_categories" value="" />
		<tr>
			<td colspan="2" class="treeView">
				<table width="100%" border="0" cellpadding="2" class="tableCategoriesADDED" cellspacing="0">
					<tr>
						<th colspan="2" class="tableCategoriesTITLE alignLeft" style="text-align:left;"><strong><?=system_showText(LANG_LISTING_CATEGORIES);?>:</strong></th>
					</tr>
					<tr>
						<td colspan="2" class="tableCategoriesCONTENT"><?=$feedDropDown?></td>
					</tr>
				</table>
			</td>
		</tr>
	<? } ?>
</table>

<? if(is_array($array_fields) && in_array("badges", $array_fields)){
   if($editorChoices) { 
       ?>
        <table class="standard-table">
			<tr>
				<th colspan="4" class="standard-tabletitle"><?=system_showText(LANG_LISTING_DESIGNATION_PLURAL);?></th>
			</tr>
			<tr>
				<td colspan="4" class="standard-tableContent">
					<?=system_showText(LANG_MSG_REQUEST_YOUR_LISTING);?><br/>
					<?=system_showText(LANG_LISTING_SUBJECTTOAPPROVAL);?>
				</td>
			</tr>
			<tr>
				<td colspan="4">

					<table align="center" cellpadding="0" cellspacing="0">
						<?
						foreach($editorChoices as $editor) {
							$listingChoiceObj = new ListingChoice($editor->getNumber("id"), $id);
						?>
							<tr>
								<th style="font-weight: bold; background: none; white-space: normal;"><?=$editor->getString("name")?></th>
								<td>
									<?
									$imageObj = new Image($editor->getNumber("image_id"));
									if ($imageObj->imageExists()) {
										echo $imageObj->getTag(IS_UPGRADE == "on" ? true : false, IMAGE_DESIGNATION_WIDTH, IMAGE_DESIGNATION_HEIGHT, $editor->getString("name", false));
									}
									?>
								</td>
								<?
								$checkedStr = "";
								if ($_SERVER['REQUEST_METHOD'] == "POST") {
									if ($_POST["choice"]) {
										if (in_array($editor->getNumber("id"), $_POST["choice"])) {
											$checkedStr = "checked";
										}
									}
								} elseif ($listingChoiceObj->getNumber("listing_id")) {
									$checkedStr = "checked";
								}
								?>
								<td style="width: auto"><input type="checkbox" name="choice[]" <?=$checkedStr?> class="inputCheck" value="<?=$editor->getNumber("id")?>" /></td>
								<td style="width: auto"><?=system_showText(LANG_LISTING_SELECT_THIS_CHOICE);?></td>
							</tr>
						<? } ?>
					</table>

				</td>
			</tr>
		</table>
        <? 
    }     
} 

if (PAYMENT_FEATURE == "on") { ?>
	<? if ((CREDITCARDPAYMENT_FEATURE == "on") || (INVOICEPAYMENT_FEATURE == "on")) { ?>
		<table class="standard-table">
			<tr>
				<th colspan="2" class="standard-tabletitle"><?=system_showText(LANG_LABEL_DISCOUNT_CODE)?></th>
			</tr>
			<? if (((!$listing->getNumber("id")) || $listing->getNumber("package_id")>0 || (($listing) && ($listing->needToCheckOut())) || (string_strpos($url_base, "/".SITEMGR_ALIAS."")) || (($listing) && ($listing->getPrice() <= 0))) && ($process != "signup")) { ?>
				<tr>
					<th><?=system_showText(LANG_LABEL_CODE)?>:</th>
					<td><input type="text" name="discount_id" value="<?=$discount_id?>" maxlength="10" /></td>
				</tr>
			<? } else { ?>
				<tr>
					<th><?=system_showText(LANG_LABEL_CODE)?>:</th>
					<td>
                        <?=(($discount_id) ? $discount_id : system_showText(LANG_NA) )?>
                        <input type="hidden" name="discount_id" value="<?=$discount_id?>" maxlength="10" />
                    </td>
				</tr>
			<? } ?>
		</table>
	<? } ?>
<? } ?>

<? if (((!$listing->getNumber("id")) || $listing->getNumber("package_id")>0 || (($listing) && ($listing->needToCheckOut())) || (string_strpos($url_base, "/".SITEMGR_ALIAS."")) || (($listing) && ($listing->getPrice() <= 0))) && ($process != "signup")) { ?>
	<script type="text/javascript" src="<?=DEFAULT_URL?>/scripts/categorytree.js"></script>
	
	<script type="text/javascript">
		<? if ($listingtemplate_id) { ?>
			loadCategoryTree('template', 'listing_', 'ListingCategory', 0, <?=$listingtemplate_id?>, '<?=DEFAULT_URL."/".EDIR_CORE_FOLDER_NAME."/".LISTING_FEATURE_FOLDER?>',<?=SELECTED_DOMAIN_ID?>);
		<? } else { ?>
			loadCategoryTree('all', 'listing_', 'ListingCategory', 0, 0, '<?=DEFAULT_URL."/".EDIR_CORE_FOLDER_NAME."/".LISTING_FEATURE_FOLDER?>',<?=SELECTED_DOMAIN_ID?>);
		<? } ?>
	</script>
<? } ?>

<script type="text/javascript">
	<? if (!THEME_NEW_UPLOADER) { ?>
		loadGallery(<?=$id ? $id : '0'?>, 'y', '<?=MEMBERS_ALIAS?>', '<?=$id ? 'editFe' : 'editF'?>', <?=$onlyMainImage ? "'false'" : "''"?>);
    <? } ?>
        
    <? if ($hasValidCoord) { ?>
        loadMap(document.listing, true);
    <? } ?>    
</script>