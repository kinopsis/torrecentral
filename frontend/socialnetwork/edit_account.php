<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /frontend/socialnetwork/edit_account.php
	# ----------------------------------------------------------------------------------------------------

?>

	<form name="account" id="account" method="post" action="<?=system_getFormAction($_SERVER["PHP_SELF"])?>" enctype="multipart/form-data">
        
		<input type="hidden" name="tab" id="tab" value="<?=$tab ? $tab: "tab_1";?>" />
		<input type="hidden" name="account_id" value="<?=$account_id?>" />

		<?
        $accountID = sess_getAccountIdFromSession();
        ?>
		<div id="cont_tab_1" style="<?=($tab == 'tab_1' || !$tab) ? '' : 'display:none'?>">
            
			<? include(INCLUDES_DIR."/forms/form_profile.php"); ?>

			<div class="btAdd">
				<p class="standardButton">
					<button type="submit" value="Submit"><?=system_showText(LANG_BUTTON_SUBMIT)?></button>
				</p>
				<p class="standardButton">
					<button type="reset" onclick="redirect('<?=DEFAULT_URL?>/<?=SOCIALNETWORK_FEATURE_NAME?>/');"><?=system_showText(LANG_BUTTON_CANCEL)?></button>
				</p>
			</div>
		</div>
        
		<div id="cont_tab_2" style="<?=($tab == 'tab_2') ? '' : 'display:none'?>">
			
			<? include(INCLUDES_DIR."/forms/form_account_members.php"); ?>
			<? include(INCLUDES_DIR."/forms/form_contact_members.php"); ?>

			<div class="btAdd">
				<p class="standardButton">
					<button type="submit" value="Submit"><?=system_showText(LANG_BUTTON_SUBMIT)?></button>
				</p>
				<p class="standardButton">
					<button type="reset" onclick="redirect('<?=DEFAULT_URL?>/<?=SOCIALNETWORK_FEATURE_NAME?>/');"><?=system_showText(LANG_BUTTON_CANCEL)?></button>
				</p>
			</div>
            
		</div>
        
	</form>

	<script type="text/javascript">
		function redirect (url) {
			window.location = url;
		}
	</script>