<?
/*
	Theme Options Theme number 0 (Default)
	Attention! Change all ID numbers! 
*/
?>
<div class="tab-pane fade in  active" id="theme0">

	<ul class="nav nav-tabs" role="tablist">
		<li class="active"><a href="#theme0-opt1" role="tab" data-toggle="tab">Theme Options</a></li>
		<li class=""><a href="#theme0-opt2" role="tab" data-toggle="tab">Background Image</a></li>
		<li><a href="#theme0-opt3" role="tab" data-toggle="tab">CSS Editor</a></li>
	</ul>

	<div  class="tab-content">

		<section class="tab-pane active" id="theme0-opt1">
			<h4>Color Options</h4>

				<h5>Main Colors</h5>
				<div class="row">
					<div class="form-group col-sm-4 col-xs-12">				  				
						<div class="col-xs-2"><input type="text" id="color-1" class="form-control color-box"></div>
						<label class="col-xs-10 control-label">Brand Primary Color</label>				  					
					</div>

					<div class="form-group col-sm-4 col-xs-12">				  				
						<div class="col-xs-2"><input type="text" id="color-2" class="form-control color-box"></div>
						<label class="col-xs-10 control-label">Brand Warning Color</label>				  					
					</div>

					<div class="form-group col-sm-4 col-xs-12">				  				
						<div class="col-xs-2"><input type="text" id="color-3" class="form-control color-box"></div>
						<label class="col-xs-10 control-label">Brand Success Color</label>				  					
					</div>

					<div class="form-group col-sm-4 col-xs-12">				  				
						<div class="col-xs-2"><input type="text" id="color-4" class="form-control color-box"></div>
						<label class="col-xs-10 control-label">Brand Info Color</label>				  					
					</div>

					<div class="form-group col-sm-4 col-xs-12">				  				
						<div class="col-xs-2"><input type="text" id="color-5" class="form-control color-box"></div>
						<label class="col-xs-10 control-label">Brand Danger Color</label>				  					
					</div>

					<div class="form-group col-sm-4 col-xs-12">				  				
						<div class="col-xs-2"><input type="text" id="color-6" class="form-control color-box"></div>
						<label class="col-xs-10 control-label">Gray Dark Color</label>				  					
					</div>

					<div class="form-group col-sm-4 col-xs-12">				  				
						<div class="col-xs-2"><input type="text" id="color-7" class="form-control color-box"></div>
						<label class="col-xs-10 control-label">Gray Light Color</label>				  					
					</div>
				</div>

				<h5>Support Colors</h5>
				<div class="row">
					<div class="form-group col-sm-4 col-xs-12">				  				
						<div class="col-xs-2"><input type="text" id="color-8" class="form-control color-box"></div>
						<label class="col-xs-10 control-label">Navbar</label>				  					
					</div>

					<div class="form-group col-sm-4 col-xs-12">				  				
						<div class="col-xs-2"><input type="text" id="color-9" class="form-control color-box"></div>
						<label class="col-xs-10 control-label">Links Color</label>				  					
					</div>

					<div class="form-group col-sm-4 col-xs-12">				  				
						<div class="col-xs-2"><input type="text" id="color-10" class="form-control color-box"></div>
						<label class="col-xs-10 control-label">Footer</label>				  					
					</div>

					<div class="form-group col-sm-4 col-xs-12">				  				
						<div class="col-xs-2"><input type="text" id="color-11" class="form-control color-box"></div>
						<label class="col-xs-10 control-label">Header</label>				  					
					</div>
				</div>	

		</section>

		<section class="tab-pane" id="theme0-opt2">
			<h4>Background Image</h4>
				<div class="row">
					<div class="col-md-3 col-xs-12">
						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
						<div class="row">
							<div class="form-group col-xs-6">
								<label for="image-size">Image Max Height</label>
								<input type="number" id="image-size" class="form-control" placeholder="230" value="230">
							</div>	
							<div class="col-xs-12">
								<div class="checkbox">
								    <label>
								      <input type="checkbox">I want a full background website
								    </label>
								    <p class="help-block">Cheking this options will create a fixed image background, we recommended a high definition image for upload</p>
								</div>
							</div>
							<div class="col-xs-12 form-group">
								<br>
								<input type="file">
							</div>	
						</div>
					
					</div>
					<div class="col-md-9 col-xs-12">
						<img src="<?=(HTTPS_MODE != "on" ? "http://" : "https://")?>placehold.it/960x230"/>
					</div>
				</div>	
		</section>

		<section class="tab-pane" id="theme0-opt3">
			<h4>CSS Editor</h4>
			<div class="row">
				<div class="col-md-3 col-xs-12">
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
				</div>
				<div class="col-md-9 col-xs-12"><textarea class="form-control" rows="10"></textarea></div>
			</div>

		</section>
		

	</div>

</div>