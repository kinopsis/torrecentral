<?
/*
	Theme Options Theme number 2 (Dining Guide)
	Attention! Change all ID numbers! 
*/
?>
<div class="tab-pane fade in " id="theme2">

	<ul class="nav nav-tabs" role="tablist">
		<li class="active"><a href="#theme2-opt1" role="tab" data-toggle="tab">Background Image</a></li>
		<li><a href="#theme2-opt2" role="tab" data-toggle="tab">CSS Editor</a></li>
	</ul>

	<div  class="tab-content">

		<section class="tab-pane active" id="theme2-opt1">
				<h4>Background Image</h4>
				<div class="row">
					<div class="col-md-3 col-xs-12">
						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
						<div class="row">
							<div class="form-group col-xs-6">
								<label for="image-size">Image Max Height</label>
								<input type="number" id="image-size" class="form-control" placeholder="230" value="230">
							</div>	
							<div class="col-xs-12">
								<div class="checkbox">
								    <label>
								      <input type="checkbox">I want a full background website
								    </label>
								    <p class="help-block">Cheking this options will create a fixed image background, we recommended a high definition image for upload</p>
								</div>
							</div>
							<div class="col-xs-12 form-group">
								<br>
								<input type="file">
							</div>	
						</div>
					
					</div>
					<div class="col-md-9 col-xs-12">
						<img src="<?=(HTTPS_MODE != "on" ? "http://" : "https://")?>placehold.it/960x230"/>
					</div>
				</div>	

		</section>

		<section class="tab-pane" id="theme2-opt2">
			<h4>CSS Editor</h4>
			<div class="row">
				<div class="col-md-3 col-xs-12">
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
				</div>
				<div class="col-md-9 col-xs-12"><textarea class="form-control" rows="10"></textarea></div>
			</div>

		</section>

	</div>

</div>