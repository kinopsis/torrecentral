<?
/*
	Theme Options Theme number 1 (Contractors)
	Attention! Change all ID numbers! 
*/
?>
<div class="tab-pane fade in" id="theme1">

	<ul class="nav nav-tabs" role="tablist">
		<li class="active"><a href="#theme1-opt1" role="tab" data-toggle="tab">Theme Options</a></li>
		<li><a href="#theme1-opt2" role="tab" data-toggle="tab">CSS Editor</a></li>
	</ul>

	<div  class="tab-content">

		<section class="tab-pane active" id="theme1-opt1">
			<h4>Color Options</h4>
				<h5>Support Colors</h5>
				<div class="row">
					<div class="form-group col-sm-4 col-xs-6">				  				
						<div class="col-xs-2"><input type="text" class="form-control color-box"></div>
						<label class="col-xs-10 control-label">Navbar</label>				  					
					</div>

					<div class="form-group col-sm-4 col-xs-6">				  				
						<div class="col-xs-2"><input type="text" class="form-control color-box"></div>
						<label class="col-xs-10 control-label">Links Color</label>				  					
					</div>

					<div class="form-group col-sm-4 col-xs-6">				  				
						<div class="col-xs-2"><input type="text" class="form-control color-box"></div>
						<label class="col-xs-10 control-label">Footer</label>				  					
					</div>

					<div class="form-group col-sm-4 col-xs-6">				  				
						<div class="col-xs-2"><input type="text" class="form-control color-box"></div>
						<label class="col-xs-10 control-label">Header</label>				  					
					</div>
				</div>	

		</section>

		<section class="tab-pane" id="theme1-opt2">
			<h4>CSS Editor</h4>
			<div class="row">
				<div class="col-md-3 col-xs-12">
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
				</div>
				<div class="col-md-9 col-xs-12"><textarea class="form-control" rows="10"></textarea></div>
			</div>

		</section>

	</div>

</div>