<?php
    /*
    * # Admin Panel for eDirectory
    * @copyright Copyright 2014 Arca Solutions, Inc.
    * @author Basecode - Arca Solutions, Inc.
    */

    # ----------------------------------------------------------------------------------------------------
	# * FILE: /ed-admin/layout/curtain.php
	# ----------------------------------------------------------------------------------------------------

?>

    <div id="curtain" class="curtain">
        <img src="<?=DEFAULT_URL?>/<?=SITEMGR_ALIAS?>/assets/img/preloader-170.gif">
    </div>