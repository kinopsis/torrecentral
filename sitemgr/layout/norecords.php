<?php
    /*
    * # Admin Panel for eDirectory
    * @copyright Copyright 2014 Arca Solutions, Inc.
    * @author Basecode - Arca Solutions, Inc.
    */

    # ----------------------------------------------------------------------------------------------------
	# * FILE: /ed-admin/layout/norecords.php
	# ----------------------------------------------------------------------------------------------------
?>

    <section class="heading">

        <h1><?=system_showText(LANG_SITEMGR_NOREC_WAIT);?></h1>
        <h2><?=system_showText(LANG_SITEMGR_NOREC_FOUND);?></h2>
        
        <hr>
        <p><?=str_replace("[a]", "<a href=\"http://support.edirectory.com\" class=\"text-warning\" target=\"_blank\">", str_replace("[/a]", "</a>", system_showText(LANG_SITEMGR_NOREC_SUPPORT)));?></p>

    </section>

    <?
    include(CLASSES_DIR."/autoload.php");
    $looseJS = "$('#check-all').addClass('hidden');";
    JavaScriptHandler::registerOnReady($looseJS);
    ?>
