<?
    /*
    * # Admin Panel for eDirectory
    * @copyright Copyright 2014 Arca Solutions, Inc.
    * @author Basecode - Arca Solutions, Inc.
    */

    # ----------------------------------------------------------------------------------------------------
	# * FILE: /ed-admin/sites/index.php
	# ----------------------------------------------------------------------------------------------------

	# ----------------------------------------------------------------------------------------------------
	# LOAD CONFIG
	# ----------------------------------------------------------------------------------------------------
	include("../../conf/loadconfig.inc.php");

	# ----------------------------------------------------------------------------------------------------
	# SESSION
	# ----------------------------------------------------------------------------------------------------
	sess_validateSMSession();
	permission_hasSMPerm();

	$url_redirect = "".DEFAULT_URL."/".SITEMGR_ALIAS."/sites";
	$url_base = "".DEFAULT_URL."/".SITEMGR_ALIAS."";

	extract($_GET);
	extract($_POST);
    
    # ----------------------------------------------------------------------------------------------------
	# SUBMIT
	# ----------------------------------------------------------------------------------------------------
	if ($_SERVER['REQUEST_METHOD'] == "POST") {
        
        if (sess_getSMIdFromSession() || $id == SELECTED_DOMAIN_ID || DEMO_LIVE_MODE) {
            header("Location: ".DEFAULT_URL."/".SITEMGR_ALIAS."/sites");
            exit;
        }
        
		$domain = new Domain($id);
		$domain->Delete();
        $message = 1;
		header("Location: ".DEFAULT_URL."/".SITEMGR_ALIAS."/sites/index.php?message=".$message);
		exit;
	}

	# ----------------------------------------------------------------------------------------------------
	# SUPORT EMAIL
	# ----------------------------------------------------------------------------------------------------
	if (is_numeric($_GET["domain_id"]) && $_GET["domain_id"] > 0) {

		setting_get("sitemgr_email",$sitemgr_email);
		$support_email = EDIR_SUPPORT_EMAIL;

		$domainObj = new Domain($_GET["domain_id"]);
		if ($domainObj->getNumber("id") != 0 && $domainObj->getString("status") == "P") {
			$support_msg = "
				<html>
					<head>
						<style>
							.email_style_settings{
								font-size:12px;
								font-family:Verdana, Arial, Sans-Serif;
								color:#000;
							}
						</style>
					</head>
					<body>
						<div class=\"email_style_settings\">
							Support Team,<br /><br />
							A new domain was created in ".DEFAULT_URL.".<br /><br />";
							$support_msg .= "<b>Domain Name: </b>".$domainObj->getString("name")."<br />";
							$support_msg .= "<b>Domain URL: </b>".$domainObj->getString("url")."<br />";
							$support_msg .= "<b>Database Host: </b>".$domainObj->getString("database_host")."<br />";
							if ($domainObj->getString("database_port")) {
								$support_msg .= "<b>Database Port: </b>".$domainObj->getString("database_port")."<br />";
							}
							$support_msg .= "<b>Database Username: </b>".$domainObj->getString("database_username")."<br />";
							$support_msg .= "<b>Database Password: </b>".$domainObj->getString("database_password")."<br />";
							$support_msg .= "<b>Database Name: </b>".$domainObj->getString("database_name")."<br />";
							$support_msg .= "<b>Article Feature: </b>".$domainObj->getString("article_feature")."<br />";
							$support_msg .= "<b>Banner Feature: </b>".$domainObj->getString("banner_feature")."<br />";
							$support_msg .= "<b>Classified Feature: </b>".$domainObj->getString("classified_feature")."<br />";
							$support_msg .= "<b>Event Feature: </b>".$domainObj->getString("event_feature")."<br />";
							$support_msg .= "<br /><br />";
							$support_msg .= "Please, schedule the server settings so the new domain start work. <br /><br />
							Regards,<br />
							The ".EDIRECTORY_TITLE." team<br /><br />
						</div>
					</body>
				</html>";
			//system_mail($support_email, "[".EDIRECTORY_TITLE."] New Domain Created", $support_msg, EDIRECTORY_TITLE." <$sitemgr_email>", "text/html", '', '', $error);
			$domainObj->ActiveDomain();
			unset($domainObj);
		}
	}

	// Page Browsing /////////////////////////////////////////
	unset($pageObj);
	$pageObj  = new pageBrowsing("Domain", $screen, false, "name", "name", $letter, "status='A'", "*", false, false, true);
	$domains = $pageObj->retrievePage();
    
	# ----------------------------------------------------------------------------------------------------
	# HEADER
	# ----------------------------------------------------------------------------------------------------
	include(SM_EDIRECTORY_ROOT."/layout/header.php");

	# ----------------------------------------------------------------------------------------------------
	# NAVBAR
	# ----------------------------------------------------------------------------------------------------
	include(SM_EDIRECTORY_ROOT."/layout/navbar.php");

	# ----------------------------------------------------------------------------------------------------
	# SIDEBAR
	# ----------------------------------------------------------------------------------------------------
	include(SM_EDIRECTORY_ROOT."/layout/sidebar-domains.php");

?>
    <main class="wrapper-dashboard togglesidebar container-fluid" id="view-content-list">
            
        <?php
        require(SM_EDIRECTORY_ROOT."/registration.php");
        require(EDIRECTORY_ROOT."/includes/code/checkregistration.php");
        require(EDIRECTORY_ROOT."/frontend/checkregbin.php");
        ?> 

		<section class="heading">
			<h1><?=system_showText(LANG_SITEMGR_MANAGE_SITES);?></h1>
			<p><?=system_showText(LANG_SITEMGR_DOMAIN_TIP);?></p>
		</section>
        <section class="row form-thumbnails">
        	<div class="row list">
        		<? include(INCLUDES_DIR."/lists/list-domains.php"); ?>
        	</div>
        </section>
        
    </main>

<?
    include(INCLUDES_DIR."/modals/modal-delete.php");
    
	# ----------------------------------------------------------------------------------------------------
	# FOOTER
	# ----------------------------------------------------------------------------------------------------
	include(SM_EDIRECTORY_ROOT."/layout/footer.php");
?>