<?
    /*
    * # Admin Panel for eDirectory
    * @copyright Copyright 2014 Arca Solutions, Inc.
    * @author Basecode - Arca Solutions, Inc.
    */

    # ----------------------------------------------------------------------------------------------------
	# * FILE: /ed-admin/configuration/google/index.php
	# ----------------------------------------------------------------------------------------------------

	# ----------------------------------------------------------------------------------------------------
	# LOAD CONFIG
	# ----------------------------------------------------------------------------------------------------
	include("../../../conf/loadconfig.inc.php");
	include CLASSES_DIR."/autoload.php";

	# ----------------------------------------------------------------------------------------------------
	# VALIDATING FEATURES
	# ----------------------------------------------------------------------------------------------------
	if (GOOGLE_MAPS_ENABLED != "on" && GOOGLE_ADS_ENABLED != "on" && GOOGLE_ANALYTICS_ENABLED != "on" && GOOGLE_TAGMANAGER_ENABLED != "on") { exit; }

	# ----------------------------------------------------------------------------------------------------
	# SESSION
	# ----------------------------------------------------------------------------------------------------
	sess_validateSMSession();
	permission_hasSMPerm();

	extract($_POST);
	extract($_GET);	

    $googleStatus = new GoogleSettings(GOOGLE_MAPS_STATUS);

	# ----------------------------------------------------------------------------------------------------
	# SUBMIT
	# ----------------------------------------------------------------------------------------------------
	if ($_SERVER['REQUEST_METHOD'] == "POST") {

        if ($gtype == "maps") {
            
            $googleSettingObj = new GoogleSettings(GOOGLE_MAPS_SETTING);
            $google_maps_key  = $googleSettingObj->formatValue($google_maps_key);
            $googleSettingObj->setString("value", $google_maps_key);
            $googleSettingObj->Save();
            $googleStatus->setString("value", ($google_maps_status ? "on" : "off"));
            $googleStatus->Save();
            $message_googlemaps = system_showText(LANG_SITEMGR_GOOGLEMAPS_SETTINGSSUCCESSCHANGED);
            MessageHandler::registerSuccess( $message_googlemaps );

        } elseif ($gtype == "ads") {
            
            $googleSettingObj = new GoogleSettings(GOOGLE_ADS_SETTING);
            $google_ad_client = $googleSettingObj->formatValue($google_ad_client);
            $googleSettingObj->setString("value", $google_ad_client);
            $googleSettingObj->Save();

            $googleSettingObj_Status = new GoogleSettings(GOOGLE_ADS_STATUS);
            $googleSettingObj_Status->setString("value", ($google_ad_status ? $google_ad_status : "off"));
            $googleSettingObj_Status->Save();

            $googleSettingObj_Type = new GoogleSettings(GOOGLE_ADS_TYPE);
            if ($google_ad_type_text == "text" && $google_ad_type_image == "image") {
                $google_ad_type = "text_image";
            } elseif ($google_ad_type_text == "text") {
                $google_ad_type = "text";
            } elseif ($google_ad_type_image == "image") {
                $google_ad_type = "image";
            } else {
                $google_ad_type = "text";
            }
            $googleSettingObj_Type->setString("value", $google_ad_type);
            $googleSettingObj_Type->Save();
            $message_googleads = system_showText(LANG_SITEMGR_GOOGLEADS_SETTINGSSUCCESSCHANGED);
            MessageHandler::registerSuccess( $message_googleads );
        } elseif ($gtype == "analytics") {
            
            $googleSettingObj = new GoogleSettings(GOOGLE_ANALYTICS_SETTING);
            $google_analytics_account = $googleSettingObj->formatValue($google_analytics_account);
            $googleSettingObj->setString("value", $google_analytics_account);
            $googleSettingObj->Save();

            $googleSettingObj = new GoogleSettings(GOOGLE_ANALYTICS_FRONT_SETTING);
            $googleSettingObj->setString("value", $google_analytics_front);
            $googleSettingObj->Save();

            $googleSettingObj = new GoogleSettings(GOOGLE_ANALYTICS_MEMBERS_SETTING);
            $googleSettingObj->setString("value", $google_analytics_members);
            $googleSettingObj->Save();

            $googleSettingObj = new GoogleSettings(GOOGLE_ANALYTICS_SITEMGR_SETTING);
            $googleSettingObj->setString("value", $google_analytics_sitemgr);
            $googleSettingObj->Save();

            $message_googleanalytics = system_showText(LANG_SITEMGR_GOOGLEANALYTICS_SETTINGSSUCCESSCHANGED);
            MessageHandler::registerSuccess( $message_googleanalytics );
        } elseif ($gtype == "tag") {
            
            $googleSettingObj_status = new GoogleSettings(GOOGLE_TAG_STATUS);
            $googleSettingObj_status->setString("value", ($google_tag_status ? $google_tag_status : "off"));
            $googleSettingObj_status->Save();

            $googleSettingObj = new GoogleSettings(GOOGLE_TAG_SETTING);
            $google_tag_client = $googleSettingObj->formatValue($google_tag_client);
            $googleSettingObj->setString("value", $google_tag_client);
            $googleSettingObj->Save();

            if (CACHE_FULL_FEATURE == "on") {
                cachefull_forceExpiration();
            }

            $message_googletag = system_showText(LANG_SITEMGR_GOOGLETAG_SETTINGSSUCCESSCHANGED);
            MessageHandler::registerSuccess( $message_googletag );
        }
	}

	# ----------------------------------------------------------------------------------------------------
	# DEFINES
	# ----------------------------------------------------------------------------------------------------	
	//Ads
    $googleSettingObj = new GoogleSettings(GOOGLE_ADS_SETTING);	
	$google_ad_client = $googleSettingObj->getString("value");

	$googleSettingObj_Channel = new GoogleSettings(GOOGLE_ADS_CHANNEL_SETTING);	
	$google_ad_channel = $googleSettingObj_Channel->getString("value");

	$googleSettingObj_Status = new GoogleSettings(GOOGLE_ADS_STATUS);	
	$google_ad_status = $googleSettingObj_Status->getString("value");
	
	$googleSettingObj_Type = new GoogleSettings(GOOGLE_ADS_TYPE);	
	$google_ad_type = $googleSettingObj_Type->getString("value");
    
    //Analytics
    $googleSettingObj = new GoogleSettings(GOOGLE_ANALYTICS_SETTING);	
	$google_analytics_account = $googleSettingObj->getString("value");

	$googleSettingObj = new GoogleSettings(GOOGLE_ANALYTICS_FRONT_SETTING);
	$google_analytics_front = $googleSettingObj->getString("value");

	$googleSettingObj = new GoogleSettings(GOOGLE_ANALYTICS_MEMBERS_SETTING);
	$google_analytics_members = $googleSettingObj->getString("value");

	$googleSettingObj = new GoogleSettings(GOOGLE_ANALYTICS_SITEMGR_SETTING);
	$google_analytics_sitemgr = $googleSettingObj->getString("value");
    
    //Maps
    $googleSettingObj = new GoogleSettings(GOOGLE_MAPS_SETTING);
    $google_maps_key = $googleSettingObj->getString("value");
    
    //Tag
    $googleSettingObj = new GoogleSettings(GOOGLE_TAG_STATUS);	
	$google_tag_status = $googleSettingObj->getString("value");

	$googleSettingObj_status = new GoogleSettings(GOOGLE_TAG_SETTING);	
	$google_tag_client = $googleSettingObj_status->getString("value");
    
    # ----------------------------------------------------------------------------------------------------
	# HEADER
	# ----------------------------------------------------------------------------------------------------
	include(SM_EDIRECTORY_ROOT."/layout/header.php");

    # ----------------------------------------------------------------------------------------------------
	# NAVBAR
	# ----------------------------------------------------------------------------------------------------
	include(SM_EDIRECTORY_ROOT."/layout/navbar.php");
    
    # ----------------------------------------------------------------------------------------------------
	# SIDEBAR
	# ----------------------------------------------------------------------------------------------------
	include(SM_EDIRECTORY_ROOT."/layout/sidebar-configuration.php");

?> 

    <main class="wrapper togglesidebar container-fluid">

        <?php
        require(SM_EDIRECTORY_ROOT."/registration.php");
        require(EDIRECTORY_ROOT."/includes/code/checkregistration.php");
        require(EDIRECTORY_ROOT."/frontend/checkregbin.php");
        ?>

        <section class="heading">
            <h1><?=string_ucwords(system_showText(LANG_SITEMGR_NAVBAR_GOOGLESETTINGS))?></h1>
            <p><?=system_showText(LANG_SITEMGR_GOOGLEPREFS_TIP_1);?></p>
        </section>

            <?php MessageHandler::render(); ?>

        <div class="tab-options">
            
            <ul class="row nav nav-tabs" role="tablist">
                <li class="<?=($message_googlemaps || !$_POST ? "active" : "")?>"><a href="#maps" role="tab" data-toggle="tab">Google Maps</a></li>
                <li class="<?=($message_googleads ? "active" : "")?>"><a href="#ads" role="tab" data-toggle="tab">Google Ads</a></li>
                <li class="<?=($message_googleanalytics ? "active" : "")?>"><a href="#analytics" role="tab" data-toggle="tab">Google Analytics</a></li>
                <li class="<?=($message_googletag ? "active" : "")?>"><a href="#tags" role="tab" data-toggle="tab">Google Tag Manager</a></li>
            </ul>
            
            <div class="row tab-content">
                <div class="container-full">
                <section id="maps" class="tab-pane <?=($message_googlemaps || !$_POST ? "active" : "")?>">
                    <form name="googlemaps" action="<?=system_getFormAction($_SERVER["PHP_SELF"])?>" method="post">
                        <input type="hidden" name="gtype" value="maps" />
                        <? include(INCLUDES_DIR."/forms/form-google-maps.php"); ?>
                    </form>
                </section>

                <section id="ads" class="tab-pane <?=($message_googleads ? "active" : "")?>">
                    <form name="googleads" action="<?=system_getFormAction($_SERVER["PHP_SELF"])?>" method="post">
                        <input type="hidden" name="gtype" value="ads" />
                        <? include(INCLUDES_DIR."/forms/form-google-ads.php"); ?>
                    </form>
                </section>

                <section id="analytics" class="tab-pane <?=($message_googleanalytics ? "active" : "")?>">
                    <form name="googleanalytics" action="<?=system_getFormAction($_SERVER["PHP_SELF"])?>" method="post">
                        <input type="hidden" name="gtype" value="analytics" />
                        <? include(INCLUDES_DIR."/forms/form-google-analytics.php"); ?>
                    </form>
                </section>

                <section id="tags" class="tab-pane <?=($message_googletag ? "active" : "")?>">
                    <form name="googletag" action="<?=system_getFormAction($_SERVER["PHP_SELF"])?>" method="post">
                        <input type="hidden" name="gtype" value="tag" />
                        <? include(INCLUDES_DIR."/forms/form-google-tags.php"); ?>
                    </form>
                </section>
                </div>
            </div>
            
        </div>

    </main>

<?php
	# ----------------------------------------------------------------------------------------------------
	# FOOTER
	# ----------------------------------------------------------------------------------------------------
	include(SM_EDIRECTORY_ROOT."/layout/footer.php");
?>