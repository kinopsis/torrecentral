<?php
    /*
    * # Admin Panel for eDirectory
    * @copyright Copyright 2014 Arca Solutions, Inc.
    * @author Basecode - Arca Solutions, Inc.
    */

	# ----------------------------------------------------------------------------------------------------
	# LOAD CONFIG
	# ----------------------------------------------------------------------------------------------------
	include("../../../conf/loadconfig.inc.php");

	# ----------------------------------------------------------------------------------------------------
	# SESSION
	# ----------------------------------------------------------------------------------------------------
	sess_validateSMSession();
	permission_hasSMPerm();

	extract($_GET);
	extract($_POST);


    # ----------------------------------------------------------------------------------------------------
	# CODE
	# ----------------------------------------------------------------------------------------------------
    if (!$step) {
        $step = 1; //start new import
    }
    
    if (!$module) { //define listing as default module to start a new import
        $module = "listing";
    }
    
    if ($step >= 4) { //include code for import process
        
        if ($module == "event"){
            include(INCLUDES_DIR."/code/import_event.php");
            $includeFile = "import_event.php"; //ajax request
        } else {
            include(INCLUDES_DIR."/code/import.php");
            $includeFile = "import.php"; //ajax request
        }
        
    }
    
    if ($_SERVER['REQUEST_METHOD'] == "POST" && $step == 4) {
        
        if (!$messageErrorUpload && $prev_step == 4) { //submit after Step 4, go to Step 5.
            $step = 5;
        }

    }
    
	# ----------------------------------------------------------------------------------------------------
	# FORM DEFINES
	# ----------------------------------------------------------------------------------------------------
	//Tabs controler
	unset($array_edir_import);
	unset($array_edir_importModule);
	unset($import_numbers);
	$num_import = 1;
	
    if ($step > 1 && !$_GET["step"]) { //Show only one tab (current module)
        
        $array_edir_import[] = @constant("LANG_".string_strtoupper($module)."_FEATURE_NAME_PLURAL");
        $array_edir_importModule[] = $module;
        $import_numbers[] = "0";
        $onclick = false;
        
    } else { //Show all available modules to import
        
        $onclick = true;
        $array_edir_import[] = LANG_LISTING_FEATURE_NAME_PLURAL;
        $array_edir_importModule[] = "listing";
        if (EVENT_FEATURE == "on" && CUSTOM_EVENT_FEATURE == "on"){
            $array_edir_import[] = LANG_EVENT_FEATURE_NAME_PLURAL;
            $array_edir_importModule[] = "event";
            $num_import++;
        }
        $import_numbers[] = "0";
        if (EVENT_FEATURE == "on" && CUSTOM_EVENT_FEATURE == "on"){
            $import_numbers[] = "1";
        }
        
    }
    
    if ($step == 3) { //get Default import settings

        if ($update_settings != "yes") {
            
            if ($_SERVER['REQUEST_METHOD'] == "POST" && $error_sameaccount) {
                
                if ($_POST["import_sameaccount_".$module]) ${"import_sameaccount_".$module} = "checked";
                if ($_POST["import_from_export_".$module]) ${"import_from_export_".$module} = "checked";
                if ($_POST["import_enable_active_".$module]) ${"import_enable_active_".$module} = "checked";
                if ($_POST["import_update_items_".$module]) ${"import_update_items_".$module} = "checked";
                if ($_POST["import_update_friendlyurl_".$module]) ${"import_update_friendlyurl_".$module} = "checked";
                if ($_POST["import_featured_categs_".$module]) ${"import_featured_categs_".$module} = "checked";
                
            } else {
                //Listing
                setting_get("import_sameaccount", $import_sameaccount_listing);
                if ($import_sameaccount_listing) $import_sameaccount_listing = "checked";

                setting_get("import_account_id", $account_id_listing);

                setting_get("import_from_export", $import_from_export_listing);
                if ($import_from_export_listing) $import_from_export_listing = "checked";

                setting_get("import_enable_listing_active", $import_enable_active_listing);
                if ($import_enable_active_listing) $import_enable_active_listing = "checked";

                setting_get("import_defaultlevel", $import_defaultlevel_listing);

                setting_get("import_update_listings", $import_update_items_listing);
                if ($import_update_items_listing) $import_update_items_listing = "checked";

                setting_get("import_update_friendlyurl", $import_update_friendlyurl_listing);
                if ($import_update_friendlyurl_listing) $import_update_friendlyurl_listing = "checked";

                setting_get("import_featured_categs", $import_featured_categs_listing);
                if ($import_featured_categs_listing) $import_featured_categs_listing = "checked";

                //Event
                setting_get("import_sameaccount_event", $import_sameaccount_event);
                if ($import_sameaccount_event) $import_sameaccount_event = "checked";

                setting_get("import_account_id_event",  $account_id_event);

                setting_get("import_from_export_event", $import_from_export_event);
                if ($import_from_export_event) $import_from_export_event = "checked";

                setting_get("import_enable_event_active", $import_enable_active_event);
                if ($import_enable_active_event) $import_enable_active_event = "checked";

                setting_get("import_defaultlevel_event", $import_defaultlevel_event);

                setting_get("import_update_events", $import_update_items_event);
                if ($import_update_items_event) $import_update_items_event = "checked";

                setting_get("import_update_friendlyurl_event", $import_update_friendlyurl_event);
                if ($import_update_friendlyurl_event) $import_update_friendlyurl_event = "checked";

                setting_get("import_featured_categs_event", $import_featured_categs_event);
                if ($import_featured_categs_event) $import_featured_categs_event = "checked";
            }
        } else {
            
            //Listing
            if ($import_sameaccount_listing) $import_sameaccount_listing = "checked";
            if ($import_from_export_listing) $import_from_export_listing = "checked";
            if ($import_enable_active_listing) $import_enable_active_listing = "checked";
            if ($import_update_items_listing) $import_update_items_listing = "checked";
            if ($import_update_friendlyurl_listing) $import_update_friendlyurl_listing = "checked";
            if ($import_featured_categs_listing) $import_featured_categs_listing = "checked";

            //Event
            if ($import_sameaccount_event) $import_sameaccount_event = "checked";
            if ($import_from_export_event) $import_from_export_event = "checked";
            if ($import_enable_active_event) $import_enable_active_event = "checked";
            if ($import_update_items_event) $import_update_items_event = "checked";
            if ($import_update_friendlyurl_event) $import_update_friendlyurl_event = "checked";
            if ($import_featured_categs_event) $import_featured_categs_event = "checked";
            
        }
    }

    
    # ----------------------------------------------------------------------------------------------------
	# HEADER
	# ----------------------------------------------------------------------------------------------------
	include(SM_EDIRECTORY_ROOT."/layout/header.php");

    # ----------------------------------------------------------------------------------------------------
	# NAVBAR
	# ----------------------------------------------------------------------------------------------------
	include(SM_EDIRECTORY_ROOT."/layout/navbar.php");
    
    # ----------------------------------------------------------------------------------------------------
	# SIDEBAR
	# ----------------------------------------------------------------------------------------------------
	include(SM_EDIRECTORY_ROOT."/layout/sidebar-content.php");

?> 
<main class="wrapper togglesidebar container-fluid">        
  
	<form role="form" action="#" method="post"> 

			<section id="edit-listing" class="row heading">
	           	<div class="container">
	           		<h1>Import content</h1>
	           		<p>Imports must be in the CSV format. Please use the template below for ALL imports. <a href="">Download the CSV Template</a>
	           			<span class="pull-right"><a href="javascript:void(0);" data-tour class="text-info tutorial-text">Do you have questions about the import? <i class="icon-help8"> </i></a></span>
	           		</p>
	           		
				</div>
            </section>

			<div class="container alert alert-info fade in hidden" role="alert">		      	
		        <h4>Great! Your listing has been created.</h4> 
		        <p>A new listing has been saved to directory. Make sure the item has the status <b>Active</b> to be displayed on live.</p>
		        <p><a href="<?=DEFAULT_URL."/".SITEMGR_ALIAS."/content/listings/"?>" class="btn btn-sm btn-info">I'm done editing</a></p>
		    </div>

           <section class="row section-form">
	           	<div class="container">
	           		<div class="col-sm-8">

	           			<?// Step 1?>
	           			<div class="panel panel-default">
	           				<div class="panel-heading">The settings below will affect the next import</div>

	           				<div class="panel-body form-horizontal">
								<div class="form-group">
					    			<div class="col-sm-offset-5 col-sm-10">
										<div class="checkbox">
											<label>
												<input type="checkbox"  id="import_from_export" name="import_from_export" ><?=system_showText(LANG_SITEMGR_IMPORT_FROMEXPORT)?>
											</label>
										</div>
									</div>
								</div>
								<div class="form-group">
					    			<div class="col-sm-offset-5 col-sm-10">
										<div class="checkbox">
											<label>
												<input type="checkbox"  id="import_from_export" name="import_from_export" ><?=system_showText(LANG_SITEMGR_IMPORT_FROMEXPORT)?>
											</label>
										</div>
									</div>
								</div>
								<div class="form-group">
					    			<div class="col-sm-offset-5 col-sm-10">
										<div class="checkbox">
											<label>
												<input type="checkbox"  id="import_from_export" name="import_from_export" ><?=system_showText(LANG_SITEMGR_IMPORT_FROMEXPORT)?>
											</label>
										</div>
									</div>
								</div>								
								<div class="form-group">
									<label class="col-sm-5 control-label"><?=system_showText(LANG_SITEMGR_SETTINGS_IMPORT_DEFAULTLEVEL)?></label>
									<div class="col-sm-7">
										<div class="selectize">
											<select name="import_defaultlevel" style="width: 150px;">
												<?
												$levelObj = new ListingLevel();
												$levelvalues = $levelObj->getLevelValues();
												foreach ($levelvalues as $levelvalue) {
													if ($import_defaultlevel==$levelvalue)
														$selected=" selected=\"selected\"";
													else $selected="";
													echo "<option value=\"".$levelvalue."\" $selected>".$levelObj->showLevel($levelvalue)."</option> ";
												}
												?>
											</select>
										</div>	
									</div>
								</div>
	           				</div>

	           				<div class="panel-footer  pager">
	           					<button class="btn btn-primary next">Next &rarr;</button>
	           				</div>
	           			</div>

	           			<?// Step 2?>
	           			<div class="panel panel-default">
	           				<div class="panel-heading">File Upload / FTP Select</div>
	           				<div class="panel-body">
	           					<div class="form-group">
	           						<label>Locate the CSV File</label>
	           						<input type="file">
	           					</div>
	           				</div>		           				       				
	           				<div class="panel-footer  pager">
	           					<button class="btn btn-primary next">Preview the file &rarr;</button>
	           				</div>
	           			</div>

	           			<?// Step 2?>
	           			<div class="panel panel-default">
	           				<div class="panel-heading">File Upload / FTP Select</div>
	           				<div class="panel-body">
	           					<div class="form-group">
	           						<p>File Name: something.csv</p>
	           						<p>Total size: 15MB</p>
	           						<p>Extimated time for importing: 150 minutes</p>
	           					</div>
	           				</div>	
	           				<div class="content-table">
		           				<table class="table table-bordered">
		           					<thead>
		           						<tr>
		           							<th>Account Name</th>
		           							<th>Account Password</th>
		           							<th>Account Contact First Name</th>
		           							<th>Account Contact Last Name</th>
		           							<th>Account Contact Company</th>
		           							<th>Account Contact Address</th>
		           							<th>Account Contact Address 2 </th>
		           							<th>Account Contact State</th>
		           							<th>Account Name</th>
		           							<th>Account Password</th>
		           							<th>Account Contact First Name</th>
		           							<th>Account Contact Last Name</th>
		           							<th>Account Contact Company</th>
		           							<th>Account Contact Address</th>
		           							<th>Account Contact Address 2 </th>
		           							<th>Account Contact State</th>
		           						</tr>
		           					</thead>
		           					<tbody>
		           						<tr>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           						</tr>
		           						<tr>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           						</tr>
		           						<tr>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           						</tr>
		           						<tr>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           						</tr>
		           						<tr>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           							<td>Information</td>
		           						</tr>
		           					</tbody>
		           				</table>   	
		           			</div>       				
	           				<div class="panel-footer  pager">
	           					<button class="btn btn-primary next">Start importing &rarr;</button>
	           				</div>
	           			</div>



	           		</div>
	           		<div class="col-sm-4">
	           			<div class="panel panel-default">
	           				<div class="panel-heading">More Information about the import process</div>
	           				<div class="panel-body small">
	           					<p>CSV formatted files with item data can be imported into your eDirectory system. Items, Accounts, Categories and Locations will be created if they are not already in the system.</p>
								<p>CSV files may be edited with spreadsheets or a text editor. Use our CSV template to create your import file. The header row is required. Use all columns even if fields are blank.</p>	
								<p>If you edit your CSV file in a text editor, please be sure that your fields are between quotes to avoid problems with other valid separators (/ | * . ; _ :).</p>
								<p>Your file will be automatically converted to UTF-8 format if it is saved in a different encoding.</p>
								<p>The import process will import one file per time. There is a maximum file size limit of 100mb per import.</p>
								<p>If you can not see any progress at "Import Log" section, please contact support.</p>
	           				</div>
	           			</div>
	           		</div>

	           	</div>
           </section>

	</form>
</main>

<aside class="tutorial-tour">
	<h1>Import system</h1>
		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. </p>
		<p><a href="index.php" class="btn btn-primary btn-sm">FAQ Section</a></p>
		<p><span class="btn btn-primary btn-sm close-help">Ok. Got it.</span></p>
	
</aside>

<?php
	# ----------------------------------------------------------------------------------------------------
	# FOOTER
	# ----------------------------------------------------------------------------------------------------
	include(SM_EDIRECTORY_ROOT."/layout/footer.php");
?>


<script>

	$(document).ready(function () {
	    ControlSidebar();
	});		
</script>