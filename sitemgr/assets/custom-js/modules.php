<?
    /*
    * # Admin Panel for eDirectory
    * @copyright Copyright 2014 Arca Solutions, Inc.
    * @author Basecode - Arca Solutions, Inc.
    */

    # ----------------------------------------------------------------------------------------------------
	# * FILE: /ed-admin/assets/custom-js/modules.php
	# ----------------------------------------------------------------------------------------------------

    //Section
    $maxCategoryAllowed = MAX_CATEGORY_ALLOWED;
    if (string_strpos($_SERVER["PHP_SELF"], "/content/".LISTING_FEATURE_FOLDER."/") !== false || string_strpos($_SERVER["PHP_SELF"], "listing-types/type.php") !== false) {
        if (string_strpos($_SERVER["PHP_SELF"], "/listing.php") !== false) {
            $feedName = "listing";
        } elseif (string_strpos($_SERVER["PHP_SELF"], "/type.php") !== false) {
            $feedName = "listingtemplate";
        }
        $maxCategoryAllowed = LISTING_MAX_CATEGORY_ALLOWED;
    } elseif (string_strpos($_SERVER["PHP_SELF"], "/content/".EVENT_FEATURE_FOLDER."/event.php") !== false) {
        $feedName = "event";
        $eventScript = true;
    } elseif (string_strpos($_SERVER["PHP_SELF"], "/content/".CLASSIFIED_FEATURE_FOLDER."/classified.php") !== false) {
        $feedName = "classified";
    } elseif (string_strpos($_SERVER["PHP_SELF"], "/content/".ARTICLE_FEATURE_FOLDER."/article.php") !== false) {
        $feedName = "article";
    } elseif (string_strpos($_SERVER["PHP_SELF"], "/content/".BANNER_FEATURE_FOLDER."/") !== false) {
        $bannerScript = true;
    } elseif (string_strpos($_SERVER["PHP_SELF"], "/content/".PROMOTION_FEATURE_FOLDER."/deal.php") !== false) {
        $promotionScript = true;
    } elseif (string_strpos($_SERVER["PHP_SELF"], "/content/".BLOG_FEATURE_FOLDER."/blog.php") !== false) {
        $feedName = "blog";
    }

    if ($loadMap) {
        include(EDIRECTORY_ROOT."/includes/code/maptuning_forms.php");
    }
    
    $tutSteps = "";
    if (is_array($arrayTutorial)) {
        foreach ($arrayTutorial as $key => $fieldTut) {
            $tutSteps .= "{
                        placement: \"".$fieldTut["placement"]."\",
                        element: \"#".$fieldTut["id"]."\",
                        title: \"".addslashes($fieldTut["field"])."\",
                        content: \"".addslashes($fieldTut["content"])."\"
                      },";
        }
    }
    $tutSteps = string_substr($tutSteps, 0, -1);

?>

	<script>
         $(window).on('load', function () {
            $('#curtain').animate({
                opacity: 0
              }, 800, function() {
                $('#curtain').remove();
            });
        });

        $(document).ready(function () {

            //Close sidebar automatically
            var wW = $(window).width();
            if (wW >= 1200) {
                ControlSidebar();
            }

            //Load Map
            <? if ($hasValidCoord) { ?>
                loadMap(document.<?=$feedName?>, true);
            <? } ?>  

            //Load video automatically
            if ($("#video").val()) {
                autoEmbed();
            }
            
            //Pre-fill categories input (selectize)
            loadCategs();
            
            if ($('#item_clicktocall_number').length) {
                
                $("#item_clicktocall_number").autocomplete(
                {
                    source: function (request, response)
                    {
                        $.ajax(
                        {
                            url: '<?=DEFAULT_URL;?>/autocomplete_twilio.php?sitemgr=1&domain_id=<?=SELECTED_DOMAIN_ID?>&account_id=<?=$accId?>',
                            dataType: 'json',
                            data:
                            {
                                term: request.term
                            },
                            success: function (data)
                            {
                                response(data);
                            }
                        });
                    },
                    delay: 1000,
                    minLength: 3,
                    select: function (event, ui)
                    {
                        $(".action-save").removeClass("disabled");
                        $(".action-save").prop("disabled", false);
                        $(".action-save").attr("onclick", "changeSendForm('copyNumber'); ");
                    }
                });

            }
            
            <? if ($promotionScript) { ?>
        
            // Initialize the selectize control for the account field
            window.selectAcc = $('#account_id').selectize({});

            // fetch the instance
            var selectize1 = window.selectAcc[0].selectize;
        
            showAmountType('<?=$aux_deal_type?>', 'show');
            calculateDiscount();
        
            <? } ?>
            

        });
        
        <? if ($promotionScript) { ?>
        
        function calculateDiscount() {

            var percentage = false;
            var realvalue = Number($('#real_price_int').val() + "." + $('#real_price_cent').val());
            var dealvalue = Number($('#deal_price_int').val() + "." + $('#deal_price_cent').val());

            if (document.getElementById("type_percentage").checked) {
                percentage = true;
            }

            if (realvalue != 'NaN' && dealvalue != 'NaN' ) {
                if (realvalue < 0) {
                    realvalue = realvalue * (-1);
                }
                
                if (dealvalue < 0) {
                    dealvalue = dealvalue * (-1);
                }

                if ((dealvalue > realvalue) && (percentage == false)) {
                    $('#amountDiscountMessage').html("<?=system_showText(LANG_MSG_VALID_MINOR)?>");
                    $('#discountAmount').html('');
                } else {
                    $('#amountDiscountMessage').html('');
                    if (percentage) {
                        discount = realvalue - ((dealvalue*realvalue)/100);
                    } else {
                        discount = 100 - ((dealvalue*100)/realvalue);
                    }
                    if (!isNaN(discount) && discount >= 0) {
                        if (discount > 100 && !percentage) {
                            discount = 100;
                        }
                        
                        if (percentage) {
                            $('#discountAmount').html('<?=CURRENCY_SYMBOL?>'+discount.toFixed(2));
                        } else {
                            $('#discountAmount').html(parseInt(discount)+'%');
                        }
                    }
                }
            }
        }

        function showAmountType(type, show) {
            if (type == '%') {
                document.getElementById('amount_monetary').innerHTML = ':';
                document.getElementById('amount_monetary').style.display = 'none';
                document.getElementById('amount_percentage').innerHTML = type;
                document.getElementById('amount_percentage').style.display = '';
                document.getElementById('label_deal_cent').style.display = 'none';
                document.getElementById('deal_price_cent').style.display = 'none';
                $('#discountAmount').html('');
                $('#amountDiscountMessage').html('');
                if (show == "not") {
                    document.getElementById('deal_price_int').value = '';
                    document.getElementById('deal_price_cent').value = '';
                }
                document.getElementById('deal_price_int').setAttribute('maxlength', 2);
            } else {
                document.getElementById('amount_monetary').innerHTML = type;
                document.getElementById('amount_monetary').style.display = '';
                document.getElementById('amount_percentage').style.display = 'none';
                document.getElementById('label_deal_cent').style.display = '';
                document.getElementById('deal_price_cent').style.display = '';
                $('#discountAmount').html('');
                $('#amountDiscountMessage').html('');
                if (show == "not") {
                    document.getElementById('deal_price_int').value = '';
                }
                document.getElementById('deal_price_int').setAttribute('maxlength', 5);
            }
        }

        function showSearchListing(option_show) {
            if (option_show == 'show') {
                $("#aux_listing_title").hide('slow');
                $("#listing_title_tip").show('slow');
                $("#listing_title").show('slow',function() {
                    $("#listing_title_cancel_button").show();
                });
            } else if (option_show == 'hide') {
                $("#listing_title_tip").hide('slow');
                $("#listing_title").hide('slow',function() {
                    $("#listing_title_cancel_button").hide();
                });
                $("#aux_listing_title").show('slow');
            } else if (option_show == 'empty') {

                /**
                * Will remove the promotion_id on Listing table
                */
                $.post('<?=DEFAULT_URL;?>/includes/code/promotion_attachlisting.php', {
                    request: 'ajax',
                    domain_id: '<?=SELECTED_DOMAIN_ID;?>',
                    remove_listing: true,
                    listing_id: <?=($aux_listing_id ? $aux_listing_id : 0)?>
                }, function(res){
                    if (res == "ok") {
                        $("#listing_title_cancel_button").hide();
                        $("#listing_title").val('');
                        $("#listing_id").val(0);
                    }
                });
            }
        }
        
        <? } ?>
    
        <? if ($eventScript) { ?>
            
            function recurringcheck() {
                if (document.getElementById("recurring").checked==true){
                    document.getElementById("reccuring_events").style.display='';
                    document.getElementById("reccuring_ends").style.display='';
                    document.getElementById("end_date").disabled=true;
                    document.getElementById("labelEndDate").style.display='none';
                    chooseperiod(document.getElementById("period").value);
                    if (document.getElementById("eventEver").checked==true){
                        document.getElementById("until_date").disabled=true;
                    } else {
                        document.getElementById("until_date").disabled=false;
                    }

                }else{
                    document.getElementById("reccuring_events").style.display='none';
                    document.getElementById("reccuring_ends").style.display='none';
                    document.getElementById("end_date").disabled=false;
                    document.getElementById("labelEndDate").style.display='';
                }
            }

            // ---------------------------------- //

            function chooseperiod(value){
                if (value=="daily" || value=="" ){
                    document.getElementById("select_day").style.display='none';
                    document.getElementById("select_week").style.display='none';
                    document.getElementById("day").disabled=true;
                    document.getElementById("week").disabled=true;
                    document.getElementById("dayofweek").disabled=true;
                    for (i=0;i<7;i++){
                        document.getElementById("dayofweek_"+i).disabled=true;
                    }
                    for (i=0;i<5;i++){
                        document.getElementById("numberofweek_"+i).disabled=true;
                    }
                    document.getElementById("month").disabled=true;
                }else if(value=='weekly'){
                    document.getElementById("select_day").style.display='none';
                    document.getElementById("of").style.display='none';
                    document.getElementById("week_of").style.display='none';
                    document.getElementById("of2").style.display='';
                    document.getElementById("of3").style.display='none';
                    document.getElementById("of4").style.display='none';
                    document.getElementById("month_of").style.display='none';
                    document.getElementById("week").style.display='none';
                    document.getElementById("weeklabel").style.display='none';
                    document.getElementById("month").style.display='none';
                    document.getElementById("month2").style.display='none';
                    document.getElementById("select_week").style.display='';

                    document.getElementById("day").disabled=true;
                    document.getElementById("dayofweek").disabled=false;
                    for (i=0;i<7;i++){
                        document.getElementById("dayofweek_"+i).disabled=false;
                    }
                    for (i=0;i<5;i++){
                        document.getElementById("numberofweek_"+i).disabled=false;
                    }
                    document.getElementById("week").disabled=true;
                    document.getElementById("month").disabled=true;
                    document.getElementById("month2").disabled=true;
                    document.getElementById("precision1").style.display='none';
                    document.getElementById("precision2").style.display='none';

                }else if(value=='monthly'){
                    document.getElementById("precision1").style.display='';
                    document.getElementById("precision2").style.display='';
                    document.getElementById("precision2").checked=true;
                    document.getElementById("select_day").style.display='';
                    document.getElementById("of").style.display='';
                    document.getElementById("week_of").style.display='';
                    document.getElementById("of2").style.display='none';
                    document.getElementById("of3").style.display='none';
                    document.getElementById("of4").style.display='';
                    document.getElementById("month_of").style.display='none';
                    document.getElementById("week").style.display='';
                    document.getElementById("weeklabel").style.display='';
                    document.getElementById("month").style.display='none';
                    document.getElementById("month2").style.display='none';
                    document.getElementById("select_week").style.display='';

                    document.getElementById("day").disabled=true;
                    document.getElementById("dayofweek").disabled=false;
                    for (i=0;i<7;i++){
                        document.getElementById("dayofweek_"+i).disabled=false;
                    }
                    for (i=0;i<5;i++){
                        document.getElementById("numberofweek_"+i).disabled=false;
                    }
                    document.getElementById("week").disabled=false;
                    document.getElementById("month").disabled=false;
                    document.getElementById("month2").disabled=true;

                }else if(value=='yearly'){
                    document.getElementById("select_day").style.display='';
                    document.getElementById("of").style.display='';
                    document.getElementById("week_of").style.display='';
                    document.getElementById("of2").style.display='';
                    document.getElementById("of3").style.display='';
                    document.getElementById("of4").style.display='none';
                    document.getElementById("month_of").style.display='';
                    document.getElementById("week").style.display='';
                    document.getElementById("weeklabel").style.display='';
                    document.getElementById("month").style.display='';
                    document.getElementById("month2").style.display='';
                    document.getElementById("select_week").style.display='';
                    document.getElementById("precision1").style.display='';
                    document.getElementById("precision2").style.display='';
                    document.getElementById("precision2").checked=true;
                    document.getElementById("day").disabled=true;
                    document.getElementById("dayofweek").disabled=false;
                    for (i=0;i<7;i++){
                        document.getElementById("dayofweek_"+i).disabled=false;
                    }
                    for (i=0;i<5;i++){
                        document.getElementById("numberofweek_"+i).disabled=false;
                    }
                    document.getElementById("week").disabled=false;
                    document.getElementById("month").disabled=true;
                    document.getElementById("month2").disabled=false;
                }
            }

            // ---------------------------------- //

            function chooseprecision(value){

                if (value=='day'){

                    var start_date = $("#start_date").val();
                    var date_format = '<?=DEFAULT_DATE_FORMAT;?>';
                    var arrStDate = start_date.split("/");
                    if (date_format == 'd/m/Y') {
                        var defDay = arrStDate[0];
                        var defMonth = arrStDate[1];
                    } else if (date_format == 'm/d/Y') {
                        var defDay = arrStDate[1];
                        var defMonth = arrStDate[0];
                    }

                    if ($("#day").val() == "") {
                        $("#day").val(defDay);
                    }

                    if ($("#month option:selected").val() == "") {
                        if (defMonth) {
                            var nMonth = document.getElementById("month");
                            nMonth[(defMonth - 1) + 1].selected = true;
                        }
                    }

                    document.getElementById("day").disabled=false;
                    document.getElementById("dayofweek").disabled=true;
                    for (i=0;i<7;i++){
                        document.getElementById("dayofweek_"+i).disabled=true;
                    }
                    for (i=0;i<5;i++){
                        document.getElementById("numberofweek_"+i).disabled=true;
                    }
                    document.getElementById("week").disabled=true;
                    document.getElementById("month").disabled=false;
                    document.getElementById("month2").disabled=true;
                    document.getElementById("precision1").checked=true;
                    document.getElementById("precision2").checked=false;
                } else if (value=='weekday') {
                    document.getElementById("day").disabled=true;
                    document.getElementById("dayofweek").disabled=false;
                    for (i=0;i<7;i++){
                    document.getElementById("dayofweek_"+i).disabled=false;
                    }
                    for (i=0;i<5;i++){
                    document.getElementById("numberofweek_"+i).disabled=false;
                    }
                    document.getElementById("week").disabled=false;
                    document.getElementById("month").disabled=true;
                    document.getElementById("month2").disabled=false;
                } else {
                    document.getElementById("day").disabled=true;
                    document.getElementById("dayofweek").disabled=false;
                    for (i=0;i<7;i++){
                        document.getElementById("dayofweek_"+i).disabled=false;
                    }
                    for (i=0;i<5;i++){
                        document.getElementById("numberofweek_"+i).disabled=false;
                    }
                    document.getElementById("week").disabled=false;
                    document.getElementById("month").disabled=true;
                    document.getElementById("week2").disabled=false;
                }
            }

            // ---------------------------------- //

            function enableUntil(op){
                if (op==1){
                    document.getElementById("until_date").disabled=true;
                } else if (op==2){
                    document.getElementById("until_date").disabled=false;
                }
            }

            <? if($recurring == "Y"){ ?>
                recurringcheck();
                chooseperiod('<?=$period?>');
                <? if($period == "monthly" || $period == "yearly" ){ ?>
                    chooseprecision('<?=$precision?>');
                <? } ?>
            <? } ?>
            
        <? } ?>        
        
        <? if ($feedName) { ?>
        
        function JS_submit() {

            feed = document.<?=$feedName?>.feed;
            return_categories = document.<?=$feedName?>.return_categories;
            if (return_categories.value.length > 0) return_categories.value = "";

            for (i = 0; i < feed.length; i++) {
                if (!isNaN(feed.options[i].value)) {
                    if (return_categories.value.length > 0) {
                        return_categories.value = return_categories.value + "," + feed.options[i].value;
                    } else { 
                        return_categories.value = return_categories.value + feed.options[i].value;
                    }
                }
            }

            document.<?=$feedName?>.submit();
        }
        
        function JS_addCategory(id) {
            var totalSelected = selectize1.getValue().split(',');
            var totalAllowed = <?=$maxCategoryAllowed?>;
            if (totalSelected.length <= totalAllowed) {
                $('#span_'+id).addClass('selected');
                seed = document.<?=$feedName?>.seed;
                feed = document.<?=$feedName?>.feed;
                var text = unescapeHTML($("#liContent"+id).html());
                <? if (is_object($listing) && is_object($listingLevelObj)) { ?>
                cat_add = <?=$listingLevelObj->getFreeCategory($level)?>;
                <? if ($listing->needToCheckOut() || (string_strpos($url_base, "/".SITEMGR_ALIAS."")) || !$listing->getNumber("id") || $listing->getNumber("package_id") == 0) { ?>
                    check_cat = 0;
                <? } else { ?>
                    check_cat = 1;
                <? } ?>

                if (feed.length == cat_add && check_cat == 1) {
                    bootbox.alert('<?=system_showText(str_replace("[max]", $listingLevelObj->getFreeCategory($level), $listingLevelObj->getFreeCategory($level) == 1 ? LANG_ITEM_ALREADY_HAS_PAID2 : LANG_ITEM_ALREADY_HAS_PAID));?>', function() {});
                } else {
                <? } ?>
                    var flag = true;
                    for (i = 0; i <feed.length; i++) {
                        if (feed.options[i].value == id) {
                            flag = false;
                        }
                    }
                    if (text && id && flag) {
                        feed.options[feed.length] = new Option(text, id);
                    }
                <? if (is_object($listing)) { ?>
                }
                <? } ?>
            }
        }
        
        <? } ?>
        
        <? if (!$members) { ?>
        
        function changeModuleLevel() {

            var auxLevel = $('#level').val();
            <? if (is_object($listing)) { ?>
                    
            var auxTemplate = $('#listingtemplate_id').val();
            var url = "<?=DEFAULT_URL.str_replace(EDIRECTORY_FOLDER, "", $_SERVER["PHP_SELF"]);?>?level="+auxLevel+"&listingtemplate_id="+auxTemplate+"<?=($id ? "&id=".$id : "")?>";
            var currTemplateId = '<?=$listingtemplate_id ? $listingtemplate_id : ''?>';
            
            <? } else { ?>
              
            var url = "<?=DEFAULT_URL.str_replace(EDIRECTORY_FOLDER, "", $_SERVER["PHP_SELF"]);?>?level="+auxLevel+"<?=($id ? "&id=".$id : "")?>";
              
            <? } ?>
            var currLevel = '<?=is_numeric($level) ? $level : ''?>';

            bootbox.confirm('<?=system_showText(LANG_CONFIRM_CHANGE_LEVEL)?>', function(result) {
                if (result) {
                    document.location.href = url;
                } else {
                    <? if (is_object($listing)) { ?>
                    $("#listingtemplate_id").val(currTemplateId);
                    <? } ?>
                    $("#level").val(currLevel);
                }
            });
        }

        <? } ?>

        //Load tiny MCE
        function system_displayTinyMCEJS(txId) {
            tinyMCE.execCommand('mceAddControl', false, txId);
        }

        //Load video iframe from URL
        function autoEmbed() {
            var videoURL = $("#video").val();

            if (videoURL) {
                $.post("<?=DEFAULT_URL."/includes/code/get_video.php"?>", {
                    video: videoURL
                }, function (ret) {
                    if (ret == "error") {
                        $("#videoMsg").removeClass("hidden");
                    } else {
                        $("#videoMsg").addClass("hidden");
                        $("#icon").css("display", "none");
                        $("#video_frame").html(ret);
                        $("#video_snippet").attr("value", ret);
                        $("#video_frame").fadeIn();
                    }
                });
            } else {
                $("#video_snippet").attr("value", "");
            }
        }

        //Remove attachment file
        function removeAttachment() {
            $("#div_attachment").addClass("hidden");
            $("#remove_attachment").attr("value", "1");
        }
        
        //Add event to category tree
        function initCategories() {
            $(".no-child > span").click( function() {
                var category = $(this).text();
                var categoryID = $(this).attr("data-catID");
                selectize1.addOption({value : categoryID, text : category});
                selectize1.addItem(categoryID);
            });
        }
        
        function loadCategs() {
            
            <? if ( $selectizeCategs ) foreach ( $selectizeCategs as $each_category ) { ?>
            selectize1.addOption({value : '<?=$each_category["value"]?>', text : '<?=addslashes($each_category["name"])?>'});
            selectize1.addItem('<?=$each_category["value"]?>');
            <? } ?>

        }
        
        var auxStepsTutorial = [<?=$tutSteps?>];
           
        <? if ($feedName) { ?>
            
        // Initialize the selectize control for the Select Categories Modal
        var $select = $('#category-select').selectize({
            plugins: ['remove_button'],
            maxItems: <?=$maxCategoryAllowed?>,
            create: false,
            onItemRemove: function(value) {
                $('#feed').val(value);
                selectize2.removeOption(value);
                JS_removeCategory(document.<?=$feedName?>.feed, false);
                $('#span_'+value).removeClass('selected');
            },
            onItemAdd: function(value, $item) {
                JS_addCategory(value);
                selectize2.addOption({value : value, text : $item[0].firstChild.data});
                selectize2.addItem(value);
            }
        });

        // Initialize the selectize control for the categories field
        var $selectCat = $('#categories').selectize({
            plugins: ['remove_button'],
            maxItems: <?=$maxCategoryAllowed?>,
            create: false,
            onItemRemove: function(value) {
                $('#feed').val(value);
                selectize1.removeOption(value);
                JS_removeCategory(document.<?=$feedName?>.feed, false);
                $('#span_'+value).removeClass('selected');
            },
            onItemAdd: function(value, text) {
                JS_addCategory(value);
            }
        });

        // fetch the instance
        var selectize1 = $select[0].selectize; //modal
        var selectize2 = $selectCat[0].selectize; //field
        
        $("#action-categories").click(function () {
            $('#modal-categories').modal('hide');
        });
        
        <? } ?>
        
        function checkboxLabelChanging(pos) {
            $('#custom_checkbox' + (pos+1)).removeClass('hidden');
        }

        function dropdownLabelChanging(pos) {
            $('#custom_dropdown' + (pos+1)).removeClass('hidden');
        }

        function textLabelChanging(pos) {
            $('#custom_text' + (pos+1)).removeClass('hidden');
        }

        function shortdescLabelChanging(pos) {
            $('#custom_short_desc' + (pos+1)).removeClass('hidden');
        }

        function longdescLabelChanging(pos) {
            $('#custom_long_desc' + (pos+1)).removeClass('hidden');
        }
        
        function changeSendForm(method){
			if (method == "checkClickToCall") {
				document.getElementById("action_clicktocall").value = "verify_number";	
			} else if (method == "clearNumber") {
				document.getElementById("action_clicktocall").value = "clearNumber";	
			} else if (method == "copyNumber") {
				document.getElementById("action_clicktocall").value = "copyNumber";	
			}
			document.getElementById('clicktocall_form').submit();	
		}

    </script>
    
    <? if (string_strpos($_SERVER["PHP_SELF"], "/listing-types") === false) { ?>

    <script src="<?=DEFAULT_URL?>/<?=SITEMGR_ALIAS?>/assets/js/module-tutorial.js"></script>
    
    <? }
    
    if ($feedName) { ?>
        <script type="text/javascript" src="<?=DEFAULT_URL?>/scripts/categorytree.js"></script>
    
        <script>
            <? 
            if ($feedName == "listing") {
                if (((!$listing->getNumber("id")) || $listing->getNumber("package_id") > 0 || (($listing) && ($listing->needToCheckOut())) || (string_strpos($url_base, "/".SITEMGR_ALIAS."")) || (($listing) && ($listing->getPrice() <= 0))) && ($process != "signup")) {
                    if ($listingtemplate_id) { ?>
                        
                        loadCategoryTree('template', 'listing_', 'ListingCategory', 0, <?=$listingtemplate_id?>, '<?=DEFAULT_URL."/".EDIR_CORE_FOLDER_NAME."/".LISTING_FEATURE_FOLDER?>',<?=SELECTED_DOMAIN_ID?>, true);
                    
                    <? } else { ?>
                        
                        loadCategoryTree('all', 'listing_', 'ListingCategory', 0, 0, '<?=DEFAULT_URL."/".EDIR_CORE_FOLDER_NAME."/".LISTING_FEATURE_FOLDER?>',<?=SELECTED_DOMAIN_ID?>, true);
                    
                    <? }
                }
            } elseif ($feedName == "listingtemplate") { ?>
                
                loadCategoryTree('main', 'listing_', 'ListingCategory', 0, 0, '<?=DEFAULT_URL."/".EDIR_CORE_FOLDER_NAME."/".LISTING_FEATURE_FOLDER?>', '<?=SELECTED_DOMAIN_ID;?>', true);
            
            <? } else { ?>                
                
                loadCategoryTree('all', '<?=$feedName?>_', '<?=ucfirst($feedName)?>Category', 0, 0, '<?=DEFAULT_URL?>', <?=SELECTED_DOMAIN_ID?>, true);
                
            <? } ?>
        </script>
    
    <? }
    
    if ($facebookScript) { ?>
        <script>
            (function(d){
                var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
                js = d.createElement('script'); js.id = id; js.async = true;
                js.src = "//connect.facebook.net/<?=EDIR_LANGUAGEFACEBOOK?>/all.js#xfbml=1";
                d.getElementsByTagName('head')[0].appendChild(js);
            }(document));
        </script>
    <? }
    
    if (string_strpos($_SERVER["PHP_SELF"], "/".LISTING_FEATURE_FOLDER."/deal.php") !== false) { ?>
        
        <script>
            
            function showSearchPromotion(option_show) {
                if (option_show == 'show') {
                    $("#aux_promotion_name").hide('slow');
                    $("#promotion_name_tip").show('slow');
                    $("#promotion_name").show('slow', function() {
                        $("#promotion_name_cancel_button").show();
                    });
                } else if(option_show == 'hide') {
                    $("#promotion_name_tip").hide('slow');
                    $("#promotion_name").hide('slow', function() {
                        $("#promotion_name_cancel_button").hide();
                    });
                    $("#aux_promotion_name").show('slow');
                } else if(option_show == 'empty') {

                    /**
                    * Will remove the promotion_id on Listing table
                    */
                    $.post('<?=DEFAULT_URL;?>/includes/code/promotion_attachlisting.php', {
                        request: 'ajax',
                        domain_id: '<?=SELECTED_DOMAIN_ID;?>',
                        remove_listing: true,
                        listing_id: <?=($id ? $id : 0)?>
                    }, function(res) {
                        if (res == "ok") {
                            $("#promotion_name_cancel_button").hide();
                            $("#promotion_name").val('');
                            $("#promotion_id").val(0);
                        }
                    });
                }
            }
            
        </script>
        
    <? }
    
    if (string_strpos($_SERVER["PHP_SELF"], "/".LISTING_FEATURE_FOLDER."/report.php") !== false) { ?>
        <script>
            
            $(document).ready(function(){
                $.get('<?=DEFAULT_URL?>/<?=SITEMGR_ALIAS?>/content/<?=LISTING_FEATURE_FOLDER?>/twilio_report.php', {
                    item_type: 'listing',
                    domain_id: '<?=SELECTED_DOMAIN_ID;?>',
                    item_id: <?=$listing->getNumber("id")?>
                }, function(res) {
                    $("#twilio_reports").html(res);
                });
            });
            
        </script>
    <? } ?>
        
    <script type="text/javascript" src="<?=DEFAULT_URL?>/scripts/jquery/jcrop/js/jquery.Jcrop.js"></script>
        
    <script>
        
        function makeMain(image_id, thumb_id, item_id, temp, item_type) {
            //Call ajax only for non-default images
            if ( $('#gallery-image-' + image_id).attr('data-image_default') == 'n') {
                $.get(DEFAULT_URL + '/makemainimage.php', {
                    image_id: image_id,
                    thumb_id: thumb_id,
                    item_id: item_id,
                    temp: temp,
                    item_type: item_type,
                    gallery_hash: '<?=$gallery_hash?>',
                    domain_id: <?=SELECTED_DOMAIN_ID?>
                }, function () {
                    //Remove image-default class from all images
                    $('.item-gallery').removeClass('image-default');
                    //Add image-default class for the image clicked
                    $('#gallery-image-' + image_id).addClass('image-default');
                    //Remove onclick event for all images
                    $('.item-gallery').attr('data-image_default', 'n');
                    //Change image_default attribute for the image clicked
                    $('#gallery-image-' + image_id).attr('data-image_default', 'y');
                });
            }
        }
        
        var jcrop_api;
        
        function editImage(image_id) {
            //Reset save button status
            $('#button-edit-img').button('reset');
            //Set current image title value
            $('#crop_image_title').attr('value', $('#img-' + image_id).attr('data-title'));
            //Set current image description value
            $('#crop_image_description').attr('value', $('#img-' + image_id).attr('data-description'));
            //Set image path
            $('#crop_image').attr('src', $('#img-' + image_id).attr('src'));
            //Auxiliary values to save image
            $('#edit-image_id').attr('value', image_id);
            $('#edit-temp').attr('value', $('#gallery-image-' + image_id).attr('data-temp'));
            $('#edit_image_type').attr('value', $('#gallery-image-' + image_id).attr('data-imgtype'));
            //Destroy crop obj
            if (jcrop_api) {
                jcrop_api.destroy();
            }
            //Initialize crop
            setJcrop($('#img-' + image_id).attr('data-width'), $('#img-' + image_id).attr('data-height'));
        }
        
        function saveImage() {
            $.post("<?= $_SERVER["PHP_SELF"] ?>", $("#form-crop").serialize(), function(data) {
                var image_id = $('#edit-image_id').val();
                if ($('#crop_image_title').val()) {
                    $('#preview-title-'+image_id).html($('#crop_image_title').val());
                }
                if ($('#crop_image_description').val()) {
                    $('#preview-description-'+image_id).html($('#crop_image_description').val());
                }
                if (data) {
                    $('#img-'+image_id).attr('src', data);
                }
                $('#modal-crop').modal('hide');
            });
        }
        
        // creating the Jcrop
        function setJcrop(imgWidth, imgHeight) {
            $('#crop_image').Jcrop({
				onChange: showCoords,
				onSelect: showCoords,
				setSelect:   [ (imgWidth/4), (imgHeight/4), (imgWidth/4*3), (imgHeight/4*3) ],
				aspectRatio: 0,
				boxWidth: 400,
				boxHeight: 400,
				bgColor: 'transparent',
				fullImageWidth: imgWidth,
				fullImageHeight: imgHeight
			},function(){
                jcrop_api = this;
            });
            function showCoords(c) {
                $('#x').val(c.x);
                $('#y').val(c.y);
                $('#x2').val(c.x2);
                $('#y2').val(c.y2);
                $('#w').val(c.w);
                $('#h').val(c.h);
                $('#image_width').val(imgWidth);
                $('#image_height').val(imgHeight);
            };
        }

        // Banner
        <? if ($bannerScript) { ?>
            var banner_tmp_form_images_content = document.getElementById("banner_with_images").innerHTML;
            var banner_tmp_form_text_content = document.getElementById("banner_with_text").innerHTML;

            function fillCaption (capt) {
                $("#mainCaption").attr("value", capt);
            }

            <?
            if ($type < 50)       echo "bannerDisableTextForm();";
            else if ($type >= 50) echo "bannerDisableImagesForm();";
            if ($forceTextForm) echo "bannerDisableImagesForm();";
            }
        ?>            
        
    </script>

    <!-- Gallery plugin - jQuery file upload -->
    
    <!-- The template to display files available for upload -->
    <script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}

        <div class="template-upload row item fade">
            <div class="col-sm-3 col-xs-6">&nbsp;</div>
            <div class="col-sm-3 col-xs-6 pull-right">
                {% if (!i) { %}
                <p><span class="btn btn-sm btn-warning btn-iconic cancel"><i class="icon-ion-ios7-close-empty"></i></span></p>
                {% } %}
            </div>
            <div class="col-sm-6 col-xs-12">
                <strong>{%=file.name%}</strong>
                <p class="error"></p>
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
            </div>
        </div>
    {% } %}
    </script>

    <!-- The template to display files available for download -->
    <script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}

        <div class="template-download row item fade">
            <div id="gallery-image-{%=file.image_id%}" data-image_id="{%=file.image_id%}" data-thumb_id="{%=file.thumb_id%}" data-item_id="{%=file.item_id%}" data-temp="{%=file.temp%}" data-item_type="{%=file.item_type%}" data-image_default="{%=file.image_default%}" data-imgtype="{%=file.type%}" class="col-sm-3 col-xs-6 item-gallery {% if (file.image_default == 'y') { %} image-default {% } %}" onclick='makeMain("{%=file.image_id%}", "{%=file.thumb_id%}", "{%=file.item_id%}", "{%=file.temp%}", "{%=file.item_type%}");'>
                {% if (file.thumbnailUrl) { %}
                <img id="img-{%=file.image_id%}" src="{%=file.thumbnailUrl%}" alt="{%=file.name%}" data-title="{%=file.title%}" data-description="{%=file.description%}" data-width="{%=file.width%}" data-height="{%=file.height%}" class="img-responsive">
                {% } %}
            </div>
            <div class="col-sm-3 col-xs-6 pull-right">
                <p>
                    {% if (file.deleteUrl) { %}
                    <span class="btn btn-sm btn-primary btn-iconic" data-toggle="modal" data-target="#modal-crop" onclick="editImage({%=file.image_id%});" href="#"><i class="icon-edit38"></i></span> 
                    <span class="btn btn-sm btn-danger btn-iconic delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}><i class="icon-ion-ios7-trash-outline"></i></span>
                    {% } else { %}
                    <span class="btn btn-sm btn-warning btn-iconic cancel"><i class="icon-ion-ios7-close-empty"></i></span>
                    {% } %}
                </p>
            </div>
            <div class="col-sm-6 col-xs-12">
                <strong id="preview-title-{%=file.image_id%}">
                    {% if (file.title) { %}
                        {%=file.title%}
                    {% } else { %}
                        {%=file.name%}
                    {% } %}
                </strong>
                {% if (file.error) { %}
                    <p>{%=file.error%}</p>
                {% } else { %}
                    <p id="preview-description-{%=file.image_id%}">
                    {% if (file.description) { %}
                        {%=file.description%}
                    {% } %}
                    </p>
                {% } %}
            </div>
        </div>
    {% } %}
    </script>

    <!--  The Templates plugin is included to render the upload/download items -->
    <script src="<?=DEFAULT_URL?>/scripts/jquery/jQuery-File-Upload-9.8.0/tmpl.min.js"></script>
    <!--  The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="<?=DEFAULT_URL?>/scripts/jquery/jQuery-File-Upload-9.8.0/load-image.all.min.js"></script>
    <!--  The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="<?=DEFAULT_URL?>/scripts/jquery/jQuery-File-Upload-9.8.0/jquery.iframe-transport.js"></script>
    <!--  The basic File Upload plugin -->
    <script src="<?=DEFAULT_URL?>/scripts/jquery/jQuery-File-Upload-9.8.0/jquery.fileupload.js"></script>
    <!--  The File Upload processing plugin -->
    <script src="<?=DEFAULT_URL?>/scripts/jquery/jQuery-File-Upload-9.8.0/jquery.fileupload-process.js"></script>
    <!--  The File Upload user interface plugin -->
    <script src="<?=DEFAULT_URL?>/scripts/jquery/jQuery-File-Upload-9.8.0/jquery.fileupload-ui.js"></script>
    <!--  The main application script -->
    <script src="<?=DEFAULT_URL?>/scripts/jquery/jQuery-File-Upload-9.8.0/main.js"></script>