<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /controller/listing/detail.php
	# ----------------------------------------------------------------------------------------------------

    ##################################################
    # LISTING
    ##################################################
    if (string_strpos($aux_array_url[$searchPos_2], ".html") !== false) {
        $browsebyitem = true;
        $aux_friendlyURL = explode(".html", $aux_array_url[$searchPos_2]);
        $listing_url =  $aux_friendlyURL[0];
        $sql = "SELECT Listing.id as id FROM Listing WHERE Listing.friendly_url = " . db_formatString($listing_url) . " LIMIT 1";
        $result = $dbObj->query($sql);
        $aux = mysql_fetch_assoc($result);
        $_GET["id"] = $aux["id"];
        $_GET["listing_id"] = $aux["id"];
        if (!$_GET["id"]) {
            $failure = true;
        }
    }

    # ----------------------------------------------------------------------------------------------------
	# VALIDATION
	# ----------------------------------------------------------------------------------------------------
	include(EDIRECTORY_ROOT."/includes/code/validate_querystring.php");
	include(EDIRECTORY_ROOT."/includes/code/validate_frontrequest.php");

	include(EDIRECTORY_ROOT."/includes/code/listingcontact.php");
    
    //Sitemgr preview
    $sitemgrPreview = sess_validateItemPreview();

	# ----------------------------------------------------------------------------------------------------
	# LISTING
	# ----------------------------------------------------------------------------------------------------
	if (($_GET["id"]) || ($_POST["id"])) {
		$id = $_GET["id"] ? $_GET["id"] : $_POST["id"];
		$listing = new Listing($id);
		$levelObj = new ListingLevel(true);
		unset($listingMsg);
		if ((!$listing->getNumber("id")) || ($listing->getNumber("id") <= 0)) {
			$listingMsg = system_showText(LANG_MSG_NOTFOUND);
		} elseif ($listing->getString("status") != "A" && !$sitemgrPreview) {
			$listingMsg = system_showText(LANG_MSG_NOTAVAILABLE);
		} elseif ($levelObj->getDetail($listing->getNumber("level")) != "y" && $levelObj->getActive($listing->getNumber("level")) == 'y') {
			$listingMsg = system_showText(LANG_MSG_NOTAVAILABLE);
		} elseif (!$sitemgrPreview) {
			report_newRecord("listing", $id, LISTING_REPORT_DETAIL_VIEW);
			$listing->setNumberViews($id);
		}			
	} else {
		header("Location: ".LISTING_DEFAULT_URL."/");
		exit;
	}

	# ----------------------------------------------------------------------------------------------------
	# REVIEWS
	# ----------------------------------------------------------------------------------------------------
	if ($id)  $sql_where[] = " item_type = 'listing' AND item_id = ".db_formatNumber($id)." ";
	$sql_where[] = " review IS NOT NULL AND review != '' ";
	$sql_where[] = " approved = '1' ";
	if ($sql_where) $sqlwhere .= " ".implode(" AND ", $sql_where)." ";
	$pageObj  = new pageBrowsing("Review", $screen, false, "`like` DESC, added DESC", "", "", $sqlwhere);
	$reviewsArr = $pageObj->retrievePage("object");

	# ----------------------------------------------------------------------------------------------------
	# CHECK INS
	# ----------------------------------------------------------------------------------------------------
	if ($id)  $sql_where2[] = " item_id = ".db_formatNumber($id)." ";
	$sql_where2[] = " quick_tip IS NOT NULL AND quick_tip != '' ";
	if ($sql_where2) $sqlwhere2 .= " ".implode(" AND ", $sql_where2)." ";
	$pageObj  = new pageBrowsing("CheckIn", $screen, 3, "added DESC", "", "", $sqlwhere2);
	$checkinsArr = $pageObj->retrievePage("object");
    
    # ----------------------------------------------------------------------------------------------------
	# HEADER
	# ----------------------------------------------------------------------------------------------------
    if (($listing->getNumber("id")) && ($listing->getNumber("id") > 0)) {
        $listCategs = $listing->getCategories(false, false, false, true, true);
        if ($listCategs) {
            foreach ($listCategs as $listCateg) {
                $category_id[] = $listCateg->getNumber("id");
            }
        }
    }
    $_POST["category_id"] = $category_id;
?>
