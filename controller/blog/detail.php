<?
	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /controller/blog/detail.php
	# ----------------------------------------------------------------------------------------------------

    # ----------------------------------------------------------------------------------------------------
    # MODULE REWRITE
    # ----------------------------------------------------------------------------------------------------
    include(EDIR_CONTROLER_FOLDER."/".BLOG_FEATURE_FOLDER."/rewrite.php");

    # ----------------------------------------------------------------------------------------------------
	# VALIDATION
	# ----------------------------------------------------------------------------------------------------
	include(EDIRECTORY_ROOT."/includes/code/validate_querystring.php");
    include(EDIRECTORY_ROOT."/includes/code/validate_frontrequest.php");

	# ----------------------------------------------------------------------------------------------------
	# SUBMIT
	# ----------------------------------------------------------------------------------------------------
    setting_get("review_approve", $post_comment_approve);
	if (($_SERVER["REQUEST_METHOD"] == "POST") && sess_getAccountIdFromSession()) {
		include(INCLUDES_DIR."/code/comment.php");
	}
    
    //Sitemgr preview
    $sitemgrPreview = sess_validateItemPreview();

	# ----------------------------------------------------------------------------------------------------
	# BLOG
	# ----------------------------------------------------------------------------------------------------
	if (($_GET["id"]) || ($_POST["id"])) {
		$id = $_GET["id"] ? $_GET["id"] : $_POST["id"];
		$post = new Post($id);
		unset($postMsg);
		if ((!$post->getNumber("id")) || ($post->getNumber("id") <= 0)) {
			$postMsg = system_showText(LANG_MSG_NOTFOUND);
		} elseif ($post->getString("status") != "A" && !$sitemgrPreview) {
			$postMsg = system_showText(LANG_MSG_NOTAVAILABLE);
		}
        if (!$sitemgrPreview) {
            report_newRecord("post", $id, POST_REPORT_DETAIL_VIEW);
            $post->setNumberViews($id);
        }
		
	} else {
		header("Location: ".BLOG_DEFAULT_URL."/");
		exit;
	}
	
	# ----------------------------------------------------------------------------------------------------
	# COMMENTS
	# ----------------------------------------------------------------------------------------------------
	$dbObj = db_getDBObJect();
	$sql_comment = " SELECT * FROM Comments WHERE post_id = $id AND reply_id = 0 AND approved = 1";
	$sql_comment .= " ORDER BY `added` DESC ";
	$result = $dbObj->query($sql_comment);
	while ($row = mysql_fetch_assoc($result)) {
		$commentArr[] = $row;
	}
    
    # ----------------------------------------------------------------------------------------------------
    # HEADER
    # ----------------------------------------------------------------------------------------------------
    if (($post->getNumber("id")) && ($post->getNumber("id") > 0)) {
        $postCategs = $post->getCategories(false, false, false, true, true);
        if ($postCategs) {
            foreach ($postCategs as $postCateg) {
                $category_id[] = $postCateg->getNumber("id");
            }
        }
    }
    $_POST["category_id"] = $category_id;
?>