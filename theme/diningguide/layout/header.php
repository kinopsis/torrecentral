<?

    /*==================================================================*\
    ######################################################################
    #                                                                    #
    # Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
    #                                                                    #
    # This file may not be redistributed in whole or part.               #
    # eDirectory is licensed on a per-domain basis.                      #
    #                                                                    #
    # ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
    #                                                                    #
    # http://www.edirectory.com | http://www.edirectory.com/license.html #
    ######################################################################
    \*==================================================================*/

    # ----------------------------------------------------------------------------------------------------
    # * FILE: /theme/diningguide/layout/header.php
    # ----------------------------------------------------------------------------------------------------

    include(INCLUDES_DIR."/code/header_code.php");

?>
<!DOCTYPE html>

<html lang="<?=system_getHeaderLang();?>">

    <head>

        <title><?=$headertag_title?></title>
        <meta name="author" content="<?=$headertag_author?>" />
        <meta name="description" content="<?=$headertag_description?>" />
        <meta name="keywords" content="<?=$headertag_keywords?>" />
        <meta charset="<?=EDIR_CHARSET;?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        
        <? $metatagHead = true; include(INCLUDES_DIR."/code/smartbanner.php"); ?>

        <!-- This function returns the favicon tag. Do not change this line. -->
        <?=system_getFavicon();?>

        <!-- This function returns the search engine meta tags. Do not change this line. -->
        <?=front_searchMetaTag();?>
        
        <!-- This function returns the meta tags rel="next"/rel="prev" to improve SEO on results pages. Do not change this line. -->
        <?=front_paginationTags($array_pages_code, $aux_items_per_page, $hideResults, $blogHome);?>

        <meta name="ROBOTS" content="index, follow" />

        <!-- This function includes all css files. Do not change this line. -->
        <!-- To change any style, it's better to edit the stylesheet files. -->
        <? front_themeFiles(); ?>

        <!-- This function returns the Default Image style. Do not change this line. -->
        <?=system_getNoImageStyle($cssfile = true);?>

        <!-- This function reads and includes all js and css files (minimized). Do not change this line. -->
        <? script_loader($js_fileLoader, $pag_content, $aux_module_per_page); ?>

        <!--[if lt IE 9]>
        <script src="<?=DEFAULT_URL."/scripts/front/html5shiv.js"?>"></script>
        <![endif]-->

    </head>
                
    <body>
	<!--[if lt IE 9]><div class="ie"><![endif]-->
    
        <!-- Google Tag Manager code - DO NOT REMOVE THIS CODE  -->
        <?=front_googleTagManager();?>
    
        <? if (DEMO_LIVE_MODE && file_exists(EDIRECTORY_ROOT."/frontend/livebar.php")) {
            include(EDIRECTORY_ROOT."/frontend/livebar.php");
        } ?>
    
        <!-- This function returns the code warning users to upgrade their browser if they are using Internet Explorer 6. Do not change this line.  -->
        <? front_includeFile("IE6alert.php", "layout", $js_fileLoader); ?>
        
        <div class="navbar navbar-static-top">
            
            <div class="header-brand container">
                 
                <div id="logo-link" class="brand"  itemscope itemtype="http://schema.org/Organization">
                    
                    <a itemprop="url" href="<?=NON_SECURE_URL?>/" target="_parent" <?=(trim(EDIRECTORY_TITLE) ? "title=\"".EDIRECTORY_TITLE."\"" : "")?>>
                        <img class="brand-logo" itemprop="logo" alt="<?=(trim(EDIRECTORY_TITLE) ? EDIRECTORY_TITLE : "&nbsp;")?>" src="<?=system_getHeaderLogo(false);?>"/>
                    </a>
                    
                    <meta itemprop="name" content="<?=EDIRECTORY_TITLE?>"/>
                    
                    <? if ($contact_address || $contact_zipcode || $contact_country || $contact_state || $contact_city) { ?>
                    
                    <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        
                        <? if ($contact_address) { ?>
                            <meta itemprop="streetAddress" content="<?=$contact_address;?>" />
                        <? } ?>
                            
                        <? if ($contact_zipcode) { ?>
                            <meta itemprop="postalCode" content="<?=$contact_zipcode;?>" />
                        <? } ?>
                            
                        <? if ($contact_country) { ?>
                            <meta itemprop="addressCountry" content="<?=$contact_country;?>" />
                        <? } ?>
                            
                        <? if ($contact_state) { ?>
                            <meta itemprop="addressRegion" content="<?=$contact_state;?>" />
                        <? } ?>
                            
                        <? if ($contact_city) { ?>
                            <meta itemprop="addressLocality" content="<?=$contact_city;?>" />
                        <? } ?>
                            
                    </div>
                    
                    <? }
                    
                    if ($contact_phone) { ?>
                        <meta itemprop="telephone" content="<?=$contact_phone;?>" />
                    <? } ?>
                        
                </div>
                
                <? 
                $addSearchCollapse = false;
                if (    string_strpos($_SERVER['REQUEST_URI'], ALIAS_LISTING_ALLCATEGORIES_URL_DIVISOR."/") === false && 
                        string_strpos($_SERVER['REQUEST_URI'], ALIAS_CONTACTUS_URL_DIVISOR.".php") === false && 
                        string_strpos($_SERVER['REQUEST_URI'], ALIAS_ADVERTISE_URL_DIVISOR.".php") === false && 
                        string_strpos($_SERVER['REQUEST_URI'], "/order_") === false && 
                        string_strpos($_SERVER['REQUEST_URI'], ALIAS_FAQ_URL_DIVISOR.".php") === false && !$hide_search) {
                    
                    $addSearchCollapse = true;
                    include(EDIRECTORY_ROOT."/searchfront.php");
                }
                ?>
            </div>
            
             <div class="navbar-inner">
                 
                <div class="container">
                    
                    <div class="hidden-desktop brand mobile" >
                        <a href="<?=NON_SECURE_URL?>/"  target="_parent" <?=(trim(EDIRECTORY_TITLE) ? "title=\"".EDIRECTORY_TITLE."\"" : "")?>>
                            <img  class="brand-logo" alt="<?=(trim(EDIRECTORY_TITLE) ? EDIRECTORY_TITLE : "&nbsp;")?>" src="<?=system_getHeaderLogo(false);?>"/>
                        </a>
                    </div>
                    
                    <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse" onclick="collapseMenu('menu');">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    
                    <? if ($addSearchCollapse) { ?>
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".search-collapse" onclick="collapseMenu('search')">
                        <span class="icon-search"></span>
                    </a>
                    <? } ?>
                    
                    <div id="nav-collapse" class="nav-collapse collapse">
                        <? include(system_getFrontendPath("header_menu.php", "layout")); ?>
                    </div>
                    
                    <div id="search-collapse" class="search-collapse collapse">
                        <? if ($addSearchCollapse) {

                            $searchResponsive = true;
                            include(system_getFrontendPath("search.php"));
                            $searchResponsive = false;
                        } ?>
                    </div>
                    
                </div>
                
            </div>
            
        </div>
        
        <div class="image-bg">
            <?=front_getBackground($customimage);?>
        </div>
        
        <div class="well container">
        
            <div class="container-fluid">
                
                <?
                //Breadcrumb
                front_addBreadcrumb();
                
                //Add newsletter only on home pages
                if ($addNewsletter) {
                    include(system_getFrontendPath("newsletter.php"));
                }
                
                //Don't show banners for advertise pages and maintenance page
                if (string_strpos($_SERVER["PHP_SELF"], "/order_") === false && string_strpos($_SERVER["REQUEST_URI"], ALIAS_ADVERTISE_URL_DIVISOR.".php") === false && string_strpos($_SERVER["PHP_SELF"], "/maintenancepage.php") === false) {
                    front_includeBanner($category_id, $banner_section);
                }
                
                //Load Slider only on the home page
                if ($loadSlider) {
                    front_includeFile("slider.php", "frontend", $js_fileLoader);
                }
                ?>