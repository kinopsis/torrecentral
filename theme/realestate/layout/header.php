<?

    /*==================================================================*\
    ######################################################################
    #                                                                    #
    # Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
    #                                                                    #
    # This file may not be redistributed in whole or part.               #
    # eDirectory is licensed on a per-domain basis.                      #
    #                                                                    #
    # ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
    #                                                                    #
    # http://www.edirectory.com | http://www.edirectory.com/license.html #
    ######################################################################
    \*==================================================================*/

    # ----------------------------------------------------------------------------------------------------
    # * FILE: /theme/realestate/layout/header.php
    # ----------------------------------------------------------------------------------------------------

    include(INCLUDES_DIR."/code/header_code.php");

?>
<!DOCTYPE html>

<html lang="<?=system_getHeaderLang();?>">

    <head>

        <title><?=$headertag_title?></title>
        <meta name="author" content="<?=$headertag_author?>" />
        <meta name="description" content="<?=$headertag_description?>" />
        <meta name="keywords" content="<?=$headertag_keywords?>" />
        <meta charset="<?=EDIR_CHARSET;?>"/>
        
        <!-- This function returns the favicon tag. Do not change this line. -->
        <?=system_getFavicon();?>

        <!-- This function returns the search engine meta tags. Do not change this line. -->
        <?=front_searchMetaTag();?>
        
        <!-- This function returns the meta tags rel="next"/rel="prev" to improve SEO on results pages. Do not change this line. -->
        <?=front_paginationTags($array_pages_code, $aux_items_per_page, $hideResults, $blogHome);?>

        <meta name="ROBOTS" content="index, follow" />

        <!-- This function includes all css files. Do not change this line. -->
        <!-- To change any style, it's better to edit the stylesheet files. -->
        <? front_themeFiles(); ?>

        <!-- This function returns the Default Image style. Do not change this line. -->
        <?=system_getNoImageStyle($cssfile = true);?>

        <!-- This function reads and includes all js and css files (minimized). Do not change this line. -->
        <? script_loader($js_fileLoader, $pag_content, $aux_module_per_page); ?>
        
        <!--[if lt IE 9]>
        <script src="<?=DEFAULT_URL."/scripts/front/html5shiv.js"?>"></script>
        <![endif]-->

    </head>
 
    <body>
       <!--[if lt IE 9]><div class="ie"><![endif]-->
        
        <!-- Google Tag Manager code - DO NOT REMOVE THIS CODE  -->
        <?=front_googleTagManager();?>
	
        <!-- This div is used for the share box, on results and detail pages. Do not change this line.  -->
        <div id="div_to_share" class="share-box" style="display: none"></div>

        <? if (DEMO_LIVE_MODE && file_exists(EDIRECTORY_ROOT."/frontend/livebar.php")) {
            include(EDIRECTORY_ROOT."/frontend/livebar.php");
        } ?>
        
        <!-- This function returns the code warning users to upgrade their browser if they are using Internet Explorer 6. Do not change this line.  -->
        <? front_includeFile("IE6alert.php", "layout", $js_fileLoader); ?>

        <div id="header-wrapper">
        
            <div id="header">
		                
                <div id="logo-link" class="brand"  itemscope itemtype="http://schema.org/Organization">
                    
                    <a itemprop="url" href="<?=NON_SECURE_URL?>/" target="_parent" <?=(trim(EDIRECTORY_TITLE) ? "title=\"".EDIRECTORY_TITLE."\"" : "")?>>
                        <img class="brand-logo" itemprop="logo" alt="<?=(trim(EDIRECTORY_TITLE) ? EDIRECTORY_TITLE : "&nbsp;")?>" src="<?=system_getHeaderLogo(false);?>"/>
                    </a>
                    
                    <meta itemprop="name" content="<?=EDIRECTORY_TITLE?>"/>
                    
                    <? if ($contact_address || $contact_zipcode || $contact_country || $contact_state || $contact_city) { ?>
                    
                    <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        
                        <? if ($contact_address) { ?>
                            <meta itemprop="streetAddress" content="<?=$contact_address;?>" />
                        <? } ?>
                            
                        <? if ($contact_zipcode) { ?>
                            <meta itemprop="postalCode" content="<?=$contact_zipcode;?>" />
                        <? } ?>
                            
                        <? if ($contact_country) { ?>
                            <meta itemprop="addressCountry" content="<?=$contact_country;?>" />
                        <? } ?>
                            
                        <? if ($contact_state) { ?>
                            <meta itemprop="addressRegion" content="<?=$contact_state;?>" />
                        <? } ?>
                            
                        <? if ($contact_city) { ?>
                            <meta itemprop="addressLocality" content="<?=$contact_city;?>" />
                        <? } ?>
                            
                    </div>
                    
                    <? }
                    
                    if ($contact_phone) { ?>
                        <meta itemprop="telephone" content="<?=$contact_phone;?>" />
                    <? } ?>
                        
                </div>

                <!-- This function returns the top navbar code. Do not change this line.  -->
                <? front_includeFile("usernavbar.php", "layout", $js_fileLoader); ?>
			
            </div>
			
        </div>
		
        <div <?=(string_strpos($_SERVER["PHP_SELF"], SOCIALNETWORK_FEATURE_NAME."/add.php") === false && string_strpos($_SERVER["PHP_SELF"], SOCIALNETWORK_FEATURE_NAME."/edit.php") === false ? "class=\"slider-wrapper\"" : "")?>>
            <div id="navbar-wrapper">
                
                <!-- You can add your own custom links below by adding a pair of <li></li> tags within the <ul> parent, place any content within. -->
                <!-- Do not change the if clause inside the <li> tags below. This clause controls when the menu shows as active or not. -->
            
                <ul id="navbar">
                    <? include(system_getFrontendPath("header_menu.php", "layout")); ?>
                </ul>
            </div>
            
            <?
            //Load Slider only on the home page
            if ($loadSlider) {
                front_includeFile("slider.php", "frontend", $js_fileLoader);
            }
            ?>

        </div>

        <div class="content-wrapper">