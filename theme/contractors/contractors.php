<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2005 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# edirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- edirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

    # ----------------------------------------------------------------------------------------------------
	# * FILE: /theme/contractors/contractors.php
	# ----------------------------------------------------------------------------------------------------

    //Use this code only for development areas
    if (file_exists(EDIRECTORY_ROOT."/custom/tokenless")) {
        include_once(CLASSES_DIR."/less/lessc.inc.php");
        $less = new lessc;
        $less->checkedCompile(THEMEFILE_DIR."/contractors/less/structure.less", THEMEFILE_DIR."/contractors/structure.css");
        $less->checkedCompile(THEMEFILE_DIR."/contractors/less/advertise.less", THEMEFILE_DIR."/contractors/plans.css");
        $less->checkedCompile(THEMEFILE_DIR."/contractors/less/content_custom.less", THEMEFILE_DIR."/contractors/content_custom.css");
        $less->checkedCompile(THEMEFILE_DIR."/contractors/less/blog.less", THEMEFILE_DIR."/contractors/blog.css");
        $less->checkedCompile(THEMEFILE_DIR."/contractors/less/detail.less", THEMEFILE_DIR."/contractors/detail.css");
        $less->checkedCompile(THEMEFILE_DIR."/contractors/less/front.less", THEMEFILE_DIR."/contractors/front.css");
        $less->checkedCompile(THEMEFILE_DIR."/contractors/less/members.less", THEMEFILE_DIR."/contractors/members.css");
        $less->checkedCompile(THEMEFILE_DIR."/contractors/less/order.less", THEMEFILE_DIR."/contractors/order.css");
        $less->checkedCompile(THEMEFILE_DIR."/contractors/less/popup.less", THEMEFILE_DIR."/contractors/popup.css");
        $less->checkedCompile(THEMEFILE_DIR."/contractors/less/profile.less", THEMEFILE_DIR."/contractors/profile.css");        
        $less->checkedCompile(THEMEFILE_DIR."/contractors/less/results.less", THEMEFILE_DIR."/contractors/results.css");
        $less->checkedCompile(THEMEFILE_DIR."/contractors/less/structure.less", THEMEFILE_DIR."/contractors/structure.css");
        $less->checkedCompile(THEMEFILE_DIR."/contractors/schemes/contractors/less/advertise.less", THEMEFILE_DIR."/contractors/schemes/contractors/plans.css");
        $less->checkedCompile(THEMEFILE_DIR."/contractors/schemes/contractors/less/bootstrap.less", THEMEFILE_DIR."/contractors/schemes/contractors/bootstrap.css");
        $less->checkedCompile(THEMEFILE_DIR."/contractors/schemes/contractors/less/content_custom.less", THEMEFILE_DIR."/contractors/schemes/contractors/content_custom.css");
        $less->checkedCompile(THEMEFILE_DIR."/contractors/schemes/contractors/less/blog.less", THEMEFILE_DIR."/contractors/schemes/contractors/blog.css");
        $less->checkedCompile(THEMEFILE_DIR."/contractors/schemes/contractors/less/detail.less", THEMEFILE_DIR."/contractors/schemes/contractors/detail.css");
        $less->checkedCompile(THEMEFILE_DIR."/contractors/schemes/contractors/less/front.less", THEMEFILE_DIR."/contractors/schemes/contractors/front.css");
        $less->checkedCompile(THEMEFILE_DIR."/contractors/schemes/contractors/less/members.less", THEMEFILE_DIR."/contractors/schemes/contractors/members.css");
        $less->checkedCompile(THEMEFILE_DIR."/contractors/schemes/contractors/less/order.less", THEMEFILE_DIR."/contractors/schemes/contractors/order.css");
        $less->checkedCompile(THEMEFILE_DIR."/contractors/schemes/contractors/less/popup.less", THEMEFILE_DIR."/contractors/schemes/contractors/popup.css");
        $less->checkedCompile(THEMEFILE_DIR."/contractors/schemes/contractors/less/profile.less", THEMEFILE_DIR."/contractors/schemes/contractors/profile.css");        
        $less->checkedCompile(THEMEFILE_DIR."/contractors/schemes/contractors/less/results.less", THEMEFILE_DIR."/contractors/schemes/contractors/results.css");
        $less->checkedCompile(THEMEFILE_DIR."/contractors/schemes/contractors/less/faq.less", THEMEFILE_DIR."/contractors/schemes/contractors/faq.css");
        $less->checkedCompile(THEMEFILE_DIR."/contractors/schemes/contractors/less/structure.less", THEMEFILE_DIR."/contractors/schemes/contractors/structure.css");
    }
    
    setting_get("scheme_custom", $scheme_custom);

    if (!$loadMembersCss) { ?>
    <link href="<?=THEMEFILE_URL;?>/<?=EDIR_THEME;?>/bootstrap.css<?=($isPopup ? "?v=2" : "" )?>" rel="stylesheet" type="text/css" media="all" />
    <link href="<?=system_getStylePath("bootstrap.css".($isPopup ? "?v=2" : "" ), EDIR_THEME);?>" rel="stylesheet" type="text/css" media="all" />
    <link href="<?=THEMEFILE_URL;?>/<?=EDIR_THEME;?>/bootstrap-responsive.css<?=($isPopup ? "?v=2" : "" )?>" rel="stylesheet" type="text/css" media="all" />
    <? } ?>
    
    <link href="<?=THEMEFILE_URL;?>/<?=EDIR_THEME;?>/structure.css<?=($isPopup ? "?v=2" : "" )?>" rel="stylesheet" type="text/css" media="all" />
    <link href="<?=system_getStylePath("structure.css", EDIR_THEME);?>" rel="stylesheet" type="text/css" media="all" />
    <link href="<?=THEMEFILE_URL;?>/<?=EDIR_THEME;?>/font-awesome.min.css" rel="stylesheet" type="text/css" media="all" />
    <!--[if lt IE 8]><link href="<?=THEMEFILE_URL;?>/<?=EDIR_THEME;?>/font-awesome-ie7.min.css" rel="stylesheet" type="text/css" media="all" /><![endif]-->

    <? if (ACTUAL_MODULE_FOLDER == PROMOTION_FEATURE_FOLDER) { ?>
        <link rel="stylesheet" href="<?=DEFAULT_URL?>/scripts/jquery/countdown/jquery.countdown.css" type="text/css" />
    <? } ?>

    <? if ((string_strpos($_SERVER['PHP_SELF'], "/".MEMBERS_ALIAS) !== false) && 
        ((string_strpos($_SERVER['PHP_SELF'], "preview.php") === false) || 
        (string_strpos($_SERVER['PHP_SELF'], "invoice.php") === false)) || 
        $loadMembersCss) { ?>
        <link href="<?=THEMEFILE_URL;?>/<?=EDIR_THEME;?>/members.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?=system_getStylePath("members.css", EDIR_THEME);?>" rel="stylesheet" type="text/css" media="all" />
    <? } ?>

    <? if ((string_strpos($_SERVER['PHP_SELF'], SOCIALNETWORK_FEATURE_NAME."/add.php") !== false) || 
        (string_strpos($_SERVER['PHP_SELF'], SOCIALNETWORK_FEATURE_NAME."/edit.php") !== false)) { ?>
        <link href="<?=THEMEFILE_URL;?>/<?=EDIR_THEME;?>/members.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?=system_getStylePath("members.css", EDIR_THEME);?>" rel="stylesheet" type="text/css" media="all" />
    <? } ?>

    <? if ((string_strpos($_SERVER['PHP_SELF'], "popup.php") !== false) || 
        (string_strpos($_SERVER['PHP_SELF'], "delete_image.php") !== false)) { ?>
        <link href="<?=THEMEFILE_URL;?>/<?=EDIR_THEME;?>/popup.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?=system_getStylePath("popup.css", EDIR_THEME);?>" rel="stylesheet" type="text/css" media="all" />
    <? } ?>

    <? if (((LOAD_MODULE_CSS_HOME == "on") || 
        (
            (string_strpos($_SERVER['REQUEST_URI'], ALIAS_LISTING_ALLCATEGORIES_URL_DIVISOR.(USE_DOT_PHP_ON_ALLCATEGORIES_LINK == "on" ? ".php" : "")) !== false) || 
            (string_strpos($_SERVER['REQUEST_URI'], ALIAS_EVENT_ALLCATEGORIES_URL_DIVISOR.(USE_DOT_PHP_ON_ALLCATEGORIES_LINK == "on" ? ".php" : "")) !== false) || 
            (string_strpos($_SERVER['REQUEST_URI'], ALIAS_CLASSIFIED_ALLCATEGORIES_URL_DIVISOR.(USE_DOT_PHP_ON_ALLCATEGORIES_LINK == "on" ? ".php" : "")) !== false) || 
            (string_strpos($_SERVER['REQUEST_URI'], ALIAS_ARTICLE_ALLCATEGORIES_URL_DIVISOR.(USE_DOT_PHP_ON_ALLCATEGORIES_LINK == "on" ? ".php" : "")) !== false) || 
            (string_strpos($_SERVER['REQUEST_URI'], ALIAS_PROMOTION_ALLCATEGORIES_URL_DIVISOR.(USE_DOT_PHP_ON_ALLCATEGORIES_LINK == "on" ? ".php" : "")) !== false) || 
            (string_strpos($_SERVER['REQUEST_URI'], ALIAS_BLOG_ALLCATEGORIES_URL_DIVISOR.(USE_DOT_PHP_ON_ALLCATEGORIES_LINK == "on" ? ".php" : "")) !== false)
        ) ||
        (string_strpos($_SERVER['REQUEST_URI'], ALIAS_ALLLOCATIONS_URL_DIVISOR.".php") !== false)) && (ACTUAL_MODULE_FOLDER != BLOG_FEATURE_FOLDER)) { ?>
        <link href="<?=THEMEFILE_URL;?>/<?=EDIR_THEME;?>/front.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?=system_getStylePath("front.css", EDIR_THEME);?>" rel="stylesheet" type="text/css" media="all" />
    <? } ?>

    <? if (ACTUAL_MODULE_FOLDER == BLOG_FEATURE_FOLDER) { ?>
        <link href="<?=THEMEFILE_URL;?>/<?=EDIR_THEME;?>/blog.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?=system_getStylePath("blog.css", EDIR_THEME);?>" rel="stylesheet" type="text/css" media="all" />
    <? } ?>

    <? if ((string_strpos($_SERVER['PHP_SELF'], "order_listing.php") !== false) || 
        (string_strpos($_SERVER['PHP_SELF'], "order_event.php") !== false) || 
        (string_strpos($_SERVER['PHP_SELF'], "order_classified.php") !== false) || 
        (string_strpos($_SERVER['PHP_SELF'], "order_article.php") !== false) || 
        (string_strpos($_SERVER['PHP_SELF'], "order_banner.php") !== false) || 
        (string_strpos($_SERVER['REQUEST_URI'], ALIAS_CLAIM_URL_DIVISOR."/") !== false) || 
        (string_strpos($_SERVER['PHP_SELF'], MEMBERS_ALIAS."/claim/") !== false)) { ?>
        <link href="<?=THEMEFILE_URL;?>/<?=EDIR_THEME;?>/order.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?=system_getStylePath("order.css", EDIR_THEME);?>" rel="stylesheet" type="text/css" media="all" />
    <? } ?>

    <? if (((string_strpos($_SERVER['PHP_SELF'], "favorites") !== false) || 
         (string_strpos($_SERVER['REQUEST_URI'], "results.php") !== false) || 
         (string_strpos($_SERVER['REQUEST_URI'], ALIAS_CATEGORY_URL_DIVISOR."/") !== false) || 
         (string_strpos($_SERVER['REQUEST_URI'], ALIAS_LOCATION_URL_DIVISOR."/") !== false) ||
         (string_strpos($_SERVER['REQUEST_URI'], ALIAS_ARCHIVE_URL_DIVISOR."/") !== false) ||
         (string_strpos($_SERVER['PHP_SELF'], "quicklists.php") !== false) || 
         (string_strpos($_SERVER['REQUEST_URI'], ALIAS_REVIEW_URL_DIVISOR."/") !== false) || 
         (string_strpos($_SERVER['REQUEST_URI'], ALIAS_CHECKIN_URL_DIVISOR."/") !== false) || 
         (string_strpos($_SERVER['REQUEST_URI'], ALIAS_CLAIM_URL_DIVISOR."/") !== false) || 
         (string_strpos($_SERVER['PHP_SELF'], SOCIALNETWORK_FEATURE_NAME."/index.php") !== false))) { 
        ?>
        <link href="<?=THEMEFILE_URL;?>/<?=EDIR_THEME;?>/results.css<?=($isPopup ? "?v=2" : "" )?>" rel="stylesheet" type="text/css" media="all" />
        <link href="<?=system_getStylePath("results.css", EDIR_THEME);?>" rel="stylesheet" type="text/css" media="all" />
    <? } ?>

    <? if ((string_strpos($_SERVER['REQUEST_URI'], ".html") !== false) && 
        (defined("ACTUAL_MODULE_FOLDER") && ACTUAL_MODULE_FOLDER != "")) { ?>
        <link href="<?=THEMEFILE_URL;?>/<?=EDIR_THEME;?>/detail.css<?=($isPopup ? "?v=2" : "" )?>" rel="stylesheet" type="text/css" media="all" />
        <link href="<?=system_getStylePath("detail.css", EDIR_THEME);?>" rel="stylesheet" type="text/css" media="all" />
    <? } ?>
        
    <? if (string_strpos($_SERVER['REQUEST_URI'], ALIAS_ADVERTISE_URL_DIVISOR.".php") !== false) { ?>
        <link href="<?=THEMEFILE_URL;?>/<?=EDIR_THEME;?>/plans.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?=system_getStylePath("plans.css", EDIR_THEME);?>" rel="stylesheet" type="text/css" media="all" />
    <? } ?>

    <? if (((string_strpos($_SERVER['PHP_SELF'], "popup.php") !== false && $_GET["pop_type"] == "advertise_preview") || 
        string_strpos($_SERVER['PHP_SELF'], "preview.php"))) { ?>
        <link href="<?=THEMEFILE_URL;?>/<?=EDIR_THEME;?>/results.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?=system_getStylePath("results.css", EDIR_THEME);?>" rel="stylesheet" type="text/css" media="all" />

        <link href="<?=THEMEFILE_URL;?>/<?=EDIR_THEME;?>/detail.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?=system_getStylePath("detail.css", EDIR_THEME);?>" rel="stylesheet" type="text/css" media="all" />
        
        <link href="<?=THEMEFILE_URL;?>/<?=EDIR_THEME;?>/plans.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?=system_getStylePath("plans.css", EDIR_THEME);?>" rel="stylesheet" type="text/css" media="all" />
    <? } ?>

    <? if ((string_strpos($_SERVER['PHP_SELF'], SOCIALNETWORK_FEATURE_NAME."/index.php") !== false) || 
        (string_strpos($_SERVER['PHP_SELF'], SOCIALNETWORK_FEATURE_NAME."/edit.php") !== false) || 
        (string_strpos($_SERVER['PHP_SELF'], SOCIALNETWORK_FEATURE_NAME."/add.php") !== false) || 
        (string_strpos($_SERVER['PHP_SELF'], "account/quicklists.php") !== false) || 
        (string_strpos($_SERVER['PHP_SELF'], "account/deals.php") !== false) || 
        (string_strpos($_SERVER['PHP_SELF'], "account/reviews.php") !== false)) { ?>
        <link href="<?=THEMEFILE_URL;?>/<?=EDIR_THEME;?>/profile.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?=system_getStylePath("profile.css", EDIR_THEME);?>" rel="stylesheet" type="text/css" media="all" />
    <? } ?>

    <? if ($aux_modal_box != "profileLogin") { ?>
    <link href="<?=THEMEFILE_URL;?>/<?=EDIR_THEME;?>/content_custom.css" rel="stylesheet" type="text/css" media="all" />
    <link href="<?=system_getStylePath("content_custom.css", EDIR_THEME);?>" rel="stylesheet" type="text/css" media="all" />
    <? } ?>
    
    <link href="<?=THEMEFILE_URL;?>/contractors/print.css" rel="stylesheet" type="text/css" media="print" />

    <?=system_backgroundImageStyle("get");?>
  
    <?=($scheme_custom == "on" && !DEMO_LIVE_MODE ? colorscheme_generateDynamicCSS() : "")?>