<?

	/*==================================================================*\
	######################################################################
	#                                                                    #
	# Copyright 2014 Arca Solutions, Inc. All Rights Reserved.           #
	#                                                                    #
	# This file may not be redistributed in whole or part.               #
	# eDirectory is licensed on a per-domain basis.                      #
	#                                                                    #
	# ---------------- eDirectory IS NOT FREE SOFTWARE ----------------- #
	#                                                                    #
	# http://www.edirectory.com | http://www.edirectory.com/license.html #
	######################################################################
	\*==================================================================*/

	# ----------------------------------------------------------------------------------------------------
	# * FILE: /classes/NewImageUploader.php
	# ----------------------------------------------------------------------------------------------------

    class NewImageUploader
    {
        public $module;
        public $galleryHash;
        public $galleryID;
        public $maxImages;
        public $domainID;

        public function __construct( $module, $galleryHash, $galleryID, $maxImages, $domainID )
        {
            $this->module      = $module;
            $this->galleryHash = $galleryHash;
            $this->galleryID   = $galleryID;
            $this->maxImages   = $maxImages;
            $this->domainID    = $domainID;
        }

        /**
         * Renders the necessary HTML for CropJS to work.
         */
        public function buildCrop()
        {
            ?>
                <div id="cropPanel" style="display: none;">
                    <input type="hidden" disabled="disabled" name="domain_id" value="<?= $this->domainID ?>">
                    <input type="hidden" disabled="disabled" name="edit_action" id="edit-action" value="crop">
                    <input type="hidden" disabled="disabled" name="edit_image_id" id="edit-image_id" value="">
                    <input type="hidden" disabled="disabled" name="edit_temp" id="edit-temp" value="">
                    <input type="hidden" disabled="disabled" name="edit_image_type" id="edit_image_type" value="">

                    <?= system_showText( LANG_LABEL_IMAGE_CROP ) ?>
                    <ul class="nav nav-pills" role="tablist">
                        <li class="active"><a href="#cropping" data-toggle="tab" role="tab" onclick="$('#edit-action').attr('value', 'crop');"><?=system_showText(LANG_LABEL_IMAGE_CROPPING);?></a></li>
                        <li class=""><a href="#crop_description" data-toggle="tab" role="tab" onclick="$('#edit-action').attr('value', 'description');"><?=system_showText(LANG_LABEL_DESCRIPTION);?></a></li>
                    </ul>

                    <div class="row tab-content content-pills">
                        <div class="tab-pane active" id="cropping">
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <img src="" id="crop_image">
                                    <?/* Crop Tool Inputs */?>
                                    <input type="hidden" disabled="disabled" name="x"  id="x">
                                    <input type="hidden" disabled="disabled" name="y"  id="y">
                                    <input type="hidden" disabled="disabled" name="x2" id="x2">
                                    <input type="hidden" disabled="disabled" name="y2" id="y2">
                                    <input type="hidden" disabled="disabled" name="w"  id="w">
                                    <input type="hidden" disabled="disabled" name="h"  id="h">
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="crop_description">
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label for="crop_image_title"><?=string_ucwords(system_showText(LANG_LABEL_IMAGE_TITLE))?></label>
                                    <input disabled="disabled" type="text" class="form-control" id="crop_image_title" name="image_title" value="" maxlength="100">
                                </div>
                                <div class="col-sm-6">
                                    <label for="crop_image_description"><?=string_ucwords(LANG_LABEL_IMAGE_DESCRIPTION)?></label>
                                    <input disabled="disabled" type="text" class="form-control" id="crop_image_description" name="image_description" value="" maxlength="100">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="cropPanelCancelButton"><?=system_showText(LANG_CANCEL);?></button>
                        <button type="button" class="btn btn-primary action-save" id="button-edit-img" data-loading-text="<?=system_showText(LANG_LABEL_FORM_WAIT);?>" onclick="saveImage();"><?=system_showText(LANG_MSG_SAVE_CHANGES);?></button>
                    </div>
                </div>
            <?
        }

        /**
         * Renders the necessary HTML for ImageUpload to work.
         */
        public function buildform()
        {
                /* Auxiliary inputs for image upload */ ?>
                <input type="hidden" id="gallery_hash" name="gallery_hash" value="<?= $this->galleryHash ?>">
                <input type="hidden" id="item_type"    name="item_type"    value="<?= $this->module      ?>">
                <input type="hidden" id="galleryid"    name="galleryid"    value="<?= $this->galleryID   ?>">
                <input type="hidden" id="max_images"   name="max_images"   value="<?= $this->maxImages   ?>">
                <input type="hidden" id="domain_id"    name="domain_id"    value="<?= $this->domainID    ?>">

                <div class="panel panel-form-media" id="tour-images">
                    <div class="panel-heading">
                        <?=system_showText(LANG_LABEL_IMAGE_PLURAL);?>
                        <div class="pull-right">
                            <input id="upload-images" type="file" name="files[]" class="filestyle upload-files" multiple>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div id="filesImages" class="files uploaded-files"></div>
                        <div id="no-filesImages" class="no-files center-block text-center">
                            <i class="icon-images9"></i>
                            <p class="text-muted"><?=system_showText(LANG_MSG_DROP_IMAGE);?></p>
                        </div>
                    </div>
                </div>
            <?php
        }

        /**
         * Adds all necessary Javascript for the entire system to work.
         */
        public function registerJavaScript()
        {
            /* The Templates plugin is included to render the upload/download items */
            JavaScriptHandler::registerFile( DEFAULT_URL."/scripts/jquery/jQuery-File-Upload-9.8.0/tmpl.min.js" );
            /* The Load Image plugin is included for the preview images and image resizing functionality */
            JavaScriptHandler::registerFile( DEFAULT_URL."/scripts/jquery/jQuery-File-Upload-9.8.0/load-image.all.min.js" );
            /* The Iframe Transport is required for browsers without support for XHR file uploads */
            JavaScriptHandler::registerFile( DEFAULT_URL."/scripts/jquery/jQuery-File-Upload-9.8.0/jquery.iframe-transport.js" );
            /* The basic File Upload plugin */
            JavaScriptHandler::registerFile( DEFAULT_URL."/scripts/jquery/jQuery-File-Upload-9.8.0/jquery.fileupload.js" );
            /* The File Upload processing plugin */
            JavaScriptHandler::registerFile( DEFAULT_URL."/scripts/jquery/jQuery-File-Upload-9.8.0/jquery.fileupload-process.js" );
            /* The File Upload user interface plugin */
            JavaScriptHandler::registerFile( DEFAULT_URL."/scripts/jquery/jQuery-File-Upload-9.8.0/jquery.fileupload-ui.js" );
            /* The main application script */
            JavaScriptHandler::registerFile( DEFAULT_URL."/scripts/jquery/jQuery-File-Upload-9.8.0/main.js" );

            /* The template to display files available for upload */
            $js = '
            {% for (var i=0, file; file=o.files[i]; i++) { %}

                <div class="template-upload row item fade">
                    <div class="col-sm-3 col-xs-6">&nbsp;</div>
                    <div class="col-sm-3 col-xs-6 pull-right">
                        {% if (!i) { %}
                        <p><span class="btn btn-sm btn-warning btn-iconic cancel"><i class="icon-ion-ios7-close-empty"></i></span></p>
                        {% } %}
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <strong>{%=file.name%}</strong>
                        <p class="error"></p>
                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
                    </div>
                </div>
            {% } %}
            ';
            JavaScriptHandler::registerLone($js, 'id="template-upload" type="text/x-tmpl"');

            /* The template to display files available for download */
            $js = '
            {% for (var i=0, file; file=o.files[i]; i++) { %}

                <div class="template-download row item fade">
                    <div id="gallery-image-{%=file.image_id%}" data-image_id="{%=file.image_id%}" data-thumb_id="{%=file.thumb_id%}" data-item_id="{%=file.item_id%}" data-temp="{%=file.temp%}" data-item_type="{%=file.item_type%}" data-image_default="{%=file.image_default%}" data-imgtype="{%=file.type%}" class="col-sm-3 col-xs-6 item-gallery {% if (file.image_default == "y") { %} image-default {% } %}" onclick="makeMain( {%=file.image_id%}, {%=file.thumb_id%}, {%=file.item_id%}, \'{%=file.temp%}\', \'{%=file.item_type%}\');">
                        {% if (file.thumbnailUrl) { %}
                        <img id="img-{%=file.image_id%}" src="{%=file.thumbnailUrl%}" alt="{%=file.name%}" data-title="{%=file.title%}" data-description="{%=file.description%}" data-width="{%=file.width%}" data-height="{%=file.height%}" class="img-responsive">
                        {% } %}
                    </div>
                    <div class="col-sm-3 col-xs-6 pull-right">
                        <p>
                            {% if (file.deleteUrl) { %}
                            <span class="btn btn-sm btn-primary btn-iconic" onclick="editImage({%=file.image_id%});" href="#"><i class="icon-edit38"></i></span>
                            <span class="btn btn-sm btn-danger btn-iconic delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields="{"withCredentials":true}"{% } %}><i class="icon-ion-ios7-trash-outline"></i></span>
                            {% } else { %}
                            <span class="btn btn-sm btn-warning btn-iconic cancel"><i class="icon-ion-ios7-close-empty"></i></span>
                            {% } %}
                        </p>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <strong id="preview-title-{%=file.image_id%}">
                            {% if (file.title) { %}
                                {%=file.title%}
                            {% } else { %}
                                {%=file.name%}
                            {% } %}
                        </strong>
                        {% if (file.error) { %}
                            <p>{%=file.error%}</p>
                        {% } else { %}
                            <p id="preview-description-{%=file.image_id%}">
                            {% if (file.description) { %}
                                {%=file.description%}
                            {% } %}
                            </p>
                        {% } %}
                    </div>
                </div>
            {% } %}
            ';
            JavaScriptHandler::registerLone($js, 'id="template-download" type="text/x-tmpl"');

            JavaScriptHandler::registerOnReady('
            $("#cropPanelCancelButton").click(function(){
                $("#cropPanel").fadeOut( function(){
                    $("#cropPanel").find("input").prop( "disabled", true );
                    $("#tour-images").fadeIn();
                });
            });
            ');

            JavaScriptHandler::registerLoose('
            function makeMain(image_id, thumb_id, item_id, temp, item_type)
            {
                //Call ajax only for non-default images
                if ( $("#gallery-image-" + image_id).data("image_default") == "n")
                {
                    $.get(DEFAULT_URL + "/makemainimage.php", {
                        image_id: image_id,
                        thumb_id: thumb_id,
                        item_id: item_id,
                        temp: temp,
                        item_type: item_type,
                        gallery_hash: "'.$this->galleryHash.'",
                        domain_id: '.$this->domainID.'
                    }, function () {
                        //Remove image-default class from all images
                        $(".item-gallery").removeClass("image-default");
                        //Add image-default class for the image clicked
                        $("#gallery-image-" + image_id).addClass("image-default");
                        //Remove onclick event for all images
                        $(".item-gallery").data("image_default", "n");
                        //Change image_default attribute for the image clicked
                        $("#gallery-image-" + image_id).data("image_default", "y");
                    });
                }
            }

            var jcrop_api;

            function editImage(image_id) {
                $("#cropPanel").find("input").prop( "disabled", false );
                
                var cropImage = $("#img-" + image_id).first();

                //Reset save button status
//                $("#button-edit-img").button("reset");

                //Set current image title value
                $("#crop_image_title").val( cropImage.data("title"));

                //Set current image description value
                $("#crop_image_description").val( cropImage.data("description"));

                //Set image path
                $("#crop_image").attr("src", cropImage.attr("src"));

                //Auxiliary values to save image
                $("#edit-image_id").val( image_id );
                $("#edit-temp").val( $("#gallery-image-" + image_id ).data("temp") );
                $("#edit_image_type").val( $("#gallery-image-" + image_id ).data("imgtype"));

                //Destroy crop obj
                if (jcrop_api) {
                    jcrop_api.destroy();
                }

                //Initialize crop
                setJcrop( cropImage.width(), cropImage.height());

                $("#tour-images").fadeOut("fast", function(){
                    $("#cropPanel").fadeIn();
                });

            }

            function saveImage() {
                $("#button-edit-img").prop( "disabled", true );
                $("#button-edit-img").html( "'.system_showText(LANG_LABEL_FORM_WAIT).'", true );


                $.post("'.$_SERVER["PHP_SELF"].'", $("#cropSection").find("input").serialize(), function(data) {
                    var image_id = $("#edit-image_id").val();
                    if ($("#crop_image_title").val()) {
                        $("#preview-title-"+image_id).html($("#crop_image_title").val());
                        $("#img-" + image_id).data("title", $("#crop_image_title").val());
                    }
                    if ($("#crop_image_description").val()) {
                        $("#preview-description-"+image_id).html($("#crop_image_description").val());
                        $("#img-" + image_id).data("description", $("#crop_image_description").val());
                    }
                    if (data) {
                        $("#img-"+image_id).attr("src", data);
                    }
                    $("#cropPanel").fadeOut("fast", function(){

                        $("#button-edit-img").prop( "disabled", false );
                        $("#button-edit-img").html( "'.system_showText(LANG_MSG_SAVE_CHANGES).'", true );
                        $("#cropPanel").find("input").prop( "disabled", true );
                        $("#tour-images").fadeIn();});
                    });
            }

            // creating the Jcrop
            function setJcrop(imgWidth, imgHeight) {
                function showCoords(c) {
                    $("#x").val(c.x);
                    $("#y").val(c.y);
                    $("#x2").val(c.x2);
                    $("#y2").val(c.y2);
                    $("#w").val(c.w);
                    $("#h").val(c.h);
                    $("#image_width").val(imgWidth);
                    $("#image_height").val(imgHeight);
                };

                $("#crop_image").Jcrop({
                    onChange        : showCoords,
                    onSelect        : showCoords,
                    setSelect       : [ (imgWidth/4), (imgHeight/4), (imgWidth/4*3), (imgHeight/4*3) ],
                    aspectRatio     : 0,
                    boxWidth        : 400,
                    boxHeight       : 400,
                    bgColor         : "transparent",
                    fullImageWidth  : imgWidth,
                    fullImageHeight : imgHeight,
                    keySupport      : false
                },function(){
                    jcrop_api = this;
                });
            }');

        }

        /* Renders the required HTML and JS to upload and crop files */
        public function render()
        {
            echo '<div id="cropSection">';
            $this->buildCrop();
            $this->buildform();
            echo '</div>';
            $this->registerJavaScript();
        }

    }
