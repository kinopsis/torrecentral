

/* File: /scripts/socialbookmarking.js */

function getAbsoluteTop(oElement){var iReturnValue=0;while(oElement!=null){iReturnValue+=oElement.offsetTop;oElement=oElement.offsetParent;}
return iReturnValue;}
function getAbsoluteLeft(oElement){var iReturnValue=0;while(oElement!=null){iReturnValue+=oElement.offsetLeft;oElement=oElement.offsetParent;}
return iReturnValue;}
function enableSocialBookMarking(id,module,url,comments,checkins){if(comments===undefined){comments=0;}
if(checkins===undefined){checkins=0;}
var left=0+getAbsoluteLeft(document.getElementById('link_social_'+id+module));var top=18+getAbsoluteTop(document.getElementById('link_social_'+id+module));$.ajax({type:"POST",url:url+"/includes/code/socialbookmarking_ajax.php",data:"id="+id+"&module="+module+"&comments="+comments+"&checkins="+checkins,success:function(msg){$('#div_to_share').html(msg);}});$('#div_to_share').css('top',top+'px').css('left',left+"px").css('z-index','1000').show('fast');}
function disableSocialBookMarking(){$('#div_to_share').hide('fast');}

/* File: /scripts/contactclick.js */

function showPhone(listingid){try{xmlhttp=new XMLHttpRequest();}catch(exc){try{xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");}catch(ex){try{xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}catch(e){xmlhttp=false;}}}
if(xmlhttp){xmlhttp.open("GET",DEFAULT_URL+'/countphoneclick.php?listing_id='+listingid,true);xmlhttp.send(null);}
document.getElementById("phoneLink"+listingid).className="hide";document.getElementById("phoneNumber"+listingid).className="show-inline";}
function showFax(listingid){try{xmlhttp=new XMLHttpRequest();}catch(exc){try{xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");}catch(ex){try{xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");}catch(e){xmlhttp=false;}}}
if(xmlhttp){xmlhttp.open("GET",DEFAULT_URL+'/countfaxclick.php?listing_id='+listingid,true);xmlhttp.send(null);}
document.getElementById("faxLink"+listingid).className="hide";document.getElementById("faxNumber"+listingid).className="show-inline";}

/* File: /scripts/jquery/jquery.accordion.js */

(function(jQuery){jQuery.fn.extend({accordion:function(){return this.each(function(){var $ul=$(this),elementDataKey='accordiated',activeClassName='active',activationEffect='slideToggle',panelSelector='ul, div',activationEffectSpeed='slow',itemSelector='li';if($ul.data(elementDataKey))
return false;$.each($ul.find('ul, li>div'),function(){$(this).data(elementDataKey,true);$(this).hide();});$.each($ul.find('h3'),function(){$(this).click(function(e){activate(this,activationEffect);return false;});$(this).bind('activate-node',function(){$ul.find(panelSelector).not($(this).parents()).not($(this).siblings()).slideUp(activationEffectSpeed);activate(this,'slideDown');});});var active=(location.hash)?$ul.find('a[href='+location.hash+']')[0]:$ul.find('li.current a')[0];if(active){activate(active,false);}
function activate(el,effect){$(el).parent(itemSelector).siblings().removeClass(activeClassName).children(panelSelector).slideUp(activationEffectSpeed);$(el).siblings(panelSelector)[(effect||activationEffect)](((effect=="show")?activationEffectSpeed:false),function(){if($(el).siblings(panelSelector).is(':visible')){$(el).parents(itemSelector).not($ul.parents()).addClass(activeClassName);}else{$(el).parent(itemSelector).removeClass(activeClassName);}
if(effect=='show'){$(el).parents(itemSelector).not($ul.parents()).addClass(activeClassName);}
$(el).parents().show();});}});}});})(jQuery);